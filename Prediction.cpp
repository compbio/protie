#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <iostream>
#include <fstream>
#include <string>
#include <map>
#include <algorithm>

#include "Prediction.h"
#include "Common.h"
#include "Fasta.h"

#define FASTA_LINE 60

using namespace std;

/**********************************************/
/***   For incorporating Contig Information ***/
// pos: -1 for clipped, 0 for normal
int align_of_interest( const map<string, vector<unit_vcf> >& map_record, int cluster, int contig, char* ref, uint32_t start, uint32_t end, uint32_t pos)
{
	int i, flag = -1;
	string s_ref;
	map<string, vector<unit_vcf> >::const_iterator it;
	copyToString( ref, s_ref );
	it = map_record.find( s_ref );
	if ( it != map_record.end() )
	{
		int limit = (int)it->second.size();
		for( i=0; i< limit; i++ )
		{
			if ( (it->second[i].pos > end ) && (it->second[i].pos > pos)){ break;}
			else if ( (it->second[i].s == start) && (it->second[i].e == end ) && (it->second[i].cluster == cluster ) && ( it->second[i].contig == contig) )
			{	flag = it->second[i].id;	}
		}
	}
	return flag;
}
/**********************************************/
void parse_Alignment( char *align_file, const map<string, vector<unit_vcf> > &map_record, map<int, vector<string> > &map_alignment )
{
	char *readline = (char*)malloc(MAX_LINE);
	char *sim      = (char*)malloc(MAX_LINE);
	char *ref      = (char*)malloc(MAX_LINE);
	char *m_ref      = (char*)malloc(MAX_LINE);
	char *m_align    = (char*)malloc(MAX_LINE);
	char *m_contig   = (char*)malloc(MAX_LINE);

	int ret = 0, offset = 0;
	int cluster, contig, support;
	uint32_t start, end, pos;
	string s_ref, s_align, s_contig, s_header;
	int vcf_id = 0;
	FILE *fp = fopen( align_file, "r");
	while( NULL != fgets(readline, MAX_LINE, fp) )
	{
		if ( !strncmp(">", readline, 1) )
		{
			ret = sscanf( readline, "> %d %d %d chr%s %u %u %s %n\n",
			&cluster, &contig, &support, ref,
			&start, &end, sim, &offset);

			pos = 0;	// 0 normal inv, -1 clipped inv; dup o.w.
			if ( !strncmp("C", readline+offset, 1))
			{ pos = -1;}
			else if ( strncmp("N", readline+offset, 1))
			{ sscanf(readline+offset, "%u\n", &pos);}


			fgets( m_ref, MAX_LINE, fp);
			fgets( m_align, MAX_LINE, fp);
			fgets( m_contig, MAX_LINE, fp);
			// Detect if we should add it to the dictionary
			// In the chromosome, check if we have that before stopping the
			vcf_id =  align_of_interest( map_record, cluster, contig, ref, start, end, pos) ;
			if ( -1 < vcf_id  )
			{
				copyToStringStrip( readline, s_header );
				copyToStringStrip( m_ref, s_ref );
				copyToStringStrip( m_align, s_align );
				copyToStringStrip( m_contig, s_contig );
				vector<string> t_vec;
				t_vec.push_back(s_header);
				t_vec.push_back(s_ref);
				t_vec.push_back(s_align);
				t_vec.push_back(s_contig);
				map_alignment[ vcf_id ] = t_vec;
			}
			//L(">\t%d\t%d\t%d\tchr%s\t%u\t%u\t%s", cluster, contig, support, ref, start, end, sim);
			//if ( -1 == pos)
			//{	L("\tClipped\n");	}
			//else if ( 0 == pos)
			//{	L("\tNormal\n");	}
			//else
			//{	L("\t%d\n", pos);	}
			//L("%s%s%s", m_ref, m_align, m_contig);
		}
	}
	//E("%lu CDS Alignment Obtained\n", map_alignment.size() );
	//map<int, vector<string> >::iterator it;
	//for( it = map_alignment.begin(); it != map_alignment.end(); it++)
	//{
	//	L(">%d\n%s\n%s\n%s\n", it->first, it->second[0].c_str(), it->second[1].c_str(), it->second[2].c_str() );
	//}
	free( readline );
	free( sim );
	free( ref );
	free( m_ref );
	free( m_align );
	free( m_contig );
}




/**********************************************/
/***   For VCF Files                        ***/
/**********************************************/
void parse_InversionPrediction( char *predict_file , map<string, vector <unit_pred> > &map_record )
{
	string total_seq="", total_rc="";

	char *readline=(char*)malloc(MAX_LINE);
	char *misc=(char*)malloc(TOKEN_LENGTH);
	char *ref=(char*)malloc(TOKEN_LENGTH);
	char *fea=(char*)malloc(TOKEN_LENGTH);
	char *s_ref=(char*)malloc(TOKEN_LENGTH);
	char *s_align=(char*)malloc(TOKEN_LENGTH);
	char *s_contig=(char*)malloc(TOKEN_LENGTH);
	char *sim=(char*)malloc(TOKEN_LENGTH);

	char header;
	int count = 0;
	int id = 0, support = 0, offset =0;
	uint32_t start = 0, end = 0;
	//float sim = 0.0; // similarity
	unit_pred tmp_pred;
	//map< string, vector<unit_pred> > map_record;
	FILE *fp = fopen( predict_file, "r");
	//FILE *out_fp= fopen( out_file, "w");
	int cur = 0, cur_gene = 0;
	int ret;
	while( NULL != fgets(readline, MAX_LINE, fp) )
	{
		//if ( 0 != strncmp(">", readline, 1) )
		//{
		//	continue;
		//}
		ret = sscanf( readline, "%c %d %d %s %d %d %s %s %n\n",
			&header, &id, &support, ref,
			&start, &end, sim,
			fea, &offset);
		if ( 7 >= ret ){ fea[0]='\0';}
		//L( "%s", readline);

		fgets(s_ref, MAX_LINE, fp);
		fgets(s_align, MAX_LINE, fp);
		fgets(s_contig, MAX_LINE, fp);
		///L( "%s", s_ref);
		//L( "%s", s_align);
		//L( "%s", s_contig);
		tmp_pred.id = id;
		tmp_pred.sup = support;
		// Notation: Convert from BED to gtf annnotation
		tmp_pred.s = start + 1;
		tmp_pred.e = end;
		//tmp_pred.sim = sim; // new identity after inversion
		copyToString( sim, tmp_pred.sim);
		copyToString( ref, tmp_pred.ref_id);
		copyToString( fea, tmp_pred.fea);
		copyToStringStrip( readline, tmp_pred.header);
		copyToString( s_ref, tmp_pred.ref);
		copyToString( s_align, tmp_pred.align);
		copyToString( s_contig, tmp_pred.contig);
		// convert from ChrSTH to STH
		map_record[string(ref).substr(3)].push_back( tmp_pred );
		count++;

	}

	E("Total Prediction Record\t%d\n", count);
	E("Total References with Predictions:\t%d\n", (int)map_record.size());
	fclose(fp);
	free(s_contig);
	free(s_align);
	free(s_ref);
	free(fea);
	free(ref);
	free(misc);
	free(readline);
}

///**********************************************/
//void copyToString( char *src, string &new_str)
//{
//	int limit=strlen(src);
//	new_str.clear();
//	new_str.reserve(limit);
//	for( int i = 0; i < limit; i ++)
//	{
//		new_str.push_back(src[i]);
//	}
//}

/**********************************************/
bool sortByStart( const unit_pred &p1, const unit_pred &p2 )
{
	return p1.s < p2.s;
}
/**********************************************/
/**********************************************/
void SortPredictionOnly( map<string, vector<unit_pred> > &new_record, map<string, vector<unit_pred> > &map_record  )
{
	int limit=0;
	map<string, vector<unit_pred> >::iterator it;
	for( it = map_record.begin(); it != map_record.end(); it++)
	{
		// sorting by the starting coordinate
		sort( it->second.begin(), it->second.end(), sortByStart);
		limit=(int)it->second.size();
		int diff = 0;

		vector<unit_pred> tmp_pred;
		tmp_pred.reserve( limit );
		tmp_pred.push_back(it->second[0]);
		L( "==%d\t%s\t%u\t%u\t%u\n", it->second[0].id, it->first.c_str(), it->second[0].s, it->second[0].e, it->second[0].s);
		for( int i =1; i < limit; i ++)
		{
			tmp_pred.push_back( it->second[i] );
		}

		new_record[it->first] = tmp_pred;
	}


}
/**********************************************/
void analyze_record( unit_pred &pred)
{
	// determine the boundary of inversion calls
	int a =0;
}
/**********************************************/
// To Compute the extended boundary of contigs on the reference
void MergePrediction( map<string, vector<unit_pred> > &sorted_record,  char *outfile  )
{
	int limit=0;
	map<string, vector<unit_pred> >::iterator it;
	for( it = sorted_record.begin(); it != sorted_record.end(); it++)
	{
		// Reading sorted record for a chromosome
		limit=(int)it->second.size();
		int diff = 0;

		//vector<unit_pred> tmp_pred;
		//tmp_pred.reserve( limit );
		//tmp_pred.push_back(it->second[0]);
		L( "==%d\t%s\t%u\t%u\t%u\n", it->second[0].id, it->first.c_str(), it->second[0].s, it->second[0].e, it->second[0].s);
		for( int i =1; i < limit; i ++)
		{
			// calculate the boundary of this record
			 analyze_record( it->second[i]);
			//if ( ( it->second[i].s != it->second[i-1].s) ||
			//	(it->second[i].e != it->second[i-1].e)
			//	)
			//{
			//	tmp_pred.push_back( it->second[i] );
			//	diff = it->second[i].s - it->second[i-1].e;
			//	if ( 0 >= diff)
			//	{
			//		E("Warning\tOverlap %s %d %u %u with %d\n", it->first.c_str(), it->second[i].id, it->second[i].s, it->second[i].e, diff) ;
			//	}
			//	L( "==%d\t%s\t%u\t%u\t%d\n", it->second[i].id, it->first.c_str(), it->second[i].s, it->second[i].e, diff);

			//}
			//else
			//{
			//	E("Warning\tduplicate %s %d %u %u\n", it->first.c_str(), it->second[i].id, it->second[i].s, it->second[i].e);
			//}
		}

		//new_record[it->first] = tmp_pred;
	}


}
/**********************************************/
void SortPrediction( map<string, vector<unit_pred> > &new_record, map<string, vector<unit_pred> > &map_record  )
{
	int limit=0;
	map<string, vector<unit_pred> >::iterator it;
	for( it = map_record.begin(); it != map_record.end(); it++)
	{
		// sorting by the starting coordinate
		sort( it->second.begin(), it->second.end(), sortByStart);
		limit=(int)it->second.size();
		int diff = 0;

		vector<unit_pred> tmp_pred;
		tmp_pred.reserve( limit );
		tmp_pred.push_back(it->second[0]);
		L( "==%d\t%s\t%u\t%u\t%u\n", it->second[0].id, it->first.c_str(), it->second[0].s, it->second[0].e, it->second[0].s);
		for( int i =1; i < limit; i ++)
		{
			if ( ( it->second[i].s != it->second[i-1].s) ||
				(it->second[i].e != it->second[i-1].e)
				)
			{
				tmp_pred.push_back( it->second[i] );
				diff = it->second[i].s - it->second[i-1].e;
				if ( 0 >= diff)
				{
					E("Warning\tOverlap %s %d %u %u with %d\n", it->first.c_str(), it->second[i].id, it->second[i].s, it->second[i].e, diff) ;
				}
				L( "==%d\t%s\t%u\t%u\t%d\n", it->second[i].id, it->first.c_str(), it->second[i].s, it->second[i].e, diff);

			}
			else
			{
				E("Warning\tduplicate %s %d %u %u\n", it->first.c_str(), it->second[i].id, it->second[i].s, it->second[i].e);
			}
		}

		new_record[it->first] = tmp_pred;
	}


}
/**********************************************/
void OutputPrediction( map<string, vector<unit_pred> > &new_record, char *output  )
{
	char *out_name=(char*)malloc(MAX_LINE);
	strcpy(out_name, output);
	strcat(out_name, ".predict");
	FILE *out_fp=fopen( out_name, "w");
	int count = 0;

	map<string, vector<unit_pred> >::iterator it;
	for( it = new_record.begin(); it != new_record.end(); it++)
	{
		int limit = it->second.size();
		for ( int i=0; i < limit; i++)
		{
			fprintf( out_fp, "%d\t%d\t%s\t%u\t%u\n", count, it->second[i].id, it->first.c_str(), it->second[i].s, it->second[i].e);
			fprintf( out_fp, "%s%s%s", it->second[i].ref.c_str(), it->second[i].align.c_str(), it->second[i].contig.c_str());
			count++;
		}
	}
	fclose(out_fp);

}
///**********************************************/
//// decide which inversion call we should keep
//int cmp_pred( const unit_pred &p1, const unit_pred &p2 )
//{
//	int flag = 0; // 0 for p1 is better, 1 otherwise
//	L("\tCMP OLD\t%d\t%u\t%u\t%f\n", p1.id, p1.s, p1.e, p1.sim);
//	L("\tCMP NEW\t%d\t%u\t%u\t%f\n", p2.id, p2.s, p2.e, p2.sim);
//
//	if ( p1.sim < p2.sim) {flag = 1;} // Pick the latter one When the later one has better alignment score
//
//	return flag;
//}
///**********************************************/
//void SortPrediction_WithOverlap( map<string, vector<unit_pred> > &new_record, map<string, vector<unit_pred> > &map_record  )
//{
//	int limit=0;
//	map<string, vector<unit_pred> >::iterator it;
//	for( it = map_record.begin(); it != map_record.end(); it++)
//	{
//		// sorting by the starting coordinate
//		sort( it->second.begin(), it->second.end(), sortByStart);
//		limit=(int)it->second.size();
//		int diff = 0;
//
//		int flag_overlap=0;
//		vector<unit_pred> tmp_pred;
//		tmp_pred.reserve( limit );
//		tmp_pred.push_back(it->second[0]);
//		int i_tmp = 0; // the index of last component in tmp_pred
//		L( "==%d\t%s\t%u\t%u\t%u\n", it->second[0].id, it->first.c_str(), it->second[0].s, it->second[0].e, it->second[0].s);
//		L( "\tAdd\t%d\t%s\t%u\t%u\t%d\n", it->second[0].id, it->first.c_str(), it->second[0].s, it->second[0].e, diff);
//		for( int i =1; i < limit; i ++)
//		{
//			//if ( ( it->second[i].s != it->second[i-1].s) ||
//			//	(it->second[i].e != it->second[i-1].e)
//			//	)
//			if ( ( it->second[i].s != tmp_pred[i_tmp].s) ||
//				(it->second[i].e != tmp_pred[i_tmp].e)
//				)
//			{
//				L( "==%d\t%s\t%u\t%u\t%d\n", it->second[i].id, it->first.c_str(), it->second[i].s, it->second[i].e, diff);
//				//diff = it->second[i].s - it->second[i-1].e;
//				diff = it->second[i].s - tmp_pred[i_tmp].e;
//				if ( 0 >= diff)
//				{
//					// Case: proper containment
//					if ( it->second[i].e <= tmp_pred[i_tmp].e )
//					{
//						L("SP\tProperContain %s %d %u %u\n", it->first.c_str(), it->second[i].id, it->second[i].s, it->second[i].e);
//						//tmp_pred.push_back( it->second[i] );
//						//i_tmp++;
//					}
//					else
//					{	// pick between it->second[i] and tmp_pred[last]
//						L("SP\tOverlap %s %d %u %u with %d\n", it->first.c_str(), it->second[i].id, it->second[i].s, it->second[i].e, diff) ;
//						flag_overlap = cmp_pred( tmp_pred[i_tmp], it->second[i]);
//						if ( 1 == flag_overlap) // pop the last item from tmp_pred
//						{
//							L("\tRemove %d %u %u %f\n", tmp_pred[i_tmp].id, tmp_pred[i_tmp].s, tmp_pred[i_tmp].e, tmp_pred[i_tmp].sim);
//							tmp_pred.pop_back();
//							L( "\tAdd\t%d\t%s\t%u\t%u\t%d\n", it->second[i].id, it->first.c_str(), it->second[i].s, it->second[i].e, diff);
//							tmp_pred.push_back( it->second[i] );
//						}
//					}
//				}
//				else
//				{
//						L( "\tAdd\t%d\t%s\t%u\t%u\t%d\n", it->second[i].id, it->first.c_str(), it->second[i].s, it->second[i].e, diff);
//						tmp_pred.push_back( it->second[i] );
//						i_tmp++;
//				}
//			}
//			else
//			{
//				L("SP\tduplicate %s %d %u %u\n", it->first.c_str(), it->second[i].id, it->second[i].s, it->second[i].e);
//			}
//		}
//
//		if ( i_tmp+1 != (int)tmp_pred.size() )
//		{
//			E("INCONSIST REF SIZE for %s: %d %d?\n", it->first.c_str(), i_tmp, (int)tmp_pred.size() );
//		}
//
//		new_record[it->first] = tmp_pred;
//	}
//
//
//}
// Modules for VCF
/**********************************************/
void parse_SV_VCF( char *predict_file , map<string, vector <unit_vcf> > &map_record )
{
	string total_seq="", total_rc="";

	char *readline=(char*)malloc(MAX_LINE);
	//char *misc=(char*)malloc(TOKEN_LENGTH);
	char *chr=(char*)malloc(TOKEN_LENGTH);
	char *ref=(char*)malloc(TOKEN_LENGTH);
	char *alt=(char*)malloc(TOKEN_LENGTH);
	char *fea=(char*)malloc(TOKEN_LENGTH);
	char *filter = (char*)malloc(TOKEN_LENGTH);

	char header;
	int count = 0;
	int id = 0, support = 0, offset =0;
	uint32_t start = 0, end = 0;
	float qual = 0.0;
	//float sim = 0.0; // similarity
	unit_vcf tmp_vcf;
	FILE *fp = fopen( predict_file, "r");
	//FILE *out_fp= fopen( out_file, "w");
	int cur = 0, cur_gene = 0;
	int ret;
	while( NULL != fgets(readline, MAX_LINE, fp) )
	{
		if ( 0 == strncmp("@", readline, 1) )
		{
			continue;
		}

		ret = sscanf( readline, "%s %u %c %s %s %f %s %n\n",
			chr, &start, &header, ref, alt,
			&qual, filter, &offset);
		//L("for %s", readline);

		//get_vcf_info( readline + offset);
		copyToString( chr, tmp_vcf.chr);
		copyToString( ref, tmp_vcf.ref);
		copyToString( alt, tmp_vcf.alt);
		tmp_vcf.s = start;
		tmp_vcf.qual = qual;
		copyToString( filter, tmp_vcf.filter);
		get_vcf_info( readline + offset, tmp_vcf);

		//L("token\t%s\t%u\t%u\t%d\n", tmp_vcf.sv.c_str(), tmp_vcf.s, tmp_vcf.e, tmp_vcf.sup);

		map_record[string(chr)].push_back( tmp_vcf );

		//if ( 7 >= ret ){ fea[0]='\0';}
		//// convert from ChrSTH to STH
		//map_record[string(ref).substr(3)].push_back( tmp_pred );
		count++;

	}

	E("Total Prediction Record\t%d\n", count);
	E("Total References with Predictions:\t%d\n", (int)map_record.size());
	fclose(fp);
	//free(s_contig);
	//free(s_align);
	//free(s_ref);
	free(fea);
	free(filter);
	free(ref);
	free(alt);
	free(chr);
	//free(misc);
	free(readline);
}
/**********************************************/
int get_vcf_info( char *option, unit_vcf &v_token)
{
	char *info=(char*)malloc(MAX_LINE);
	char *tag=(char*)malloc(MAX_LINE);
	char *misc=(char*)malloc(MAX_LINE);
	char c;
	int offset;
	int ret = 4;
	int is_set= 0;
	//L(">>%s\n", option);
	while( *option )
	{
		ret = sscanf( option, "%[^\=]%c%[^\;]%c%n", info, &c, tag, &c, &offset);
		option += offset;
		//L("%d\t%d\t%s\t%s\n", offset, ret, info, tag);
		// Bad Way


		if ( 0 == strncmp("SVTYPE", info, 6 )) // SVTYPE
		{
			copyToString( tag, v_token.sv);
		}
		else if ( 0 == strncmp("END", info, 3 )) // END. for inversion
		{
			sscanf(tag, "%u", &(v_token.e) );
			is_set ++;
		}
		else if ( 0 == strncmp("Min", info, 3 ))
		{
			sscanf(tag, "%d", &(v_token.min) );
		}
		else if ( 0 == strncmp("Max", info, 3 ))
		{
			sscanf(tag, "%d", &(v_token.max) );
		}
		else if ( 0 == strncmp("Avg", info, 3 ))
		{
			sscanf(tag, "%d", &(v_token.max) );
		}
		else if ( 0 == strncmp("SOURCE", info, 6 ))
		{
			sscanf(tag, "%u,%u%n", &(v_token.s), &(v_token.e), &offset );
			is_set ++;
		}
		//else if ( 0 == strncmp("M", info, 1 ))
		//{
		//	sscanf(tag, "%d", &(v_token.sup) );
		//}
		else if ( 0 == strncmp("Id", info, 2 )) // Identity
		{
			sscanf(tag, "%f", &(v_token.sim) );
		}
		else if ( 0 == strncmp("ID", info, 2 )) // Identity
		{
			sscanf(tag, "%d", &(v_token.id) );
		}
		else if ( 0 == strncmp("Cl", info, 2 )) // Cluster
		{
			sscanf(tag, "%d", &(v_token.cluster) );
		}
		else if ( 0 == strncmp("Co", info, 2 )) // Cluster
		{
			sscanf(tag, "%d", &(v_token.contig) );
		}
		else if ( 0 == strncmp("P", info, 1 )) // optional: patient id
		{
			copyToString( tag, v_token.patient);
		}
		else if ( 0 == strncmp("Gene", info, 4 )) // optional: gene id
		{
			copyToString( tag, v_token.gid);
		}
		else if ( 0 == strncmp("Trans", info, 5 )) // optional: transcript id
		{
			copyToString( tag, v_token.tid);
		}
		else if ( 0 == strncmp("Num", info, 3 )) // optional: number of overlapping genes
		{
			sscanf(tag, "%d", &(v_token.num_gene) );
		}
		else if ( 0 == strncmp("CDS", info, 3 )) // optional: type of cds overlap
		{
			sscanf(tag, "%d", &(v_token.cds) );
		}
		else if ( 0 == strncmp("Over", info, 4 )) // optional: Overlap length
		{
			sscanf(tag, "%d", &(v_token.ol) );
		}
		if ( 4 != ret ){ break; } // for the last record without colon

	}

	free(misc);
	free(tag);
	free(info);
	return is_set;
}
/**********************************************/
// We are interestedd in either CDS or UTR or exon or start/stop codon
int sv_of_interest( const unit_vcf &vcf )
{
	int flag = ( (vcf.sv == "INV") || (vcf.sv == "DUP") );
	return flag;
}
/**********************************************/
// What DUP records do here should be
// BP1 BP2 BP3
void parse_ShortSV_VCF( char *predict_file , map<string, vector <unit_vcf> > &map_record )
{
	string total_seq="", total_rc="";

	char *readline=(char*)malloc(MAX_LINE);
	char *chr=(char*)malloc(TOKEN_LENGTH);
	char *ref=(char*)malloc(TOKEN_LENGTH);
	char *alt=(char*)malloc(TOKEN_LENGTH);
	char *fea=(char*)malloc(TOKEN_LENGTH);
	char *filter = (char*)malloc(TOKEN_LENGTH);
	char *misc = (char*)malloc(MAX_LINE);

	char header;
	int count = 0;
	int id = 0, support = 0, offset =0;
	uint32_t start = 0, end = 0;
	float qual = 0.0;
	int parse_end = 0;
	unit_vcf tmp_vcf;
	FILE *fp = fopen( predict_file, "r");
	int cur = 0, cur_gene = 0;
	int ret;

	while( NULL != fgets(readline, MAX_LINE, fp) )
	{
		if ( 0 == strncmp("#", readline, 1) )
		{	continue;	}

		//ret = sscanf( readline, "%s %u %c %s %s %f %s %n\n",
		//	chr, &start, &header, ref, alt,
		//	&qual, filter, &offset);
		ret = sscanf( readline, "%s %u %c %s %s %f %s %s\n",
			chr, &start, &header, ref, alt,
			&qual, filter, misc);

		//get_vcf_info( readline + offset);
		copyToString( chr, tmp_vcf.chr);
		copyToString( ref, tmp_vcf.ref);
		copyToString( alt, tmp_vcf.alt);
		tmp_vcf.pos = start;	// just in case it's one bp;
		tmp_vcf.s = start;
		tmp_vcf.qual = qual;
		copyToString( filter, tmp_vcf.filter);
		copyToStringStrip( misc, tmp_vcf.info);
		parse_end = get_vcf_info( misc, tmp_vcf);
		//E("B4 %s\n", readline + offset);
		//copyToStringStrip( readline + offset, tmp_vcf.info);
		//E("AF %s\n", readline + offset);
		//parse_end = get_vcf_info( readline + offset, tmp_vcf);

		if ( 1 != parse_end ){E("Invalid Coordinate %s\n", readline);}
		else if ( !sv_of_interest( tmp_vcf) ) { continue;}
		//L("token\t%s\t%u\t%u\t%d\n", tmp_vcf.sv.c_str(), tmp_vcf.s, tmp_vcf.e, tmp_vcf.sup);

		map_record[string(chr)].push_back( tmp_vcf );

		//if ( 7 >= ret ){ fea[0]='\0';}
		//// convert from ChrSTH to STH
		//map_record[string(ref).substr(3)].push_back( tmp_pred );
		count++;
	}
	E("Total Prediction Record\t%d\n", count);
	E("Total References with Predictions:\t%d\n", (int)map_record.size());
	fclose(fp);

	free(fea);
	free(filter);
	free(ref);
	free(alt);
	free(chr);
	free(readline);
}
/**********************************************/
bool sortVCFPos( const unit_vcf &p1, const unit_vcf &p2 )
{
	return (
		(p1.pos < p2.pos)
		|| ( (p1.pos == p2.pos) && (p1.s < p2.s) )
		|| ( (p1.pos == p2.pos) && (p1.s == p2.s) && (p1.e < p2.e) )
		|| ( (p1.pos == p2.pos) && (p1.s == p2.s) && (p1.e == p2.e) && ( p1.alt < p2.alt ) )
		);
}
/**********************************************/
bool sortVCFStart( const unit_vcf &p1, const unit_vcf &p2 )
{
	return (
		(p1.s < p2.s)
		|| ( (p1.s == p2.s) && (p1.e < p2.e) )
		|| ( (p1.s == p2.s) && (p1.e == p2.e) && ( p1.alt < p2.alt ) )
		);
}
/**********************************************/
bool cmpVCFPos( const unit_vcf &p1, const unit_vcf &p2 )
{
	return (
		( p1.pos < p2.pos )
		|| ( ( p1.pos==p2.pos ) && ( p1.s < p2.s ))
		|| ( ( p1.pos==p2.pos ) && ( p1.s == p2.s ) && ( p1.e < p2.e ))
		|| ( ( p1.pos == p2.pos ) &&  ( p1.s==p2.s ) && ( p1.e == p2.e ) && ( p1.alt < p2.alt ))
		);
}
/**********************************************/
int sv_type( const unit_vcf &t_vcf)
{
	int flag = 0;
	if ( "INV" == t_vcf.sv)
	{
		flag = 5;
	}
	else if ( "DUP" == t_vcf.sv)
	{
		flag = 4;
	}
	else if ( "INS" == t_vcf.sv)
	{
		flag = 3;
	}
	else if ( "DEL" == t_vcf.sv)
	{
		flag = 2;
	}
	else if ( "SNV" == t_vcf.sv)
	{
		flag = 1;
	}
	//else{ flag = 0;}
	return flag;
}
/**********************************************/
// decide which SV call we should keep
int cmp_vcf( const unit_vcf &p1, const unit_vcf &p2 )
{
	int flag = 0; // 0 for p1 is better, 1 otherwise
	L("\tCMP OLD\t%d\t%u\t%u\t%f\t%s\t%d\t%s\t%s\n", p1.cluster, p1.s, p1.e, p1.sim, p1.sv.c_str(), p1.sup, p1.ref.c_str(), p1.alt.c_str() );
	L("\tCMP NEW\t%d\t%u\t%u\t%f\t%s\t%d\t%s\t%s\n", p2.cluster, p2.s, p2.e, p2.sim, p2.sv.c_str(), p2.sup, p2.ref.c_str(), p2.alt.c_str() );

	if ( p1.sv == p2.sv )
	{
		if ( p1.sim < p2.sim) {flag = 1;} // Pick the latter one When the later one has better alignment score
	}
	else if ( sv_type(p1) < sv_type(p2))
	{
		flag = 1;
	}
	return flag;
}
// Assume VCF is sorted by starting positon but with Overlap
/**********************************************/
void sortVCF_WithOverlap( map<string, vector<unit_vcf> > &map_record  )
{
	int limit = 0; // items in single chromosome
	int diff = 0; // clusters
	int i_tmp = 0; // the index of last component in tmp_vcf
	int flag_overlap = 0;

	map<string, vector<unit_vcf> >::iterator it;
	for( it = map_record.begin(); it != map_record.end(); it++ )
	{
		// sorting by the starting coordinate
		sort( it->second.begin(), it->second.end(), sortVCFStart);

		limit=(int)it->second.size();
		vector<unit_vcf> tmp_vcf;
		tmp_vcf.reserve( limit );
		tmp_vcf.push_back( it->second[0] );
		i_tmp = 0;

		//L( "==0\t%s\t%u\t%u\t%u\n", it->first.c_str(), it->second[0].s, it->second[0].e, it->second[0].s);
		L( "\tAdd\t0\t%s\t%u\t%u\t%d\n", it->first.c_str(), it->second[0].s, it->second[0].e, diff);

		for( int i = 1; i < limit; i++ )
		{
			if ( ( it->second[i].s != tmp_vcf[i_tmp].s) ||
				(it->second[i].e != tmp_vcf[i_tmp].e) )
			{
				diff = it->second[i].s - tmp_vcf[i_tmp].e;
				if ( 0 >= diff) // overlapping
				{
					// Case 1 : proper containment
					if ( it->second[i].e <= tmp_vcf[i_tmp].e )
					{
						L("SV1\tProperContain %s %d %u %u %s\n", it->first.c_str(), i , it->second[i].s, it->second[i].e, it->second[i].sv.c_str()) ;
						//tmp_pred.push_back( it->second[i] );
						//i_tmp++;
					}
					else
					{	// pick between it->second[i] and tmp_vcf[last]
						L("SV2\tOverlap %s %d %u %u with %d\n", it->first.c_str(), i, it->second[i].s, it->second[i].e, diff) ;
						flag_overlap = cmp_vcf( tmp_vcf[i_tmp], it->second[i]);
						if ( 1 == flag_overlap) // pop the last item from tmp_vcf
						{
							L("\tRemove %u %u %f %s %d\n", tmp_vcf[i_tmp].s, tmp_vcf[i_tmp].e, tmp_vcf[i_tmp].sim, tmp_vcf[i_tmp].sv.c_str(), tmp_vcf[i_tmp].sup);
							tmp_vcf.pop_back();
							L( "\tAdd\t%d\t%s\t%u\t%u\t%s\t%d\t%d\n", i, it->first.c_str(), it->second[i].s, it->second[i].e, it->second[i].sv.c_str(), it->second[i].sup, diff);
							tmp_vcf.push_back( it->second[i] );
						}
					}
				}
				else
				{
						L( "\tAdd\t%d\t%s\t%u\t%u\t%s\t%d\n", it->second[i].id, it->first.c_str(), it->second[i].s, it->second[i].e, it->second[i].sv.c_str(), diff);
						tmp_vcf.push_back( it->second[i] );
						i_tmp++;
				}
				//////////
			}
			else
			{
				L("SV\tduplicate %s %d %u %u\n", it->first.c_str(), i, it->second[i].s, it->second[i].e);
			}
		}

	}
}
/**********************************************/
void sortVCF( map<string, vector<unit_vcf> > &map_record  )
{
	map<string, vector<unit_vcf> >::iterator it;
	for( it = map_record.begin(); it != map_record.end(); it++ )
	{
		// sorting by the starting coordinate
		sort( it->second.begin(), it->second.end(), sortVCFStart);
	}
}
/**********************************************/
void sortVCF_POS( map<string, vector<unit_vcf> > &map_record  )
{
	map<string, vector<unit_vcf> >::iterator it;
	for( it = map_record.begin(); it != map_record.end(); it++ )
	{
		// sorting by the starting coordinate
		sort( it->second.begin(), it->second.end(), sortVCFPos);
	}
}
/**********************************************/
void OutputVCF( map<string, vector<unit_vcf> > &map_record, char *output  )
{
	char *out_name=(char*)malloc(MAX_LINE);
	strcpy(out_name, output);
	strcat(out_name, ".predict");
	FILE *out_fp=fopen( out_name, "w");
	int count = 0;

	map<string, vector<unit_vcf> >::iterator it;
	for( it = map_record.begin(); it != map_record.end(); it++)
	{
		int limit = it->second.size();
		for ( int i=0; i < limit; i++)
		{
			fprintf( out_fp, "%s\t%u\t.\t%s\t%s\t%f\t%s", it->first.c_str(), it->second[i].pos, it->second[i].ref.c_str(), it->second[i].alt.c_str(), it->second[i].qual, it->second[i].filter.c_str());
			fprintf( out_fp, "\tSVTYPE=%s;Cluster=%d;MinSupport=%d;MaxSupport=%d;Identity=%f;", it->second[i].sv.c_str(), it->second[i].cluster, it->second[i].min, it->second[i].max, it->second[i].sim);
			if ( 0== it->second[i].sv.compare("INV"))
			{
				fprintf( out_fp, "END=%u\n", it->second[i].e );
			}
			else if ( 0== it->second[i].sv.compare("DUP"))
			{
				fprintf( out_fp, "SOURCE=%u,%u\n", it->second[i].s, it->second[i].e );
			}
		}
	}
	fclose(out_fp);

}
/**********************************************/
int equal_bp( const unit_vcf &t1, const unit_vcf &t2)
{
	// if two events have identical type, s, e, and pos, and alt
	int flag = 0; // not equal
	if ( ( t1.sv == t2.sv)
		&& ( t1.s == t2.s)
		&& ( t1.e == t2.e)
		&& ( t1.pos == t2.pos)
		&& ( t1.alt == t2.alt) //
	)
	{	flag = 1;
	}
	return flag;
}
/**********************************************/
void merge_vcf_item( FILE *out, const char *option, int min, int max, float sim)
{
	char *info=(char*)malloc(MAX_LINE);
	char *tag=(char*)malloc(MAX_LINE);
	char *misc=(char*)malloc(MAX_LINE);
	char c;
	int t_offset=0, offset=0;
	uint32_t s, e;
	int ret=4;
	//fprintf(out,"\n==%s\n", option);
	while( *(option+t_offset))
	{
		ret = sscanf( option+t_offset, "%[^\=]=%[^\;]%c%n", info, tag, &c, &offset);
		//fprintf(out, ">%s %s\n", info, tag);
		if ( 0 == strncmp("Min", info, 3 )) // END. for inversion
		{
			fprintf(out,"%s=%d;", info, min);
		}
		else if ( 0 == strncmp("Max", info, 3 ))
		{
			fprintf(out,"%s=%d;", info, max);
		}
		else if ( 0 == strncmp("Ide", info, 3 ))
		{
			fprintf(out,"%s=%f;", info, sim);
		}
		else
		{
			fprintf(out,"%s=%s;", info, tag);
		}
		t_offset += offset;
		if ( 3 != ret){break;} // for the last record without colon
	}
	fprintf(out,"\n");
	free(misc);
	free(tag);
	free(info);

}
/**********************************************/
// Remove only Identical Records
void VCF_RemoveDuplicate( const map<string, vector<unit_vcf> > &map_record, char *outfile )
{
	int limit = 0; // items in single chromosome
	//int diff = 0; // clusters
	//int i_tmp = 0; // the index of last component in tmp_vcf
	//int flag_overlap = 0;
	FILE *out_fp = fopen(outfile, "w");

	int i =0, j = 0, chunk = 0, chunk_size=0;
	int min = 0, max = 0;
	float sim = 0.0;
	map<string, vector<unit_vcf> >::const_iterator it;
	int count = 0, merge = 0, total = 0;
	for( it = map_record.begin(); it != map_record.end(); it++ )
	{
		limit=(int)it->second.size();
		vector<unit_vcf> tmp_vcf;
		//tmp_vcf.reserve( limit );
		//tmp_vcf.push_back( it->second[0] ); // push
		//i_tmp = 0;

		i = 0;
		j = 0;
		chunk = 0;
		while( i < limit)
		{
			// grouping the cluster starting from i. j should be the next start position
			L( "Record\t%d %s:\t%u\t%u\t%u\t%d\n", i, it->first.c_str(), it->second[i].pos, it->second[i].s, it->second[i].e, it->second[i].cluster);
			min = it->second[i].min;
			max = it->second[i].max;
			sim = it->second[i].sim;
			j = i+1;
			while( ( j < limit ) && ( equal_bp( it->second[i], it->second[j]) ) ) // Duplicate
			{
				L( "Merge\t%d %d:\t%u\t%u\t%u\t%d\n",j , i, it->second[i].pos, it->second[i].s, it->second[i].e, it->second[j].cluster);
				//L("Increment %d\n", j);
				min += it->second[j].min;
				max += it->second[j].max;
				if ( sim < it->second[j].sim){ sim = it->second[j].sim;}
				j++;
				merge++;
			} //now from i to j-1
			//L( "Right in  %d\t%s\t%u\t%u\t%u\n", j-1, it->first.c_str(), it->second[j-1].pos, it->second[j-1].s, it->second[j-1].e);
			chunk_size =  j - i;
			chunk += chunk_size;
			//L("Interval size %d (%d)\n", chunk, chunk_size );

			fprintf( out_fp, "%s\t%u\t.\t%s\t%s\t", it->second[i].chr.c_str(), it->second[i].pos, it->second[i].ref.c_str(), it->second[i].alt.c_str() );
			fprintf( out_fp, "%f\t%s\t", it->second[i].qual, it->second[i].filter.c_str() );
			merge_vcf_item( out_fp, it->second[i].info.c_str(), min, max, sim);
			total++;

			i = j;
		}
		E("Done %s: %d %d (items, intervals)\n", it->first.c_str(), limit, chunk);
		count += limit;
	}
	E("Numebr of All/Merge/Final Records:\t%d\t%d\t%d\n", count, merge, total);
	fclose(out_fp);
}
/**********************************************/
// decide relations of two SVs
int compare_vcf( const unit_vcf &p1, const unit_vcf &p2 )
{
	// assuming p1.s <= p2.s and p1.e <= p2.e
	int flag = 0;
	if (p1.s == p2.s)
	{
		if ( p1.e == p2.e)
		{	flag = 1 ; } // identical interval
		else if ( p1.e < p2.e)
		{	flag = 2 ; } // super-interval
		else
		{	flag = 0; } // sub-interval
	}
	else
	{
		if ( p1.e == p2.e)
		{	flag = 0 ; } // sub interval
		else
		{	flag = 3 ; }// partial
	}
	return flag;
}


/**********************************************/
/**********************************************/
int overlap_bp( const unit_vcf &t1, uint32_t &lb, uint32_t &rb )
{
	// if this even falls within lb and rb, then report 1
	int flag = 0; // not equal
	if ( "DUP" == t1.sv)
	{
		if ( ( lb <= t1.pos ) && ( t1.pos <= rb) ){ flag = 1;}
	}
	else if ("INV" == t1.sv)
	{
		if ( ( lb <= t1.s ) && ( t1.e <= rb) )
		{	flag = 1;	}
		else if ( ( t1.s <= lb) && ( rb <= t1.e) )
		{	flag = 1;	}
		else if ( ( lb <= t1.s) && ( t1.s  <= rb))
		{	flag = 1;	}
		else if ( ( lb <= t1.e) && ( t1.e <= rb))
		{	flag = 1;	}
	}
	return flag;
}
/**********************************************/
// Initiate an interval when creating a new cluster
void create_interval_bd( const unit_vcf &vcf_t, uint32_t &lb, uint32_t &rb)
{
	lb = vcf_t.pos;
	if ( "DUP" == vcf_t.sv)
	{	rb = vcf_t.pos;	}
	else if ("INV" == vcf_t.sv)
	{	rb = vcf_t.e;	}
}
/**********************************************/
// Adjust an interval after meeting overlapping one
void adj_interval_bd( const unit_vcf &vcf_t, uint32_t &lb, uint32_t &rb)
{
	if ( "DUP" == vcf_t.sv)
	{
		if ( vcf_t.pos < lb){ lb = vcf_t.pos;}
		if ( rb < vcf_t.pos ){ rb = vcf_t.pos;}
	}
	else if ("INV" == vcf_t.sv)
	{
		if ( vcf_t.pos < lb){ lb = vcf_t.pos;}
		if ( rb < vcf_t.e ){ rb = vcf_t.e;}
	}
}
/**********************************************/
// To DO: Output Conflicting VCF which might not happen simultaneously
// Assumption: VCF Vector are already sorted by POS Position
//
void clusterVCF( const map<string, vector<unit_vcf> > &map_record, char *outfile  )
{
	int limit = 0; // items in single chromosome
	int i_tmp = 0; // the index of last component in tmp_vcf
	int flag_overlap = 0;

	int i =0, j = 0;
	int chunk = 0, chunk_size;
	int cmp_flag = 0;
	int clus_id = 0;
	int n_mc = 0, n_in_mc = 0; // number of clusters with multiple items. Items in such clsuters
	uint32_t lb =0, rb =0;
	int count = 0, merge = 0, total = 0, bad_event= 0;
	uint32_t prev_rb =0, diff =0;
	map<string, vector<unit_vcf> >::const_iterator it;
	for( it = map_record.begin(); it != map_record.end(); it++ )
	{
		// sorting by the starting coordinate
		//sort( it->second.begin(), it->second.end(), sortVCFStart);

		limit=(int)it->second.size();
		vector<unit_vcf> tmp_vcf;
		tmp_vcf.reserve( limit );
		tmp_vcf.push_back( it->second[0] );
		i_tmp = 0;

		i = 0;	j = 0;
		chunk = 0;
		clus_id = 0;
		lb = 0; rb = 0; prev_rb = 0; diff = 0;
		n_mc = 0; n_in_mc=0;
		while( i < limit)
		{
			// grouping the cluster starting from i. j should be the next start position
			create_interval_bd( it->second[i], lb, rb);
			L( "\nStart\t%d\t%s\t%d\t%s\t%u\t%u\t%u\t%d\t%u\t%u\n", i, it->first.c_str(), it->second[i].id, it->second[i].sv.c_str(), it->second[i].pos, it->second[i].s, it->second[i].e, clus_id, lb, rb );
			j = i+1;
			while( (j < limit ) && ( overlap_bp( it->second[j], lb, rb) ) ) // Overlapping
			{
				adj_interval_bd( it->second[j], lb, rb);
				L( "Merge\t%d %d:\t%d\t%s\t%u\t%u\t%u\t%d\t%u\t%u\n", j , i, it->second[j].id, it->second[j].sv.c_str(), it->second[j].pos, it->second[j].s, it->second[j].e, clus_id, lb, rb );
				//cmp_flag = compare_vcf( it->second[i], it->second[j] );
				//// Case 1 : identical
				//if ( 1 == cmp_flag )
				//{
				//	L("SV\tduplicate %s %d %d (%u %u)\n", it->first.c_str(), j, i, it->second[j].s, it->second[j].e);
				//}
				//// Case 2 : proper containment
				//else if ( 2 == cmp_flag )
				//{
				//	L("SV\tSuperSet %s %d %d %u %u %s\n", it->first.c_str(), j, i, it->second[j].s, it->second[j].e, it->second[j].sv.c_str()) ;
				//}
				//// Case 3 : Tricky Case
				//else if ( 3 == cmp_flag )
				//{
				//	L("SV\tPartial %s %d %d %u %u %s %u %u %s\n", it->first.c_str(), j, i , it->second[j].s, it->second[j].e, it->second[j].sv.c_str(),
				//		it->second[i].s, it->second[i].e, it->second[i].sv.c_str()) ;
				//}
				j++;
			}
			L( "Exten\t%d\t%d\t%s\t%u\t%u\t%u\t%u\t%u\n", i, j-1, it->first.c_str(), it->second[j-1].pos, it->second[j-1].s, it->second[j-1].e, lb, rb);
			chunk_size =  j - i;
			chunk += chunk_size;
			if ( 1 < chunk_size ) { n_mc++; n_in_mc+= chunk_size;}
			diff = lb - prev_rb - 1;
			L("Interval %d %d %d %u\n", total, chunk, chunk_size, diff );
			prev_rb = rb;
			i = j;
			clus_id++; // increment cluster id
			total++;
		}
		E("Done %s: %d %d (items, intervals)\n", it->first.c_str(), limit, chunk);
		merge += n_in_mc;
		bad_event += n_mc;
		count+= limit;
	}
	E("Summary for All/Merge/Final/BadCluster:\t%d\t%d\t%d\t%d\n", count, merge, total, bad_event);
}
/**********************************************/
// decide which SV call we should keep
int overlap_vcf( const unit_vcf &p1, const unit_vcf &p2 )
{
	int flag = 0;
	if ( (p1.s >= p2.s) && ( p1.e <= p2.e))
	{
		flag = p1.e - p1.s + 1;
	}
	else if ( (p1.s <= p2.s) && ( p2.e <= p1.e))
	{
		flag = p2.e - p2.s + 1;
	}
	else if ( (p1.s <= p2.s) && ( p2.s <= p1.e))
	{
		flag = p1.e - p2.s + 1;
	}
	else if ( (p2.s <= p1.s) && ( p1.s <= p2.e))
	{
		flag = p2.e - p1.s + 1;
	}
	return flag;
}
/**********************************************/
void cmp_patients_VCF(  map<string, vector<unit_vcf> > &base_record, map<string, vector<unit_vcf> > &map_record, char *outfile  )
{
	int t_l = 0, b_l = 0; // target_length and base_length
	int diff = 0;
	int i_tmp = 0; // the index of last component in tmp_vcf
	int flag_overlap = 0;
	int pred_length = 0;
	int j = 0, start = 0; // the current index in base
	int best_hit = -1, best_l = 0;
	map<string, vector<unit_vcf> >::iterator it;
	map<string, vector<unit_vcf> >::iterator base_it;

	FILE *fp = fopen( outfile, "w");
	for( it = map_record.begin(); it != map_record.end(); it++ )
	{
		base_it = base_record.find(it->first);
		// sorting by the starting coordinate
		sort( it->second.begin(), it->second.end(), sortVCFStart);
		t_l = (int)it->second.size();
		//E("searching %s\n", it->first.c_str());
		if ( base_it != base_record.end() )
		{

			// sorting by the starting coordinate
			sort( base_it->second.begin(), base_it->second.end(), sortVCFStart);
			//E("Hey %s\n", base_it->first.c_str());
			b_l = (int)base_it->second.size();
			start = 0;
			for( int i = 1; i < t_l; i++ )
			{
				flag_overlap = 0; // 0 for germline
				best_hit = -1;
				best_l = 0;
				pred_length = it->second[i].e - it->second[i].s+1;
				// vcf info
				fprintf(fp, "%s\t%u\t.\t%s\t%s\t%f\t%s\tSVTYPE=%s;", it->second[i].chr.c_str(), it->second[i].s, it->second[i].ref.c_str(), it->second[i].alt.c_str(), it->second[i].qual, it->second[i].filter.c_str(), it->second[i].sv.c_str());
				fprintf(fp, "END=%u;Cluster=%d;MinSupport=%d;MaxSupport=%d;Identity=%f;P=%s;", it->second[i].e, it->second[i].cluster, it->second[i].sup, it->second[i].sup, it->second[i].sim, it->second[i].patient.c_str());
				//fprintf(fp,"start %d\n", i);

				j = start;
				while( ( j < b_l) )// && (it->second[i].e < base_it->second[j].s) )
				{
					//fprintf(fp, "\nwhat\t%d\t%d\t%d\n", i, j, best_l);
					if ( it->second[i].s > base_it->second[j].e)
					{
						//fprintf(fp,"passing %u\t%u\n", base_it->second[j].s, base_it->second[j].e);
						start = j;
					}
					else if ( it->second[i].e < base_it->second[j].s)
					{
						if ( 0 == flag_overlap )
						{
							fprintf(fp, "G=Somatic;GR=0;\n");
						}
						else
						{
							fprintf(fp, "G=Germline;GR=%f;\n", best_l*1.0/pred_length);
							//fprintf(fp, "G=Germline;GR=%f_%d_%d\n", best_l*1.0/pred_length, best_l, pred_length);
						}
						break;
					}
					else if ( best_l < overlap_vcf( it->second[i], base_it->second[j]) )
					{
						best_l = overlap_vcf( it->second[i], base_it->second[j]);
						//fprintf(fp, "\nRP\t%d\t%d\n", best_l, overlap( it->second[i], base_it->second[j]));

						best_hit = j;
						flag_overlap = 1;
					}
					j++;
				}

			}
		}
		else
		// Chromosome Missing. All Records are Somatic
		{
			//E("Not FOUND %s\n", it->first.c_str());
			for( int i = 1; i < t_l; i++ )
			{
				fprintf(fp, "%s\t%u\t.\t%s\t%s\t%f\t%s\tSVTYPE=%s;", it->second[i].chr.c_str(), it->second[i].s, it->second[i].ref.c_str(), it->second[i].alt.c_str(), it->second[i].qual, it->second[i].filter.c_str(), it->second[i].sv.c_str());
				fprintf(fp, "END=%u;Cluster=%d;MinSupport=%d;MaxSupport=%d;Identity=%f;P=%s;", it->second[i].e, it->second[i].cluster, it->second[i].sup, it->second[i].sup, it->second[i].sim, it->second[i].patient.c_str());
				fprintf(fp, "G=Somatic;GR=0;\n");
			}
		}
	}
}

/**********************************************/
int valid_vcf( const unit_vcf vcf_t, float inv_id, int inv_sup, float dup_id, int dup_sup)
{
	int flag = 0; // 1 for pass , 0 otherwise
	if ( "INV" == vcf_t.sv)
	{
		if ( (inv_sup <= vcf_t.max) && ( inv_id <= vcf_t.sim) ){ flag = 1;}
	}
	else
	{
		if ( (dup_sup <= vcf_t.max) && ( dup_id <= vcf_t.sim) ){ flag = 1;}
	}

	return flag;
}
/**********************************************/
// Filter VCF based on special condition
void FilterVCF( const map<string, vector<unit_vcf> > &map_record, char *output, float min_id, int min_sup  )
{
	char *pass_name=(char*)malloc(MAX_LINE);
	char *miss_name=(char*)malloc(MAX_LINE);
	strcpy(pass_name, output);
	strcat(pass_name, ".pass");
	strcpy(miss_name, output);
	strcat(miss_name, ".miss");
	FILE *p_fp=fopen( pass_name, "w");
	FILE *m_fp=fopen( miss_name, "w");
	int count = 0;
	int vcf_flag = 0, n_pass = 0, n_miss = 0;

	map<string, vector<unit_vcf> >::const_iterator it;
	for( it = map_record.begin(); it != map_record.end(); it++)
	{
		int limit = it->second.size();
		for ( int i=0; i < limit; i++)
		{
			vcf_flag = valid_vcf( it->second[i], min_id*100, min_sup, 100.0, 10 );
			if ( vcf_flag )
			{
				n_pass++;
				fprintf(p_fp, "%s\t%u\t.\t%s\t%s\t%f\t%s\t%s\n", it->second[i].chr.c_str(), it->second[i].s, it->second[i].ref.c_str(), it->second[i].alt.c_str(), it->second[i].qual, it->second[i].filter.c_str(), it->second[i].info.c_str());
				if ( "INV" == it->second[i].sv)
				{
					L("Error %s %f\n", it->second[i].sv.c_str(), it->second[i].sim - min_id*100);
				}
				else
				{
					L("Error %s %f\n", it->second[i].sv.c_str(), it->second[i].sim - 100.0);
				}
				//L("Error %f\n", it->second[i].id - );
			}
			else
			{
				n_miss++;
				fprintf(m_fp, "%s\t%u\t.\t%s\t%s\t%f\t%s\t%s\n", it->second[i].chr.c_str(), it->second[i].s, it->second[i].ref.c_str(), it->second[i].alt.c_str(), it->second[i].qual, it->second[i].filter.c_str(), it->second[i].info.c_str());
			}
		}
		count += limit;
	}
	E( "Number of All/Pass/Miss Record:%d %d %d\n", count, n_pass, n_miss);
	fclose(p_fp);
	fclose(m_fp);
	free(miss_name);
	free(pass_name);

}
/**********************************************/
// Filter VCF based on special condition
void MergeFasta( char *predict_file,  map<string, int > &seq_map, char *outfile, char *lib_name )
{
	char *freq_name=(char*)malloc(MAX_LINE);
	char *fa_name=(char*)malloc(MAX_LINE);
	strcpy(freq_name, outfile);
	strcat(freq_name, ".freq");
	strcpy(fa_name, outfile);
	strcat(fa_name, ".fa");
	FILE *freq_fp=fopen( freq_name, "w");
	FILE *out_fp=fopen( fa_name, "w");

	char *readline=(char*)malloc(MAX_LINE);
	FILE *fp = fopen( predict_file, "r");
	int count = 0, n_seq = 0;
	string seq;

	map<string, int>::iterator it;

	while( NULL != fgets(readline, MAX_LINE, fp) )
	{
		if ( 0 == strncmp(">", readline, 1) ){continue;}
		copyToStringStrip(readline, seq);
		it = seq_map.find(seq);
		if ( seq_map.end() != it )
		{
			it->second++;
		}
		else
		{
			seq_map[seq] = 1;
		}
		count++;
		if ( 0 == count%1000000){E(".");}
	}

	int idx = 0;
	for( it = seq_map.begin(); it != seq_map.end(); it++)
	{
		fprintf(out_fp, ">%s_%d\n%s\n", lib_name, idx, it->first.c_str());
		n_seq += it->second;
		if ( 100 > (int)it->first.size())
			fprintf(freq_fp, "%d\t%s\t%d\n", idx++, it->first.c_str(), it->second);
		else
			fprintf(freq_fp, "%d\t%lu\t%d\n", idx++, it->first.size(), it->second);
	}
    E("\nStatistics:\t%lu\t%d\t%d\n", seq_map.size(), n_seq, count );
	fclose(fp);
	fclose(out_fp);
	fclose(freq_fp);
	free(readline);
	free(freq_name);
	free(fa_name);

}











///**********************************************/
//void parse_InversionAnnoModified( char *predict_file , map<string, vector <unit_pred> > &map_record )
//{
//	string total_seq="", total_rc="";
//
//	char *readline=(char*)malloc(MAX_LINE);
//	//char *header=(char*)malloc(TOKEN_LENGTH);
//	char *misc=(char*)malloc(TOKEN_LENGTH);
//	char *ref=(char*)malloc(TOKEN_LENGTH);
//	char *fea1=(char*)malloc(TOKEN_LENGTH);
//	//char *fea2=(char*)malloc(TOKEN_LENGTH);
//
//	char header;
//	int count = 0;
//	int id = 0, support = 0,
//		t_i = 0;//offset =0;
//	uint32_t start = 0, end = 0;
//	float g1 = 0.0, g2 = 0.0, g3 = 0.0;
//	unit_pred tmp_pred;
//	//map< string, vector<unit_pred> > map_record;
//	FILE *fp = fopen( predict_file, "r");
//	//FILE *out_fp= fopen( out_file, "w");
//	int cur = 0, cur_gene = 0;
//	while( NULL != fgets(readline, MAX_LINE, fp) )
//	{
//		//if ( 0 != strncmp(">", readline, 1) )
//		//{
//		//	continue;
//		//}
//		sscanf( readline, "%d %d %d %d %s %u %u %f %f %f %f %f %f %s\n",
//			&t_i, &t_i, &id, &t_i, ref,
//			&start, &end,
//			&g1, &g2, &g3, &g3, &g3, &g3,
//			fea1);
//
//		// id is the id WITHIN a patient report
//		tmp_pred.id = id;
//		// Reason: Alex's code reports a bed-like annotation so we have to add 1 here
//		tmp_pred.s = start + 1;
//		tmp_pred.e = end;
//		tmp_pred.sim = g1;
//		// convert from ChrSTH to STH
//		map_record[string(ref).substr(3)].push_back( tmp_pred );
//		count++;
//
//	}
//
//	E("Total Prediction Record\t%d\n", count);
//	E("Total References with Predictions:\t%d\n", (int)map_record.size());
//	//fclose(out_fp);
//	fclose(fp);
//
//	//free(fea2);
//	free(fea1);
//	free(ref);
//	free(misc);
//	//free(header);
//	free(readline);
//}
///**********************************************/
//void GenerateRef( map<string, vector<unit_pred> > &sort_record, map<string, string > &chr_map, char *output  )
//{
//	map<string, vector< unit_pred> >::iterator it;
//	map<string, string>::iterator chr_it;
//
//	map<string, int> flag_map;
//	for( it = sort_record.begin(); it != sort_record.end(); it++)
//	{
//		flag_map[ it->first ] = 0;
//	}
//
//
//	string ref="";
//	vector<unit_pred> tmp_vec;
//	int limit=0;
//	int num = 0;
//	int pos = 0;
//	int left = 0, right = 0; // zero-based
//	int r_length = 0; // original reference length
//	int sanity = 0;
//	int i = 0, j = 0;
//	FILE *out_fp= fopen( output, "w");
//	//for( chr_it = sort_record.begin(); it != sort_record.end(); it++ )
//	for( chr_it = chr_map.begin(); chr_it != chr_map.end(); chr_it++ )
//	{
//		ref = chr_it->first;sanity=0;
//		E("Checking Reference %s\n", ref.c_str() );
//		fprintf(out_fp, ">%s\n", ref.c_str() );
//		pos = 0;
//
//		it = sort_record.find( ref );
//		r_length=(int)chr_it->second.size();
//		if ( sort_record.end() == it) // without inversions
//		{
//			E("Reference unchanged: %s\n", ref.c_str() );
//			for( j = 0; j < r_length; j++)
//			{
//				fputc( chr_it->second[j], out_fp);
//				pos++;
//				sanity++;
//				if ( 0 == pos%FASTA_LINE ){fputc('\n', out_fp); pos=0;}
//			}
//			if ( 0 != pos%FASTA_LINE ){fputc('\n', out_fp);}
//			if (sanity != r_length){E("Incon\t%d\t%d\n", sanity, r_length);}
//			L("SANITY\t%d\t%d\n", sanity, r_length);
//		}
//		else
//		{
//			L("Ref %s %d\n", ref.c_str(), r_length );
//			// starting inversion for a chromosome
//			tmp_vec = it->second;
//
//			num = 0;
//
//			limit = (int)tmp_vec.size(); // number of inversions in this chromosome
//
//			left = 0;
//			for( i = 0; i < limit; i++ )
//			{
//				L("WHY\t%d\t%d\n", sanity, left);
//				right = (int)tmp_vec[i].s-2;  // 0-based coordinates of the previous rightmost position
//				L( "Inv\t%d\t+\t%d\t%d\n", num, left, right);
//				for( j =left; j <=right; j++)
//				{
//					fputc( chr_it->second[j], out_fp);
//					pos++;
//					sanity++;
//					if ( 0 == pos%FASTA_LINE ){fputc('\n', out_fp); pos=0;}
//				}
//				num++;
//
//				L("Hey %u %u\n", tmp_vec[i].s, tmp_vec[i].e);
//				L( "Inv\t%d\t-\t%u\t%u\n", num, tmp_vec[i].s-1, tmp_vec[i].e-1 );
//
//				// Output Reverse Complement Here
//				left = (int)tmp_vec[i].s-1;
//				right = (int)tmp_vec[i].e-1;
//				for(int j =right; j >=left; j--)
//				{
//					switch( chr_it->second[j])
//					{
//						case 'A':
//							fputc( 'T', out_fp);
//							break;
//						case 'a':
//							fputc( 'T', out_fp);
//							break;
//						case 'C':
//							fputc( 'G', out_fp);
//							break;
//						case 'c':
//							fputc( 'G', out_fp);
//							break;
//						case 'G':
//							fputc( 'C', out_fp);
//							break;
//						case 'g':
//							fputc( 'C', out_fp);
//							break;
//						case 'T':
//							fputc( 'A', out_fp);
//							break;
//						case 't':
//							fputc( 'A', out_fp);
//							break;
//						default:
//							fputc( 'N', out_fp);
//							break;
//
//					}
//					pos++;
//					sanity++;
//					if ( 0 == pos%FASTA_LINE ){fputc('\n', out_fp); pos=0;}
//				}
//				left = (int)tmp_vec[i].e; // 0-based for the next leftmost position
//				num++;
//			}
//			right = r_length-1;
//			L( "Inv\t%d\t+\t%d\t%d\n", num, left, right);
//			for(int j =left; j <=right; j++)
//			{
//				fputc( chr_it->second[j], out_fp);
//				pos++;
//				sanity++;
//				if ( 0 == pos%FASTA_LINE ){fputc('\n', out_fp); pos=0;}
//			}
//			if ( 0 != pos%FASTA_LINE ){fputc('\n', out_fp);}
//			flag_map[ ref ] = 1;
//			if (sanity != r_length){E("Incon\t%d\t%d\n", sanity, r_length);}
//			L("SANITY\t%d\t%d\n", sanity, r_length);
//		}
//	}
//
//	for( it = sort_record.begin(); it != sort_record.end(); it++)
//	{
//		if ( 0 == flag_map[it->first] ) // to make sure two genomes are of equal length
//		{	E("Error: Sequences %s not found in refenrece genome!\n", it->first.c_str() );	}
//	}
//
//	fclose(out_fp);
//}
//
//
///**********************************************/
//void parse_EventAnno( char *predict_file , map<string, vector <unit_abe> > &map_record )
//{
//	//string total_seq="", total_rc="";
//
//	char *readline=(char*)malloc(MAX_LINE);
//	//char *header=(char*)malloc(TOKEN_LENGTH);
//	char *misc=(char*)malloc(TOKEN_LENGTH);
//	char *ref=(char*)malloc(TOKEN_LENGTH);
//	char *fea=(char*)malloc(TOKEN_LENGTH);
//
//	char header;
//	int count = 0;
//	int id = 0, support = 0,
//		offset =0;
//	uint32_t start = 0, end = 0;
//	float g1 = 0.0, g2 = 0.0, g3 = 0.0;
//	unit_abe tmp_abe;
//	//map< string, vector<unit_pred> > map_record;
//	FILE *fp = fopen( predict_file, "r");
//	//FILE *out_fp= fopen( out_file, "w");
//	int cur = 0, cur_gene = 0;
//	while( NULL != fgets(readline, MAX_LINE, fp) )
//	{
//		sscanf( readline, "%d %s %u %u %s %s %s %n\n",
//			&id, ref,
//			&start, &end, &misc, &misc,
//			fea, &offset);
//
//		L( "ORIG\t%s", readline);
//		L( "%d\t%s\t%u\t%u\t%s\n",
//			id, ref, start, end, fea);
//		tmp_abe.id = id;
//		//tmp_abe.ref = ref;
//		tmp_abe.s = start;
//		tmp_abe.e = end;
//		if ( 0 == strncmp( "inv", fea, 3) )
//		{
//			tmp_abe.event = 0; // inversion
//		}
//		else if ( 0 == strncmp( "d", fea, 1) )
//		{
//			tmp_abe.event = 2; //deletion
//		}
//		else
//		{
//			tmp_abe.event = 1; // insertion
//		}
//
//		// convert from ChrSTH to STH
//		map_record[string(ref)].push_back( tmp_abe );
//		count++;
//
//	}
//
//	E("Total Prediction Record\t%d\n", count);
//	//E("Total References with Predictions:\t%d\n", (int)map_record.size());
//	//fclose(out_fp);
//	fclose(fp);
//
//	free(fea);
//	free(ref);
//	//free(misc);
//	free(readline);
//}
//
///**********************************************/
//// sort event by Starting position
//bool seByStart( const unit_abe &p1, const unit_abe &p2 )
//{
//	return p1.s < p2.s;
//}
//
///**********************************************/
//void SortEventPrediction( map<string, vector<unit_abe> > &new_record, map<string, vector<unit_abe> > &map_record  )
//{
//	int limit=0;
//	map<string, vector<unit_abe> >::iterator it;
//	for( it = map_record.begin(); it != map_record.end(); it++)
//	{
//		// sorting by the starting coordinate
//		sort( it->second.begin(), it->second.end(), seByStart);
//		limit=(int)it->second.size();
//		int diff = 0;
//
//		vector<unit_abe> tmp_pred;
//		tmp_pred.reserve( limit );
//		tmp_pred.push_back(it->second[0]);
//		L( "==%d\t%s\t%u\t%u\t%u\n", it->second[0].id, it->first.c_str(), it->second[0].s, it->second[0].e, it->second[0].s);
//		for( int i =1; i < limit; i ++)
//		{
//			if ( ( it->second[i].s != it->second[i-1].s) ||
//				(it->second[i].e != it->second[i-1].e)
//				)
//			{
//				tmp_pred.push_back( it->second[i] );
//				diff = it->second[i].s - it->second[i-1].e;
//				if ( 0 >= diff)
//				{
//					E("Warning\tOverlap %s %d %u %u with %d\n", it->first.c_str(), it->second[i].id, it->second[i].s, it->second[i].e, diff) ;
//				}
//				L( "==%d\t%s\t%u\t%u\t%d\n", it->second[i].id, it->first.c_str(), it->second[i].s, it->second[i].e, diff);
//
//			}
//			else
//			{
//				E("Warning\tduplicate %s %d %u %u\n", it->first.c_str(), it->second[i].id, it->second[i].s, it->second[i].e);
//			}
//		}
//
//		new_record[it->first] = tmp_pred;
//	}
//
//
//}
///**********************************************/
//void OutputEventPrediction( map<string, vector<unit_abe> > &new_record, char *output  )
//{
//	char *out_name=(char*)malloc(MAX_LINE);
//	strcpy(out_name, output);
//	strcat(out_name, ".predict");
//	FILE *out_fp=fopen( out_name, "w");
//	int count = 0;
//
//	map<string, vector<unit_abe> >::iterator it;
//	for( it = new_record.begin(); it != new_record.end(); it++)
//	{
//		int limit = it->second.size();
//		for ( int i=0; i < limit; i++)
//		{
//			fprintf( out_fp, "%d\t%d\t%s\t%u\t%u\n", count, it->second[i].id, it->first.c_str(), it->second[i].s, it->second[i].e);
//			count++;
//		}
//	}
//	fclose(out_fp);
//
//}
///**********************************************/
//// Output the sequence and information of newly generated genome containing all aberrations
//void NewRef( map<string, vector<unit_abe> > &sort_record, map<string, string > &chr_map, char *output  )
//{
//	map<string, vector< unit_abe> >::iterator it;
//	map<string, string>::iterator chr_it;
//
//	map<string, int> flag_map;
//	for( it = sort_record.begin(); it != sort_record.end(); it++)
//	{
//		flag_map[ it->first ] = 0; // If a chromosome is contained in the fa file or not
//	}
//
//
//	string ref="";
//	string abe_type="";
//	vector<unit_abe> tmp_vec;
//	int limit=0;
//	int num = 0;
//	int pos = 0;
//	int left = 0, right = 0; // zero-based
//	int r_length = 0;
//	int sanity = 0;
//	int i = 0, j = 0;
//	int shift = 0; // difference between new genome and original one. Insert indicates positive while delete indicates negative
//	FILE *out_fp= fopen( output, "w");
//	int n_l = 0, n_r = 0; // new left and new right
//	int diff = 0;
//
//	// HARD CODED The Replacement: Always use chromosome two here
//	string rep_chr="2";
//	string rep_seq=chr_map["2"];
//	int rep_length = (int)rep_seq.size();
//
//	for( chr_it = chr_map.begin(); chr_it != chr_map.end(); chr_it++ )
//	{
//		ref = chr_it->first; sanity=0;
//		E("Checking Reference %s\n", ref.c_str() );
//		//fprintf(out_fp, ">%s\n", ref.c_str() );
//		pos = 0;
//
//		it = sort_record.find( ref );
//		r_length=(int)chr_it->second.size();
//		if ( sort_record.end() == it) // without aberrations
//		{
//			E("Chromosome %s unchanged: skipping\n", ref.c_str() );
//		}
//		else
//		{
//			L("Scanning Ref %s with %d\n", ref.c_str(), r_length );
//			// starting aberrations for a chromosome
//			fprintf(out_fp, ">%s_new\n", ref.c_str() );
//			tmp_vec = it->second;
//
//			num = 0;
//
//			limit = (int)tmp_vec.size(); // number of aberrations in this chromosome
//
//			left = 0;
//			for( i = 0; i < limit; i++ )
//			{
//				diff = tmp_vec[i].e - tmp_vec[i].s + 1;
//				right = (int)tmp_vec[i].s-2;  // 0-based coordinates of the previous rightmost position
//				//L( ">>NOR\t%d\t+\t%d\t%d\n", num, left, right);
//				n_l = left + shift;
//				n_r = right + shift;
//				L( "NOR\t%d\t.\t%d\t%d\t%d\t%d\t%d\t%d\n", num, left, right, right-left+1, shift, n_l, n_r);
//				for( j =left; j <=right; j++)
//				{
//					fputc( chr_it->second[j], out_fp);
//					pos++;
//					sanity++;
//
//					if ( 0 == pos%FASTA_LINE ){fputc('\n', out_fp); pos=0;}
//				}
//
//				num++;
//				//if ( sanity != n_r )
//				//{
//				//	L("WARNING\t%d\t%d\n", sanity, n_r);
//					//abort();
//				//}
//
//				abe_type="INV";
//				//if (1 == tmp_vec[i].event)
//				if (10 == tmp_vec[i].event)
//				{
//					abe_type="INSERT";
//					shift += diff;
//
//					if ( tmp_vec[i].e < rep_length )
//					{
//						left = tmp_vec[i].s-1;
//						right = tmp_vec[i].e-1;
//					}
//					else
//					{
//						left = tmp_vec[i].s% (rep_length - diff);
//						right = left + diff - 1;
//					}
//					L( "%s\t%d\t+\t%u\t%u\t%d\t%d\t%s\t%d\t%d\n", abe_type.c_str(), num, tmp_vec[i].s-1, tmp_vec[i].e-1, diff, shift,  rep_chr.c_str(), left, right);
//
//					for( j =left; j <=right; j++)
//					{
//						fputc( rep_seq[j], out_fp);
//						pos++;
//						sanity++;
//						if ( 0 == pos%FASTA_LINE ){fputc('\n', out_fp); pos=0;}
//					}
//
//					// Insertion is NOT only replacement!
//					for( j =left; j <=right; j++)
//					{
//						fputc( chr_it->second[j], out_fp);
//						pos++;
//						sanity++;
//
//						if ( 0 == pos%FASTA_LINE ){fputc('\n', out_fp); pos=0;}
//					}
//
//
//				}
//				//else if (2 == tmp_vec[i].event)
//				else if (11 == tmp_vec[i].event)
//				{
//					abe_type="DEL";
//					shift -= diff;
//					L( "%s\t%d\t-\t%u\t%u\t%d\t%d\n", abe_type.c_str(), num, tmp_vec[i].s-1, tmp_vec[i].e-1, diff, shift );
//				}
//				else
//				{
//
//					//L("==%s\t%u\t%u\n", abe_type.c_str(), tmp_vec[i].s, tmp_vec[i].e);
//					//L( "%s\t%d\t.\t%u\t%u\n", abe_type.c_str(), num, tmp_vec[i].s-1, tmp_vec[i].e-1 );
//					L( "%s\t%d\t!\t%u\t%u\t%d\t%d\n", abe_type.c_str(), num, tmp_vec[i].s-1, tmp_vec[i].e-1, diff, shift );
//
//					// Output Reverse Complement Here
//					left = (int)tmp_vec[i].s-1;
//					right = (int)tmp_vec[i].e-1;
//					for(int j =right; j >=left; j--)
//					{
//						switch( chr_it->second[j])
//						{
//							case 'A':
//								fputc( 'T', out_fp);
//								break;
//							case 'a':
//								fputc( 'T', out_fp);
//								break;
//							case 'C':
//								fputc( 'G', out_fp);
//								break;
//							case 'c':
//								fputc( 'G', out_fp);
//								break;
//							case 'G':
//								fputc( 'C', out_fp);
//								break;
//							case 'g':
//								fputc( 'C', out_fp);
//								break;
//							case 'T':
//								fputc( 'A', out_fp);
//								break;
//							case 't':
//								fputc( 'A', out_fp);
//								break;
//							default:
//								fputc( 'N', out_fp);
//								break;
//
//						}
//						pos++;
//						sanity++;
//						if ( 0 == pos%FASTA_LINE ){fputc('\n', out_fp); pos=0;}
//					}
//
//				}
//				left = (int)tmp_vec[i].e; // 0-based for the next leftmost position
//				num++;
//			}
//			right = r_length-1;
//			//L( "<<\t%d\t+\t%d\t%d\n", num, left, right);
//			n_l = left + shift;
//			n_r = right + shift;
//			L( "NOR\t%d\t.\t%d\t%d\t%d\t%d\t%d\t%d\n", num, left, right, right-left+1, shift, n_l, n_r);
//			for( j =left; j <=right; j++)
//			{
//				fputc( chr_it->second[j], out_fp);
//				pos++;
//				sanity++;
//				if ( 0 == pos%FASTA_LINE ){fputc('\n', out_fp); pos=0;}
//			}
//
//			flag_map[ ref ] = 1;
//			//if (sanity != r_length){E("Incon\t%d\t%d", sanity, r_length);}
//			if (sanity != n_r+1){E("Incon\t%d\t%d\n", sanity, n_r+1);}
//			L("SANITY\t%d\t%d\n", sanity, n_r+1);
//		}
//	}
//
//	for( it = sort_record.begin(); it != sort_record.end(); it++)
//	{
//		if ( 0 == flag_map[it->first] ) // to make sure two genomes are of equal length
//		{	E("Error: Sequences %s not found in refenrece genome!\n", it->first.c_str() );	}
//	}
//
//	fclose(out_fp);
//}
///**********************************************/
//void GetInvFastq( char *predict_file , map<string, string > &chr_map )
//{
//	map<string, string>::iterator chr_it;
//	int flank = 10;
//	//string total_seq="", total_rc="";
//
//	char *readline=(char*)malloc(MAX_LINE);
//	//char *header=(char*)malloc(TOKEN_LENGTH);
//	char *misc=(char*)malloc(TOKEN_LENGTH);
//	char *ref=(char*)malloc(TOKEN_LENGTH);
//	char *fea=(char*)malloc(TOKEN_LENGTH);
//	char *out_name=(char*)malloc(TOKEN_LENGTH);
//
//	char header;
//	int count = 0;
//	int id = 0, support = 0,
//		offset =0;
//	uint32_t start = 0, end = 0;
//	int left =0, right = 0;
//	//float g1 = 0.0, g2 = 0.0, g3 = 0.0;
//	unit_abe tmp_abe;
//	//map< string, vector<unit_pred> > map_record;
//	FILE *fp = fopen( predict_file, "r");
//	//FILE *out_fp= fopen( out_file, "w");
//	int cur = 0, cur_gene = 0;
//	while( NULL != fgets(readline, MAX_LINE, fp) )
//	{
//		sscanf( readline, "%d %s %u %u %n\n",
//			&id, ref,
//			&start, &end, &offset);
//
//		chr_it = chr_map.find( string(ref) );
//		if ( chr_map.end() == chr_it )
//		{
//			L("Reference Missing %s\n", ref);
//		}
//		else
//		{
//			// Inversion Only
//			//strcpy(id, out_name);
//			out_name[0]='\0';
//			sprintf(out_name, "%d", id);
//			strcat(out_name, ".fq");
//			FILE *out_fp = fopen( out_name, "w");
//			fprintf(out_fp, "@%d_%u_%u\n", id, start, end);
//			left = (int)start - 1;
//			right = (int)end -1;
//			for(int i =left; i <= right; i++)
//			{ fputc( chr_it->second[i], out_fp);}
//			fprintf( out_fp, "\n+\n");
//			for(int i =left; i <= right; i++)
//			{ fputc( 'H', out_fp);}
//			fputc( '\n', out_fp);
//			fclose(out_fp);
//			// Inversion + Flanking 10
//			out_name[0]='\0';
//			sprintf(out_name, "%d", id);
//			//strcpy(id, out_name);
//			strcat(out_name, ".fastq");
//			FILE *flank_fp = fopen( out_name, "w");
//			fprintf(flank_fp, "@%d_%u_%u\n", id, start-flank, end + flank);
//			left = (int)start - flank -1;
//			right = (int)end + flank -1;
//			for(int i =left; i <= right; i++)
//			{ fputc( chr_it->second[i], flank_fp);}
//			fprintf( flank_fp, "\n+\n");
//			for(int i =left; i <= right; i++)
//			{ fputc( 'H', flank_fp);}
//			fputc( '\n', flank_fp);
//			fclose(flank_fp);
//			count++;
//		}
//
//	}
//
//	E("Total Prediction Record\t%d\n", count);
//	//E("Total References with Predictions:\t%d\n", (int)map_record.size());
//	//fclose(out_fp);
//	fclose(fp);
//
//
//	free(out_name);
//	free(fea);
//	free(ref);
//	//free(misc);
//	free(readline);
//}
///**********************************************/
//void OutputSingleFasta( map<string, string > &chr_map, char *outfile, int max_line )
//{
//	map<string, string>::iterator chr_it;
//	//int flank = 10;
//	//string total_seq="", total_rc="";
//
//	char *readline=(char*)malloc(MAX_LINE);
//	//char *header=(char*)malloc(TOKEN_LENGTH);
//	//char *misc=(char*)malloc(TOKEN_LENGTH);
//	//char *ref=(char*)malloc(TOKEN_LENGTH);
//	//char *fea=(char*)malloc(TOKEN_LENGTH);
//	//char *out_name=(char*)malloc(TOKEN_LENGTH);
//
//	//int left = 0,right =0;
//	int limit = 0;
//	//int num_line = 0;
//	int num_char = 0;
//	int i =0;
//	FILE *out_fp = fopen( outfile, "w");
//	for( chr_it = chr_map.begin(); chr_it != chr_map.end(); chr_it++)
//	{
//		limit = (int)chr_it->second.size();
//		fprintf(out_fp, ">%s\n", chr_it->first.c_str());
//		//right = left + max_line -1;
//
//		//while( left < limit )
//		//{
//		//	right = left + max_line -1;
//		//
//		//	if ( right > limit -1)
//		//	{
//		//		right = limit - 1;
//		//	}
//		//
//		//	for( int i = left; i <= right; i++)
//		//	{
//		//		fputc( chr_it->second[i], out_fp );
//		//	}
//		//
//		//	fputc( '\n', out_fp );
//		//	left += max_line;
//		//}
//
//		// We need to do the next iteration
//
//		for( i = 0; i < limit; i++)
//		{
//			if ( (0 == i%max_line) && (0 < i))
//			{
//				fputc( '\n', out_fp );
//			}
//			fputc( chr_it->second[i], out_fp );
//		}
//		if ( max_line-1 != i%max_line)
//		{
//				fputc( '\n', out_fp );
//		}
//	}
//
//
//	//E("Total Prediction Record\t%d\n", count);
//	//E("Total References with Predictions:\t%d\n", (int)map_record.size());
//	fclose(out_fp);
//	//fclose(fp);
//
//
//	//free(out_name);
//	//free(fea);
//	//free(ref);
//	//free(misc);
//	free(readline);
//}
///**********************************************/
//void GetInvSingleFasta( char *predict_file , map<string, string > &chr_map, char *out_file, int flank )
//{
//	map<string, string>::iterator chr_it;
//	//int flank = 10;
//	//string total_seq="", total_rc="";
//
//	char *readline=(char*)malloc(MAX_LINE);
//	//char *header=(char*)malloc(TOKEN_LENGTH);
//	char *misc=(char*)malloc(TOKEN_LENGTH);
//	char *ref=(char*)malloc(TOKEN_LENGTH);
//	char *fea=(char*)malloc(TOKEN_LENGTH);
//	char *out_name=(char*)malloc(TOKEN_LENGTH);
//
//	char header;
//	int count = 0;
//	int id = 0, support = 0,
//		offset =0;
//	uint32_t start = 0, end = 0;
//	int left =0, right = 0;
//	//float g1 = 0.0, g2 = 0.0, g3 = 0.0;
//	unit_abe tmp_abe;
//	//map< string, vector<unit_pred> > map_record;
//	FILE *fp = fopen( predict_file, "r");
//	FILE *out_fp= fopen( out_file, "w");
//	int cur = 0, cur_gene = 0;
//	while( NULL != fgets(readline, MAX_LINE, fp) )
//	{
//		sscanf( readline, "%d %s %u %u %n\n",
//			&id, ref,
//			&start, &end, &offset);
//
//		chr_it = chr_map.find( string(ref) );
//		if ( chr_map.end() == chr_it )
//		{
//			L("Reference Missing %s\n", ref);
//		}
//		else
//		{
//			// Inversion Only
//			//strcpy(id, out_name);
//			fprintf( out_fp, ">%d_%u_%u\n", id, start-flank, end + flank);
//			left = (int)start - flank -1;
//			right = (int)end + flank -1;
//			for(int i =left; i <= right; i++)
//			{ fputc( chr_it->second[i], out_fp);}
//			fputc( '\n', out_fp);
//			count++;
//		}
//
//	}
//
//	E("Total Prediction Record\t%d\n", count);
//	//E("Total References with Predictions:\t%d\n", (int)map_record.size());
//	fclose(out_fp);
//	fclose(fp);
//
//
//	free(out_name);
//	free(fea);
//	free(ref);
//	//free(misc);
//	free(readline);
//}
///**********************************************/
//void OutputPredictFastq( char *predict_file , map<string, string > &chr_map )
//{
//	map<string, string>::iterator chr_it;
//
//	char *readline=(char*)malloc(MAX_LINE);
//	//char *header=(char*)malloc(TOKEN_LENGTH);
//	char *misc=(char*)malloc(TOKEN_LENGTH);
//	char *ref=(char*)malloc(TOKEN_LENGTH);
//	char *fea1=(char*)malloc(TOKEN_LENGTH);
//	char *fea2=(char*)malloc(TOKEN_LENGTH);
//	char *out_name=(char*)malloc(TOKEN_LENGTH);
//
//	char header;
//	int count = 0;
//	int id = 0, support = 0,
//		offset =0;
//	uint32_t start = 0, end = 0;
//	int left =0, right = 0;
//	float g1 = 0.0, g2 = 0.0, g3 = 0.0;
//	unit_pred tmp_pred;
//	//map< string, vector<unit_pred> > map_record;
//	FILE *fp = fopen( predict_file, "r");
//	//FILE *out_fp= fopen( out_file, "w");
//	int cur = 0, cur_gene = 0;
//	while( NULL != fgets(readline, MAX_LINE, fp) )
//	{
//		if ( 0 != strncmp(">", readline, 1) )
//		{
//			continue;
//		}
//		sscanf( readline, "%c %d %d %s %d %d %f %f %f %s %s %n\n",
//			&header, &id, &support, ref,
//			&start, &end, &g1, &g2, &g3,
//			fea1, fea2, &offset);
//		chr_it = chr_map.find( string(ref).substr(3) );
//		if ( chr_map.end() == chr_it )
//		{
//			L("Reference Missing %s\n", ref);
//		}
//		else
//		{
//			// Inversion Only
//			//strcpy(id, out_name);
//			out_name[0]='\0';
//			sprintf(out_name, "%d_%d", id, start);
//			strcat(out_name, ".fq");
//			FILE *out_fp = fopen( out_name, "w");
//			fprintf(out_fp, "@%d_%u_%u\n", id, start, end);
//			left = (int)start; // since trevnI use bed-style system
//			right = (int)end -1;
//			for(int i =left; i <= right; i++)
//			{ fputc( chr_it->second[i], out_fp);}
//			fprintf( out_fp, "\n+\n");
//			for(int i =left; i <= right; i++)
//			{ fputc( 'H', out_fp);}
//			fputc( '\n', out_fp);
//			fclose(out_fp);
//			//count++;
//		}
//
//		count++;
//
//	}
//
//	E("Total Prediction Record\t%d\n", count);
//	//E("Total References with Predictions:\t%d\n", (int)map_record.size());
//	//fclose(out_fp);
//	fclose(fp);
//
//	free(out_name);
//	free(fea2);
//	free(fea1);
//	free(ref);
//	free(misc);
//	//free(header);
//	free(readline);
//}
///**********************************************/
//void OutputSplitSeq( char *pred_file, char *out_file  )
//{
//
//	vector<string> token_array, fusion_array;
//
//	char *readline=(char*)malloc(MAX_LINE);
//
//	FILE *fp = fopen( pred_file, "r");
//	FILE *out_fp= fopen( out_file, "w");
//	while( NULL != fgets(readline, MAX_LINE, fp) )
//	{
//
//		if( 0 == strncmp("cluster", readline, 7 ) ) // header line in tsv file
//		{
//			continue;
//		}
//
//		token_array = splitString( readline, '\t' );
//		fusion_array = splitStringOld( token_array[1], '|');
//		fprintf(out_fp, ">%s\n", token_array[0].c_str());
//		fprintf(out_fp, "%s%s\n", fusion_array[0].c_str(), fusion_array[1].c_str() );
//	}
//	fclose(fp);
//	fclose(out_fp);
//}
///**********************************************/
//void ExtractMappings( char *fastq_file, char *gene_file, char *out_file  )
//{
//	string rid="";
//
//	map<string, int> read_dict;
//
//	char *readline=(char*)malloc(MAX_LINE);
//	E("Collecting Read Names\n");
//	vector<string> tmp_list;
//	FILE *fp = fopen( gene_file, "r");
//	while( NULL != fgets(readline, MAX_LINE, fp) )
//	{
//		tmp_list= splitString(readline, '\t');
//		read_dict[ tmp_list[0] ] = 1;
//	}
//	fclose(fp);
//
//	int flag = 0; // if we should print the remaining three lines
//	map<string, int>::iterator it;
//
//	E("Extracting Reads\n");
//	int count = 0;
//	FILE *fastq_fp = fopen( fastq_file, "r");
//	FILE *out_fp= fopen( out_file, "w");
//	while( NULL != fgets(readline, MAX_LINE, fastq_fp) )
//	{
//		tmp_list = splitString(readline, '\t');
//		rid = tmp_list[0];
//		it = read_dict.find(rid);
//		if (it != read_dict.end() )
//		{
//			fprintf( out_fp, "%s", readline);
//		}
//		count++;
//		if (0 == count%1000000){E(".");}
//	}
//	E(".\n");
//	fclose(fastq_fp);
//	fclose(out_fp);
//}
///**********************************************/
//void SelectMappings( char *fastq_file, char *gene_file, char *out_file  )
//{
//	string rid="";
//
//	map<string, int> read_dict;
//
//	char *readline=(char*)malloc(MAX_LINE);
//	E("Collecting Read Names\n");
//	vector<string> tmp_list;
//	FILE *fp = fopen( gene_file, "r");
//	while( NULL != fgets(readline, MAX_LINE, fp) )
//	{
//		tmp_list= splitString(readline, '\t');
//		read_dict[ tmp_list[0] ] = 1;
//		//E("%d\n", (int)tmp_list.size() );
//		//E("%s_%s_%s\n", tmp_list[0], tmp_list[1], tmp_list[1].substr(1));
//		//read_dict[ tmp_list[1].substr(1) ] = 1;
//	}
//	fclose(fp);
//
//
//	int flag = 0; // if we should print the remaining three lines
//	map<string, int>::iterator it;
//
//	E("Extracting Reads\n");
//	int count = 0;
//	FILE *fastq_fp = fopen( fastq_file, "r");
//	FILE *out_fp= fopen( out_file, "w");
//	while( NULL != fgets(readline, MAX_LINE, fastq_fp) )
//	{
//		tmp_list = splitString(readline, '\t');
//		rid = tmp_list[0];
//		it = read_dict.find(rid);
//		if (it != read_dict.end() )
//		{
//			fprintf( out_fp, "%s", readline);
//		}
//		count++;
//		if (0 == count%1000000){E(".");}
//	}
//	E(".\n");
//	fclose(fastq_fp);
//	fclose(out_fp);
//}
///**********************************************/
//void ClassifyMappings( char *sam_file  )
//{
//	string rid="";
//
//	map<string, int> read_dict;
//
//	char *readline=(char*)malloc(MAX_LINE);
//	char *readid=(char*)malloc(MAX_LINE);
//	uint32_t flag;
//	int offset;
//
//	E("Parsing SAM File\n");
//	//vector<string> tmp_list;
//	FILE *con_fp = fopen( "concord.read", "w");
//	FILE *dis_fp = fopen( "discon.read", "w");
//	FILE *oea_fp = fopen( "oea.read", "w");
//	//FILE *fp = fopen( sam_file, "r");
//	FILE *fp;// = fopen( sam_file, "r");
//	if ( '\0' == sam_file[0] )
//	{
//		fp = stdin;
//	}
//	else
//	{
//		fp = fopen(sam_file, "r");
//	}
//	while( NULL != fgets(readline, MAX_LINE, fp) )
//	{
//		sscanf( readline, "%s %u %n", readid, &flag, &offset);
//		// If a concordant mappings
//		if ( ( flag & 0x4 )
//			|| ( flag & 0x8)
//			)
//		{
//			fprintf(oea_fp, "%s", readline );
//		}
//		else if ( flag & 0x2 )
//		{
//			fprintf(con_fp, "%s", readline );
//		}
//		else
//		{
//			fprintf(dis_fp, "%s",readline );
//		}
//	}
//	fclose(fp);
//	fclose(con_fp);
//	fclose(dis_fp);
//	fclose(oea_fp);
//
//
//	//E("Extracting Reads\n");
//	//int count = 0;
//	//FILE *fastq_fp = fopen( fastq_file, "r");
//	//FILE *out_fp= fopen( out_file, "w");
//	//while( NULL != fgets(readline, MAX_LINE, fastq_fp) )
//	//{
//	//	tmp_list = splitString(readline, '\t');
//	//	rid = tmp_list[0];
//	//	it = read_dict.find(rid);
//	//	if (it != read_dict.end() )
//	//	{
//	//		fprintf( out_fp, readline);
//	//	}
//	//	count++;
//	//	if (0 == count%1000000){E(".");}
//	//}
//	//E(".\n");
//	//fclose(fastq_fp);
//	//fclose(out_fp);
//}
///**********************************************/
//void ClassifyBadMappings( char *sam_file  )
//{
//	string rid="";
//
//	map<string, int> read_dict;
//
//	char *readline=(char*)malloc(MAX_LINE);
//	char *readid=(char*)malloc(MAX_LINE);
//	uint32_t flag;
//	int offset;
//
//	E("Parsing SAM File\n");
//	//vector<string> tmp_list;
//	FILE *con_fp = fopen( "concord.read", "w");
//	FILE *dis_fp = fopen( "discon.read", "w");
//	FILE *oea_fp = fopen( "oea.read", "w");
//	FILE *unmap_fp = fopen( "unmapped.read", "w");
//	//FILE *fp = fopen( sam_file, "r");
//	FILE *fp;// = fopen( sam_file, "r");
//	if ( '\0' == sam_file[0] )
//	{
//		fp = stdin;
//	}
//	else
//	{
//		fp = fopen(sam_file, "r");
//	}
//	while( NULL != fgets(readline, MAX_LINE, fp) )
//	{
//		sscanf( readline, "%s %u %n", readid, &flag, &offset);
//		// If a concordant mappings
//		if ( ( flag & 0x4 ) && ( flag & 0x8) )
//		{
//			fprintf(unmap_fp, "%s", readline );
//		}
//		else if ( ( flag & 0x4 )
//			|| ( flag & 0x8)
//			)
//		{
//			fprintf(oea_fp, "%s", readline );
//		}
//		else if ( flag & 0x2 )
//		{
//			fprintf(con_fp, "%s", readline );
//		}
//		else
//		{
//			fprintf(dis_fp, "%s", readline );
//		}
//	}
//	fclose(fp);
//	fclose(con_fp);
//	fclose(dis_fp);
//	fclose(oea_fp);
//	fclose(unmap_fp);
//
//
//	//E("Extracting Reads\n");
//	//int count = 0;
//	//FILE *fastq_fp = fopen( fastq_file, "r");
//	//FILE *out_fp= fopen( out_file, "w");
//	//while( NULL != fgets(readline, MAX_LINE, fastq_fp) )
//	//{
//	//	tmp_list = splitString(readline, '\t');
//	//	rid = tmp_list[0];
//	//	it = read_dict.find(rid);
//	//	if (it != read_dict.end() )
//	//	{
//	//		fprintf( out_fp, readline);
//	//	}
//	//	count++;
//	//	if (0 == count%1000000){E(".");}
//	//}
//	//E(".\n");
//	//fclose(fastq_fp);
//	//fclose(out_fp);
//}
//
///**********************************************/
//void CountBadMappings( char *sam_file  )
//{
//	map<string, int> read_dict;
//
//	char *readline=(char*)malloc(MAX_LINE);
//	char *readid=(char*)malloc(MAX_LINE);
//	uint32_t flag;
//	int offset;
//
//	uint32_t count=0;
//	uint32_t n_con = 0;
//	uint32_t n_dis = 0;
//	uint32_t n_oea = 0;
//	uint32_t n_un =0;
//	uint32_t sum=0;
//
//	E("Parsing SAM File\n");
//	//vector<string> tmp_list;
//	FILE *con_fp = fopen( "concord.read", "w");
//	FILE *dis_fp = fopen( "discon.read", "w");
//	FILE *oea_fp = fopen( "oea.read", "w");
//	FILE *unmap_fp = fopen( "unmapped.read", "w");
//	//FILE *fp = fopen( sam_file, "r");
//	FILE *fp;// = fopen( sam_file, "r");
//	if ( '\0' == sam_file[0] )
//	{
//		fp = stdin;
//	}
//	else
//	{
//		fp = fopen(sam_file, "r");
//	}
//	while( NULL != fgets(readline, MAX_LINE, fp) )
//	{
//		sscanf( readline, "%s %u %n", readid, &flag, &offset);
//		// If a concordant mappings
//		if ( ( flag & 0x4 ) && ( flag & 0x8) )
//		{
//			fprintf(unmap_fp, "%s", readline );
//			n_un++;
//			sum++;
//		}
//		else if ( ( flag & 0x4 )
//			|| ( flag & 0x8)
//			)
//		{
//			fprintf(oea_fp, "%s", readline );
//			n_oea++;
//			sum++;
//		}
//		else if ( flag & 0x2 )
//		{
//			fprintf(con_fp, "%s", readline );
//			n_con++;
//			sum++;
//		}
//		else
//		{
//			fprintf(dis_fp, "%s", readline );
//			n_dis++;
//			sum++;
//		}
//		count++;
//
//		if ( 0 == count%10000000)
//		{
//
//			E("Check: %u\t%u\t%u\t%u\t%u\t%u\n", sum-count, count, n_con, n_un, n_oea, n_dis);
//		}
//	}
//	fclose(fp);
//	fclose(con_fp);
//	fclose(dis_fp);
//	fclose(oea_fp);
//	fclose(unmap_fp);
//
//
//}
///**********************************************/
//void CountMappings( char *sam_file  )
//{
//	char *readline=(char*)malloc(MAX_LINE);
//	//char *readid=(char*)malloc(MAX_LINE);
//	//uint32_t flag;
//	//int offset;
//
//	uint32_t count=0;
//	//uint32_t n_con = 0;
//	//uint32_t n_dis = 0;
//	//uint32_t n_oea = 0;
//	//uint32_t n_un =0;
//	//uint32_t sum=0;
//
//	E("Parsing SAM File\n");
//	//vector<string> tmp_list;
//	//FILE *con_fp = fopen( "concord.read", "w");
//	//FILE *dis_fp = fopen( "discon.read", "w");
//	//FILE *oea_fp = fopen( "oea.read", "w");
//	//FILE *unmap_fp = fopen( "unmapped.read", "w");
//	FILE *fp;// = fopen( sam_file, "r");
//	if ( '\0' == sam_file[0] )
//	{
//		fp = stdin;
//	}
//	else
//	{
//		fp = fopen(sam_file, "r");
//	}
//
//
//	while( NULL != fgets(readline, MAX_LINE, fp) )
//	{
//		//sscanf( readline, "%s %u %n", readid, &flag, &offset);
//		//// If a concordant mappings
//		//if ( flag & 0x2 )
//		//{
//		//	fprintf(con_fp, readline );
//		//	n_con++;
//		//	sum++;
//		//}
//		//else if ( ( flag & 0x4 ) && ( flag & 0x8) )
//		//{
//		//	fprintf(unmap_fp, readline );
//		//	n_un++;
//		//	sum++;
//		//}
//		//else if ( ( flag & 0x4 )
//		//	|| ( flag & 0x8)
//		//	)
//		//{
//		//	fprintf(oea_fp, readline );
//		//	n_oea++;
//		//	sum++;
//		//}
//		//else
//		//{
//		//	fprintf(dis_fp, readline );
//		//	n_dis++;
//		//	sum++;
//		//}
//		count++;
//
//		if ( 0 == count%10000000)
//		{
//
//			E("Check: %u\t%s\n", count, readline );
//		}
//	}
//	fclose(fp);
//	//fclose(con_fp);
//	//fclose(dis_fp);
//	//fclose(oea_fp);
//	//fclose(unmap_fp);
//
//
//}
///**********************************************/
//int calculate_ml( char *cigar)
//{
//	int length = 0;
//	int tmp = 0;
//	if (strcmp(cigar, "*") == 0)
//		{	return 0;	}
//
//	while( *cigar )
//	{
//		if (isdigit(*cigar))
//			{ tmp = 10 * tmp + (*cigar - '0');}
//		else
//		{
//			if ( 'M' == *cigar )
//			{
//				length += tmp;
//			}
//			else if ('I' == *cigar )
//			{
//				length += tmp;
//			}
//			else if ('S' ==*cigar 	)
//			{
//				length += tmp;
//			}
//			else if ('H' ==*cigar 	)
//			{
//				length += tmp;
//			}
//			else if ('=' ==*cigar 	)
//			{
//				length += tmp;
//			}
//			else if ('X' ==*cigar 	)
//			{
//				length += tmp;
//			}
//			tmp = 0;
//		}
//		cigar++;
//	}
//
//	return length;
//}
///**********************************************/
//void GetFastqFromSam( char *sam_file, char *output  )
//{
//	string rid="";
//
//	map<string, int> read_dict;
//
//	char *readline=(char*)malloc(MAX_LINE);
//	char *readid=(char*)malloc(MAX_LINE);
//	char *prev_id=(char*)malloc(MAX_LINE);
//	char *refid=(char*)malloc(MAX_LINE);
//	char *cigar=(char*)malloc(MAX_LINE);
//	char *rnext=(char*)malloc(MAX_LINE);
//	char *seq=(char*)malloc(MAX_LINE);
//	char *qual=(char*)malloc(MAX_LINE);
//	uint32_t flag, pos, mapq, pnext, tlen;
//	int offset;
//
//	// Read
//	char *r1=(char*)malloc(MAX_LINE);
//	char *r2=(char*)malloc(MAX_LINE);
//	char *r3=(char*)malloc(MAX_LINE);
//	char *q1=(char*)malloc(MAX_LINE);
//	char *q2=(char*)malloc(MAX_LINE);
//	char *q3=(char*)malloc(MAX_LINE);
//
//	int fi_size = -1, se_size = -1;
//	int sin_size = -1;
//
//	// Output File Name
//	char *fastq1=(char*)malloc(MAX_LINE);
//	char *fastq2=(char*)malloc(MAX_LINE);
//	char *fastq_misc=(char*)malloc(MAX_LINE);
//	char *fastq_single=(char*)malloc(MAX_LINE);
//	strcpy(fastq1, output);
//	strcpy(fastq2, output);
//	strcpy(fastq_misc, output);
//	strcpy(fastq_single, output);
//	strcat(fastq1, "_1.fastq");
//	strcat(fastq2, "_2.fastq");
//	strcat(fastq_misc, ".clipped");
//	strcat(fastq_single, ".single");
//
//	uint32_t num_read = 0, num_clip = 0,
//		num_what = 0, num_what2 =0;
//
//	E("Parsing SAM File\n");
//	//vector<string> tmp_list;
//	FILE *first_fp = fopen( fastq1, "w");
//	FILE *second_fp = fopen( fastq2, "w");
//	FILE *misc_fp = fopen( fastq_misc, "w");
//	FILE *single_fp = fopen( fastq_single, "w");
//	//FILE *fp = fopen( sam_file, "r");
//	FILE *fp;// = fopen( sam_file, "r");
//	if ( '\0' == sam_file[0] )
//	{
//		fp = stdin;
//	}
//	else
//	{
//		fp = fopen(sam_file, "r");
//	}
//	uint32_t count= 0;
//	//int fir_read=0, se_flag= 0; // if we obtain the first/second mate of a pair
//	int fi_l = 0, se_l =0;
//	int sin_l = 0;
//	int seq_length = 0;
//	while( NULL != fgets(readline, MAX_LINE, fp) )
//	{
//		if ( 0 == strncmp( readline, "@", 1) )
//		{
//			continue;
//		}
//
//		sscanf( readline, "%s %u %s %u %u %s %s %u %u %s %s %n", readid, &flag,
//			refid, &pos, &mapq, cigar, rnext, &pnext, &tlen, seq, qual,
//		 	&offset);
//		//E("Sequence Length:%d \n", mate_length);
//		//break;
//
//		// Output Pairs
//		if ( 0 != strncmp( prev_id, readid, MAX_LINE ) )
//		{
//			if ( ( (fi_size == fi_l)) && (se_size == se_l) )
//			{
//				fprintf(first_fp, "@%s/1\n%s\n+\n%s\n", prev_id, r1, q1);
//				fprintf(second_fp, "@%s/2\n%s\n+\n%s\n", prev_id, r2, q2);
//				num_read++;
//			}
//			else
//			{
//				if ( 0 < fi_l)
//				{fprintf(misc_fp, "@%s/1\n%s\n+\n%s\n", prev_id, r1, q1);}
//				if ( 0 < se_l)
//				{fprintf(misc_fp, "@%s/2\n%s\n+\n%s\n", prev_id, r2, q2 );}
//				if ( 0 == fi_l +se_l ) // single
//				{
//					if (  sin_l == sin_size  )
//					{
//						fprintf(single_fp, "@%s\n%s\n+\n%s\n", prev_id, r1, q1 );
//					}
//					else if ( 0 < sin_l )
//					{
//						fprintf(misc_fp, "@%s\n%s\n+\n%s\n", prev_id, r1, q1 );
//						num_what2++;
//					}
//					num_what++;
//				}
//				num_clip++;
//			}
//			copy_string(readid, prev_id);
//			fi_l=0;
//			se_l=0;
//			sin_l = 0;
//		}
//		// Start New Pairs
//		seq_length =(int)strlen( seq );
//		if ( flag & 0x40 ) // first read
//		{
//			// otaining the length of the first mate
//			if ( 0 > fi_size )
//			{
//				fi_size = calculate_ml( cigar );
//				if ( 0 == fi_size )
//				{
//					fi_size = seq_length;
//				}
//				E("First Mate Length:%d \n", fi_size);
//			}
//			// Update the read content when needed
//			if ( fi_l < seq_length  ) // get a longer read
//			{
//				if ( flag & 0x10 ) // reverse complement
//				{
//					copy_string_rc( seq, r1);
//					copy_string_reverse( qual, q1);
//				}
//				else
//				{
//					copy_string( seq, r1);
//					copy_string( qual, q1);
//				}
//				fi_l = seq_length;
//			}
//		}
//		else if ( flag & 0x80 )
//		{
//			// otaining the length of the second mate
//			if ( 0 > se_size )
//			{
//				se_size = calculate_ml( cigar );
//				if ( 0 == se_size )
//				{
//					se_size = seq_length;
//				}
//				E("Second Mate Length:%d \n", se_size);
//			}
//			// Update the read content when needed
//			if ( se_l < seq_length  ) // get a longer read
//			{
//				if ( flag & 0x10 ) // reverse complement
//				{
//					copy_string_rc( seq, r2);
//					copy_string_reverse( qual, q2);
//				}
//				else
//				{
//					copy_string( seq, r2);
//					copy_string( qual, q2);
//				}
//				se_l = seq_length;
//			}
//		}
//		else // some A Hole mix single and paired together
//		{
//			// otaining the length of single end
//			if ( 0 > sin_size )
//			{
//				sin_size = calculate_ml( cigar );
//				if ( 0 == sin_size )
//				{
//					sin_size = seq_length;
//				}
//				E("\nSingle Mate Length:%d \n", sin_size);
//			}
//
//			if (sin_l < seq_length )
//			{
//				if ( flag & 0x10 ) // reverse complement
//				{
//					copy_string_rc( seq, r3);
//					copy_string_reverse( qual, q3);
//				}
//				else
//				{
//					copy_string( seq, r3);
//					copy_string( qual, q3);
//				}
//				sin_l = seq_length;
//			}
//		}
//
//		count++;
//		if (0 ==  count%10000000){E(".");}
//	}
//	// Output Last Pair
//	if ( ( (fi_size == fi_l)) && (se_size == se_l) )
//	{
//		fprintf(first_fp, "@%s/1\n%s\n+\n%s\n", prev_id, r1, q1);
//		fprintf(second_fp, "@%s/2\n%s\n+\n%s\n", prev_id, r2, q2);
//		num_read++;
//	}
//	else
//	{
//		if ( 0 < fi_l)
//		{fprintf(misc_fp, "@%s/1\n%s\n+\n%s\n", prev_id, r1, q1);}
//		if ( 0 < se_l)
//		{fprintf(misc_fp, "@%s/2\n%s\n+\n%s\n", prev_id, r2, q2 );}
//		//if ( 0 == fi_l + se_l ){ num_what++; }
//		if ( 0 == fi_l +se_l ) // single
//		{
//			if (  sin_l == sin_size  )
//			{
//				fprintf(single_fp, "@%s\n%s\n+\n%s\n", prev_id, r3, q3 );
//			}
//			else
//			{
//				fprintf(misc_fp, "@%s\n%s\n+\n%s\n", prev_id, r3, q3 );
//				num_what2++;
//			}
//			num_what++;
//		}
//		num_clip++;
//	}
//
//	E("\n");
//	E("Summary:\t%u lines parsed\n", count);
//	E("Summary:\t%u proper pairs extracted;\t%u bad ones\n", num_read, num_clip-1);
//	E("Summary:\t%u single read extracted;\t%u clipped ones\n", num_what-1, num_what2);
//	//if ( num_what != num_what2+1 )
//	//{
//	//	E("Inconsistent single end read: %u %u\n", num_what - 1, num_what2 );
//	//}
//	// since the last two variables are both incremented in the first line
//	fclose(fp);
//	fclose(single_fp);
//	fclose(misc_fp);
//	fclose(first_fp);
//	fclose(second_fp);
//
//}
//
///**********************************************/
//void GetFastqFromSam_STDOUT( char *sam_file, char *output  )
//{
//	string rid="";
//
//	map<string, int> read_dict;
//
//	char *readline=(char*)malloc(MAX_LINE);
//	char *readid=(char*)malloc(MAX_LINE);
//	char *prev_id=(char*)malloc(MAX_LINE);
//	char *refid=(char*)malloc(MAX_LINE);
//	char *cigar=(char*)malloc(MAX_LINE);
//	char *rnext=(char*)malloc(MAX_LINE);
//	char *seq=(char*)malloc(MAX_LINE);
//	char *qual=(char*)malloc(MAX_LINE);
//	uint32_t flag, pos, mapq, pnext, tlen;
//	int offset;
//
//	// Read
//	char *r1=(char*)malloc(MAX_LINE);
//	char *r2=(char*)malloc(MAX_LINE);
//	char *r3=(char*)malloc(MAX_LINE);
//	char *q1=(char*)malloc(MAX_LINE);
//	char *q2=(char*)malloc(MAX_LINE);
//	char *q3=(char*)malloc(MAX_LINE);
//
//	int fi_size = -1, se_size = -1;
//	int sin_size = -1;
//
//	// Output File Name
//	//char *fastq1=(char*)malloc(MAX_LINE);
//	//char *fastq2=(char*)malloc(MAX_LINE);
//	char *fastq_misc=(char*)malloc(MAX_LINE);
//	char *fastq_single=(char*)malloc(MAX_LINE);
//	//strcpy(fastq1, output);
//	//strcpy(fastq2, output);
//	strcpy(fastq_misc, output);
//	strcpy(fastq_single, output);
//	//strcat(fastq1, "_1.fastq");
//	//strcat(fastq2, "_2.fastq");
//	strcat(fastq_misc, ".clipped");
//	strcat(fastq_single, ".single");
//
//	uint32_t num_read = 0, num_clip = 0,
//		num_what = 0, num_what2 =0;
//
//	E("Parsing SAM File\n");
//	//vector<string> tmp_list;
//	//FILE *first_fp = fopen( fastq1, "w");
//	//FILE *second_fp = fopen( fastq2, "w");
//	FILE *misc_fp = fopen( fastq_misc, "w");
//	FILE *single_fp = fopen( fastq_single, "w");
//	//FILE *fp = fopen( sam_file, "r");
//	FILE *fp;// = fopen( sam_file, "r");
//	if ( '\0' == sam_file[0] )
//	{
//		fp = stdin;
//	}
//	else
//	{
//		fp = fopen(sam_file, "r");
//	}
//	uint32_t count= 0;
//	//int fir_read=0, se_flag= 0; // if we obtain the first/second mate of a pair
//	int fi_l = 0, se_l =0;
//	int sin_l = 0;
//	int seq_length = 0;
//	while( NULL != fgets(readline, MAX_LINE, fp) )
//	{
//		if ( 0 == strncmp( readline, "@", 1) )
//		{
//			continue;
//		}
//
//		sscanf( readline, "%s %u %s %u %u %s %s %u %u %s %s %n", readid, &flag,
//			refid, &pos, &mapq, cigar, rnext, &pnext, &tlen, seq, qual,
//		 	&offset);
//		//E("Sequence Length:%d \n", mate_length);
//		//break;
//
//		// Output Pairs
//		if ( 0 != strncmp( prev_id, readid, MAX_LINE ) )
//		{
//			if ( ( (fi_size == fi_l)) && (se_size == se_l) )
//			{
//				L( "@%s/1\n%s\n+\n%s\n@%s/2\n%s\n+\n%s\n", prev_id, r1, q1, prev_id, r2, q2);
//				//fprintf(first_fp, "@%s/1\n%s\n+\n%s\n", prev_id, r1, q1);
//				//fprintf(second_fp, "@%s/2\n%s\n+\n%s\n", prev_id, r2, q2);
//				num_read++;
//			}
//			else
//			{
//				if ( 0 < fi_l)
//				{fprintf(misc_fp, "@%s/1\n%s\n+\n%s\n", prev_id, r1, q1);}
//				if ( 0 < se_l)
//				{fprintf(misc_fp, "@%s/2\n%s\n+\n%s\n", prev_id, r2, q2 );}
//				if ( 0 == fi_l +se_l ) // single
//				{
//					if (  sin_l == sin_size  )
//					{
//						fprintf(single_fp, "@%s\n%s\n+\n%s\n", prev_id, r1, q1 );
//					}
//					else if ( 0 < sin_l )
//					{
//						fprintf(misc_fp, "@%s\n%s\n+\n%s\n", prev_id, r1, q1 );
//						num_what2++;
//					}
//					num_what++;
//				}
//				num_clip++;
//			}
//			copy_string(readid, prev_id);
//			fi_l=0;
//			se_l=0;
//			sin_l = 0;
//		}
//		// Start New Pairs
//		seq_length =(int)strlen( seq );
//		if ( flag & 0x40 ) // first read
//		{
//			// otaining the length of the first mate
//			if ( 0 > fi_size )
//			{
//				fi_size = calculate_ml( cigar );
//				if ( 0 == fi_size )
//				{
//					fi_size = seq_length;
//				}
//				E("First Mate Length:%d \n", fi_size);
//			}
//			// Update the read content when needed
//			if ( fi_l < seq_length  ) // get a longer read
//			{
//				if ( flag & 0x10 ) // reverse complement
//				{
//					copy_string_rc( seq, r1);
//					copy_string_reverse( qual, q1);
//				}
//				else
//				{
//					copy_string( seq, r1);
//					copy_string( qual, q1);
//				}
//				fi_l = seq_length;
//			}
//		}
//		else if ( flag & 0x80 )
//		{
//			// otaining the length of the second mate
//			if ( 0 > se_size )
//			{
//				se_size = calculate_ml( cigar );
//				if ( 0 == se_size )
//				{
//					se_size = seq_length;
//				}
//				E("Second Mate Length:%d \n", se_size);
//			}
//			// Update the read content when needed
//			if ( se_l < seq_length  ) // get a longer read
//			{
//				if ( flag & 0x10 ) // reverse complement
//				{
//					copy_string_rc( seq, r2);
//					copy_string_reverse( qual, q2);
//				}
//				else
//				{
//					copy_string( seq, r2);
//					copy_string( qual, q2);
//				}
//				se_l = seq_length;
//			}
//		}
//		else // some A Hole mix single and paired together
//		{
//			// otaining the length of single end
//			if ( 0 > sin_size )
//			{
//				sin_size = calculate_ml( cigar );
//				if ( 0 == sin_size )
//				{
//					sin_size = seq_length;
//				}
//				E("\nSingle Mate Length:%d \n", sin_size);
//			}
//
//			if (sin_l < seq_length )
//			{
//				if ( flag & 0x10 ) // reverse complement
//				{
//					copy_string_rc( seq, r3);
//					copy_string_reverse( qual, q3);
//				}
//				else
//				{
//					copy_string( seq, r3);
//					copy_string( qual, q3);
//				}
//				sin_l = seq_length;
//			}
//		}
//
//		count++;
//		if (0 ==  count%10000000){E(".");}
//	}
//	// Output Last Pair
//	if ( ( (fi_size == fi_l)) && (se_size == se_l) )
//	{
//		L( "@%s/1\n%s\n+\n%s\n@%s/2\n%s\n+\n%s\n", prev_id, r1, q1, prev_id, r2, q2);
//		//fprintf(first_fp, "@%s/1\n%s\n+\n%s\n", prev_id, r1, q1);
//		//fprintf(second_fp, "@%s/2\n%s\n+\n%s\n", prev_id, r2, q2);
//		num_read++;
//	}
//	else
//	{
//		if ( 0 < fi_l)
//		{fprintf(misc_fp, "@%s/1\n%s\n+\n%s\n", prev_id, r1, q1 );}
//		if ( 0 < se_l)
//		{fprintf(misc_fp, "@%s/2\n%s\n+\n%s\n", prev_id, r2, q2 );}
//		//if ( 0 == fi_l + se_l ){ num_what++; }
//		if ( 0 == fi_l +se_l ) // single
//		{
//			if (  sin_l == sin_size  )
//			{
//				fprintf(single_fp, "@%s\n%s\n+\n%s\n", prev_id, r3, q3 );
//			}
//			else
//			{
//				fprintf(misc_fp, "@%s\n%s\n+\n%s\n", prev_id, r3, q3 );
//				num_what2++;
//			}
//			num_what++;
//		}
//		num_clip++;
//	}
//
//	E("\n");
//	E("Summary:\t%u lines parsed\n", count);
//	E("Summary:\t%u proper pairs extracted;\t%u bad ones\n", num_read, num_clip-1);
//	E("Summary:\t%u single read extracted;\t%u clipped ones\n", num_what-1, num_what2);
//	//if ( num_what != num_what2+1 )
//	//{
//	//	E("Inconsistent single end read: %u %u\n", num_what - 1, num_what2 );
//	//}
//	// since the last two variables are both incremented in the first line
//	fclose(fp);
//	fclose(single_fp);
//	fclose(misc_fp);
//	//fclose(first_fp);
//	//fclose(second_fp);
//
//}
///**********************************************/
//void GetFastqFromSam_Merge( char *sam_file, char *output  )
//{
//	string rid="";
//
//	map<string, int> read_dict;
//
//	char *readline=(char*)malloc(MAX_LINE);
//	char *readid=(char*)malloc(MAX_LINE);
//	char *prev_id=(char*)malloc(MAX_LINE);
//	char *refid=(char*)malloc(MAX_LINE);
//	char *cigar=(char*)malloc(MAX_LINE);
//	char *rnext=(char*)malloc(MAX_LINE);
//	char *seq=(char*)malloc(MAX_LINE);
//	char *qual=(char*)malloc(MAX_LINE);
//	uint32_t flag, pos, mapq, pnext, tlen;
//	int offset;
//
//	// Read
//	char *r1=(char*)malloc(MAX_LINE);
//	char *r2=(char*)malloc(MAX_LINE);
//	char *q1=(char*)malloc(MAX_LINE);
//	char *q2=(char*)malloc(MAX_LINE);
//
//	int fi_size = -1, se_size = -1;
//
//	// Output File Name
//	char *fastq=(char*)malloc(MAX_LINE);
//	//char *fastq1=(char*)malloc(MAX_LINE);
//	//char *fastq2=(char*)malloc(MAX_LINE);
//	char *fastq_misc=(char*)malloc(MAX_LINE);
//	strcpy(fastq, output);
//	//strcpy(fastq1, output);
//	//strcpy(fastq2, output);
//	strcpy(fastq_misc, output);
//	strcat(fastq, ".fastq");
//	//strcat(fastq1, "_1.fastq");
//	//strcat(fastq2, "_2.fastq");
//	strcat(fastq_misc, ".clipped");
//
//	uint32_t num_read = 0, num_clip = 0, num_what = 0;
//
//	E("Parsing SAM File\n");
//	//vector<string> tmp_list;
//	FILE *fq_fp = fopen( fastq, "w");
//	//FILE *first_fp = fopen( fastq1, "w");
//	//FILE *second_fp = fopen( fastq2, "w");
//	FILE *misc_fp = fopen( fastq_misc, "w");
//	//FILE *fp = fopen( sam_file, "r");
//	FILE *fp;// = fopen( sam_file, "r");
//	if ( '\0' == sam_file[0] )
//	{
//		fp = stdin;
//	}
//	else
//	{
//		fp = fopen(sam_file, "r");
//	}
//	int count=0;
//	//int fir_read=0, se_flag= 0; // if we obtain the first/second mate of a pair
//	int fi_l = 0, se_l =0;
//	int seq_length = 0;
//	while( NULL != fgets(readline, MAX_LINE, fp) )
//	{
//		if ( 0 == strncmp( readline, "@", 1) )
//		{
//			continue;
//		}
//
//		sscanf( readline, "%s %u %s %u %u %s %s %u %u %s %s %n", readid, &flag,
//			refid, &pos, &mapq, cigar, rnext, &pnext, &tlen, seq, qual,
//		 	&offset);
//		//E("Sequence Length:%d \n", mate_length);
//		//break;
//
//		// Output Pairs
//		if ( 0 != strncmp( prev_id, readid, MAX_LINE ) )
//		{
//			if ( ( (fi_size == fi_l)) && (se_size == se_l) )
//			{
//				fprintf(fq_fp, "@%s/1\n%s\n+\n%s\n@%s/2\n%s\n+\n%s\n", prev_id, r1, q1, prev_id, r2, q2);
//				num_read++;
//				//fprintf(first_fp, "@%s/1\n%s\n+\n%s\n", prev_id, r1, q1);
//				//fprintf(second_fp, "@%s/2\n%s\n+\n%s\n", prev_id, r2, q2);
//			}
//			else
//			{
//				if ( 0 < fi_l)
//				{fprintf(misc_fp, "@%s/1\n%s\n+\n%s\n", prev_id, r1, q1);}
//				if ( 0 < se_l)
//				{fprintf(misc_fp, "@%s/2\n%s\n+\n%s\n", prev_id, r2, q2 );}
//				if ( 0 == fi_l +se_l )
//				{ num_what++; }
//				num_clip++;
//			}
//			copy_string(readid, prev_id);
//			fi_l=0;
//			se_l=0;
//		}
//		// Start New Pairs
//		seq_length =(int)strlen( seq );
//		if ( flag & 0x40 ) // first read
//		{
//			// otaining the length of the first mate
//			if ( 0 > fi_size )
//			{
//				fi_size = calculate_ml( cigar );
//				if ( 0 == fi_size )
//				{
//					fi_size = seq_length;
//				}
//				E("First Mate Length:%d \n", fi_size);
//			}
//			// Update the read content when needed
//			if ( fi_l < seq_length  ) // get a longer read
//			{
//				if ( flag & 0x10 ) // reverse complement
//				{
//					copy_string_rc( seq, r1);
//					copy_string_reverse( qual, q1);
//				}
//				else
//				{
//					copy_string( seq, r1);
//					copy_string( qual, q1);
//				}
//				fi_l = seq_length;
//			}
//		}
//		else if ( flag & 0x80 )
//		{
//			// otaining the length of the second mate
//			if ( 0 > se_size )
//			{
//				se_size = calculate_ml( cigar );
//				if ( 0 == se_size )
//				{
//					se_size = seq_length;
//				}
//				E("Second Mate Length:%d \n", se_size);
//			}
//			// Update the read content when needed
//			if ( se_l < seq_length  ) // get a longer read
//			{
//				if ( flag & 0x10 ) // reverse complement
//				{
//					copy_string_rc( seq, r2);
//					copy_string_reverse( qual, q2);
//				}
//				else
//				{
//					copy_string( seq, r2);
//					copy_string( qual, q2);
//				}
//				se_l = seq_length;
//			}
//		}
//
//		count++;
//		if (0 ==  count%1000000){E(".");}
//	}
//	// Output Last Pair
//	//if ( 0 != strncmp( prev_id, readid, MAX_LINE ) )
//	//{
//	if ( ( (fi_size == fi_l)) && (se_size == se_l) )
//	{
//		fprintf(fq_fp, "@%s/1\n%s\n+\n%s\n@%s/2\n%s\n+\n%s\n", prev_id, r1, q1, prev_id, r2, q2);
//		num_read++;
//		//fprintf(first_fp, "@%s/1\n%s\n+\n%s\n", prev_id, r1, q1);
//		//fprintf(second_fp, "@%s/2\n%s\n+\n%s\n", prev_id, r2, q2);
//	}
//	else
//	{
//		if ( 0 < fi_l)
//		{fprintf(misc_fp, "@%s/1\n%s\n+\n%s\n", prev_id, r1, q1);}
//		if ( 0 < se_l)
//		{fprintf(misc_fp, "@%s/2\n%s\n+\n%s\n", prev_id, r2, q2 );}
//		if ( 0 == fi_l +se_l )	{ num_what++; }
//		num_clip++;
//	}
//	//}
//	E("\n");
//	E("Summary:%d lines parsed\n", count);
//	E("Summary:%u proper reads extracted;\t%u clipped reads;\t%u special cases\n", num_read, num_clip-1, num_what-1);
//	// since the last two variables are both incremented in the first line
//	fclose(fp);
//	fclose(fq_fp);
//	fclose(misc_fp);
//	//fclose(first_fp);
//	//fclose(second_fp);
//
//}
///**********************************************/
//void copy_string( char *src, char *dest)
//{
//	int limit=strlen(src);
//	for( int i = 0; i < limit; i ++)
//	{
//		dest[i]=src[i];
//	}
//	dest[limit]='\0';
//}
///**********************************************/
//void copy_string_reverse( char *src, char *dest)
//{
//	int limit=strlen(src);
//	for( int i = 0; i < limit; i ++)
//	{
//		dest[i]=src[limit-1-i];
//	}
//	dest[limit]='\0';
//}
///**********************************************/
//void copy_string_rc( char *src, char *dest)
//{
//	int limit=strlen(src);
//	for( int i = 0; i < limit; i ++)
//	{
//		switch( src[limit-1-i])
//		{
//			case 'A':
//				dest[i]='T';
//				break;
//			case 'C':
//				dest[i]='G';
//				break;
//			case 'G':
//				dest[i]='C';
//				break;
//			case 'T':
//				dest[i]='A';
//				break;
//			default:
//				dest[i]='N';
//				break;
//		}
//	}
//	dest[limit]='\0';
//}
///**********************************************/
//int clip_length( char *cigar)
//{
//	int length = 0;
//	int tmp = 0;
//	if (strcmp(cigar, "*") == 0)
//		{	return -1;	}
//
//	while( *cigar )
//	{
//		if (isdigit(*cigar))
//			{ tmp = 10 * tmp + (*cigar - '0');}
//		else
//		{
//			//if ( 'M' == *cigar )
//			//{
//			//	length += tmp;
//			//}
//			//else if ('I' == *cigar )
//			//{
//			//	length += tmp;
//			//}
//			if ('S' ==*cigar 	)
//			{
//				length += tmp;
//			}
//			else if ('H' ==*cigar 	)
//			{
//				length += tmp;
//			}
//			//else if ('=' ==*cigar 	)
//			//{
//			//	length += tmp;
//			//}
//			//else if ('X' ==*cigar 	)
//			//{
//			//	length += tmp;
//			//}
//			tmp = 0;
//		}
//		cigar++;
//	}
//
//	return length;
//}
///**********************************************/
//void CheckClipping( char *sam_file, char *output  )
//{
//	string rid="";
//
//	map<string, int> read_dict;
//
//	char *readline=(char*)malloc(MAX_LINE);
//	char *readid=(char*)malloc(MAX_LINE);
//	char *prev_id=(char*)malloc(MAX_LINE);
//	char *refid=(char*)malloc(MAX_LINE);
//	char *cigar=(char*)malloc(MAX_LINE);
//	char *rnext=(char*)malloc(MAX_LINE);
//	char *seq=(char*)malloc(MAX_LINE);
//	char *qual=(char*)malloc(MAX_LINE);
//	uint32_t flag, pos, mapq, pnext, tlen;
//	int offset;
//
//	//// Read
//	//char *r1=(char*)malloc(MAX_LINE);
//	//char *r2=(char*)malloc(MAX_LINE);
//	//char *q1=(char*)malloc(MAX_LINE);
//	//char *q2=(char*)malloc(MAX_LINE);
//
//	int fi_size = -1, se_size = -1;
//
//	// Output File Name
//	//char *fastq1=(char*)malloc(MAX_LINE);
//	//char *fastq2=(char*)malloc(MAX_LINE);
//	//char *fastq_misc=(char*)malloc(MAX_LINE);
//	//strcpy(fastq1, output);
//	//strcpy(fastq2, output);
//	//strcpy(fastq_misc, output);
//	//strcat(fastq1, "_1.fastq");
//	//strcat(fastq2, "_2.fastq");
//	//strcat(fastq_misc, ".clipped");
//
//	E("Parsing SAM File\n");
//	//vector<string> tmp_list;
//	//FILE *first_fp = fopen( fastq1, "w");
//	//FILE *second_fp = fopen( fastq2, "w");
//	FILE *out_fp = fopen( output, "w");
//	//FILE *fp = fopen( sam_file, "r");
//	FILE *fp;// = fopen( sam_file, "r");
//	if ( '\0' == sam_file[0] )
//	{
//		fp = stdin;
//	}
//	else
//	{
//		fp = fopen(sam_file, "r");
//	}
//	int count=0;
//	//int fir_read=0, se_flag= 0; // if we obtain the first/second mate of a pair
//	int fi_l = 0, se_l =0;
//	int seq_length = 0;
//	int clip_l = 0;
//	int num_3 =0, num_4 = 0, num_5 =0, num_6 = 0, num_7 = 0, num_8 = 0, num_9 = 0;
//	int bad = 0;
//	double ratio = 0.0;
//	while( NULL != fgets(readline, MAX_LINE, fp) )
//	{
//		if ( 0 == strncmp( readline, "@", 1) )
//		{
//			continue;
//		}
//
//		sscanf( readline, "%s %u %s %u %u %s %s %u %u %s %s %n", readid, &flag,
//			refid, &pos, &mapq, cigar, rnext, &pnext, &tlen, seq, qual,
//		 	&offset);
//
//		if ( 256 <=  flag )
//		{
//			bad++;
//			continue;
//		}
//		count++;
//		if (0 ==  count%1000000){E(".");}
//		// Start New Pairs
//		seq_length =(int)strlen( seq );
//		if ( 0 > fi_size )
//		{
//			fi_size = calculate_ml( cigar );
//			if ( 0 == fi_size )
//			{
//				fi_size = seq_length;
//			}
//			E("First Mate Length:%d \n", fi_size);
//		}
//		clip_l = clip_length(cigar);
//		if ( 0 > clip_l) { clip_l == fi_size;} // for unmapped case
//		ratio = clip_l*1.0/fi_size;
//
//		if ( 0.3 <= ratio )
//		{
//			fprintf( out_fp, "%s", readline);
//			num_3 ++;
//			if ( 0.9 <= ratio) {num_9++;}
//			else if ( 0.8 <= ratio){ num_8++;}
//			else if ( 0.7 <= ratio){ num_7++;}
//			else if ( 0.6 <= ratio){ num_6++;}
//			else if ( 0.5 <= ratio ) { num_5++;}
//			else if (0.4 <= ratio) {num_4++;}
//
//		}
//		//if ( flag & 0x40 ) // first read
//		//{
//		//	// otaining the length of the first mate
//		//	if ( 0 > fi_size )
//		//	{
//		//		fi_size = calculate_ml( cigar );
//		//		if ( 0 == fi_size )
//		//		{
//		//			fi_size = seq_length;
//		//		}
//		//		E("First Mate Length:%d \n", fi_size);
//		//	}
//		//	// Update the read content when needed
//		//	if ( fi_l < seq_length  ) // get a longer read
//		//	{
//		//		if ( flag & 0x10 ) // reverse complement
//		//		{
//		//			copy_string_rc( seq, r1);
//		//			copy_string_reverse( qual, q1);
//		//		}
//		//		else
//		//		{
//		//			copy_string( seq, r1);
//		//			copy_string( qual, q1);
//		//		}
//		//		fi_l = seq_length;
//		//	}
//		//}
//		//else if ( flag & 0x80 )
//		//{
//		//	// otaining the length of the second mate
//		//	if ( 0 > se_size )
//		//	{
//		//		se_size = calculate_ml( cigar );
//		//		if ( 0 == se_size )
//		//		{
//		//			se_size = seq_length;
//		//		}
//		//		E("Second Mate Length:%d \n", se_size);
//		//	}
//		//	// Update the read content when needed
//		//	if ( se_l < seq_length  ) // get a longer read
//		//	{
//		//		if ( flag & 0x10 ) // reverse complement
//		//		{
//		//			copy_string_rc( seq, r2);
//		//			copy_string_reverse( qual, q2);
//		//		}
//		//		else
//		//		{
//		//			copy_string( seq, r2);
//		//			copy_string( qual, q2);
//		//		}
//		//		se_l = seq_length;
//		//	}
//		//}
//
//		//count++;
//		//if (0 ==  count%1000000){E(".");}
//	}
//	//// Output Last Pair
//	////if ( 0 != strncmp( prev_id, readid, MAX_LINE ) )
//	////{
//	//if ( ( (fi_size == fi_l)) && (se_size == se_l) )
//	//{
//	//	fprintf(first_fp, "@%s/1\n%s\n+\n%s\n", prev_id, r1, q1);
//	//	fprintf(second_fp, "@%s/2\n%s\n+\n%s\n", prev_id, r2, q2);
//	//}
//	//else
//	//{
//	//	if ( 0 < fi_l)
//	//	{fprintf(misc_fp, "@%s\n%s\n+\n%s\n", prev_id, r1, q1);}
//	//	if ( 0 < se_l)
//	//	{fprintf(misc_fp, "@%s\n%s\n+\n%s\n", prev_id, r2, r2 );}
//	//}
//	////}
//	E("\n");
//	//E("%d\n%d\n%d\n%d\n%d\n", count, num_3, num_4, num_5, num_6);
//	E("%d\t%d\n", count, bad );
//	E("%d\n%d\n%d\n%d\n%d\n%d\n%d\n",num_3, num_4, num_5, num_6, num_7, num_8, num_9);
//	fclose(fp);
//	fclose(out_fp);
//	//fclose(first_fp);
//	//fclose(second_fp);
//
//}
///**********************************************/
//int check_error ( char * option_tag)
//{
//	char *tag=(char*)malloc(MAX_LINE);
//	char c;
//	int value=-1;
//	int offset;
//	while( *option_tag )
//	{
//		sscanf( option_tag, "%s %n", tag, &offset);
//		if ( 0 == strncmp("NM", tag, 2))
//		{
//			value=atoi(tag+5);
//			//E("%s\t%d\n", tag, value);
//		}
//		option_tag+=offset;
//
//	}
//	free(tag);
//	return value;
//}
///**********************************************/
//void CheckBowtieMappings( char *sam_file, char *output, int max_e  )
//{
//
//	char *readline=(char*)malloc(MAX_LINE);
//	char *readid=(char*)malloc(MAX_LINE);
//	char *prev_id=(char*)malloc(MAX_LINE);
//	char *refid=(char*)malloc(MAX_LINE);
//	char *cigar=(char*)malloc(MAX_LINE);
//	char *rnext=(char*)malloc(MAX_LINE);
//	char *seq=(char*)malloc(MAX_LINE);
//	char *qual=(char*)malloc(MAX_LINE);
//	uint32_t flag, pos, mapq, pnext, tlen;
//	int offset;
//
//	// Read
//	char *r1=(char*)malloc(MAX_LINE);
//	char *r2=(char*)malloc(MAX_LINE);
//	char *q1=(char*)malloc(MAX_LINE);
//	char *q2=(char*)malloc(MAX_LINE);
//
//	int fi_size = -1, se_size = -1;
//
//	// Output File Name
//	char *fastq1=(char*)malloc(MAX_LINE);
//	char *fastq2=(char*)malloc(MAX_LINE);
//	char *fastq_misc=(char*)malloc(MAX_LINE);
//	strcpy(fastq1, output);
//	strcpy(fastq2, output);
//	strcpy(fastq_misc, output);
//	strcat(fastq1, "_1.fastq");
//	strcat(fastq2, "_2.fastq");
//	strcat(fastq_misc, ".clipped");
//
//	vector <int> vec_clip, vec_tlen;
//
//	uint32_t num_read = 0, num_clip = -1; // since the first mapping always add 1 to num_clip
//	int e1=1024, e2=1024;
//	int bad_mappings=0;
//	int NH=0;
//	E("Parsing Bowtie SAM File\n");
//	//vector<string> tmp_list;
//	FILE *first_fp = fopen( fastq1, "w");
//	FILE *second_fp = fopen( fastq2, "w");
//	FILE *misc_fp = fopen( fastq_misc, "w");
//	//FILE *fp = fopen( sam_file, "r");
//	FILE *fp;// = fopen( sam_file, "r");
//	if ( '\0' == sam_file[0] )
//	{
//		fp = stdin;
//	}
//	else
//	{
//		fp = fopen(sam_file, "r");
//	}
//	int count=0;
//	//int fir_read=0, se_flag= 0; // if we obtain the first/second mate of a pair
//	int fi_l = 0, se_l =0;
//	int seq_length = 0;
//	while( NULL != fgets(readline, MAX_LINE, fp) )
//	{
//		if ( 0 == strncmp( readline, "@", 1) )
//		{
//			continue;
//		}
//
//		sscanf( readline, "%s %u %s %u %u %s %s %u %u %s %s %n", readid, &flag,
//			refid, &pos, &mapq, cigar, rnext, &pnext, &tlen, seq, qual,
//		 	&offset);
//		//E("%d %s", count, readline);
//		//error = check_error( readline + offset);
//		//if ( max_e >= error)
//		//{
//		//	//E("%s", readline);
//		//	continue;
//		//}
//		//E("Sequence Length:%d \n", mate_length);
//		//break;
//
//		// Output Pairs
//		if ( 0 != strncmp( prev_id, readid, MAX_LINE ) )
//		{
//			if (  (fi_size == fi_l) && (se_size == se_l) &&
//				( (e1 > max_e) || ( e2 > max_e))
//				)
//			{
//				fprintf(first_fp, "@%s/1\n%s\n+\n%s\n", prev_id, r1, q1);
//				fprintf(second_fp, "@%s/2\n%s\n+\n%s\n", prev_id, r2, q2);
//				num_read++;
//			}
//			else
//			{
//				//if ( 0 < fi_l)
//				//{fprintf(misc_fp, "@%s\n%s\n+\n%s\n", prev_id, r1, q1);}
//				//if ( 0 < se_l)
//				//{fprintf(misc_fp, "@%s\n%s\n+\n%s\n", prev_id, r2, r2 );}
//				//fprintf(misc_fp, "%s\t%d\t%d\n", prev_id, e1, e2);
//				num_clip++;
//			}
//			fprintf(misc_fp, "%s\t%d\t%d\n", prev_id, e1, e2);
//			copy_string(readid, prev_id);
//			fi_l=0;
//			se_l=0;
//			e1=1024; // reset clipping
//			e2=1024; // reset clipping
//			NH=0;
//			vec_clip.clear();
//			vec_clip.clear();
//		}
//		// Start New Pairs
//		if ( 256 <= flag )
//		{
//			bad_mappings++;
//			continue;
//		}
//		seq_length =(int)strlen( seq );
//
//		if ( flag & 0x40 ) // first read
//		{
//			// otaining the length of the first mate
//			if ( 0 > fi_size )
//			{
//				fi_size = calculate_ml( cigar );
//				if ( 0 == fi_size )
//				{
//					fi_size = seq_length;
//				}
//				E("First Mate Length:%d \n", fi_size);
//			}
//			// Update the read content when needed
//			if ( fi_l < seq_length  ) // only targets on mappings without HARD CLIPPING
//			{
//				if ( flag & 0x10 ) // reverse complement
//				{
//					copy_string_rc( seq, r1);
//					copy_string_reverse( qual, q1);
//				}
//				else
//				{
//					copy_string( seq, r1);
//					copy_string( qual, q1);
//				}
//				fi_l = seq_length;
//				e1 = check_error( readline + offset);
//				//E("%d\t%s", e1, readline);
//			}
//		}
//		else if ( flag & 0x80 )
//		{
//			// otaining the length of the second mate
//			if ( 0 > se_size )
//			{
//				se_size = calculate_ml( cigar );
//				if ( 0 == se_size )
//				{
//					se_size = seq_length;
//				}
//				E("Second Mate Length:%d \n", se_size);
//			}
//			// Update the read content when needed
//			if ( se_l < seq_length  ) // only targets on mappings without HARD CLIPPING
//			{
//				if ( flag & 0x10 ) // reverse complement
//				{
//					copy_string_rc( seq, r2);
//					copy_string_reverse( qual, q2);
//				}
//				else
//				{
//					copy_string( seq, r2);
//					copy_string( qual, q2);
//				}
//				se_l = seq_length;
//				e2 = check_error( readline + offset);
//				//E("%d\t%s", e2, readline);
//			}
//		}
//		NH++;
//		count++;
//		if (0 ==  count%1000000){E(".");}
//	}
//	//// Output Last Pair
//	//if (  (fi_size == fi_l) && (se_size == se_l) &&
//	//		( (e1 > max_e) || ( e2 > max_e))
//	//   )
//	//{
//	//	fprintf(first_fp, "@%s/1\n%s\n+\n%s\n", prev_id, r1, q1);
//	//	fprintf(second_fp, "@%s/2\n%s\n+\n%s\n", prev_id, r2, q2);
//	//	num_read++;
//	//}
//	//else
//	//{
//	//	num_clip++;
//	//}
//	//fprintf(misc_fp, "%s\t%d\t%d\n", prev_id, e1, e2);
//	E("\n");
//	E("Summary:%u reads extracted\t%u concordant reads\t%d minor mappings\n", num_read, num_clip, bad_mappings);
//	fclose(fp);
//	fclose(misc_fp);
//	fclose(first_fp);
//	fclose(second_fp);
//
//}
///**********************************************/
//void CountReadOcc( char *sam_file, char *output, int max_e  )
//{
//
//	char *readline=(char*)malloc(MAX_LINE);
//	char *readid=(char*)malloc(MAX_LINE);
//	char *prev_id=(char*)malloc(MAX_LINE);
//	char *tmp_buffer=(char*)malloc(MAX_LINE);
//	//char *cigar=(char*)malloc(MAX_LINE);
//	//char *rnext=(char*)malloc(MAX_LINE);
//	//char *seq=(char*)malloc(MAX_LINE);
//	//char *qual=(char*)malloc(MAX_LINE);
//	uint32_t flag, pos, mapq, pnext, tlen;
//	int offset;
//
//	// Read
//	//char *r1=(char*)malloc(MAX_LINE);
//	//char *r2=(char*)malloc(MAX_LINE);
//	//char *q1=(char*)malloc(MAX_LINE);
//	//char *q2=(char*)malloc(MAX_LINE);
//
//	//int fi_size = -1, se_size = -1;
//
//	// Output File Name
//	//char *fastq1=(char*)malloc(MAX_LINE);
//	//char *fastq2=(char*)malloc(MAX_LINE);
//	//char *fastq_misc=(char*)malloc(MAX_LINE);
//	//strcpy(fastq1, output);
//	//strcpy(fastq2, output);
//	//strcpy(fastq_misc, output);
//	//strcat(fastq1, "_1.fastq");
//	//strcat(fastq2, "_2.fastq");
//	//strcat(fastq_misc, ".clipped");
//
//	uint32_t num_read = 0, all_read = 0,  num_clip = -1; // since the first mapping always add 1 to num_clip
//	int e1=0, e2=0;
//	int occ=0;
//	int bad_mappings=0;
//	E("Parsing BWA SAM File\n");
//	//vector<string> tmp_list;
//	//FILE *first_fp = fopen( fastq1, "w");
//	//FILE *second_fp = fopen( fastq2, "w");
//	FILE *misc_fp = fopen( output, "w");
//	//FILE *fp = fopen( sam_file, "r");
//	FILE *fp;// = fopen( sam_file, "r");
//	if ( '\0' == sam_file[0] )
//	{
//		fp = stdin;
//	}
//	else
//	{
//		fp = fopen(sam_file, "r");
//	}
//	int count=0;
//	//int fir_read=0, se_flag= 0; // if we obtain the first/second mate of a pair
//	int fi_l = 0, se_l =0;
//	int seq_length = 0;
//	while( NULL != fgets(readline, MAX_LINE, fp) )
//	{
//		if ( 0 == strncmp( readline, "@", 1) )
//		{
//			continue;
//		}
//
//		sscanf( readline, "%s %n", readid, &offset);
//
//		// Output Pairs
//		if ( 0 != strncmp( prev_id, readid, MAX_LINE ) )
//		{
//			if ( 2 != occ ) // first line will pass
//			{
//				L("%s\t%d\n", readid, occ);
//				fprintf(misc_fp, "%s", tmp_buffer);
//				num_read++;
//			}
//			copy_string(readid, prev_id);
//			occ = 0;
//			all_read++;
//			tmp_buffer[0]='\0';
//		}
//		// Start New Pairs
//		strcat( tmp_buffer, readline );
//		occ++;
//		count++;
//		if (0 ==  count%10000000){E(".");}
//	}
//	if ( 2 != occ )
//	{
//		L("%s\t%d\n", readid, occ);
//		fprintf(misc_fp, "%s", tmp_buffer);
//		num_read++;
//	}
//	all_read++;
//	E("\n");
//	E("Summary:%u lines\t%u reads\t%u weird reads extracted\n", count, all_read-1, num_read-1);
//	fclose(fp);
//	fclose(misc_fp);
//	//fclose(first_fp);
//	//fclose(second_fp);
//
//}
///**********************************************/
//int get_pair_quality( const vector<int> &vec_clip, const vector<int> &vec_tlen, int max_e )
//{
//	int flag = 0;
//	int lim1= (int)vec_clip.size();
//	int i = 0;
//	while( i+1 < lim1 ) // Is it possible that it changes sth compared to i<lim1?
//	{
//		if ( abs(vec_tlen[i]) == abs(vec_tlen[i+1] ))
//		{
//			if ( (vec_clip[i] <= max_e) && ( vec_clip[i+1] <= max_e ) )
//			{
//				flag = 1;
//				break;
//			}
//			i+=2;
//		}
//		else
//		{
//			i++;
//			continue;
//		}
//	}
//	return flag;
//}
//// We assume -N was used in bwa search but all mappings are grouped together
//// First we assign a large clipping value, say 100
//// Whenever we find a smaller clipping (or better mapping), we adjust the error, and also reset the other
//// The final values will be the "better" mappping pair we want
//// If a pair has both mate being 100bp and at least one pair with both less than 5 clipping, then it's a good one.
///**********************************************/
//void CheckBWAMappings( char *sam_file, char *output, int max_e  )
//{
//
//	char *readline=(char*)malloc(MAX_LINE);
//	char *readid=(char*)malloc(MAX_LINE);
//	char *prev_id=(char*)malloc(MAX_LINE);
//	char *refid=(char*)malloc(MAX_LINE);
//	char *cigar=(char*)malloc(MAX_LINE);
//	char *rnext=(char*)malloc(MAX_LINE);
//	char *seq=(char*)malloc(MAX_LINE);
//	char *qual=(char*)malloc(MAX_LINE);
//	char *tmp_buffer=(char*)malloc(MAX_LINE);
//	uint32_t flag, pos, mapq, pnext;
//	int tlen;
//	int offset;
//
//	// Read
//	char *r1=(char*)malloc(MAX_LINE);
//	char *r2=(char*)malloc(MAX_LINE);
//	char *q1=(char*)malloc(MAX_LINE);
//	char *q2=(char*)malloc(MAX_LINE);
//
//	int fi_size = -1,	// length of the first mate obtained from cigar
//		se_size = -1;	// length of the second mate obtained from cigar
//
//	// Output File Name
//	char *fastq1=(char*)malloc(MAX_LINE);
//	char *fastq2=(char*)malloc(MAX_LINE);
//	char *fastq_misc=(char*)malloc(MAX_LINE);
//	//char *fastq_log=(char*)malloc(MAX_LINE);
//	strcpy(fastq1, output);
//	strcpy(fastq2, output);
//	strcpy(fastq_misc, output);
//	//strcpy(fastq_log, output);
//	strcat(fastq1, "_1.fastq");
//	strcat(fastq2, "_2.fastq");
//	strcat(fastq_misc, ".clipped");
//	//strcat(fastq_log, ".log");
//
//	vector <int> vec_clip, vec_tlen;
//
//	uint32_t num_read = 0, num_clip = -1; // since the first mapping always add 1 to num_clip
//	int e1=1024, e2=1024;
//	int bad_mappings=0;
//	int NH=0;
//	E("Parsing Bowtie SAM File\n");
//	//vector<string> tmp_list;
//	FILE *first_fp = fopen( fastq1, "w");
//	FILE *second_fp = fopen( fastq2, "w");
//	FILE *misc_fp = fopen( fastq_misc, "w");
//
//	FILE *fp;// = fopen( sam_file, "r");
//	if ( '\0' == sam_file[0] )
//	{
//		fp = stdin;
//	}
//	else
//	{
//		fp = fopen(sam_file, "r");
//	}
//	int count=0;
//	int fi_l = 0, se_l =0;
//	int seq_length = 0;
//	int quality_pair = 0; // if a pair is good or not
//
//	while( NULL != fgets(readline, MAX_LINE, fp) )
//	{
//		if ( 0 == strncmp( readline, "@", 1) )//header
//		{	continue;	}
//
//		sscanf( readline, "%s %u %s %u %u %s %s %u %d %s %s %n", readid, &flag,
//			refid, &pos, &mapq, cigar, rnext, &pnext, &tlen, seq, qual,
//		 	&offset);
//
//
//		// Output Pairs
//		if ( 0 != strncmp( prev_id, readid, MAX_LINE ) )
//		{
//			quality_pair = get_pair_quality( vec_clip, vec_tlen, max_e);
//			if (  (fi_size == fi_l) && (se_size == se_l) && // full length reads
//				!quality_pair  // and not good mappings are available
//				)
//			{
//				fprintf(first_fp, "@%s/1\n%s\n+\n%s\n", prev_id, r1, q1);
//				fprintf(second_fp, "@%s/2\n%s\n+\n%s\n", prev_id, r2, q2);
//				fprintf(misc_fp, "%s", tmp_buffer);
//				num_read++;
//			}
//			else
//			{
//				//if ( 0 < fi_l)
//				//{fprintf(misc_fp, "@%s\n%s\n+\n%s\n", prev_id, r1, q1);}
//				//if ( 0 < se_l)
//				//{fprintf(misc_fp, "@%s\n%s\n+\n%s\n", prev_id, r2, r2 );}
//				//fprintf(misc_fp, "%s", tmp_buffer);
//				num_clip++;
//			}
//			L("%s\t%d\t%d\t%d\n", prev_id, e1, e2, NH);
//			copy_string(readid, prev_id);
//			fi_l=0;
//			se_l=0;
//			e1=0; // reset clipping
//			e2=0; // reset clipping
//			tmp_buffer[0]='\0';
//			NH=0;
//			vec_clip.clear();
//			vec_tlen.clear();
//		}
//
//		// Start New Pairs
//		if ( 256 <= flag )	// secondary alignment
//		{
//			bad_mappings++;
//			continue;
//		}
//
//		seq_length =(int)strlen( seq );
//
//		if ( flag & 0x40 ) // first read
//		{
//			// Obtaining the length of the first mate
//			if ( 0 > fi_size )
//			{
//				fi_size = calculate_ml( cigar );
//				if ( 0 == fi_size )
//				{
//					fi_size = seq_length;
//				}
//				E("First Mate Length:%d \n", fi_size);
//			}
//			// Update the read content when needed
//			if ( fi_size == seq_length  ) // only targets on mappings without HARD CLIPPING
//			{
//				if ( flag & 0x10 ) // reverse complement
//				{
//					copy_string_rc( seq, r1);
//					copy_string_reverse( qual, q1);
//				}
//				else
//				{
//					copy_string( seq, r1);
//					copy_string( qual, q1);
//				}
//				fi_l = seq_length;
//				e1 = clip_length( cigar );
//				if ( 0 > e1) { e1 = fi_size;} // just in case unmapped reads
//				vec_clip.push_back(e1);
//				vec_tlen.push_back(tlen);
//				//E("%d\t%s", e1, readline);
//			}
//		}
//		else if ( flag & 0x80 )
//		{
//			// Obtaining the length of the second mate
//			if ( 0 > se_size )
//			{
//				se_size = calculate_ml( cigar );
//				if ( 0 == se_size )
//				{
//					se_size = seq_length;
//				}
//				E("Second Mate Length:%d \n", se_size);
//			}
//			// Update the read content when needed
//			if ( se_size == seq_length  ) // only targets on mappings without HARD CLIPPING
//			{
//				if ( flag & 0x10 ) // reverse complement
//				{
//					copy_string_rc( seq, r2);
//					copy_string_reverse( qual, q2);
//				}
//				else
//				{
//					copy_string( seq, r2);
//					copy_string( qual, q2);
//				}
//				se_l = seq_length;
//				e2 = clip_length( cigar );
//				if ( 0 > e2) { e2 = se_size;}
//				vec_clip.push_back(e2);
//				vec_tlen.push_back(tlen);
//				//E("%d\t%s", e2, readline);
//			}
//		}
//		strncat(tmp_buffer, readline, MAX_LINE);
//		NH++;
//		count++;
//		if (0 ==  count%10000000){E(".");}
//	}
//	//// Output Last Pair
//
//	quality_pair = get_pair_quality( vec_clip, vec_tlen, max_e);
//	if (  (fi_size == fi_l) && (se_size == se_l) && // full length reads
//			!quality_pair  // and no good mappings are available
//		)
//		{
//			fprintf(first_fp, "@%s/1\n%s\n+\n%s\n", prev_id, r1, q1);
//			fprintf(second_fp, "@%s/2\n%s\n+\n%s\n", prev_id, r2, q2);
//			fprintf(misc_fp, "%s", tmp_buffer);
//			num_read++;
//		}
//	else
//	{
//		//if ( 0 < fi_l)
//		//{fprintf(misc_fp, "@%s\n%s\n+\n%s\n", prev_id, r1, q1);}
//		//if ( 0 < se_l)
//		//{fprintf(misc_fp, "@%s\n%s\n+\n%s\n", prev_id, r2, r2 );}
//		//fprintf(misc_fp, "%s", tmp_buffer);
//		num_clip++;
//	}
//	L("%s\t%d\t%d\t%d\n", prev_id, e1, e2, NH);
//	//if (  (fi_size == fi_l) && (se_size == se_l) &&
//	//		( (e1 > max_e) || ( e2 > max_e))
//	//   )
//	//{
//	//	fprintf(first_fp, "@%s/1\n%s\n+\n%s\n", prev_id, r1, q1);
//	//	fprintf(second_fp, "@%s/2\n%s\n+\n%s\n", prev_id, r2, q2);
//	//	num_read++;
//	//}
//	//else
//	//{
//	//	num_clip++;
//	//}
//	//fprintf(misc_fp, "%s\t%d\t%d\n", prev_id, e1, e2);
//	E("\n");
//	E("Summary:%u lines parsed\n", count);
//	E("Summary:%u reads extracted;\t%u concordant mappings;\t%u secondary mappings\n", num_read, num_clip, bad_mappings );
//	fclose(fp);
//	fclose(misc_fp);
//	fclose(first_fp);
//	fclose(second_fp);
//	free(readline);
//	free(readid);
//	free(prev_id);
//	free(refid);
//	free(cigar);
//	free(rnext);
//	free(seq);
//	free(qual);
//	free(tmp_buffer);
//	free(r1);
//	free(r2);
//	free(q1);
//	free(q2);
//	free(fastq1);
//	free(fastq2);
//	free(fastq_misc);
//
//
//}
///**********************************************/
//void MergeMates( char *first_file, char *second_file, char *output  )
//{
//	char *id1=(char*)malloc(MAX_LINE);
//	char *r1=(char*)malloc(MAX_LINE);
//	char *opt1=(char*)malloc(MAX_LINE);
//	char *q1=(char*)malloc(MAX_LINE);
//	char *id2=(char*)malloc(MAX_LINE);
//	char *r2=(char*)malloc(MAX_LINE);
//	char *opt2=(char*)malloc(MAX_LINE);
//	char *q2=(char*)malloc(MAX_LINE);
//	//char *readid=(char*)malloc(MAX_LINE);
//	//char *readid=(char*)malloc(MAX_LINE);
//	//uint32_t flag;
//	//int offset;
//
//	uint32_t count=0;
//	//uint32_t n_con = 0;
//	//uint32_t n_dis = 0;
//	//uint32_t n_oea = 0;
//	//uint32_t n_un =0;
//	//uint32_t sum=0;
//
//	E("Parsing SAM File\n");
//	//vector<string> tmp_list;
//	//FILE *con_fp = fopen( "concord.read", "w");
//	//FILE *dis_fp = fopen( "discon.read", "w");
//	//FILE *oea_fp = fopen( "oea.read", "w");
//	//FILE *unmap_fp = fopen( "unmapped.read", "w");
//	FILE *fp1 = fopen(first_file, "r");
//	FILE *fp2 = fopen(second_file, "r");
//	FILE *out_fp = fopen(output, "w");
//
//
//
//	while( NULL != fgets(id1, MAX_LINE, fp1) )
//	{
//		fgets(r1, MAX_LINE, fp1);
//		fgets(opt1, MAX_LINE, fp1);
//		fgets(q1, MAX_LINE, fp1);
//		fgets(id2, MAX_LINE, fp2);
//		fgets(r2, MAX_LINE, fp2);
//		fgets(opt2, MAX_LINE, fp2);
//		fgets(q2, MAX_LINE, fp2);
//		fprintf(out_fp, "%s%s%s%s%s%s%s%s", id1, r1, opt1, q1, id2, r2, opt2, q2);
//		count+=4;
//
//		if ( 0 == count%10000000)
//		{
//
//			E(".");
//		}
//	}
//	E("\n");
//	fclose(fp1);
//	fclose(fp2);
//	fclose(out_fp);
//	//fclose(con_fp);
//	//fclose(dis_fp);
//	//fclose(oea_fp);
//	//fclose(unmap_fp);
//
//
//}
///**********************************************/
//void DNATranslate( const string &g_seq, vector<string> &trans_vec,  int flag)//, int left_i, int right_i )
//{
//	trans_vec.clear();
//	//pos_vec.clear();
//	int limit= (int)g_seq.size();
//	int last = limit -2; // last starting position in 1-based system
//	//int split_i = left_i - 1; // location of the breakpoint
//	//if ( 0 == flag) // reverse
//	//	{ split_i = right_i;}
//
//
//
//	string trigram;
//	string tmp_str;
//	//char aa;
//	for( int start =0; start < 3; start++) // three reading frames
//	{
//		tmp_str = "";
//		for( int i=start; i < last ; i+=3 )
//		{
//			trigram = g_seq.substr(i,3);
//			tmp_str.push_back( ToProtein(trigram) );
//			//if ( (i < split_i) && ())
//			//{
//			//}
//
//			//L("%d: %s %c F%d %d %d\n", i, trigram.c_str(), aa, start, last, limit);
//		}
//		L("%d-%d:%s\n", flag, start, tmp_str.c_str() );
//		trans_vec.push_back(tmp_str);
//	}
//}
//
///**********************************************/
//void make_rc( const string &raw_str, string &new_str)
//{
//	new_str = "";
//	int limit = (int)raw_str.size();
//	for( int i =0; i < limit; i++ )
//	{
//		switch( raw_str[limit-1-i])
//		{
//			case 'A':
//				new_str.push_back('T');
//				break;
//			case 'C':
//				new_str.push_back('G');
//				break;
//			case 'G':
//				new_str.push_back('C');
//				break;
//			case 'T':
//				new_str.push_back('A');
//				break;
//			default:
//				new_str.push_back('N');
//		}
//	}
//}
//
//
//
///**********************************************/
//// assume left <= right
//// change the the indexof aa
//int bp_dist( int left, int right, int bp)
//{
//	int dist = 0;
//	int tmp_bp = 0;
//	if ( left <= bp)
//	{
//		if (  right < bp)
//		{
//			dist = right - bp;
//		}
//		else
//		{
//			tmp_bp = bp+1;
//			dist =1 + (tmp_bp -left)/3;
//			//if ( 2 == (tmp_bp-left)%3 )
//			//{
//			//	dist =(tmp_bp -left)/3;
//			//}
//			//else
//			//{
//			//	dist =1 + (tmp_bp -left)/3;
//			//}
//		}
//	}
//	else
//	{
//		dist = left -bp;
//	}
//	return dist;
//}
//
///**********************************************/
//char ToProtein ( const string &trigram)
//{
//	map<string, char> codon;
//	codon["TTT"]='F';
//	codon["TTC"]='F';
//	codon["TTA"]='L';
//	codon["TTG"]='L';
//	codon["CTT"]='L';
//	codon["CTC"]='L';
//	codon["CTA"]='L';
//	codon["CTG"]='L';
//	codon["ATT"]='I';
//	codon["ATC"]='I';
//	codon["ATA"]='I';
//	codon["ATG"]='M';
//	codon["GTT"]='V';
//	codon["GTC"]='V';
//	codon["GTA"]='V';
//	codon["GTG"]='V';
//	codon["TCT"]='S';
//	codon["TCC"]='S';
//	codon["TCA"]='S';
//	codon["TCG"]='S';
//	codon["CCT"]='P';
//	codon["CCC"]='P';
//	codon["CCA"]='P';
//	codon["CCG"]='P';
//	codon["ACT"]='T';
//	codon["ACC"]='T';
//	codon["ACA"]='T';
//	codon["ACG"]='T';
//	codon["GCT"]='A';
//	codon["GCC"]='A';
//	codon["GCA"]='A';
//	codon["GCG"]='A';
//	codon["TAT"]='Y';
//	codon["TAC"]='Y';
//	codon["TAA"]='-';
//	codon["TAG"]='-';
//	codon["CAT"]='H';
//	codon["CAC"]='H';
//	codon["CAA"]='Q';
//	codon["CAG"]='Q';
//	codon["AAT"]='N';
//	codon["AAC"]='N';
//	codon["AAA"]='K';
//	codon["AAG"]='K';
//	codon["GAT"]='D';
//	codon["GAC"]='D';
//	codon["GAA"]='E';
//	codon["GAG"]='E';
//	codon["TGT"]='C';
//	codon["TGC"]='C';
//	codon["TGA"]='-';
//	codon["TGG"]='W';
//	codon["CGT"]='R';
//	codon["CGC"]='R';
//	codon["CGA"]='R';
//	codon["CGG"]='R';
//	codon["AGT"]='S';
//	codon["AGC"]='S';
//	codon["AGA"]='R';
//	codon["AGG"]='R';
//	codon["GGT"]='G';
//	codon["GGC"]='G';
//	codon["GGA"]='G';
//	codon["GGG"]='G';
//	map< string, char >::iterator it;
//	it = codon.find(trigram);
//	char aa;
//	if ( it != codon.end() )
//	{
//		aa = (*it).second;
//	}
//	else
//	{
//		aa = '*';
//		E("Warning in %s\n", trigram.c_str());
//	}
//	return aa;
//}
//
///*********************************************/
//void SummarizeFasta( char *pred_file, char *out_file )
//{
//	vector<string> token_array;
//	map<string, vector<tr_length> >::iterator it;
//	string t_id;
//	//int reg_num = 0, // the number of gene regions for a single id
//	//	f_id, // the index for multi-region transcript
//	//	f_length; // length of the transcript
//	FILE *fp = fopen(pred_file, "r");
//	FILE *out_fp = fopen(out_file, "w");
//	int count=0;
//	int seq_length = 0, total_length = 0 ;
//
//
//	char *readline  = (char*)malloc(MAX_LINE);
//	char *seqname = (char*)malloc(MAX_LINE);
//
//	while( NULL != fgets(readline, MAX_LINE, fp) )
//	{
//		if( 0 == strncmp(">", readline, 1 ) )
//		{
//			strcpy( seqname, readline);
//			//continue;
//		}
//		else
//		{
//			seq_length = (int)strlen(readline);
//			count++;
//			fprintf( out_fp, "%d\t%d\n", count, seq_length);
//			//count++;
//			total_length += seq_length;
//		}
//
//	}
//	E("Summary: %d seq with %f avg aa length.\n", count, total_length*1.0/count);
//	fclose( out_fp);
//	fclose( fp );
//
//}
//
///*********************************************/
//void ExtractPeptideFromFasta( char *pred_file, char *out_file )
//{
//	vector<string> header_array;
//	vector<string> token_array;
//	map<string, vector<tr_length> >::iterator it;
//	string t_id;
//	//int reg_num = 0, // the number of gene regions for a single id
//	//	f_id, // the index for multi-region transcript
//	//	f_length; // length of the transcript
//	FILE *fp = fopen(pred_file, "r");
//	FILE *out_fp = fopen(out_file, "w");
//	int count=0;
//	int pos = 0;
//	int left = 0, right = 0;
//	int seq_length = 0, total_length = 0 ;
//	int bp_pos =0;
//	int i =0;
//	int new_bp = 0;
//
//	char *readline  = (char*)malloc(MAX_LINE);
//	char *seqname = (char*)malloc(MAX_LINE);
//	string final_str;
//	while( NULL != fgets(readline, MAX_LINE, fp) )
//	{
//		if( 0 == strncmp(">", readline, 1 ) )
//		{
//			header_array = splitString(readline, '_');
//			bp_pos = atoi( header_array[4].c_str() );
//			E("%d %s_%s_%s_%s_%s\n", bp_pos, header_array[0].c_str(), header_array[1].c_str(), header_array[2].c_str(), header_array[3].c_str(), header_array[4].c_str() );
//			//continue;
//		}
//		else
//		{
//			if ( 0 > bp_pos )
//			{
//			E("NO_BP\t%s_%s_%s_%s_%s\n", header_array[0].c_str(), header_array[1].c_str(), header_array[2].c_str(), header_array[3].c_str(), header_array[4].c_str() );
//			}
//			else
//			{
//				seq_length = (int)strlen(readline);
//				left = -1;
//				right = -1;
//				E("BP\t%d\t%s_%s_%s_%s_%s\n", seq_length,  header_array[0].c_str(), header_array[1].c_str(), header_array[2].c_str(), header_array[3].c_str(), header_array[4].c_str() );
//				E("%s", readline);
//				for( i = 0; i < seq_length; i ++)
//				{
//					if ('K' == readline[i] || 'R' == readline[i] )
//					{
//						if ( i <  bp_pos -1 ) // 0-based vs 1-based
//						{
//							left = i+1;
//							E("%d\n", left);
//						}
//						else if ( bp_pos -1  <= i) // 0-based vs 1-based
//						{
//							right = i+1;
//							E("%d\n", right);
//							E("%d\t%d\n", left, right);
//							break;
//						}
//					}
//				}
//				if ( -1 == right )
//				{
//					right = seq_length-1;
//					E("CHR to %d\n", right);
//				}
//				if ( -1 == left)
//				{
//					left = 1;// why not ZERO?
//					E("CHL to %d\n", left);
//				}
//				//left + 1 to right -1
//				if ( left < right )
//				{
//					final_str ="";
//					for( i = left; i <= right-1; i++ )
//					{
//						final_str += readline[i];
//					}
//					new_bp = bp_pos - left; // 1-based bp in the new sequence
//					E("SEQ\t%d\t%s\n",new_bp, final_str.c_str());
//					fprintf( out_fp, "%s_%s_%s_%s_%s_%d\n", header_array[0].c_str(), header_array[1].c_str(), header_array[2].c_str(), header_array[3].c_str(), header_array[4].c_str(), new_bp );
//					fprintf(out_fp, "%s\n", final_str.c_str());
//				}
//				else
//				{
//					E("BAD_BP\t%s_%s_%s_%s_%s\t%d\t%d\n", header_array[0].c_str(), header_array[1].c_str(), header_array[2].c_str(), header_array[3].c_str(), header_array[4].c_str(), left, right );
//				}
//			}
//
//		}
//
//	}
//	//E("Summary: %d seq with %f avg aa length.\n", count, total_length*1.0/count);
//	fclose( out_fp);
//	fclose( fp );
//
//}
//
///*********************************************/
//// Let's allow for 1 mis-cleavage
//void ExtractPeptideFromFasta_Mis( char *pred_file, char *out_file )
//{
//	vector<string> header_array;
//	vector<string> token_array;
//	map<string, vector<tr_length> >::iterator it;
//	string t_id;
//	//int reg_num = 0, // the number of gene regions for a single id
//	//	f_id, // the index for multi-region transcript
//	//	f_length; // length of the transcript
//	FILE *fp = fopen(pred_file, "r");
//	FILE *out_fp = fopen(out_file, "w");
//	int count=0;
//	int pos = 0;
//	int l1=0, l2 =0;
//	int r1 = 0, r2 = 0;
//	//int left = 0, right = 0;
//	int seq_length = 0, total_length = 0 ;
//	int bp_pos =0;
//	int i =0;
//	int new_bp = 0;
//	int status = 0;
//	vector<int> l_vec, r_vec;
//
//	char *readline  = (char*)malloc(MAX_LINE);
//	char *seqname = (char*)malloc(MAX_LINE);
//	string final_str;
//	while( NULL != fgets(readline, MAX_LINE, fp) )
//	{
//		if( 0 == strncmp(">", readline, 1 ) )
//		{
//			header_array = splitString(readline, '_');
//			bp_pos = atoi( header_array[4].c_str() );
//			E("%d %s_%s_%s_%s_%s\n", bp_pos, header_array[0].c_str(), header_array[1].c_str(), header_array[2].c_str(), header_array[3].c_str(), header_array[4].c_str() );
//			//continue;
//		}
//		else// sequence part
//		{
//			if ( 0 > bp_pos )
//			{
//			E("NO_BP\t%s_%s_%s_%s_%s\n", header_array[0].c_str(), header_array[1].c_str(), header_array[2].c_str(), header_array[3].c_str(), header_array[4].c_str() );
//			}
//			else
//			{
//				seq_length = (int)strlen(readline);
//				l1 = -1, l2 = -1;//left = -1;
//				r1 = -1, r2 = -1;//right = -1;
//				E("BP\t%d\t%s_%s_%s_%s_%s\n", seq_length,  header_array[0].c_str(), header_array[1].c_str(), header_array[2].c_str(), header_array[3].c_str(), header_array[4].c_str() );
//				E("%s", readline);
//				for( i = 0; i < seq_length; i ++)
//				{
//					if ('K' == readline[i] || 'R' == readline[i] )
//					{
//						if ( i <  bp_pos -1 ) // 0-based vs 1-based
//						{
//							l1 = l2;
//							l2 = i+1;	//left = i+1;
//							E("L\t%d\t%d\n", l1, l2);
//						}
//						else if ( bp_pos -1  <= i) // 0-based vs 1-based
//						{
//							if ( -1 == r1 )
//							{
//								r1 = i+1;
//								E("R1\t%d\n", r1);
//							}
//							else
//							{
//								r2 = i+1;
//								E("R2\t%d\t%d\n", r1, r2);
//								break;
//							}
//						}
//					}
//
//				}
//				E("ALL\t%d\t%d\t%d\t%d\t%d\n", l1, l2, bp_pos, r1, r2);
//				l_vec.clear();
//				r_vec.clear();
//
//				if ( -1 == l1)
//				{
//					l1 = 0;
//					E("CHL to %d\n", l1);
//					// we don't care if l2 is -1
//				}
//				if ( -1 == r2 )
//				{
//					r2 = seq_length-1;
//					E("CHR to %d\n", r2);
//					//don't care if r1 is -1
//				}
//				final_str="";
//				status = generate_peptide(final_str, l1, r1, readline);
//				if ( 1 == status )
//				{
//					new_bp = bp_pos - l1; // 1-based bp in the new sequence
//					E("SEQ-11\t%d\t%s\n",new_bp, final_str.c_str());
//					fprintf( out_fp, "%s_%s_%s_%s_%s_0_%d\n", header_array[0].c_str(), header_array[1].c_str(), header_array[2].c_str(), header_array[3].c_str(), header_array[4].c_str(), new_bp );
//					fprintf(out_fp, "%s\n", final_str.c_str());
//				}
//
//				if ( -1 == l2 )
//				{
//					final_str="";
//					status = generate_peptide(final_str, l1, r2, readline);
//					if ( 1 == status )
//					{
//						new_bp = bp_pos - l1; // 1-based bp in the new sequence
//						E("SEQ-12\t%d\t%s\n",new_bp, final_str.c_str());
//						fprintf( out_fp, "%s_%s_%s_%s_%s_1_%d\n", header_array[0].c_str(), header_array[1].c_str(), header_array[2].c_str(), header_array[3].c_str(), header_array[4].c_str(), new_bp );
//						fprintf(out_fp, "%s\n", final_str.c_str());
//					}
//				}
//
//				final_str="";
//				status = generate_peptide(final_str, l2, r1, readline);
//				if ( 1 == status )
//				{
//					new_bp = bp_pos - l2; // 1-based bp in the new sequence
//					E("SEQ-21\t%d\t%s\n",new_bp, final_str.c_str());
//					fprintf( out_fp, "%s_%s_%s_%s_%s_2_%d\n", header_array[0].c_str(), header_array[1].c_str(), header_array[2].c_str(), header_array[3].c_str(), header_array[4].c_str(), new_bp );
//					fprintf(out_fp, "%s\n", final_str.c_str());
//				}
//
//				final_str="";
//				status = generate_peptide(final_str, l2, r2, readline);
//				if ( 1 == status )
//				{
//					new_bp = bp_pos - l2; // 1-based bp in the new sequence
//					E("SEQ-22\t%d\t%s\n",new_bp, final_str.c_str());
//					fprintf( out_fp, "%s_%s_%s_%s_%s_3_%d\n", header_array[0].c_str(), header_array[1].c_str(), header_array[2].c_str(), header_array[3].c_str(), header_array[4].c_str(), new_bp );
//					fprintf(out_fp, "%s\n", final_str.c_str());
//				}
//				//if ( left < right )
//				//{
//				//	final_str ="";
//				//	for( i = left; i <= right-1; i++ )
//				//	{
//				//		final_str += readline[i];
//				//	}
//				//	new_bp = bp_pos - left; // 1-based bp in the new sequence
//				//	E("SEQ\t%d\t%s\n",new_bp, final_str.c_str());
//				//	fprintf( out_fp, "%s_%s_%s_%s_%s_%d\n", header_array[0].c_str(), header_array[1].c_str(), header_array[2].c_str(), header_array[3].c_str(), header_array[4].c_str(), new_bp );
//				//	fprintf(out_fp, "%s\n", final_str.c_str());
//				//}
//				//else
//				//{
//				//	E("BAD_BP\t%s_%s_%s_%s_%s\t%d\t%d\n", header_array[0].c_str(), header_array[1].c_str(), header_array[2].c_str(), header_array[3].c_str(), header_array[4].c_str(), left, right );
//				//}
//			}
//
//		}
//
//	}
//	//E("Summary: %d seq with %f avg aa length.\n", count, total_length*1.0/count);
//	fclose( out_fp);
//	fclose( fp );
//
//}
//int generate_peptide( string &final_str, int left, int right, char *raw_str)
//{
//	int status = -1;
//	int length = 0;
//	if ( (-1 < left) && (-1 < right ) )
//	{
//		length = right - left;
//		if ( 5 <= length )
//		{
//			status = 1; // we need to generate string
//			for( int i =left; i <= right -1; i++)
//			{
//				final_str += raw_str[i];
//			}
//		}
//		else{E("TOOSHORT\t%d\t%d\n", left, right);}
//
//	}
//	else{E("BAD_BP\t%d\t%d\n", left, right);}
//	return status;
//}
// OBSOLETE
/**********************************************/
//void ConvertToCount( char *pred_file, map<string, vector<tr_length> > &length_map , char *out_file )
//{
//	vector<string> token_array;
//	map<string, vector<tr_length> >::iterator it;
//	string t_id;
//	int reg_num = 0, // the number of gene regions for a single id
//		f_id, // the index for multi-region transcript
//		f_length; // length of the transcript
//	FILE *fp = fopen(pred_file, "r");
//	FILE *out_fp = fopen(out_file, "w");
//	int f_count=0;
//	float tmp_rpkm =0.0, f_rpkm = 0.0;
//	int seq_depth[12]= {0,0,0, 80000000, 81000000, 82000000, 80001000, 80999000, 82001000, 79999000,81001000, 81999000 };
//	//
//	float factor_vec[12];
//	for( int i = 0; i < 12; i++)
//	{
//		factor_vec[i] = seq_depth[i]*1.0/1000000000;
//	}
//
//
//
//	char *readline  = (char*)malloc(MAX_LINE);
//	while( NULL != fgets(readline, MAX_LINE, fp) )
//	{
//		if( 0 == strncmp("T", readline, 1 ) )
//		{
//			continue;
//		}
//		token_array = splitString(readline, '\t');
//		t_id = token_array[0];
//		it = length_map.find(t_id);
//		if ( it != length_map.end())
//		{
//			reg_num = (int)(*it).second.size();
//			if ( 1 == reg_num)
//			{
//				f_id = 0;
//				f_length = (*it).second[0].length;
//			}
//			else
//			{
//				f_id = select_longest_region((*it).second );
//				f_length = (*it).second[f_id].length;
//			}
//			L("OK\t%s\t%d\t%d\t%d\n", t_id.c_str(), f_id, reg_num, f_length );
//			fprintf(out_fp, "%s", t_id.c_str());
//			for( int i = 3; i < 12; i ++)
//			{
//				tmp_rpkm = atof(token_array[i].c_str());
//				f_rpkm = tmp_rpkm * f_length * factor_vec[i];
//				//fprintf(out_fp, "\t%f", f_rpkm);
//				f_count = (int)(floor(f_rpkm+0.5f));
//				fprintf(out_fp, "\t%d", f_count);
//
//			}
//			fprintf(out_fp, "\n");
//		}
//		else
//		{
//			L("NOT_FOUND\t%s\n", t_id.c_str() );
//		}
//
//
//	}
//
//	fclose( out_fp );
//	fclose( fp );
//}
//
///**********************************************/
//int select_longest_region( vector<tr_length> &tr_vec)
//{
//	int id = 0,
//		max_l = 0,
//		limit;
//	limit = (int)tr_vec.size();
//
//	for( int i = 0; i < limit; i ++)
//	{
//		if ( tr_vec[i].length > max_l )
//		{
//			max_l = tr_vec[i].length;
//			id = i;
//		}
//	}
//	return id;
//}
//
///**********************************************/
//void OutputJoinedFusion( char *out_file, vector<fusion> &fusion_vec )
//{
//	int limit=(int)fusion_vec.size();
//	int limit_1, limit_2;
//	int i = 0, j = 0,
//		j1 = 0, j2 =0,
//		loc1 = 0, loc2 = 0,
//		end1, end2; // end1 indicates the smaller location
//	string join_id;
//	vector<int> v_1, v_2;
//
//	FILE *out_fp= fopen(out_file, "w");
//	for( i =0; i < limit; i++ )
//	{
//		j = fusion_vec[i].group;
//		join_id = fusion_vec[j].id;
//		v_1 = fusion_vec[i].coor1;
//		v_2 = fusion_vec[i].coor2;
//
//		// Make sure the ends match in grouped predictions
//		loc1 = v_1[0];
//		loc2 = v_2[0];
//		if (loc1 <= loc2)
//		{
//			end1 = 0;
//			end2 = 1;
//		}
//		else
//		{
//			end1 = 1;
//			end2 = 0;
//		}
//
//		limit_1 = (int)v_1.size();
//		j1 = 0;
//		while( j1 < limit_1 )
//		{
//			fprintf(out_fp, "%s\t%d\t%s\t%c\t%d\t%d\n", join_id.c_str(), end1,  fusion_vec[i].chr1.c_str(),  fusion_vec[i].strand1, v_1[j1], v_1[j1+1] );
//			L("%s\t%d\t%s\t%c\t%d\t%d\t%s\t0\n", join_id.c_str(), end1,  fusion_vec[i].chr1.c_str(),  fusion_vec[i].strand1, v_1[j1], v_1[j1+1], fusion_vec[i].id.c_str() );
//			j1 += 2;
//		}
//		limit_2 = (int)v_2.size();
//		j2 = 0;
//		while( j2 < limit_2 )
//		{
//			fprintf(out_fp, "%s\t%d\t%s\t%c\t%d\t%d\n", join_id.c_str(), end2,  fusion_vec[i].chr2.c_str(),  fusion_vec[i].strand2, v_2[j2], v_2[j2+1] );
//			L( "%s\t%d\t%s\t%c\t%d\t%d\t%s\t1\n", join_id.c_str(), end2, fusion_vec[i].chr2.c_str(),  fusion_vec[i].strand2, v_2[j2], v_2[j2+1], fusion_vec[i].id.c_str() );
//			j2 += 2;
//		}
//
//
//		//L("F\t%s\t%s\n", fusion_vec[i].id.c_str(), join_id.c_str() );
//	}
//	fclose(out_fp);
//}
///**********************************************/
//void joinFusion( vector<fusion> &fusion_vec )
//{
//	int limit=(int)fusion_vec.size();
//	int j = 0;
//	string join_id;
//	for( int i =0; i < limit; i++ )
//	{
//		j = fusion_vec[i].group;
//		join_id = fusion_vec[j].id;
//		//L("F\t%s\t%s\n", fusion_vec[i].id.c_str(), join_id.c_str() );
//	}
//}
///**********************************************/
//void clusterFusion( vector<fusion> &fusion_vec )
//{
//	int limit=(int)fusion_vec.size();
//	int hit;
//	for( int i =0; i < limit; i++ )
//	{
//		if ( -1 == fusion_vec[i].group )
//		{
//			fusion_vec[i].group = i;
//			for( int j = i+1; j < limit; j++)
//			{
//				//L("COMP\t%d\t%d\t%d\n", i, j, limit);
//				if ( -1 == fusion_vec[j].group )
//				{
//					hit = compare_fusion( fusion_vec[i], fusion_vec[j]) ;
//					if ( 2 == hit )
//					{
//						fusion_vec[j].group = i;
//					}
//
//				}
//			}
//		}
//	}
//}
//
//
///**********************************************/
//// For prediction intervals (A1,B1) and (A2,B2)
//int compare_fusion( fusion &f_1, fusion &f_2)
//{
//	int hit = 0,
//		s_hit =0; // second round of comparison
//
//	if ( (f_1.chr1 == f_2.chr1) && (f_1.chr2 == f_2.chr2) ) //A1-A2 and B1-B2
//	{
//		if ( f_1.coor1[0] <= f_2.coor1[0] )// A1-A2
//		{
//			hit +=	compare_seg( f_1.coor1, f_2.coor1 );
//		}
//		else
//		{
//			hit +=	compare_seg( f_2.coor1, f_1.coor1 );
//		}
//
//		if ( 1 == hit ) // B1-B2
//		{
//			if ( f_1.coor2[0] <= f_2.coor2[0] )// A1-B2
//			{
//				hit +=	compare_seg( f_1.coor2, f_2.coor2 );
//			}
//			else
//			{
//				hit +=	compare_seg( f_2.coor2, f_1.coor2 );
//			}
//		}
//	}
//	if ( (2 > hit) && (f_1.chr1 == f_2.chr2) && (f_1.chr2 == f_2.chr1) ) //A1-B2 and A1-B2
//	{
//		hit = 0; // Just in case only one end matched
//		if ( f_1.coor1[0] <= f_2.coor2[0] )// A1-B2
//		{
//			hit +=	compare_seg( f_1.coor1, f_2.coor2 );
//		}
//		else
//		{
//			hit +=	compare_seg( f_2.coor2, f_1.coor1 );
//		}
//
//		if ( 1 == s_hit ) // A2-B1
//		{
//			if ( f_1.coor2[0] <= f_2.coor1[0] )
//			{
//				hit +=	compare_seg( f_1.coor2, f_2.coor1 );
//			}
//			else
//			{
//				hit +=	compare_seg( f_2.coor1, f_1.coor2 );
//			}
//		}
//	}
//
//	if ( 2 == hit )
//	{
//		//L("MATCHED\t%s\t%s\n", f_1.id.c_str(), f_2.id.c_str() );
//	}
//
//	return hit;
//}
///**********************************************/
//// Assumption: starting point of v_1 os less than that of v_2
//int compare_seg( vector<int> &v_1, vector<int> &v_2)
//{
//	int hit = 0;
//	int limit_1 = (int)v_1.size();
//	int limit_2 = (int)v_2.size();
//
//	int i = 0,
//		j = 0;
//	//OutputIntVec(v_1);
//	//OutputIntVec(v_2);
//	while( (i < limit_1) && ( j < limit_2))
//	{
//		if ( v_1[i] <= v_2[j] )
//		{
//			if ( v_1[i+1] >= v_2[j] )
//			{
//				hit = 1;
//				//L("M_1\t%d\t%d\t%d\t%d\t%d\t%d\n", i, v_1[i], v_1[i+1], j, v_2[j+1], v_2[j+2]);
//				break;// Overlap
//			}
//			else
//			{
//				i += 2;
//			}
//		}
//		else
//		{
//			if ( v_1[i] <= v_2[j+1] )
//			{
//				//L("M_3\t%d\t%d\t%d\t%d\t%d\t%d\n", i, v_1[i], v_1[i+1], j, v_2[j+1], v_2[j+2]);
//				hit = 1;
//				break;
//			}
//			else
//			{
//				j += 2;
//			}
//		}
//	}
//
//	return hit;
//}
///**********************************************/
//void OutputIntVec( vector<int> &v_1)
//{
//	int limit = (int)v_1.size();
//	L("CO");
//	for( int i =0; i < limit; i++)
//	{
//		L("\t%d", v_1[i]);
//	}
//	L("\n");
//}
///**********************************************/
///***   Original Prediction Conversion Part  ***/
///**********************************************/
//
///**********************************************/
//void ParseTopHatFusion( char *fm_file, char *out_file, map<string, GENEPosition> &PTMap, map<string, vector<string> > &gene_table, map<string, string> &ensg_table, int read_length)
//{
//	vector<string> token_array;
//
//	string strand;
//	string chr1, chr2,
//		gene1, gene2,
//		g_in1, g_in2, // in case gene id was reported instead of gene name
//		trans1, trans2,
//		trans_name1, trans_name2,
//		strand1, strand2;
//	string current_fusion,
//			new_fusion;
//	vector<string> vec_trans1, vec_trans2;
//	map<string, string>::iterator t1_it, t2_it;
//	map<string, vector<string> >::iterator t_it;
//	map<string, GENEPosition>::iterator p1_it, p2_it;
//	vector<ExonPosition> vec_ep1, vec_ep2;
//	vector<int> end_point;
//
//	string check_ori;
//
//	int pos1=0, pos2=0;
//	int h1, h2, // if gene_id exist
//		t1, t2; // if we find compatible transcript
//	int ghit1, ghit2, thit1, thit2;
//	int count = 1;
//	//int dir_flag = 0;	//int exon1 = -1, exon2= -1;
//	int limit, i;
//	FILE *out_fp= fopen(out_file, "w");
//	FILE *fp = fopen(fm_file, "r");
//	char *readline  = (char*)malloc(MAX_LINE);
//	while( NULL != fgets(readline, MAX_LINE, fp) )
//	{
//		token_array = splitString(readline, '\t');
//		chr1 = token_array[2];
//		chr2 = token_array[5];
//		gene1 = token_array[1];
//		gene2= token_array[4];
//		//trans_name1 = token_array[1];
//		//trans_name2 = token_array[8];
//		// Does TopHat-Fusion work in 0-based coordinate system?
//		pos1 = atoi(token_array[3].c_str());
//		pos2 = atoi(token_array[6].c_str());
//		//strand1 = token_array[3];
//		//strand2 = token_array[10];
//		// Strategy 1: Find the first COMPATIBLE transcript
//		L("Read %s", readline );
//		h1 = 1;
//		h2 = 1;
//		ghit1=0;
//		ghit2=0;
//		g_in1 = gene1;
//		if ( 0 == strncmp( gene1.c_str(), "ENSG", 4 ) )	// reported via gene_id
//		{
//			t1_it = ensg_table.find(gene1);
//			if ( ensg_table.end() == t1_it )
//			{
//				L("WRONG GENE1 ID\t%s\n", gene1.c_str());
//				h1 = 0;	//exit (EXIT_FAILURE);
//			}
//			else
//			{	g_in1 = t1_it -> second;	}
//		}
//		if ( 1 == h1 )
//		{
//			t_it = gene_table.find(g_in1);
//			if ( gene_table.end() == t_it)
//			{
//				L("WRONG GENE1 NAME\t%s\n", g_in1.c_str());
//				//exit (EXIT_FAILURE);
//			}else
//			{
//				vec_trans1 = t_it->second; // locate transcript
//				ghit1=1;
//			}
//		}
//
//		g_in2 = gene2;
//		if ( 0 == strncmp( gene2.c_str(), "ENSG", 4 ) )	// report by gene_id in TopHat-fusion
//		{
//			t2_it = ensg_table.find(gene2);
//			if ( ensg_table.end() == t2_it )
//			{
//				L("WRONG GENE2 ID\t%s\n", gene2.c_str());
//				h2 = 0;	//exit (EXIT_FAILURE);
//			}
//			else
//			{	g_in2 = t2_it -> second;	}
//		}
//		if ( 1 == h2 )
//		{
//			t_it = gene_table.find(g_in2);
//			if ( gene_table.end() == t_it)
//			{
//				L("WRONG GENE2 NAME\t%s\n", g_in2.c_str());
//				//exit (EXIT_FAILURE);
//			}
//			else
//			{
//				vec_trans2 = t_it->second;
//				ghit2 =1;
//			}
//		}
//
//		// Transcript Level Searching
//		thit1 = 0;
//		thit2 = 0;
//
//		if (  1 == ghit1 )
//		{
//			//L("Candidates\t%d\t%d\n", (int)vec_trans1.size(), (int)vec_trans2.size() );
//			t1 = locate_trans( trans1, strand1, vec_trans1, PTMap, chr1, pos1 ); // 1 means there exists compatible transcripts
//			if ( 1 == t1 )	{	thit1 = 1;	}
//			else{	L("MISSING T1 of %s for %d\n", g_in1.c_str(), pos1); }
//		}
//		if ( 1 == ghit2 )
//		{
//			t2 = locate_trans( trans2, strand2, vec_trans2, PTMap, chr2, pos2 ); // 1 means there exists compatible transcript
//			if ( 1 == t2 ) {	thit2 = 1;	}
//			else{ L("MISSING T2 of %s for %d\n", g_in2.c_str(), pos2); }
//		}
//		L("Summary\t%s-%d\t%d%d%d%d\n", token_array[0].c_str(), count, ghit1, thit1, ghit2, thit2 );
//		// 5' gene
//		if ( 1 == thit1 )
//		{
//			p1_it = PTMap.find(trans1);
//			if ( '+' == strand1[0]) //go to smaller coordinate
//			{	locate_segment(trans1, pos1, read_length, p1_it->second, end_point, 0);	//L("P_1+\n");
//			}
//			else
//			{	locate_segment(trans1, pos1, read_length, p1_it->second, end_point, 1);	//L("P_1-\n");
//			}
//		}
//		else
//		{
//			if ( 0 == ghit1) {check_strand( gene1, strand1 ); }
//			if ( '+' == strand1[0]) //go to smaller coordinate
//			{	approx_segment( pos1, read_length, end_point, 0);	//L("P_1+\n");
//			}
//			else
//			{	approx_segment( pos1, read_length, end_point, 1);	//L("P_1+\n");
//			}
//
//		}
//		// output 5' gene
//		limit = (int)end_point.size();
//		if (  '+' == strand1[0])
//		{
//			i = limit - 1;
//			while( 0 <= i )
//			{   fprintf(out_fp, "%s-%d\t0\t%s\t+\t%d\t%d\n", token_array[0].c_str(), count, chr1.c_str(), end_point[i], end_point[i-1] );
//			    i -= 2;
//			}L("O_1+\n");
//		}
//		else
//		{
//			i = 0;
//			while( i < limit )
//			{   fprintf(out_fp, "%s-%d\t0\t%s\t+\t%d\t%d\n", token_array[0].c_str(), count, chr1.c_str(), end_point[i], end_point[i+1] );
//			    i += 2;
//			}L("O_1-\n");
//		}
//
//		// 3' gene
//		if ( 1 == thit2 )
//		{
//			p2_it = PTMap.find(trans2);
//			if ( '+' == strand2[0]) //go to smaller coordinate
//			{	locate_segment(trans2, pos2, read_length, p2_it->second, end_point, 1);	//L("P_2+\n");
//			}
//			else
//			{	locate_segment(trans2, pos2, read_length, p2_it->second, end_point, 0);	//L("P_2-\n");
//			}
//		}
//		else
//		{
//			if ( 0 == ghit2) {check_strand( gene2, strand2 ); }
//			if ( '+' == strand1[0]) //go to smaller coordinate
//			{	approx_segment( pos2, read_length, end_point, 1);	//L("P_1+\n");
//			}
//			else
//			{	approx_segment( pos2, read_length, end_point, 0);	//L("P_1+\n");
//			}
//
//		}
//		// output 3' gene
//		limit = (int)end_point.size();
//		if ( '+' == strand[0] )
//		{
//			i = 0;
//			while( i < limit )
//			{   fprintf(out_fp, "%s-%d\t1\t%s\t+\t%d\t%d\n", token_array[0].c_str(), count, chr2.c_str(), end_point[i], end_point[i+1] );
//			    i += 2;
//			}L("O_2+\n");
//		}
//		else
//		{
//			i = limit - 1;
//			while( 0 <= i )
//			{   fprintf(out_fp, "%s-%d\t1\t%s\t+\t%d\t%d\n", token_array[0].c_str(), count, chr2.c_str(), end_point[i], end_point[i-1] );
//			    i -= 2;
//			}L("O_2-\n");
//		}
//		count++;
//	}
//	fclose(fp);
//	fclose(out_fp);
//}
//
///**********************************************/
//// Hardcoding here: SANP25 and GUCD1 are reverse-stranded
//void check_strand( string &gene_id, string &strand)
//{
//	strand[0]= '+';
//	if ( ('S' == gene_id[0] ) || ( 'G' == gene_id[0]) )
//	{
//		strand[0]= '-';	//L("CHECK\t%s\t%s\n", gene_id.c_str(), strand.c_str());
//	}//	{L("CHECK\t%s\t%s\n", gene_id.c_str(), strand.c_str() );}
//
//}
///**********************************************/
//int locate_trans( string &trans_id, string &strand,  vector<string> &vec_id, map<string, GENEPosition> &PTMap, string &chr_str, int pos )
//{
//	int limit=(int)vec_id.size();
//	string t_id;
//	GENEPosition tmp_gp;
//	map<string, GENEPosition>::iterator pit;
//
//	int hit=0, loc = -1;
//	for( int i = 0; i < limit; i++ )
//	{
//		t_id = vec_id[i];
//		pit = PTMap.find(t_id);
//		tmp_gp = pit->second;
//		strand[0] = tmp_gp.strand; // no matter if we have compatible transcript or not
//
//		//L("NOW\t%d\t%d\t%d\t%s\t%s\t%s\n", pos, i, limit, t_id.c_str(), tmp_gp.chr_name.c_str(), chr_str.c_str() );
//		if ( tmp_gp.chr_name == chr_str )
//		{
//			L("Checking %s of %d in %s\n", t_id.c_str(), (int)tmp_gp.epos.size(), chr_str.c_str() );
//			loc = find_exon( pos, tmp_gp.epos);
//			if ( 0 <= loc )
//			{
//				trans_id = vec_id[i];
//				//strand[0] = tmp_gp.strand;
//				hit = 1;
//				break; // got a compatible transcript
//			}
//		}
//
//	}
//	return hit;
//}
//
///**********************************************/
//void approx_segment( int pos, int read_length, vector<int> &end_point, int direction)
//{	//direction: 0 for going to leftside, 1 for going to rightside
//	end_point.clear();
//	if ( 1 == direction )
//	{
//		end_point.push_back( pos );
//		end_point.push_back( pos + read_length -1 );
//		L("APP\t%d\t%d\t%d\n", direction, end_point[0], end_point[1]);
//	}
//	else
//	{
//		end_point.push_back( pos );
//		end_point.push_back( pos - read_length + 1 );
//		L("APP\t%d\t%d\t%d\n", direction, end_point[1], end_point[0]);
//	}
//}
///**********************************************/
//void ParseSOAPfuse( char *fm_file, char *out_file, map<string, GENEPosition> &PTMap, map<string, vector<string> > &gene_table, map<string, string> &enst_table, int read_length)
//{
//	vector<string> token_array;
//
//	string strand;
//	string chr1, chr2,
//		gene1, gene2,
//		trans1, trans2,
//		trans_name1, trans_name2,
//		strand1, strand2;
//	string current_fusion,
//			new_fusion;
//	//vector<string> vec_trans1, vec_trans2;
//	map<string, string>::iterator t1_it, t2_it;
//	map<string, vector<string> >::iterator t_it;
//	map<string, GENEPosition>::iterator p1_it, p2_it;
//	vector<ExonPosition> vec_ep1, vec_ep2;
//	vector<int> end_point;
//
//	string check_ori;
//
//	int pos1=0, pos2=0;
//	int count = 1;
//	//int dir_flag = 0;	//int exon1 = -1, exon2= -1;
//	int limit, i;
//	FILE *out_fp= fopen(out_file, "w");
//	FILE *fp = fopen(fm_file, "r");
//	char *readline  = (char*)malloc(MAX_LINE);
//	while( NULL != fgets(readline, MAX_LINE, fp) )
//	{
//		if ( 0 == strncmp(readline, "up_", 3) )	// first entry of header: "up_gene" in *.trans
//			{continue;}
//		token_array = splitString(readline, '\t');
//		//rand = token_array[4];
//		chr1 = token_array[2];
//		chr2 = token_array[9];
//		gene1 = token_array[0];
//		gene2= token_array[7];
//		trans_name1 = token_array[1];
//		trans_name2 = token_array[8];
//		pos1 = atoi(token_array[5].c_str());
//		pos2 = atoi(token_array[12].c_str());
//		strand1 = token_array[3];
//		strand2 = token_array[10];
//		//new_fusion = chr1 + "_" + token_array[5] + "_" + chr2 + "_" + token_array[12];
//		new_fusion = gene1 + "_" + token_array[5] + "_" + gene2 + "_" + token_array[12];
//		if ( new_fusion != current_fusion )
//		{
//			L("New Record:%d %s %s \n%s", count, current_fusion.c_str() , new_fusion.c_str(), readline);
//			t1_it = enst_table.find(trans_name1);
//			if ( enst_table.end() == t1_it)
//			{ L("Error in Trans1_Name:%s\n", trans1.c_str());}
//			trans1 = t1_it->second;
//			t2_it = enst_table.find(trans_name2);
//			if ( enst_table.end() == t2_it)
//			{ L("Error in Trans2_Name:%s\n", trans2.c_str());}
//			trans2 = t2_it->second;
//
//			p1_it = PTMap.find(trans1);
//			p2_it = PTMap.find(trans2);
//			if ( PTMap.end() == p1_it || PTMap.end() == p2_it)
//			{
//				E("Error in %s: cannot locate transcript %s or %s\n", token_array[0].c_str(), trans1.c_str(), trans2.c_str());
//				continue;
//			}
//			// 5' gene
//			//if ( 0 == strcmp( '+', strand[0]) ) // go to left
//			if ( "+" == strand1 ) // go to smaller coor
//			{
//				locate_segment(trans1, pos1, read_length, p1_it->second, end_point, 0);
//				limit = (int)end_point.size();
//				i = limit-1;
//				while(0 <= i)
//				{
//					fprintf(out_fp, "%s\t0\t%s\t+\t%d\t%d\n", new_fusion.c_str(), chr1.c_str(), end_point[i], end_point[i-1] );
//					i -= 2;
//				}
//
//			}
//			else
//			{
//				locate_segment(trans1, pos1, read_length, p1_it->second, end_point, 1);
//				limit = (int)end_point.size();
//				i = 0;
//				while( i < limit )
//				{
//					fprintf(out_fp, "%s\t0\t%s\t-\t%d\t%d\n", new_fusion.c_str(), chr1.c_str(), end_point[i], end_point[i+1] );
//					i += 2;
//				}
//			}
//			// 3' gene
//			//if ( 0 == strcmp( '+', strand[1] ))
//			if ( "+"== strand2 ) // go to larger coor
//			{
//				locate_segment(trans2, pos2, read_length, p2_it->second, end_point, 1);
//				limit = (int)end_point.size();
//				i = 0;
//				while( i < limit )
//				{
//					fprintf(out_fp, "%s\t1\t%s\t-\t%d\t%d\n", new_fusion.c_str(), chr2.c_str(), end_point[i], end_point[i+1] );
//					i += 2;
//				}
//			}
//			else
//			{
//				locate_segment(trans2, pos2, read_length, p2_it->second, end_point, 0);
//				limit = (int)end_point.size();
//				i = limit-1;
//				while(0 <= i)
//				{
//					fprintf(out_fp, "%s\t1\t%s\t+\t%d\t%d\n", new_fusion.c_str(), chr2.c_str(), end_point[i], end_point[i-1] );
//					i -= 2;
//				}
//			}
//
//
//			current_fusion = new_fusion;
//		}
//
//		//check_ori = gene1+"->"+gene2;
//		//if (check_ori != token_array[18])
//		//{
//		//	E("Warning: Wrong Orientation\t%sn", token_array[0].c_str());
//		//}
//
//		//trans1 = vec_trans1[0];
//		//trans2 = vec_trans2[0];
//
//		//p1_it = PTMap.find(trans1);
//		//p2_it = PTMap.find(trans2);
//		//if ( PTMap.end() == p1_it || PTMap.end() == p2_it)
//		//{
//		//	E("Error in %s: cannot locate transcript %s or %s\n", token_array[0].c_str(), trans1.c_str(), trans2.c_str());
//		//	continue;
//		//}
//		//// 5' gene
//		////if ( 0 == strcmp( '+', strand[0]) ) // go to left
//		//if ( '+' == strand[0] ) // go to smaller coor
//		//{
//		//	locate_segment(trans1, pos1, read_length, p1_it->second, end_point, 0);
//		//	limit = (int)end_point.size();
//		//	i = limit-1;
//		//	while(0 <= i)
//		//	{
//		//		fprintf(out_fp, "%s\t0\t%s\t+\t%d\t%d\n", token_array[0].c_str(), chr1.c_str(), end_point[i], end_point[i-1] );
//		//		i -= 2;
//		//	}
//
//		//}
//		//else
//		//{
//		//	locate_segment(trans1, pos1, read_length, p1_it->second, end_point, 1);
//		//	limit = (int)end_point.size();
//		//	i = 0;
//		//	while( i < limit )
//		//	{
//		//		fprintf(out_fp, "%s\t0\t%s\t-\t%d\t%d\n", token_array[0].c_str(), chr1.c_str(), end_point[i], end_point[i+1] );
//		//		i += 2;
//		//	}
//		//}
//		//// 3' gene
//		////if ( 0 == strcmp( '+', strand[1] ))
//		//if ( '+'== strand[1] ) // go to larger coor
//		//{
//		//	locate_segment(trans2, pos2, read_length, p2_it->second, end_point, 1);
//		//	i = 0;
//		//	while( i < limit )
//		//	{
//		//		fprintf(out_fp, "%s\t1\t%s\t+\t%d\t%d\n", token_array[0].c_str(), chr2.c_str(), end_point[i], end_point[i+1] );
//		//		i += 2;
//		//	}
//		//}
//		//else
//		//{
//		//	locate_segment(trans2, pos2, read_length, p2_it->second, end_point, 0);
//		//	limit = (int)end_point.size();
//		//	i = limit-1;
//		//	while(0 <= i)
//		//	{
//		//		fprintf(out_fp, "%s\t1\t%s\t+\t%d\t%d\n", token_array[0].c_str(), chr2.c_str(), end_point[i], end_point[i-1] );
//		//		i -= 2;
//		//	}
//		//}
//
//
//		// Sanity Checking
//		//t_it = gene_table.find(gene1);
//		//if (gene_table.end() != t_it)
//		//{
//		//	if ( vec_trans1.size() != t_it->second.size() )
//		//	{
//		//		E("Warning: Inconsist Trans Num for %s in %s:%d\t%d\n",gene1.c_str(),  token_array[0].c_str(), (int)vec_trans1.size(),  (int)t_it->second.size());
//		//	}
//		//}
//		//else
//		//{
//		//	E("Warning: Gene Missing:%s\n", gene1.c_str());
//		//}
//		//E("Num_Entry\t%d\n", (int)token_array.size());
//		count++;
//	}
//	fclose(fp);
//	fclose(out_fp);
//}
///**********************************************/
//void ParseFusionMap( char *fm_file, char *out_file, map<string, GENEPosition> &PTMap, map<string, vector<string> > &gene_table , int read_length)
//{
//	vector<string> token_array;
//
//	string strand;
//	string chr1, chr2,
//		gene1, gene2,
//		trans1, trans2;
//	vector<string> vec_trans1, vec_trans2;
//	map<string, vector<string> >::iterator t_it;
//	map<string, GENEPosition>::iterator p1_it, p2_it;
//	vector<ExonPosition> vec_ep1, vec_ep2;
//	vector<int> end_point;
//
//	string check_ori;
//	int pos1=0, pos2=0;
//	//int dir_flag = 0;	//int exon1 = -1, exon2= -1;
//	int limit=0, i=0;
//	FILE *out_fp= fopen(out_file, "w");
//	FILE *fp = fopen(fm_file, "r");
//	char *readline  = (char*)malloc(MAX_LINE);
//	while( NULL != fgets(readline, MAX_LINE, fp) )
//	{
//		if ( 0 == strncmp(readline, "Fus", 3) )
//			{continue;}
//		token_array = splitString(readline, '\t');
//		strand = token_array[4];
//		chr1 = token_array[5];
//		chr2 = token_array[7];
//		gene1 = token_array[9];
//		gene2= token_array[13];
//		vec_trans1 = splitStringOld( token_array[10], ',');
//		vec_trans2 = splitStringOld( token_array[14], ',');
//		pos1 = atoi(token_array[6].c_str());
//		pos2 = atoi(token_array[8].c_str());
//		check_ori = gene1+"->"+gene2;
//		if (check_ori != token_array[18])
//		{
//			E("Warning: Wrong Orientation\t%sn", token_array[0].c_str());
//		}
//
//		trans1 = vec_trans1[0];
//		trans2 = vec_trans2[0];
//
//		p1_it = PTMap.find(trans1);
//		p2_it = PTMap.find(trans2);
//		if ( PTMap.end() == p1_it || PTMap.end() == p2_it)
//		{
//			E("Error in %s: cannot locate transcript %s or %s\n", token_array[0].c_str(), trans1.c_str(), trans2.c_str());
//			continue;
//		}
//		// 5' gene
//		//if ( 0 == strcmp( '+', strand[0]) ) // go to left
//		if ( '+' == strand[0] ) // go to smaller coor
//		{
//			locate_segment(trans1, pos1, read_length, p1_it->second, end_point, 0);
//			limit = (int)end_point.size();
//			i = limit-1;
//			while(0 <= i)
//			{
//				fprintf(out_fp, "%s\t0\t%s\t+\t%d\t%d\n", token_array[0].c_str(), chr1.c_str(), end_point[i], end_point[i-1] );
//				i -= 2;
//			}
//
//		}
//		else if ('-' == strand[0])
//		{
//			locate_segment(trans1, pos1, read_length, p1_it->second, end_point, 1);
//			limit = (int)end_point.size();
//			i = 0;
//			while( i < limit )
//			{
//				fprintf(out_fp, "%s\t0\t%s\t-\t%d\t%d\n", token_array[0].c_str(), chr1.c_str(), end_point[i], end_point[i+1] );
//				i += 2;
//			}
//		}
//		else
//		{
//			E("WrongStrand:%s\n", token_array[0].c_str());
//		}
//		// 3' gene
//		//if ( 0 == strcmp( '+', strand[1] ))
//		if ( '+'== strand[1] ) // go to larger coor
//		{
//			locate_segment(trans2, pos2, read_length, p2_it->second, end_point, 1);
//			limit = (int)end_point.size();
//			i = 0;
//			while( i < limit )
//			{
//				fprintf(out_fp, "%s\t1\t%s\t-\t%d\t%d\n", token_array[0].c_str(), chr2.c_str(), end_point[i], end_point[i+1] );
//				i += 2;
//			}
//		}
//		else if ('-'==strand[1])
//		{
//			locate_segment(trans2, pos2, read_length, p2_it->second, end_point, 0);
//			limit = (int)end_point.size();
//			i = limit-1;
//			while(0 <= i)
//			{
//				fprintf(out_fp, "%s\t1\t%s\t+\t%d\t%d\n", token_array[0].c_str(), chr2.c_str(), end_point[i], end_point[i-1] );
//				i -= 2;
//			}
//		}
//		else
//		{
//			E("WrongStrand:%s\n", token_array[0].c_str());
//		}
//
//
//		// Sanity Checking
//		//t_it = gene_table.find(gene1);
//		//if (gene_table.end() != t_it)
//		//{
//		//	if ( vec_trans1.size() != t_it->second.size() )
//		//	{
//		//		E("Warning: Inconsist Trans Num for %s in %s:%d\t%d\n",gene1.c_str(),  token_array[0].c_str(), (int)vec_trans1.size(),  (int)t_it->second.size());
//		//	}
//		//}
//		//else
//		//{
//		//	E("Warning: Gene Missing:%s\n", gene1.c_str());
//		//}
//		//E("Num_Entry\t%d\n", (int)token_array.size());
//
//	}
//	fclose(fp);
//	fclose(out_fp);
//}
//
//
//
//// Determine the segments upstream
//// TODO: check if it make sense for ext_length > 0
//void locate_segment( string &trans, int pos, int read_length, GENEPosition &gp, vector<int> &end_point, int direction)
//{
//	// direction: 0 for going left, 1 for going right
//	end_point.clear();
//
//	vector<ExonPosition> vec_ep = gp.epos;
//	L("Exon Num for %s is %d\n", trans.c_str(), (int)gp.epos.size());
//	int e_i = find_exon( pos, vec_ep );
//	if ( -1 == e_i)
//	{
//		E("Cannot Find Exon for %d in %s\n", pos, trans.c_str());
//		exit(EXIT_FAILURE);
//	}
//	//int l_diff = pos-vec_ep[e_i].start;
//	//int r_diff = vec_ep[e_i].end-pos;
//	int ext_length = read_length;
//	int tmp_ext = 0;
//	int limit = (int)vec_ep.size();
//	L("S:\t%d in %d of %s:%d %d\n", pos, e_i, trans.c_str(), vec_ep[e_i].start, vec_ep[e_i].end);
//	//if ( (read_length >l_diff) || (read_length>r_diff))
//	//{
//	//	L("D:\t%d in %d of %s:%d %d\n", pos, e_i, trans.c_str(), vec_ep[e_i].start, vec_ep[e_i].end);
//	//}
//	int cur_i = e_i; // index of splitting exon
//	int cur_c = 0; // counter of STEP
//	// go forward rl
//	int l_end, r_end;
//	if ( 1 == direction )
//	{
//		l_end = pos;
//		//while( 0 < ext_length ) // need to extend so many bases
//		while( (0 < ext_length) && (cur_i < limit) ) // need to extend so many bases
//		{
//			r_end = l_end + ext_length - 1;
//			if ( r_end <= vec_ep[cur_i].end ) // end in this exon
//			{
//				ext_length = 0;
//			}
//			else
//			{
//				r_end = vec_ep[cur_i].end;
//				tmp_ext = r_end - l_end + 1;
//				ext_length -= tmp_ext;
//			}
//			end_point.push_back(l_end);
//			end_point.push_back(r_end);
//			L("Add %d_%d: %d %d from %d %d; %d to extend\n", direction,cur_c, l_end, r_end, vec_ep[cur_i].start, vec_ep[cur_i].end, ext_length);
//			cur_i++;
//			cur_c++;
//			l_end = vec_ep[cur_i].start;
//		}
//
//		if ( 0 < ext_length){L("NotEnd %s %d %d: ends in %d %d; %d to extend\n", trans.c_str(), pos, direction, l_end, r_end, ext_length);}
//
//	}
//	else // go upstream; coordinates will be in decreasing/REVERSED order
//	{
//		r_end = pos;
//		//while( 0 < ext_length ) //still need to decrease so many bases
//		while( ( 0 < ext_length) && ( 0 <= cur_i ) ) //still need to decrease so many bases
//		{
//			l_end = r_end - ext_length + 1;
//			if ( vec_ep[cur_i].start <= l_end) // end in this exon
//			{
//				ext_length = 0;
//			}
//			else
//			{
//				l_end = vec_ep[cur_i].start;
//				tmp_ext = r_end - l_end + 1;
//				ext_length -= tmp_ext;
//			}
//			//end_point.push_back(l_end);
//			end_point.push_back(r_end);
//			end_point.push_back(l_end);
//			L("Add %d_%d: %d %d from %d %d; %d to extend\n", direction,cur_c, l_end, r_end, vec_ep[cur_i].start, vec_ep[cur_i].end, ext_length);
//			cur_i--; // To fit the structure in gtf reader
//			cur_c++;
//			r_end = vec_ep[cur_i].end;
//		}
//		if ( 0 < ext_length){L("NotEnd %s %d %d: ends in %d %d; %d to extend\n", trans.c_str(), pos, direction, l_end, r_end, ext_length);}
//	}
//
//}
///**********************************************/
//// Determine the exon containing breakpoint
//int find_exon( int pos, vector<ExonPosition> &vec_ep )
//{
//	int limit = (int)vec_ep.size();
//	int i = 0;
//	int hit = -1;
//	while( i < limit)
//	{
//		if ( (vec_ep[i].start <= pos) && ( pos <= vec_ep[i].end))
//		{
//			hit = i;
//		}
//		++i;
//	}
//	return hit;
//}
