#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <iostream>
#include <fstream>
#include <string>
#include <map>

#include "Fusion.h"
#include "Common.h"
//#include "GeneAnnotation.h"

#define FASTA_LINE 50

using namespace std;
// Get peptides from deFuse prediction results
/**********************************************/
void ConvertToProteins( char *pred_file, char *out_file, char *lib_name, int pep_length, int mode )
{
	vector<string> token_array,
					fusion_array,
					protein_array,
					fragment_array;

	string total_seq="", total_rc="";
	int left_i = 0, right_i = 0, split_i = 0; // 1-based system
	int frame_num = 0, pep_num = 0;
	int orient = 0;
	int line_count = 0;
	int dist = 0; // dist to breakpoint
	int cond =0; // condition flag
	FILE *fp = fopen( pred_file, "r");
	FILE *out_fp= fopen( out_file, "w");
	char *readline=(char*)malloc(MAX_LINE);
	while( NULL != fgets(readline, MAX_LINE, fp) )
	{

		if( 0 == strncmp("cluster", readline, 7 ) ) // header line in tsv file
		{
			continue;
		}
		
		token_array = splitString( readline, '\t' );
		fusion_array = splitStringOld( token_array[1], '|');
		left_i = (int)fusion_array[0].size();
		right_i = (int)fusion_array[1].size();
		if ( 0 < right_i )
		{
			total_seq = fusion_array[0] + fusion_array[1];
		}

		//L("Number of Token\t%d\n", (int)fusion_array.size());
		L("Fusion %s\n", total_seq.c_str());
		//DNATranslate(fusion_array[1]);
		// Orient 1: 5->3
		orient = 1;
		DNATranslate(total_seq, protein_array,  orient );
		split_i = left_i;
		frame_num = (int)protein_array.size(); 
		for( int i =0; i < frame_num; i ++ )
		{
			fragment_array = splitStringOld( protein_array[i], '-');
			pep_num = (int) fragment_array.size();
			L(">%d-%d\n%s\n",orient,  i, protein_array[i].c_str());
			int acc_l = i+1, acc_r = 0, tmp_len=0; // 1-based accumulated length
			cond = 0; // reset for this frame
			for( int j = 0; j < pep_num; j++)
			{
				tmp_len=(int)fragment_array[j].size();
				acc_r = acc_l + 3*tmp_len -1;
				if ( pep_length < tmp_len ) // actual peptide
				{
					dist = bp_dist(acc_l, acc_r, split_i);
					if ( mode <= 1)
					{
							fprintf( out_fp, ">%s_%s_%d_%d_%d\n", lib_name, token_array[0].c_str(), orient, i+1, dist );
							fprintf( out_fp, "%s\n", fragment_array[j].c_str());
					}
					else
					{
						size_t found = fragment_array[j].find('M');
						if (found != std::string::npos)
						{
							fprintf( out_fp, ">%s_%s_%d_%d_%d\n", lib_name, token_array[0].c_str(), orient, i+1, dist );
							fprintf( out_fp, "%s\n", fragment_array[j].substr(found).c_str());
							cond = 1;
						}
					}
					
					L("=%d %d %d %d %d %s\n", j, acc_l, acc_r, split_i, dist, fragment_array[j].c_str());
					if ( 1 == mode) 
						{ break; } // only need the first peptide
					if ( (3 == mode) && (1 ==cond))
						{ break;}
				}
				else
				{
					L("=%d %d %d %d %s\n", j, acc_l, acc_r, split_i,  fragment_array[j].c_str());
				}
				acc_l = acc_r + 4;
			}
		}
		//DNATranslate(fusion_array[0], 1);
		// Orient 0: 3->5
		make_rc( total_seq, total_rc);
		orient = 0;
		DNATranslate(total_rc, protein_array,  orient);
		split_i = right_i;
		frame_num = (int)protein_array.size(); 
		for( int i =0; i < frame_num; i ++ )
		{
			fragment_array = splitStringOld( protein_array[i], '-');
			pep_num = (int) fragment_array.size();
			L(">%d-%d\n%s\n",orient,  i, protein_array[i].c_str());
			int acc_l = i+1, acc_r = 0, tmp_len=0; // 1-based accumulated length
			cond = 0; // reset for this frame
			for( int j = 0; j < pep_num; j++)
			{
				tmp_len=(int)fragment_array[j].size();
				acc_r = acc_l + 3*tmp_len -1;
				if ( pep_length < tmp_len ) // actual peptide
				{
					dist = bp_dist(acc_l, acc_r, split_i);
					if ( mode <= 1)
					{
							fprintf( out_fp, ">%s_%s_%d_%d_%d\n", lib_name, token_array[0].c_str(), orient, i+1, dist );
							fprintf( out_fp, "%s\n", fragment_array[j].c_str());
					}
					else
					{
						size_t found = fragment_array[j].find('M');
						if (found != std::string::npos)
						{
							fprintf( out_fp, ">%s_%s_%d_%d_%d\n", lib_name, token_array[0].c_str() , orient, i+1, dist );
							fprintf( out_fp, "%s\n", fragment_array[j].substr(found).c_str());
							cond = 1;
						}
					}
					
					L("=%d %d %d %d %d %s\n", j, acc_l, acc_r, split_i, dist, fragment_array[j].c_str());
					if ( 1 == mode) 
						{ break; } // only need the first peptide
					if ( (3 == mode) && (1 ==cond))
						{ break;}
				}
				else
				{
					L("=%d %d %d %d %s\n", j, acc_l, acc_r, split_i,  fragment_array[j].c_str());
				}
				acc_l = acc_r + 4;
			}
			//fragment_array = splitStringOld( protein_array[i], '-');
			//pep_num = (int) fragment_array.size();
			//L(">%d-%d\n%s\n",orient,  i, protein_array[i].c_str());
			//int acc_l = i+1, acc_r = 0, tmp_len=0; // 1-based accumulated length
			//for( int j = 0; j < pep_num; j++)
			//{
			//	tmp_len=(int)fragment_array[j].size();
			//	acc_r = acc_l + 3*tmp_len -1;
			//	if ( pep_length < tmp_len )
			//	{
			//		dist = bp_dist(acc_l, acc_r, split_i);
			//		fprintf( out_fp, ">%s_%d_%d_%d_%d\n", lib_name, line_count, orient, i+1, dist );
			//		fprintf( out_fp, "%s\n", fragment_array[j].c_str());
			//		L("=%d %d %d %d %d %s\n", j, acc_l, acc_r, split_i, dist, fragment_array[j].c_str());
			//	}
			//	L("=%d %d %d %d %s\n", j, acc_l, acc_r, split_i,  fragment_array[j].c_str());
			//	acc_l = acc_r + 4;
			//}
		}

		line_count++;
	}
	fclose(fp);
	fclose(out_fp);
}


/**********************************************/
void DNATranslate( const string &g_seq, vector<string> &trans_vec,  int flag)//, int left_i, int right_i )
{
	trans_vec.clear();
	//pos_vec.clear();
	int limit= (int)g_seq.size();
	int last = limit -2; // last starting position in 1-based system
	//int split_i = left_i - 1; // location of the breakpoint
	//if ( 0 == flag) // reverse
	//	{ split_i = right_i;}



	string trigram;
	string tmp_str;
	//char aa;
	for( int start =0; start < 3; start++) // three reading frames
	{
		tmp_str = "";
		for( int i=start; i < last ; i+=3 )
		{
			trigram = g_seq.substr(i,3);
			tmp_str.push_back( ToProtein(trigram) );
			//if ( (i < split_i) && ())
			//{
			//}

			//L("%d: %s %c F%d %d %d\n", i, trigram.c_str(), aa, start, last, limit);
		}
		L("%d-%d:%s\n", flag, start, tmp_str.c_str() );
		trans_vec.push_back(tmp_str);
	}
}

///**********************************************/
//void make_rc( const string &raw_str, string &new_str)
//{
//	new_str = "";
//	int limit = (int)raw_str.size();
//	for( int i =0; i < limit; i++ )
//	{
//		switch( raw_str[limit-1-i])
//		{
//			case 'A':
//				new_str.push_back('T');
//				break;
//			case 'C':
//				new_str.push_back('G');
//				break;
//			case 'G':
//				new_str.push_back('C');
//				break;
//			case 'T':
//				new_str.push_back('A');
//				break;
//			default:
//				new_str.push_back('N');
//		}
//	}
//}



/**********************************************/
// assume left <= right
// change the the indexof aa
int bp_dist( int left, int right, int bp)
{
	int dist = 0;
	int tmp_bp = 0;
	if ( left <= bp)
	{
		if (  right < bp)
		{
			dist = right - bp;
		}
		else
		{
			tmp_bp = bp+1;
			dist =1 + (tmp_bp -left)/3;
			//if ( 2 == (tmp_bp-left)%3 )
			//{
			//	dist =(tmp_bp -left)/3;
			//}
			//else
			//{
			//	dist =1 + (tmp_bp -left)/3;
			//}
		}
	}
	else
	{
		dist = left -bp;
	}
	return dist;
}

/**********************************************/
char ToProtein ( const string &trigram)
{
	map<string, char> codon;
	codon["TTT"]='F';
	codon["TTC"]='F';
	codon["TTA"]='L';
	codon["TTG"]='L';
	codon["CTT"]='L';
	codon["CTC"]='L';
	codon["CTA"]='L';
	codon["CTG"]='L';
	codon["ATT"]='I';
	codon["ATC"]='I';
	codon["ATA"]='I';
	codon["ATG"]='M';
	codon["GTT"]='V';
	codon["GTC"]='V';
	codon["GTA"]='V';
	codon["GTG"]='V';
	codon["TCT"]='S';
	codon["TCC"]='S';
	codon["TCA"]='S';
	codon["TCG"]='S';
	codon["CCT"]='P';
	codon["CCC"]='P';
	codon["CCA"]='P';
	codon["CCG"]='P';
	codon["ACT"]='T';
	codon["ACC"]='T';
	codon["ACA"]='T';
	codon["ACG"]='T';
	codon["GCT"]='A';
	codon["GCC"]='A';
	codon["GCA"]='A';
	codon["GCG"]='A';
	codon["TAT"]='Y';
	codon["TAC"]='Y';
	codon["TAA"]='-';
	codon["TAG"]='-';
	codon["CAT"]='H';
	codon["CAC"]='H';
	codon["CAA"]='Q';
	codon["CAG"]='Q';
	codon["AAT"]='N';
	codon["AAC"]='N';
	codon["AAA"]='K';
	codon["AAG"]='K';
	codon["GAT"]='D';
	codon["GAC"]='D';
	codon["GAA"]='E';
	codon["GAG"]='E';
	codon["TGT"]='C';
	codon["TGC"]='C';
	codon["TGA"]='-';
	codon["TGG"]='W';
	codon["CGT"]='R';
	codon["CGC"]='R';
	codon["CGA"]='R';
	codon["CGG"]='R';
	codon["AGT"]='S';
	codon["AGC"]='S';
	codon["AGA"]='R';
	codon["AGG"]='R';
	codon["GGT"]='G';
	codon["GGC"]='G';
	codon["GGA"]='G';
	codon["GGG"]='G';
	map< string, char >::iterator it;
	it = codon.find(trigram);
	char aa;
	if ( it != codon.end() )
	{
		aa = (*it).second;
	}
	else
	{
		aa = '*';
		E("Warning in %s\n", trigram.c_str());
	}
	return aa;
}

/*********************************************/
void SummarizeFasta( char *pred_file, char *out_file )
{
	vector<string> token_array;
	map<string, vector<tr_length> >::iterator it;
	string t_id;
	//int reg_num = 0, // the number of gene regions for a single id
	//	f_id, // the index for multi-region transcript
	//	f_length; // length of the transcript
	FILE *fp = fopen(pred_file, "r");
	FILE *out_fp = fopen(out_file, "w");
	int count=0;
	int seq_length = 0, total_length = 0 ;


	char *readline  = (char*)malloc(MAX_LINE);
	char *seqname = (char*)malloc(MAX_LINE);

	while( NULL != fgets(readline, MAX_LINE, fp) )
	{
		if( 0 == strncmp(">", readline, 1 ) )
		{
			strcpy( seqname, readline);
			//continue;
		}
		else
		{
			seq_length = (int)strlen(readline);
			count++;
			fprintf( out_fp, "%d\t%d\n", count, seq_length);
			//count++;
			total_length += seq_length;
		}

	}
	E("Summary: %d seq with %f avg aa length.\n", count, total_length*1.0/count);
	fclose( out_fp);
	fclose( fp );

}

/*********************************************/
void ExtractPeptideFromFasta( char *pred_file, char *out_file )
{
	vector<string> header_array;
	vector<string> token_array;
	map<string, vector<tr_length> >::iterator it;
	string t_id;
	//int reg_num = 0, // the number of gene regions for a single id
	//	f_id, // the index for multi-region transcript
	//	f_length; // length of the transcript
	FILE *fp = fopen(pred_file, "r");
	FILE *out_fp = fopen(out_file, "w");
	int count=0;
	int pos = 0;
	int left = 0, right = 0;
	int seq_length = 0, total_length = 0 ;
	int bp_pos =0;
	int i =0;
	int new_bp = 0;

	char *readline  = (char*)malloc(MAX_LINE);
	char *seqname = (char*)malloc(MAX_LINE);
	string final_str;
	while( NULL != fgets(readline, MAX_LINE, fp) )
	{
		if( 0 == strncmp(">", readline, 1 ) )
		{
			header_array = splitString(readline, '_');
			bp_pos = atoi( header_array[4].c_str() );
			E("%d %s_%s_%s_%s_%s\n", bp_pos, header_array[0].c_str(), header_array[1].c_str(), header_array[2].c_str(), header_array[3].c_str(), header_array[4].c_str() );
			//continue;
		}
		else
		{
			if ( 0 > bp_pos )
			{
			E("NO_BP\t%s_%s_%s_%s_%s\n", header_array[0].c_str(), header_array[1].c_str(), header_array[2].c_str(), header_array[3].c_str(), header_array[4].c_str() );
			}
			else
			{
				seq_length = (int)strlen(readline);
				left = -1;
				right = -1;
				E("BP\t%d\t%s_%s_%s_%s_%s\n", seq_length,  header_array[0].c_str(), header_array[1].c_str(), header_array[2].c_str(), header_array[3].c_str(), header_array[4].c_str() );
				E("%s", readline);	
				for( i = 0; i < seq_length; i ++)
				{
					if ('K' == readline[i] || 'R' == readline[i] )
					{
						if ( i <  bp_pos -1 ) // 0-based vs 1-based
						{
							left = i+1;
							E("%d\n", left);	
						}
						else if ( bp_pos -1  <= i) // 0-based vs 1-based 
						{
							right = i+1;
							E("%d\n", right);	
							E("%d\t%d\n", left, right);	
							break;
						}
					}
				}
				if ( -1 == right )
				{
					right = seq_length-1;
					E("CHR to %d\n", right);
				}
				if ( -1 == left)
				{
					left = 1;// why not ZERO?
					E("CHL to %d\n", left);
				}
				//left + 1 to right -1
				if ( left < right )
				{
					final_str ="";
					for( i = left; i <= right-1; i++ )
					{
						final_str += readline[i];
					}
					new_bp = bp_pos - left; // 1-based bp in the new sequence
					E("SEQ\t%d\t%s\n",new_bp, final_str.c_str());
					fprintf( out_fp, "%s_%s_%s_%s_%s_%d\n", header_array[0].c_str(), header_array[1].c_str(), header_array[2].c_str(), header_array[3].c_str(), header_array[4].c_str(), new_bp );
					fprintf(out_fp, "%s\n", final_str.c_str());
				}
				else
				{
					E("BAD_BP\t%s_%s_%s_%s_%s\t%d\t%d\n", header_array[0].c_str(), header_array[1].c_str(), header_array[2].c_str(), header_array[3].c_str(), header_array[4].c_str(), left, right );
				}
			}

		}

	}
	//E("Summary: %d seq with %f avg aa length.\n", count, total_length*1.0/count);
	fclose( out_fp);
	fclose( fp );

}

/*********************************************/
// Let's allow for 1 mis-cleavage
void ExtractPeptideFromFasta_Mis( char *pred_file, char *out_file )
{
	vector<string> header_array;
	vector<string> token_array;
	map<string, vector<tr_length> >::iterator it;
	string t_id;
	//int reg_num = 0, // the number of gene regions for a single id
	//	f_id, // the index for multi-region transcript
	//	f_length; // length of the transcript
	FILE *fp = fopen(pred_file, "r");
	FILE *out_fp = fopen(out_file, "w");
	int count=0;
	int pos = 0;
	int l1=0, l2 =0;
	int r1 = 0, r2 = 0;
	//int left = 0, right = 0;
	int seq_length = 0, total_length = 0 ;
	int bp_pos =0;
	int i =0;
	int new_bp = 0;
	int status = 0;
	vector<int> l_vec, r_vec;

	char *readline  = (char*)malloc(MAX_LINE);
	char *seqname = (char*)malloc(MAX_LINE);
	string final_str;
	while( NULL != fgets(readline, MAX_LINE, fp) )
	{
		if( 0 == strncmp(">", readline, 1 ) )
		{
			header_array = splitString(readline, '_');
			bp_pos = atoi( header_array[4].c_str() );
			E("%d %s_%s_%s_%s_%s\n", bp_pos, header_array[0].c_str(), header_array[1].c_str(), header_array[2].c_str(), header_array[3].c_str(), header_array[4].c_str() );
			//continue;
		}
		else// sequence part
		{
			if ( 0 > bp_pos )
			{
			E("NO_BP\t%s_%s_%s_%s_%s\n", header_array[0].c_str(), header_array[1].c_str(), header_array[2].c_str(), header_array[3].c_str(), header_array[4].c_str() );
			}
			else
			{
				seq_length = (int)strlen(readline);
				l1 = -1, l2 = -1;//left = -1;
				r1 = -1, r2 = -1;//right = -1;
				E("BP\t%d\t%s_%s_%s_%s_%s\n", seq_length,  header_array[0].c_str(), header_array[1].c_str(), header_array[2].c_str(), header_array[3].c_str(), header_array[4].c_str() );
				E("%s", readline);	
				for( i = 0; i < seq_length; i ++)
				{
					if ('K' == readline[i] || 'R' == readline[i] )
					{
						if ( i <  bp_pos -1 ) // 0-based vs 1-based
						{
							l1 = l2;
							l2 = i+1;	//left = i+1;
							E("L\t%d\t%d\n", l1, l2);	
						}
						else if ( bp_pos -1  <= i) // 0-based vs 1-based 
						{
							if ( -1 == r1 )
							{
								r1 = i+1;
								E("R1\t%d\n", r1);	
							}
							else
							{
								r2 = i+1;
								E("R2\t%d\t%d\n", r1, r2);	
								break;
							}
						}
					}

				}
				E("ALL\t%d\t%d\t%d\t%d\t%d\n", l1, l2, bp_pos, r1, r2);
				l_vec.clear();
				r_vec.clear();

				if ( -1 == l1)
				{
					l1 = 0;
					E("CHL to %d\n", l1); 
					// we don't care if l2 is -1
				}
				if ( -1 == r2 )
				{
					r2 = seq_length-1;
					E("CHR to %d\n", r2);
					//don't care if r1 is -1
				}
				final_str="";
				status = generate_peptide(final_str, l1, r1, readline);
				if ( 1 == status )
				{
					new_bp = bp_pos - l1; // 1-based bp in the new sequence
					E("SEQ-11\t%d\t%s\n",new_bp, final_str.c_str());
					fprintf( out_fp, "%s_%s_%s_%s_%s_0_%d\n", header_array[0].c_str(), header_array[1].c_str(), header_array[2].c_str(), header_array[3].c_str(), header_array[4].c_str(), new_bp );
					fprintf(out_fp, "%s\n", final_str.c_str());
				}
			
				if ( -1 == l2 )
				{
					final_str="";
					status = generate_peptide(final_str, l1, r2, readline);
					if ( 1 == status )
					{
						new_bp = bp_pos - l1; // 1-based bp in the new sequence
						E("SEQ-12\t%d\t%s\n",new_bp, final_str.c_str());
						fprintf( out_fp, "%s_%s_%s_%s_%s_1_%d\n", header_array[0].c_str(), header_array[1].c_str(), header_array[2].c_str(), header_array[3].c_str(), header_array[4].c_str(), new_bp );
						fprintf(out_fp, "%s\n", final_str.c_str());
					}
				}
				
				final_str="";
				status = generate_peptide(final_str, l2, r1, readline);
				if ( 1 == status )
				{
					new_bp = bp_pos - l2; // 1-based bp in the new sequence
					E("SEQ-21\t%d\t%s\n",new_bp, final_str.c_str());
					fprintf( out_fp, "%s_%s_%s_%s_%s_2_%d\n", header_array[0].c_str(), header_array[1].c_str(), header_array[2].c_str(), header_array[3].c_str(), header_array[4].c_str(), new_bp );
					fprintf(out_fp, "%s\n", final_str.c_str());
				}

				final_str="";
				status = generate_peptide(final_str, l2, r2, readline);
				if ( 1 == status )
				{
					new_bp = bp_pos - l2; // 1-based bp in the new sequence
					E("SEQ-22\t%d\t%s\n",new_bp, final_str.c_str());
					fprintf( out_fp, "%s_%s_%s_%s_%s_3_%d\n", header_array[0].c_str(), header_array[1].c_str(), header_array[2].c_str(), header_array[3].c_str(), header_array[4].c_str(), new_bp );
					fprintf(out_fp, "%s\n", final_str.c_str());
				}
				//if ( left < right )
				//{
				//	final_str ="";
				//	for( i = left; i <= right-1; i++ )
				//	{
				//		final_str += readline[i];
				//	}
				//	new_bp = bp_pos - left; // 1-based bp in the new sequence
				//	E("SEQ\t%d\t%s\n",new_bp, final_str.c_str());
				//	fprintf( out_fp, "%s_%s_%s_%s_%s_%d\n", header_array[0].c_str(), header_array[1].c_str(), header_array[2].c_str(), header_array[3].c_str(), header_array[4].c_str(), new_bp );
				//	fprintf(out_fp, "%s\n", final_str.c_str());
				//}
				//else
				//{
				//	E("BAD_BP\t%s_%s_%s_%s_%s\t%d\t%d\n", header_array[0].c_str(), header_array[1].c_str(), header_array[2].c_str(), header_array[3].c_str(), header_array[4].c_str(), left, right );
				//}
			}

		}

	}
	//E("Summary: %d seq with %f avg aa length.\n", count, total_length*1.0/count);
	fclose( out_fp);
	fclose( fp );

}
int generate_peptide( string &final_str, int left, int right, char *raw_str)
{
	int status = -1;
	int length = 0;
	if ( (-1 < left) && (-1 < right ) )
	{
		length = right - left;
		if ( 5 <= length )
		{
			status = 1; // we need to generate string
			for( int i =left; i <= right -1; i++)
			{
				final_str += raw_str[i];
			}
		}
		else{E("TOOSHORT\t%d\t%d\n", left, right);}

	}
	else{E("BAD_BP\t%d\t%d\n", left, right);}
	return status;
}
// OBSOLETE
/**********************************************/
//void ConvertToCount( char *pred_file, map<string, vector<tr_length> > &length_map , char *out_file )
//{
//	vector<string> token_array;
//	map<string, vector<tr_length> >::iterator it;
//	string t_id;
//	int reg_num = 0, // the number of gene regions for a single id
//		f_id, // the index for multi-region transcript
//		f_length; // length of the transcript
//	FILE *fp = fopen(pred_file, "r");
//	FILE *out_fp = fopen(out_file, "w");
//	int f_count=0;	
//	float tmp_rpkm =0.0, f_rpkm = 0.0;
//	int seq_depth[12]= {0,0,0, 80000000, 81000000, 82000000, 80001000, 80999000, 82001000, 79999000,81001000, 81999000 };
//	//
//	float factor_vec[12];
//	for( int i = 0; i < 12; i++)
//	{
//		factor_vec[i] = seq_depth[i]*1.0/1000000000;
//	}
//
//
//
//	char *readline  = (char*)malloc(MAX_LINE);
//	while( NULL != fgets(readline, MAX_LINE, fp) )
//	{
//		if( 0 == strncmp("T", readline, 1 ) )
//		{
//			continue;
//		}
//		token_array = splitString(readline, '\t');
//		t_id = token_array[0];
//		it = length_map.find(t_id);
//		if ( it != length_map.end())
//		{
//			reg_num = (int)(*it).second.size();
//			if ( 1 == reg_num) 
//			{
//				f_id = 0;
//				f_length = (*it).second[0].length;
//			}
//			else 
//			{
//				f_id = select_longest_region((*it).second );
//				f_length = (*it).second[f_id].length;
//			}
//			L("OK\t%s\t%d\t%d\t%d\n", t_id.c_str(), f_id, reg_num, f_length );
//			fprintf(out_fp, "%s", t_id.c_str());
//			for( int i = 3; i < 12; i ++)
//			{
//				tmp_rpkm = atof(token_array[i].c_str());
//				f_rpkm = tmp_rpkm * f_length * factor_vec[i];
//				//fprintf(out_fp, "\t%f", f_rpkm);		
//				f_count = (int)(floor(f_rpkm+0.5f));
//				fprintf(out_fp, "\t%d", f_count);		
//
//			}
//			fprintf(out_fp, "\n");		
//		}
//		else
//		{
//			L("NOT_FOUND\t%s\n", t_id.c_str() );
//		}
//
//
//	}
//
//	fclose( out_fp );
//	fclose( fp );
//}
//
///**********************************************/
//int select_longest_region( vector<tr_length> &tr_vec)
//{
//	int id = 0,
//		max_l = 0,
//		limit;
//	limit = (int)tr_vec.size();
//
//	for( int i = 0; i < limit; i ++)
//	{
//		if ( tr_vec[i].length > max_l )
//		{
//			max_l = tr_vec[i].length;
//			id = i;
//		}
//	}
//	return id;
//}
//
///**********************************************/
//void OutputJoinedFusion( char *out_file, vector<fusion> &fusion_vec )
//{
//	int limit=(int)fusion_vec.size();
//	int limit_1, limit_2;
//	int i = 0, j = 0,
//		j1 = 0, j2 =0,
//		loc1 = 0, loc2 = 0,
//		end1, end2; // end1 indicates the smaller location
//	string join_id;
//	vector<int> v_1, v_2;
//
//	FILE *out_fp= fopen(out_file, "w");
//	for( i =0; i < limit; i++ )
//	{
//		j = fusion_vec[i].group;
//		join_id = fusion_vec[j].id;
//		v_1 = fusion_vec[i].coor1;
//		v_2 = fusion_vec[i].coor2;
//
//		// Make sure the ends match in grouped predictions
//		loc1 = v_1[0];
//		loc2 = v_2[0];
//		if (loc1 <= loc2)
//		{
//			end1 = 0;
//			end2 = 1;
//		}
//		else
//		{
//			end1 = 1;
//			end2 = 0;
//		}
//
//		limit_1 = (int)v_1.size();
//		j1 = 0;
//		while( j1 < limit_1 )
//		{
//			fprintf(out_fp, "%s\t%d\t%s\t%c\t%d\t%d\n", join_id.c_str(), end1,  fusion_vec[i].chr1.c_str(),  fusion_vec[i].strand1, v_1[j1], v_1[j1+1] );
//			L("%s\t%d\t%s\t%c\t%d\t%d\t%s\t0\n", join_id.c_str(), end1,  fusion_vec[i].chr1.c_str(),  fusion_vec[i].strand1, v_1[j1], v_1[j1+1], fusion_vec[i].id.c_str() );
//			j1 += 2;
//		}
//		limit_2 = (int)v_2.size();
//		j2 = 0;
//		while( j2 < limit_2 )
//		{
//			fprintf(out_fp, "%s\t%d\t%s\t%c\t%d\t%d\n", join_id.c_str(), end2,  fusion_vec[i].chr2.c_str(),  fusion_vec[i].strand2, v_2[j2], v_2[j2+1] );
//			L( "%s\t%d\t%s\t%c\t%d\t%d\t%s\t1\n", join_id.c_str(), end2, fusion_vec[i].chr2.c_str(),  fusion_vec[i].strand2, v_2[j2], v_2[j2+1], fusion_vec[i].id.c_str() );
//			j2 += 2;
//		}
//
//
//		//L("F\t%s\t%s\n", fusion_vec[i].id.c_str(), join_id.c_str() );
//	}
//	fclose(out_fp);
//}
///**********************************************/
//void joinFusion( vector<fusion> &fusion_vec )
//{
//	int limit=(int)fusion_vec.size();
//	int j = 0;
//	string join_id;
//	for( int i =0; i < limit; i++ )
//	{
//		j = fusion_vec[i].group;
//		join_id = fusion_vec[j].id;
//		//L("F\t%s\t%s\n", fusion_vec[i].id.c_str(), join_id.c_str() );
//	}
//}
///**********************************************/
//void clusterFusion( vector<fusion> &fusion_vec )
//{
//	int limit=(int)fusion_vec.size();
//	int hit;
//	for( int i =0; i < limit; i++ )
//	{
//		if ( -1 == fusion_vec[i].group )
//		{
//			fusion_vec[i].group = i;
//			for( int j = i+1; j < limit; j++)
//			{
//				//L("COMP\t%d\t%d\t%d\n", i, j, limit);
//				if ( -1 == fusion_vec[j].group )
//				{
//					hit = compare_fusion( fusion_vec[i], fusion_vec[j]) ;
//					if ( 2 == hit )
//					{
//						fusion_vec[j].group = i;
//					}
//
//				}
//			}
//		}
//	}
//}
//
//
///**********************************************/
//// For prediction intervals (A1,B1) and (A2,B2)
//int compare_fusion( fusion &f_1, fusion &f_2)
//{
//	int hit = 0, 
//		s_hit =0; // second round of comparison
//	
//	if ( (f_1.chr1 == f_2.chr1) && (f_1.chr2 == f_2.chr2) ) //A1-A2 and B1-B2
//	{
//		if ( f_1.coor1[0] <= f_2.coor1[0] )// A1-A2
//		{ 
//			hit +=	compare_seg( f_1.coor1, f_2.coor1 ); 
//		}
//		else 
//		{ 	
//			hit +=	compare_seg( f_2.coor1, f_1.coor1 ); 
//		}
//
//		if ( 1 == hit ) // B1-B2
//		{
//			if ( f_1.coor2[0] <= f_2.coor2[0] )// A1-B2
//			{ 
//				hit +=	compare_seg( f_1.coor2, f_2.coor2 ); 
//			}
//			else 
//			{ 	
//				hit +=	compare_seg( f_2.coor2, f_1.coor2 ); 
//			}
//		}
//	}
//	if ( (2 > hit) && (f_1.chr1 == f_2.chr2) && (f_1.chr2 == f_2.chr1) ) //A1-B2 and A1-B2
//	{
//		hit = 0; // Just in case only one end matched
//		if ( f_1.coor1[0] <= f_2.coor2[0] )// A1-B2
//		{ 
//			hit +=	compare_seg( f_1.coor1, f_2.coor2 ); 
//		}
//		else 
//		{ 	
//			hit +=	compare_seg( f_2.coor2, f_1.coor1 ); 
//		}
//
//		if ( 1 == s_hit ) // A2-B1
//		{
//			if ( f_1.coor2[0] <= f_2.coor1[0] )
//			{ 
//				hit +=	compare_seg( f_1.coor2, f_2.coor1 ); 
//			}
//			else 
//			{ 	
//				hit +=	compare_seg( f_2.coor1, f_1.coor2 ); 
//			}
//		}
//	}
//
//	if ( 2 == hit )
//	{
//		//L("MATCHED\t%s\t%s\n", f_1.id.c_str(), f_2.id.c_str() );
//	}
//
//	return hit;
//}
///**********************************************/
//// Assumption: starting point of v_1 os less than that of v_2
//int compare_seg( vector<int> &v_1, vector<int> &v_2)
//{
//	int hit = 0;
//	int limit_1 = (int)v_1.size();
//	int limit_2 = (int)v_2.size();
//
//	int i = 0,
//		j = 0;
//	//OutputIntVec(v_1);
//	//OutputIntVec(v_2);
//	while( (i < limit_1) && ( j < limit_2))
//	{
//		if ( v_1[i] <= v_2[j] )
//		{
//			if ( v_1[i+1] >= v_2[j] )
//			{
//				hit = 1;
//				//L("M_1\t%d\t%d\t%d\t%d\t%d\t%d\n", i, v_1[i], v_1[i+1], j, v_2[j+1], v_2[j+2]);
//				break;// Overlap
//			}
//			else 
//			{
//				i += 2;
//			}
//		}
//		else 
//		{
//			if ( v_1[i] <= v_2[j+1] )
//			{
//				//L("M_3\t%d\t%d\t%d\t%d\t%d\t%d\n", i, v_1[i], v_1[i+1], j, v_2[j+1], v_2[j+2]);
//				hit = 1;
//				break;
//			}
//			else
//			{
//				j += 2;
//			}
//		}
//	}
//
//	return hit;
//}
///**********************************************/
//void OutputIntVec( vector<int> &v_1)
//{
//	int limit = (int)v_1.size();
//	L("CO");
//	for( int i =0; i < limit; i++)
//	{
//		L("\t%d", v_1[i]);
//	}
//	L("\n");
//}
///**********************************************/
///***   Original Prediction Conversion Part  ***/
///**********************************************/
//
///**********************************************/
//void ParseTopHatFusion( char *fm_file, char *out_file, map<string, GENEPosition> &PTMap, map<string, vector<string> > &gene_table, map<string, string> &ensg_table, int read_length)
//{
//	vector<string> token_array;
//	
//	string strand;
//	string chr1, chr2,
//		gene1, gene2,
//		g_in1, g_in2, // in case gene id was reported instead of gene name
//		trans1, trans2,
//		trans_name1, trans_name2,
//		strand1, strand2;
//	string current_fusion,
//			new_fusion;
//	vector<string> vec_trans1, vec_trans2;
//	map<string, string>::iterator t1_it, t2_it;
//	map<string, vector<string> >::iterator t_it;
//	map<string, GENEPosition>::iterator p1_it, p2_it;
//	vector<ExonPosition> vec_ep1, vec_ep2;
//	vector<int> end_point;
//
//	string check_ori;
//
//	int pos1=0, pos2=0;
//	int h1, h2, // if gene_id exist
//		t1, t2; // if we find compatible transcript
//	int ghit1, ghit2, thit1, thit2;
//	int count = 1;
//	//int dir_flag = 0;	//int exon1 = -1, exon2= -1;
//	int limit, i;
//	FILE *out_fp= fopen(out_file, "w");
//	FILE *fp = fopen(fm_file, "r");
//	char *readline  = (char*)malloc(MAX_LINE);
//	while( NULL != fgets(readline, MAX_LINE, fp) )
//	{
//		token_array = splitString(readline, '\t');
//		chr1 = token_array[2];
//		chr2 = token_array[5];
//		gene1 = token_array[1];
//		gene2= token_array[4];
//		//trans_name1 = token_array[1];
//		//trans_name2 = token_array[8];
//		// Does TopHat-Fusion work in 0-based coordinate system?
//		pos1 = atoi(token_array[3].c_str());
//		pos2 = atoi(token_array[6].c_str());
//		//strand1 = token_array[3];
//		//strand2 = token_array[10];
//		// Strategy 1: Find the first COMPATIBLE transcript
//		L("Read %s", readline );
//		h1 = 1;
//		h2 = 1;
//		ghit1=0;
//		ghit2=0;
//		g_in1 = gene1;
//		if ( 0 == strncmp( gene1.c_str(), "ENSG", 4 ) )	// reported via gene_id 
//		{
//			t1_it = ensg_table.find(gene1);
//			if ( ensg_table.end() == t1_it )
//			{
//				L("WRONG GENE1 ID\t%s\n", gene1.c_str());
//				h1 = 0;	//exit (EXIT_FAILURE);
//			}
//			else
//			{	g_in1 = t1_it -> second;	}
//		}
//		if ( 1 == h1 ) 
//		{
//			t_it = gene_table.find(g_in1);
//			if ( gene_table.end() == t_it)
//			{
//				L("WRONG GENE1 NAME\t%s\n", g_in1.c_str());
//				//exit (EXIT_FAILURE);
//			}else
//			{
//				vec_trans1 = t_it->second; // locate transcript
//				ghit1=1;
//			}
//		}
//
//		g_in2 = gene2;
//		if ( 0 == strncmp( gene2.c_str(), "ENSG", 4 ) )	// report by gene_id in TopHat-fusion
//		{
//			t2_it = ensg_table.find(gene2);
//			if ( ensg_table.end() == t2_it )
//			{
//				L("WRONG GENE2 ID\t%s\n", gene2.c_str());
//				h2 = 0;	//exit (EXIT_FAILURE);
//			}
//			else
//			{	g_in2 = t2_it -> second;	}
//		}
//		if ( 1 == h2 )
//		{
//			t_it = gene_table.find(g_in2);
//			if ( gene_table.end() == t_it)
//			{
//				L("WRONG GENE2 NAME\t%s\n", g_in2.c_str());
//				//exit (EXIT_FAILURE);
//			}
//			else
//			{
//				vec_trans2 = t_it->second;
//				ghit2 =1;
//			}
//		}
//		
//		// Transcript Level Searching
//		thit1 = 0;
//		thit2 = 0;
//
//		if (  1 == ghit1 ) 
//		{
//			//L("Candidates\t%d\t%d\n", (int)vec_trans1.size(), (int)vec_trans2.size() );
//			t1 = locate_trans( trans1, strand1, vec_trans1, PTMap, chr1, pos1 ); // 1 means there exists compatible transcripts
//			if ( 1 == t1 )	{	thit1 = 1;	}
//			else{	L("MISSING T1 of %s for %d\n", g_in1.c_str(), pos1); }
//		}
//		if ( 1 == ghit2 )
//		{
//			t2 = locate_trans( trans2, strand2, vec_trans2, PTMap, chr2, pos2 ); // 1 means there exists compatible transcript
//			if ( 1 == t2 ) {	thit2 = 1;	}
//			else{ L("MISSING T2 of %s for %d\n", g_in2.c_str(), pos2); }
//		}
//		L("Summary\t%s-%d\t%d%d%d%d\n", token_array[0].c_str(), count, ghit1, thit1, ghit2, thit2 );
//		// 5' gene
//		if ( 1 == thit1 )
//		{
//			p1_it = PTMap.find(trans1);
//			if ( '+' == strand1[0]) //go to smaller coordinate
//			{	locate_segment(trans1, pos1, read_length, p1_it->second, end_point, 0);	//L("P_1+\n");
//			}
//			else
//			{	locate_segment(trans1, pos1, read_length, p1_it->second, end_point, 1);	//L("P_1-\n");
//			}
//		}	
//		else
//		{
//			if ( 0 == ghit1) {check_strand( gene1, strand1 ); }
//			if ( '+' == strand1[0]) //go to smaller coordinate
//			{	approx_segment( pos1, read_length, end_point, 0);	//L("P_1+\n");
//			}
//			else
//			{	approx_segment( pos1, read_length, end_point, 1);	//L("P_1+\n");
//			}
//			
//		}
//		// output 5' gene
//		limit = (int)end_point.size();
//		if (  '+' == strand1[0])
//		{
//			i = limit - 1;
//			while( 0 <= i )
//			{   fprintf(out_fp, "%s-%d\t0\t%s\t+\t%d\t%d\n", token_array[0].c_str(), count, chr1.c_str(), end_point[i], end_point[i-1] );
//			    i -= 2;
//			}L("O_1+\n");
//		}
//		else
//		{
//			i = 0;
//			while( i < limit )
//			{   fprintf(out_fp, "%s-%d\t0\t%s\t+\t%d\t%d\n", token_array[0].c_str(), count, chr1.c_str(), end_point[i], end_point[i+1] );
//			    i += 2;		
//			}L("O_1-\n");
//		}
//		
//		// 3' gene
//		if ( 1 == thit2 )
//		{
//			p2_it = PTMap.find(trans2);
//			if ( '+' == strand2[0]) //go to smaller coordinate
//			{	locate_segment(trans2, pos2, read_length, p2_it->second, end_point, 1);	//L("P_2+\n");
//			}
//			else
//			{	locate_segment(trans2, pos2, read_length, p2_it->second, end_point, 0);	//L("P_2-\n");
//			}
//		}	
//		else
//		{
//			if ( 0 == ghit2) {check_strand( gene2, strand2 ); }
//			if ( '+' == strand1[0]) //go to smaller coordinate
//			{	approx_segment( pos2, read_length, end_point, 1);	//L("P_1+\n");
//			}
//			else
//			{	approx_segment( pos2, read_length, end_point, 0);	//L("P_1+\n");
//			}
//			
//		}
//		// output 3' gene
//		limit = (int)end_point.size();
//		if ( '+' == strand[0] )
//		{
//			i = 0;
//			while( i < limit )
//			{   fprintf(out_fp, "%s-%d\t1\t%s\t+\t%d\t%d\n", token_array[0].c_str(), count, chr2.c_str(), end_point[i], end_point[i+1] );
//			    i += 2;
//			}L("O_2+\n");
//		}
//		else
//		{
//			i = limit - 1;
//			while( 0 <= i )
//			{   fprintf(out_fp, "%s-%d\t1\t%s\t+\t%d\t%d\n", token_array[0].c_str(), count, chr2.c_str(), end_point[i], end_point[i-1] );
//			    i -= 2;
//			}L("O_2-\n");
//		}
//		count++;
//	}
//	fclose(fp);
//	fclose(out_fp);
//}
//
///**********************************************/
//// Hardcoding here: SANP25 and GUCD1 are reverse-stranded
//void check_strand( string &gene_id, string &strand)
//{
//	strand[0]= '+';
//	if ( ('S' == gene_id[0] ) || ( 'G' == gene_id[0]) )
//	{
//		strand[0]= '-';	//L("CHECK\t%s\t%s\n", gene_id.c_str(), strand.c_str());
//	}//	{L("CHECK\t%s\t%s\n", gene_id.c_str(), strand.c_str() );}
//
//}
///**********************************************/
//int locate_trans( string &trans_id, string &strand,  vector<string> &vec_id, map<string, GENEPosition> &PTMap, string &chr_str, int pos )
//{
//	int limit=(int)vec_id.size();
//	string t_id;
//	GENEPosition tmp_gp;
//	map<string, GENEPosition>::iterator pit;
//
//	int hit=0, loc = -1;
//	for( int i = 0; i < limit; i++ )
//	{
//		t_id = vec_id[i];
//		pit = PTMap.find(t_id);
//		tmp_gp = pit->second;
//		strand[0] = tmp_gp.strand; // no matter if we have compatible transcript or not
//
//		//L("NOW\t%d\t%d\t%d\t%s\t%s\t%s\n", pos, i, limit, t_id.c_str(), tmp_gp.chr_name.c_str(), chr_str.c_str() );
//		if ( tmp_gp.chr_name == chr_str )
//		{
//			L("Checking %s of %d in %s\n", t_id.c_str(), (int)tmp_gp.epos.size(), chr_str.c_str() );
//			loc = find_exon( pos, tmp_gp.epos);
//			if ( 0 <= loc )
//			{
//				trans_id = vec_id[i];
//				//strand[0] = tmp_gp.strand;
//				hit = 1;
//				break; // got a compatible transcript
//			}
//		}
//
//	}
//	return hit;
//}
//
///**********************************************/
//void approx_segment( int pos, int read_length, vector<int> &end_point, int direction)
//{	//direction: 0 for going to leftside, 1 for going to rightside
//	end_point.clear();
//	if ( 1 == direction )
//	{
//		end_point.push_back( pos );
//		end_point.push_back( pos + read_length -1 );
//		L("APP\t%d\t%d\t%d\n", direction, end_point[0], end_point[1]);
//	}
//	else
//	{
//		end_point.push_back( pos );
//		end_point.push_back( pos - read_length + 1 );
//		L("APP\t%d\t%d\t%d\n", direction, end_point[1], end_point[0]);
//	}
//}
///**********************************************/
//void ParseSOAPfuse( char *fm_file, char *out_file, map<string, GENEPosition> &PTMap, map<string, vector<string> > &gene_table, map<string, string> &enst_table, int read_length)
//{
//	vector<string> token_array;
//	
//	string strand;
//	string chr1, chr2,
//		gene1, gene2,
//		trans1, trans2,
//		trans_name1, trans_name2,
//		strand1, strand2;
//	string current_fusion,
//			new_fusion;
//	//vector<string> vec_trans1, vec_trans2;
//	map<string, string>::iterator t1_it, t2_it;
//	map<string, vector<string> >::iterator t_it;
//	map<string, GENEPosition>::iterator p1_it, p2_it;
//	vector<ExonPosition> vec_ep1, vec_ep2;
//	vector<int> end_point;
//
//	string check_ori;
//
//	int pos1=0, pos2=0;
//	int count = 1;
//	//int dir_flag = 0;	//int exon1 = -1, exon2= -1;
//	int limit, i;
//	FILE *out_fp= fopen(out_file, "w");
//	FILE *fp = fopen(fm_file, "r");
//	char *readline  = (char*)malloc(MAX_LINE);
//	while( NULL != fgets(readline, MAX_LINE, fp) )
//	{
//		if ( 0 == strncmp(readline, "up_", 3) )	// first entry of header: "up_gene" in *.trans
//			{continue;}
//		token_array = splitString(readline, '\t');
//		//rand = token_array[4];
//		chr1 = token_array[2];
//		chr2 = token_array[9];
//		gene1 = token_array[0];
//		gene2= token_array[7];
//		trans_name1 = token_array[1];
//		trans_name2 = token_array[8];
//		pos1 = atoi(token_array[5].c_str());
//		pos2 = atoi(token_array[12].c_str());
//		strand1 = token_array[3];
//		strand2 = token_array[10];
//		//new_fusion = chr1 + "_" + token_array[5] + "_" + chr2 + "_" + token_array[12];	
//		new_fusion = gene1 + "_" + token_array[5] + "_" + gene2 + "_" + token_array[12];	
//		if ( new_fusion != current_fusion )
//		{
//			L("New Record:%d %s %s \n%s", count, current_fusion.c_str() , new_fusion.c_str(), readline);
//			t1_it = enst_table.find(trans_name1);
//			if ( enst_table.end() == t1_it)
//			{ L("Error in Trans1_Name:%s\n", trans1.c_str());}
//			trans1 = t1_it->second;
//			t2_it = enst_table.find(trans_name2);
//			if ( enst_table.end() == t2_it)
//			{ L("Error in Trans2_Name:%s\n", trans2.c_str());}
//			trans2 = t2_it->second;
//		
//			p1_it = PTMap.find(trans1);
//			p2_it = PTMap.find(trans2);
//			if ( PTMap.end() == p1_it || PTMap.end() == p2_it)
//			{
//				E("Error in %s: cannot locate transcript %s or %s\n", token_array[0].c_str(), trans1.c_str(), trans2.c_str());
//				continue;
//			}
//			// 5' gene	
//			//if ( 0 == strcmp( '+', strand[0]) ) // go to left
//			if ( "+" == strand1 ) // go to smaller coor
//			{
//				locate_segment(trans1, pos1, read_length, p1_it->second, end_point, 0);
//				limit = (int)end_point.size();
//				i = limit-1;
//				while(0 <= i)
//				{
//					fprintf(out_fp, "%s\t0\t%s\t+\t%d\t%d\n", new_fusion.c_str(), chr1.c_str(), end_point[i], end_point[i-1] );
//					i -= 2;
//				}
//
//			}
//			else
//			{
//				locate_segment(trans1, pos1, read_length, p1_it->second, end_point, 1);
//				limit = (int)end_point.size();
//				i = 0;
//				while( i < limit )
//				{
//					fprintf(out_fp, "%s\t0\t%s\t-\t%d\t%d\n", new_fusion.c_str(), chr1.c_str(), end_point[i], end_point[i+1] );
//					i += 2;
//				}
//			}
//			// 3' gene
//			//if ( 0 == strcmp( '+', strand[1] ))
//			if ( "+"== strand2 ) // go to larger coor
//			{
//				locate_segment(trans2, pos2, read_length, p2_it->second, end_point, 1);
//				limit = (int)end_point.size();
//				i = 0;
//				while( i < limit )
//				{
//					fprintf(out_fp, "%s\t1\t%s\t-\t%d\t%d\n", new_fusion.c_str(), chr2.c_str(), end_point[i], end_point[i+1] );
//					i += 2;
//				}
//			}
//			else
//			{
//				locate_segment(trans2, pos2, read_length, p2_it->second, end_point, 0);
//				limit = (int)end_point.size();
//				i = limit-1;
//				while(0 <= i)
//				{
//					fprintf(out_fp, "%s\t1\t%s\t+\t%d\t%d\n", new_fusion.c_str(), chr2.c_str(), end_point[i], end_point[i-1] );
//					i -= 2;
//				}
//			}
//		
//		
//			current_fusion = new_fusion;
//		}
//
//		//check_ori = gene1+"->"+gene2;
//		//if (check_ori != token_array[18])
//		//{
//		//	E("Warning: Wrong Orientation\t%sn", token_array[0].c_str());
//		//}
//
//		//trans1 = vec_trans1[0];
//		//trans2 = vec_trans2[0];
//
//		//p1_it = PTMap.find(trans1);
//		//p2_it = PTMap.find(trans2);
//		//if ( PTMap.end() == p1_it || PTMap.end() == p2_it)
//		//{
//		//	E("Error in %s: cannot locate transcript %s or %s\n", token_array[0].c_str(), trans1.c_str(), trans2.c_str());
//		//	continue;
//		//}
//		//// 5' gene	
//		////if ( 0 == strcmp( '+', strand[0]) ) // go to left
//		//if ( '+' == strand[0] ) // go to smaller coor
//		//{
//		//	locate_segment(trans1, pos1, read_length, p1_it->second, end_point, 0);
//		//	limit = (int)end_point.size();
//		//	i = limit-1;
//		//	while(0 <= i)
//		//	{
//		//		fprintf(out_fp, "%s\t0\t%s\t+\t%d\t%d\n", token_array[0].c_str(), chr1.c_str(), end_point[i], end_point[i-1] );
//		//		i -= 2;
//		//	}
//
//		//}
//		//else
//		//{
//		//	locate_segment(trans1, pos1, read_length, p1_it->second, end_point, 1);
//		//	limit = (int)end_point.size();
//		//	i = 0;
//		//	while( i < limit )
//		//	{
//		//		fprintf(out_fp, "%s\t0\t%s\t-\t%d\t%d\n", token_array[0].c_str(), chr1.c_str(), end_point[i], end_point[i+1] );
//		//		i += 2;
//		//	}
//		//}
//		//// 3' gene
//		////if ( 0 == strcmp( '+', strand[1] ))
//		//if ( '+'== strand[1] ) // go to larger coor
//		//{
//		//	locate_segment(trans2, pos2, read_length, p2_it->second, end_point, 1);
//		//	i = 0;
//		//	while( i < limit )
//		//	{
//		//		fprintf(out_fp, "%s\t1\t%s\t+\t%d\t%d\n", token_array[0].c_str(), chr2.c_str(), end_point[i], end_point[i+1] );
//		//		i += 2;
//		//	}
//		//}
//		//else
//		//{
//		//	locate_segment(trans2, pos2, read_length, p2_it->second, end_point, 0);
//		//	limit = (int)end_point.size();
//		//	i = limit-1;
//		//	while(0 <= i)
//		//	{
//		//		fprintf(out_fp, "%s\t1\t%s\t+\t%d\t%d\n", token_array[0].c_str(), chr2.c_str(), end_point[i], end_point[i-1] );
//		//		i -= 2;
//		//	}
//		//}
//
//
//		// Sanity Checking
//		//t_it = gene_table.find(gene1);
//		//if (gene_table.end() != t_it)
//		//{
//		//	if ( vec_trans1.size() != t_it->second.size() )
//		//	{
//		//		E("Warning: Inconsist Trans Num for %s in %s:%d\t%d\n",gene1.c_str(),  token_array[0].c_str(), (int)vec_trans1.size(),  (int)t_it->second.size());
//		//	}
//		//}
//		//else
//		//{
//		//	E("Warning: Gene Missing:%s\n", gene1.c_str());
//		//}
//		//E("Num_Entry\t%d\n", (int)token_array.size());
//		count++;
//	}
//	fclose(fp);
//	fclose(out_fp);
//}
///**********************************************/
//void ParseFusionMap( char *fm_file, char *out_file, map<string, GENEPosition> &PTMap, map<string, vector<string> > &gene_table , int read_length)
//{
//	vector<string> token_array;
//	
//	string strand;
//	string chr1, chr2,
//		gene1, gene2,
//		trans1, trans2;
//	vector<string> vec_trans1, vec_trans2;
//	map<string, vector<string> >::iterator t_it;
//	map<string, GENEPosition>::iterator p1_it, p2_it;
//	vector<ExonPosition> vec_ep1, vec_ep2;
//	vector<int> end_point;
//
//	string check_ori;
//	int pos1=0, pos2=0;
//	//int dir_flag = 0;	//int exon1 = -1, exon2= -1;
//	int limit=0, i=0;
//	FILE *out_fp= fopen(out_file, "w");
//	FILE *fp = fopen(fm_file, "r");
//	char *readline  = (char*)malloc(MAX_LINE);
//	while( NULL != fgets(readline, MAX_LINE, fp) )
//	{
//		if ( 0 == strncmp(readline, "Fus", 3) )
//			{continue;}
//		token_array = splitString(readline, '\t');
//		strand = token_array[4];
//		chr1 = token_array[5];
//		chr2 = token_array[7];
//		gene1 = token_array[9];
//		gene2= token_array[13];
//		vec_trans1 = splitStringOld( token_array[10], ',');
//		vec_trans2 = splitStringOld( token_array[14], ',');
//		pos1 = atoi(token_array[6].c_str());
//		pos2 = atoi(token_array[8].c_str());
//		check_ori = gene1+"->"+gene2;
//		if (check_ori != token_array[18])
//		{
//			E("Warning: Wrong Orientation\t%sn", token_array[0].c_str());
//		}
//
//		trans1 = vec_trans1[0];
//		trans2 = vec_trans2[0];
//
//		p1_it = PTMap.find(trans1);
//		p2_it = PTMap.find(trans2);
//		if ( PTMap.end() == p1_it || PTMap.end() == p2_it)
//		{
//			E("Error in %s: cannot locate transcript %s or %s\n", token_array[0].c_str(), trans1.c_str(), trans2.c_str());
//			continue;
//		}
//		// 5' gene	
//		//if ( 0 == strcmp( '+', strand[0]) ) // go to left
//		if ( '+' == strand[0] ) // go to smaller coor
//		{
//			locate_segment(trans1, pos1, read_length, p1_it->second, end_point, 0);
//			limit = (int)end_point.size();
//			i = limit-1;
//			while(0 <= i)
//			{
//				fprintf(out_fp, "%s\t0\t%s\t+\t%d\t%d\n", token_array[0].c_str(), chr1.c_str(), end_point[i], end_point[i-1] );
//				i -= 2;
//			}
//
//		}
//		else if ('-' == strand[0])
//		{
//			locate_segment(trans1, pos1, read_length, p1_it->second, end_point, 1);
//			limit = (int)end_point.size();
//			i = 0;
//			while( i < limit )
//			{
//				fprintf(out_fp, "%s\t0\t%s\t-\t%d\t%d\n", token_array[0].c_str(), chr1.c_str(), end_point[i], end_point[i+1] );
//				i += 2;
//			}
//		}
//		else
//		{
//			E("WrongStrand:%s\n", token_array[0].c_str());
//		}
//		// 3' gene
//		//if ( 0 == strcmp( '+', strand[1] ))
//		if ( '+'== strand[1] ) // go to larger coor
//		{
//			locate_segment(trans2, pos2, read_length, p2_it->second, end_point, 1);
//			limit = (int)end_point.size();
//			i = 0;
//			while( i < limit )
//			{
//				fprintf(out_fp, "%s\t1\t%s\t-\t%d\t%d\n", token_array[0].c_str(), chr2.c_str(), end_point[i], end_point[i+1] );
//				i += 2;
//			}
//		}
//		else if ('-'==strand[1])
//		{
//			locate_segment(trans2, pos2, read_length, p2_it->second, end_point, 0);
//			limit = (int)end_point.size();
//			i = limit-1;
//			while(0 <= i)
//			{
//				fprintf(out_fp, "%s\t1\t%s\t+\t%d\t%d\n", token_array[0].c_str(), chr2.c_str(), end_point[i], end_point[i-1] );
//				i -= 2;
//			}
//		}
//		else
//		{
//			E("WrongStrand:%s\n", token_array[0].c_str());
//		}
//
//
//		// Sanity Checking
//		//t_it = gene_table.find(gene1);
//		//if (gene_table.end() != t_it)
//		//{
//		//	if ( vec_trans1.size() != t_it->second.size() )
//		//	{
//		//		E("Warning: Inconsist Trans Num for %s in %s:%d\t%d\n",gene1.c_str(),  token_array[0].c_str(), (int)vec_trans1.size(),  (int)t_it->second.size());
//		//	}
//		//}
//		//else
//		//{
//		//	E("Warning: Gene Missing:%s\n", gene1.c_str());
//		//}
//		//E("Num_Entry\t%d\n", (int)token_array.size());
//		
//	}
//	fclose(fp);
//	fclose(out_fp);
//}
//
//
//
//// Determine the segments upstream
//// TODO: check if it make sense for ext_length > 0
//void locate_segment( string &trans, int pos, int read_length, GENEPosition &gp, vector<int> &end_point, int direction)
//{
//	// direction: 0 for going left, 1 for going right
//	end_point.clear();
//
//	vector<ExonPosition> vec_ep = gp.epos;
//	L("Exon Num for %s is %d\n", trans.c_str(), (int)gp.epos.size());
//	int e_i = find_exon( pos, vec_ep );
//	if ( -1 == e_i)
//	{
//		E("Cannot Find Exon for %d in %s\n", pos, trans.c_str());
//		exit(EXIT_FAILURE);
//	}
//	//int l_diff = pos-vec_ep[e_i].start;
//	//int r_diff = vec_ep[e_i].end-pos;
//	int ext_length = read_length;
//	int tmp_ext = 0;
//	int limit = (int)vec_ep.size();
//	L("S:\t%d in %d of %s:%d %d\n", pos, e_i, trans.c_str(), vec_ep[e_i].start, vec_ep[e_i].end);
//	//if ( (read_length >l_diff) || (read_length>r_diff))
//	//{
//	//	L("D:\t%d in %d of %s:%d %d\n", pos, e_i, trans.c_str(), vec_ep[e_i].start, vec_ep[e_i].end);
//	//}
//	int cur_i = e_i; // index of splitting exon
//	int cur_c = 0; // counter of STEP
//	// go forward rl
//	int l_end, r_end;
//	if ( 1 == direction )
//	{
//		l_end = pos;
//		//while( 0 < ext_length ) // need to extend so many bases
//		while( (0 < ext_length) && (cur_i < limit) ) // need to extend so many bases
//		{
//			r_end = l_end + ext_length - 1;
//			if ( r_end <= vec_ep[cur_i].end ) // end in this exon
//			{
//				ext_length = 0;
//			}
//			else
//			{
//				r_end = vec_ep[cur_i].end;
//				tmp_ext = r_end - l_end + 1;
//				ext_length -= tmp_ext;
//			}
//			end_point.push_back(l_end);
//			end_point.push_back(r_end);
//			L("Add %d_%d: %d %d from %d %d; %d to extend\n", direction,cur_c, l_end, r_end, vec_ep[cur_i].start, vec_ep[cur_i].end, ext_length);
//			cur_i++;
//			cur_c++;
//			l_end = vec_ep[cur_i].start;
//		}
//
//		if ( 0 < ext_length){L("NotEnd %s %d %d: ends in %d %d; %d to extend\n", trans.c_str(), pos, direction, l_end, r_end, ext_length);}
//
//	}
//	else // go upstream; coordinates will be in decreasing/REVERSED order
//	{
//		r_end = pos;
//		//while( 0 < ext_length ) //still need to decrease so many bases
//		while( ( 0 < ext_length) && ( 0 <= cur_i ) ) //still need to decrease so many bases
//		{
//			l_end = r_end - ext_length + 1;
//			if ( vec_ep[cur_i].start <= l_end) // end in this exon
//			{
//				ext_length = 0;
//			}
//			else
//			{
//				l_end = vec_ep[cur_i].start;
//				tmp_ext = r_end - l_end + 1;
//				ext_length -= tmp_ext;
//			}
//			//end_point.push_back(l_end);
//			end_point.push_back(r_end);
//			end_point.push_back(l_end);
//			L("Add %d_%d: %d %d from %d %d; %d to extend\n", direction,cur_c, l_end, r_end, vec_ep[cur_i].start, vec_ep[cur_i].end, ext_length);
//			cur_i--; // To fit the structure in gtf reader
//			cur_c++;
//			r_end = vec_ep[cur_i].end;
//		}
//		if ( 0 < ext_length){L("NotEnd %s %d %d: ends in %d %d; %d to extend\n", trans.c_str(), pos, direction, l_end, r_end, ext_length);}
//	}
//	
//}
///**********************************************/
//// Determine the exon containing breakpoint
//int find_exon( int pos, vector<ExonPosition> &vec_ep )
//{
//	int limit = (int)vec_ep.size();
//	int i = 0;
//	int hit = -1;
//	while( i < limit)
//	{
//		if ( (vec_ep[i].start <= pos) && ( pos <= vec_ep[i].end))
//		{
//			hit = i;
//		}
//		++i;
//	}
//	return hit;
//}
