#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Common.h"
#include "CommandLineParser.h"
#include "Fasta.h"
#include "Msgf.h"
#include "Prediction.h"
//#include "Repeat.h"
//#include "Fasta.h"
#include "GeneAnnotation.h"
//#include "Read.h"
#include "Compare.h"
#include "Peptide.h"

char versionNumber[10]="0.3";
char versionNumberF[10]="0.1";

using namespace std;
/**********************************************/
int main(int argc, char *argv[])
{
	if ( !parseCommandLine(argc, argv) )
	{
		return 1;
	}

	time_t s_time, f_time;
	struct tm * timeinfo;
	time( &s_time );
	timeinfo= localtime(&s_time);
	E( "%s\n", asctime(timeinfo) );
	
	if ( Mode_GetFusionPep )
	{
		E("Reading deFuse prediction results from %s\n", Predictname );
		ParseDefuse( Predictname, Outputname );
	}

	// Adding 18th entry to MSGF TSV format as Raw Peptide Sequences
	// Adding 19th entry to MSGF TSV format indicating Protein Database
	else if ( Mode_CheckProtID )
	{
		E("Reading ID from (Ensembl) Protein Fasta Database\n");
		map<string, int> map_ProtID;
		readEnsemblProteinID( FastqFilename, map_ProtID);		

		E("Reading TSV Files from MS-GF+ Results\n");
		classifyTSV_by_Protein( Predictname, Outputname, map_ProtID);
	}
	
	else if ( Mode_CheckDtaID ) // Obtained Information for Spectra( DTA entry ) from Known Searches
	{
		E("Reading MS-GF+ Results from KnownDB Searches\n");
		map<string, int> map_GoodDta;
		readDTA_from_TSV( RepeatFilename, map_GoodDta, Error_Bound );

		annotateDTA_by_KnownSearch( Predictname, Outputname, map_GoodDta, Error_Bound);

	}

	else if ( Mode_CheckPepSEQ ) // So far still a all-against-all substring running version
	{
		// Construct Peptides for Known, Novel, and Putative
		E("Reading Sequence from (Ensembl) Protein Fasta Database\n");
		map<string, int> map_KnownSeq;
		map<string, int> map_NovelSeq;
		map<string, int> map_PutativeSeq;
		//map<string, int> map_ProtSeq;
		//readSeqEnsemblProteinID_0( FastqFilename, map_ProtSeq);		
		readSeqEnsemblProteinID( FastqFilename, map_KnownSeq, map_NovelSeq, map_PutativeSeq );		
		E("Known/Novel/Putative has %ld %ld %ld Entries\n", map_KnownSeq.size(), map_NovelSeq.size(), map_PutativeSeq.size() );
		

		//map<string, int>::iterator it;
		//for(it = map_KnownSeq.begin(); it != map_KnownSeq.end(); it++){L("%d\t%s\n", it->second, it->first.c_str());}
		
		E("Reading Aberrant TSV Files from MS-GF+ Results\n");
		classifyTSV_by_PeptideSeq( Predictname, Outputname, map_KnownSeq, map_NovelSeq, map_PutativeSeq, Error_Bound, Decoy_Mode);
	}
	else if ( Mode_CheckMultiHit ) // Just to make sure the results not screwed by multiple matching records containing at least one known sequences
	{
		E("Reading Aberrant TSV Files with PepInfo\n");
		checkMultiHitTSV( Predictname, Outputname, Error_Bound, Decoy_Mode);
	}
	
	else if ( Mode_CountEvent ) 
	{
		E( "Collecting Event Frequencies From Fasta DB File: %s\n", FastqFilename);
		map<int, pc_t> map_freq;
		getPepFreq( FastqFilename, map_freq );
		
		E( "Reading TSV For Counting Number of Relevant Events %s\n", Predictname);
		// Whole Peptide Sets are too large. Let's try to only target on those relevant to specific PSMs for post-analysis
		map<string, int> map_abepep; 
		countEvent_in_TSV( Predictname, Outputname, map_freq, map_abepep, Abe_Mode );
		//match_MSGF_TSV( Predictname, Outputname, map_pepcount, map_sv_pep);
		E( "Peptides of Interestss:%lu\n" , map_abepep.size());
		//
		//// Dump map_sv_pep when needed
		OutputSVPEP( Outputname, map_abepep);
	}
	
	// Fusion and SV splits since this step
	// What I try to do is as follows.
	// First I Decide the spectra that I want to consider
	// Then I extract all FASTA Entries connected to it
	// Then I get get Obtained Information for Spectra from Known Searches
	else if ( Mode_GetFusionEvent_0 ) 
	{
		E( "Collecting Event Frequencies From Fasta DB File: %s\n", FastqFilename);
		map<int, pc_t> map_freq;
		getPepFreq( FastqFilename, map_freq );
	
		// Step 2
		// Instead of Only Count, Let's try to extract all the event header
		// it's something like (AAAAAAA, 2): AAAAAAA appears in 2 PSMs
		map<string, int> map_abepep;
		//countEvent_in_TSV( Predictname, Outputname, map_freq, map_abepep, Abe_Mode );
		collectPep_in_TSV( Predictname, Outputname, map_freq, map_abepep, Abe_Mode);

		// Step 3
		map<string, vector<string> > map_event;
		getFusionEvent_in_TSV( SeqFilename, Outputname, map_abepep, map_event, Abe_Mode );
		E("Checking: %ld peptides\t%ld indices in event\n", map_abepep.size(), map_event.size() );

		// Step 4
		getDTAEvent_in_TSV_0( Predictname, Outputname, map_freq, map_event, Abe_Mode );
		//expandTSV_by_Event();

	}
	else if ( Mode_GetFusionEvent ) 
	{
		E( "Collecting Event Frequencies From Fasta DB File: %s\n", FastqFilename);
		map<int, pc_t> map_freq;
		getPepFreq( FastqFilename, map_freq );
	
		// Step 2
		// Instead of Only Count, Let's try to extract all the event header
		// it's something like (AAAAAAA, 2): AAAAAAA appears in 2 PSMs
		map<string, int> map_abepep;
		//countEvent_in_TSV( Predictname, Outputname, map_freq, map_abepep, Abe_Mode );
		collectPep_in_TSV( Predictname, Outputname, map_freq, map_abepep, Abe_Mode);

		// Step 3
		map<string, vector<string> > map_event;
		getFusionEvent_in_TSV( SeqFilename, Outputname, map_abepep, map_event, Abe_Mode );
		E("Checking: %ld peptides\t%ld indices in event\n", map_abepep.size(), map_event.size() );

		// Step 4
		// Get all events, annotate the dta based on event source. In the meantime expand events from correct mixtures.
		getDTAEvent_in_TSV( Predictname, Outputname, map_freq, map_event, Abe_Mode, Mix_Mode );

	}
	else if ( Mode_deFuseInfo ) 
	{
		E( "Collecting deFuse Annotation From File: %s\n", FastqFilename);
		map<string, vector<string> > map_info;
		get_defuse_table( FastqFilename, map_info);
		//getPepFreq( FastqFilename, map_freq );
		//
		extract_defuse_entry( Predictname, Outputname, map_info);
	
		//// Step 2
		//// Instead of Only Count, Let's try to extract all the event header
		//// it's something like (AAAAAAA, 2): AAAAAAA appears in 2 PSMs
		//map<string, int> map_abepep;
		////countEvent_in_TSV( Predictname, Outputname, map_freq, map_abepep, Abe_Mode );
		//collectPep_in_TSV( Predictname, Outputname, map_freq, map_abepep, Abe_Mode);

		//// Step 3
		//map<string, vector<string> > map_event;
		//getFusionEvent_in_TSV( SeqFilename, Outputname, map_abepep, map_event, Abe_Mode );
		//E("Checking: %ld peptides\t%ld indices in event\n", map_abepep.size(), map_event.size() );

		//// Step 4
		//// Get all events, annotate the dta based on event source. In the meantime expand events from correct mixtures.
		//getDTAEvent_in_TSV( Predictname, Outputname, map_freq, map_event, Abe_Mode, Mix_Mode );

	}
	else if ( Mode_GetSVEvent ) 
	{
		
		E( "Collecting Event Frequencies From Fasta DB File: %s\n", FastqFilename);
		map<int, pc_t> map_freq;
		getPepFreq( FastqFilename, map_freq );

		int tmp_mode = 2;
	
		// Step 2
		// Instead of Only Count, Let's try to extract all the event header
		// it's something like (AAAAAAA, 2): AAAAAAA appears in 2 PSMs
		map<string, int> map_abepep;
		//countEvent_in_TSV( Predictname, Outputname, map_freq, map_abepep, Abe_Mode );
		collectPep_in_TSV( Predictname, Outputname, map_freq, map_abepep, tmp_mode);
		// Why do we need map_abepep here?	



		//// Step 2.5: get target peptide table
		//// map_abepep is the only thing you need to care
		//E("Extract Sets of SVs from Relevant Peptides %s\n", GtfFilename );
		//collectSVInfo_in_TSV( GtfFilename, Outputname, map_abepep );

		// To Much

		//// Step 3
		//map<string, vector<string> > map_event;
		//getSVEvent_in_TSV( SeqFilename, Outputname, map_abepep, map_event, Abe_Mode );
		//E("Checking: %ld peptides\t%ld indices in event\n", map_abepep.size(), map_event.size() );
		
		E("Extract Sets of SVs from Relevant Peptides %s\n", GtfFilename );
		map<string, vector<string> > map_event;
		//collectSVInfo_in_TSV( GtfFilename, Outputname, map_abepep, map_event );
		// We rewrite the peptable and pep format in 2016
		collectSVInfo_in_TSV( GtfFilename, SeqFilename, Outputname, map_abepep, map_event );
		E("Checking: %lu peptides %lu indices in event\n", map_abepep.size(), map_event.size() );
		
		// Step 4
		// Get all events, annotate the dta based on event source. In the meantime expand those events from correct mixtures.
		getDTA_SVEvent_in_TSV( Predictname, Outputname, map_freq, map_event, tmp_mode, Mix_Mode );

	}
	else if ( Mode_AdjustPvalue)
	{
		E("Reading Sequence from (Ensembl) Protein Fasta Database\n");
		map<string, int> map_KnownSeq;
		map<string, int> map_NovelSeq;
		map<string, int> map_PutativeSeq;
		readSeqEnsemblProteinID( FastqFilename, map_KnownSeq, map_NovelSeq, map_PutativeSeq );		
		E("Known/Novel/Putative has %ld %ld %ld Entries\n", map_KnownSeq.size(), map_NovelSeq.size(), map_PutativeSeq.size() );
		
		
		E( "Adjusting Peptide-Level P Value according to two-class classification\n");
		E( "\tNote that identical spectra have to be grouped together.\n");
		getTwoClassPvalue( Predictname, Outputname, map_KnownSeq, map_NovelSeq, map_PutativeSeq );
		//AdjPvalue( Outputname ); 
		AdjPepQvalue( Outputname ); 
	}
	// Extract MicroSVs locating on CDS regions which may have translated peptides
	else if ( Mode_CheckVCF )
	{
		// Reading Gene Annotations
		E( "Reading Gene Annotations from Ensembl Database\n" );		
		map<string, vector<isoform> > iso_map;
		map<string, vector<gene_data> > gene_map;
		ensembl_Reader( GtfFilename, iso_map, gene_map );
		SortGeneInContig( gene_map );

		// Reading VCF File and Sorting in Memory
		E( "Reading Predictions From MiStrVar VCF\n" );
		map<string, vector <unit_vcf> > map_record;
		parse_ShortSV_VCF( Predictname, map_record );
		sortVCF_POS( map_record );
		//OutputVCF(map_record, Outputname);

		// Compare VCF and Gene Intervals
		LocateVCF_GeneModel( map_record, gene_map, iso_map, Outputname );
	}
	else if ( Mode_ConvertVCF )
	{
		// Reading Gene Annotations
		E( "Reading Gene Annotations from Ensembl Database\n" );		
		map<string, vector<isoform> > iso_map;
		map<string, vector<gene_data> > gene_map;
		ensembl_Reader( GtfFilename, iso_map, gene_map );
		SortGeneInContig( gene_map );

		// Reading Reference Genome for Making Transcript
		E( "Reading Reference Genome\n" );
		map<string, string> map_ref;
		LoadFasta( FastqFilename, map_ref );
		

		// Reading VCF File and Sorting in Memory
		E( "Reading Predictions From MiStrVar VCF\n" );
		map<string, vector <unit_vcf> > map_record;
		parse_ShortSV_VCF( Predictname, map_record );
		sortVCF_POS( map_record );
		//OutputVCF(map_record, Outputname);

		// Reading Alignment File
		E( "Reading Contigs From MiStrVar Alignment File\n" );
		map<int, vector<string> > map_alignment;
		parse_Alignment( TargetFilename, map_record, map_alignment );


		// Designed for mistrvar 24029bf
		// Analyze VCF Records Based on Contigs and Gene Information
		convert_VCF( map_record, map_alignment, map_ref, gene_map, iso_map, Outputname);
	}
	else if ( Mode_GetSVPep)
	{
		// Reading Gene Annotations
		E( "Reading Annotated Transcript File from protie ( mode: --convertVCF )\n" );		
		ParseSVTranscript( Predictname, Outputname );
	}
	else if ( Mode_MergePep )
	{
		E( "An Attempt To Merge Identitical Items in a Large Fasta File\n");
		map<string, int > test_record;
		
		MergeFasta( FastqFilename, test_record, Outputname, Libraryname );
	}
	else
	{
		E( "Mode not specified properly\n" );
	}
	
	time( &f_time );
	timeinfo= localtime(&f_time);
	E( "\n%s\n", asctime(timeinfo) );

	return 0;
}

