#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <string.h>
#include <iostream>
#include <fstream>
#include <map>
#include <algorithm>
#include "GeneAnnotation.h"

int _sample_num = 0;

int ana_flag = 0;

using namespace std;
/**********************************************/
bool comp_isoform_start( const isoform &is_1, const isoform &is_2)
{
	int l1 = (int) is_1.exon.size();
	int l2 = (int) is_2.exon.size();
	E("%s %s %d %d\n", is_1.id.c_str(), is_2.id.c_str(), l1, l2);
	return ( (is_1.exon[0].e_s < is_2.exon[0].e_s) || ( is_1.exon[l1-1].e_e < is_2.exon[l2-1].e_e));
}

/**********************************************/
bool comp_mask( const t_mask &m1, const t_mask &m2)
{
	return ( (m1.start < m2.start) || ( (m1.start == m2.start) && (m1.end < m2.end)) );
}
/**********************************************/
bool comp_gene( const GENEBoundary &gb1, const GENEBoundary &gb2)
{
	return ( gb1.start < gb2.start );
}
/**********************************************/
bool comp_gtf( const GTFRecord &gtf1, const GTFRecord &gtf2)
{
	//return ( (gtf1.start < gtf2.start) || ( (gtf1.start == gtf2.start)&&(gtf1.end < gtf2.end) ) );
	return ( ( gtf1.start < gtf2.start ) || ( ( gtf1.start == gtf2.start ) && ( gtf1.end < gtf2.end )  ) );
}

/**********************************************/
bool comp_exon( const Exon &exon1, const Exon &exon2)
{
	return ( exon1.e_s < exon2.e_s );
}

/**********************************************/
bool comp_gene_in_contig( const gene_data &g1, const gene_data &g2)
{
	return ( (g1.start < g2.start) || ( (g1.start == g2.start) && (g1.end < g2.end) ) );
}

/**********************************************/
bool comp_gene_in_contig_end( const gene_data &g1, const gene_data &g2)
{
	return ( g1.end < g2.end );
}
/**********************************************/
void adjust_gene( map<string, gene_data> &map_gene, const string g_id, char *gene_name, const isoform &new_iso )
{
	map<string, gene_data>::iterator it;
	it = map_gene.find(g_id);

	uint32_t left = 0, right = 0;
	int limit = (int)new_iso.exon.size();
	left = new_iso.exon[0].e_s;
	right = new_iso.exon[limit-1].e_e;

	if ( it != map_gene.end())
	{
		if ( it->second.start > left )
		{
			it->second.start = left;
		}
		if ( it->second.end < right )
		{
			it->second.end = right;
		}
		if ( 1 == new_iso.cds_iso)
		{
			it->second.cds_gene = 1;
		}
	}
	else
	{
		gene_data tmp_gene;
		tmp_gene.gene_id = g_id;
		tmp_gene.gene_name = string(gene_name);
		tmp_gene.chr = new_iso.ref;
		tmp_gene.strand = new_iso.strand;
		tmp_gene.start = left;
		tmp_gene.end = right;
		tmp_gene.cds_gene = new_iso.cds_iso;
		map_gene[ g_id ] = tmp_gene;
	}	
}
/**********************************************/
// We are interestedd in either CDS or exons. UTR and start/stop codon can be skipped for now.
int feature_of_interest( char *fea ) 
{
	int flag = ( !strncmp( "C", fea, 1) || !strncmp("e", fea, 1) );
	return flag;
}
/**********************************************/
//void clear_gtf_info( GTFAttr &g_token)
//{
//}
/**********************************************/
int get_gtf_info( char *option, GTFAttr &g_token)
{
	char *info=(char*)malloc(MAX_LINE);
	char *tag=(char*)malloc(MAX_LINE);
	char c;
	int ret = 5, offset;
	int is_set= 0;
	//L("Analyzing %s\n", option);
	while( *option )
	{
		ret = sscanf( option, "%s %c%[^\"]%c%c%n", info, &c, tag, &c, &c, &offset);
		option += offset;
		//L(">>> %d %s %s\n", ret, info, tag);
		//L("<<< %s\n", option);

		if ( 5 != ret){	break;	} // for the last record
		if ( 0 == strncmp("gene_id", info, 7 )) 
		{	copyToString( tag, g_token.gene_id);	is_set++;	}
		else if ( 0 == strncmp("gene_name", info, 9 )) 
		{	copyToString( tag, g_token.gene_name);	is_set++;	}
		else if ( 0 == strncmp("transcript_id", info, 13 )) 
		{	copyToString( tag, g_token.trans_id);	is_set++;	}
		else if ( 0 == strncmp("transcript_name", info, 15 )) 
		{	copyToString( tag, g_token.trans_name);	is_set++;	}
		else if ( 0 == strncmp("gene_biotype", info, 12 )) 
		{	copyToString( tag, g_token.biotype);	is_set++;	}
	}

	free(info);
	free(tag);
	return is_set;
}
/**********************************************/
// Reading Ensembl Version GTF File with the following properties
// Entries of same isoform are grouped together;
// Entries of the same gene are grouped together;
// ToDo: We do not fully make use of these properties in GRCh37.75 
void ensembl_Reader( char *gtf_file, map<string, vector <isoform> > &iso_gene_map, map<string, vector<gene_data> > &gene_sorted_map)
{
	char *readline 	= (char*)malloc(MAX_LINE);
	char *seq 		= (char*)malloc(TOKEN_LENGTH);
	char *src	 	= (char*)malloc(TOKEN_LENGTH);
	char *fea 		= (char*)malloc(TOKEN_LENGTH);
	char *misc 		= (char*)malloc(TOKEN_LENGTH);
	char *gid_str 	= (char*)malloc(TOKEN_LENGTH);
	char *gn_str 	= (char*)malloc(TOKEN_LENGTH);
	char *tid_str 	= (char*)malloc(TOKEN_LENGTH);
	char *tn_str 	= (char*)malloc(TOKEN_LENGTH);
	char *bio_str 	= (char*)malloc(TOKEN_LENGTH);
	
	uint32_t start = 0, end = 0; 
	uint32_t exon_s = 0, exon_e = 0, cds_s = 0, cds_e = 0; 
	// start and ending position of exon unit and cds region
	//uint32_t s1 = 0, e1 = 0, s2 = 0, e2 = 0; // start and ending position of exon unit and cds region
	
	char strand;
	char del;
	char fr_cha;
	int frame=0, offset=0, of2=0;
	int count = 0;	
	string gstr;
	char *gene_id = (char*)malloc(TOKEN_LENGTH);
	char *gene_name = (char*)malloc(TOKEN_LENGTH);
	char *trans_id = (char*)malloc(TOKEN_LENGTH);
	char *trans_name = (char*)malloc(TOKEN_LENGTH);
	trans_id[0]='\0';
	CDS cds_token;
	Exon exon_token;
	isoform iso_token;
	GTFAttr gtf_attr;

	int cds_gene = 0; // if any of this transcript contains CDS
	map< string, gene_data> map_gene;
	gene_data g_token;

	int num_attr = 0;
	FILE *fp = fopen( gtf_file, "r" );
	while( NULL != fgets( readline, MAX_LINE, fp) )
	{
		if ( 0 == strncmp("#", readline, 1) ) {	continue; }
		
		sscanf( readline, "%s %s %s %u %u %s %c %c%n",
			seq, src, fea,  &start, &end, misc, &strand, &fr_cha, &offset );

		//L(">%s %s %s %u %u %s %c %c\n", seq, src, fea, start, end, misc, strand, fr_cha);
		//L("=%d\n", offset);
		//sscanf(readline +offset, "%[^\"]\"%[^\"]\"%[^\"]\"%[^\"]\" %n", // %s %s %u %u %s %c %d %n",
		//	misc, gid_str, misc, tid_str, &offset2//misc, tid_str, gn_st
		////	seq, src, fea, &start, &end, score, &strand, &frame, &offset
		//);
		
		if ( !feature_of_interest( fea ) ) { continue; }
		sscanf( readline + offset, 
			"%s %c%[^\"]%s %s %c%[^\"]%s\ 
			%s %s %s %c%[^\"]%s\
			%s %s %s %c%[^\"]%s %s %c%[^\"]%s %n",
			misc, &del, gid_str, misc, misc, &del, tid_str, misc,
			misc, misc, misc, &del, gn_str, misc, 
			misc, misc, misc, &del, bio_str, misc, misc, &del, tn_str, misc, &of2	);

		// extra string copy slows down the following function
		// num_attr = get_gtf_info( readline + offset, gtf_attr );
		// L(">>%s\n", gid_str);L("$$%s\n", tid_str);L("^^%s\n", gn_str); //L("<<%s\n", tn_str); //L("--%s\n", bio_str); 
		
		// Start New Transcript
		if ( 0 != strncmp( trans_id, tid_str, TOKEN_LENGTH ) )
		{
			if ( '\0' != trans_id[0] )
			{	
				if ( 0 < exon_s + exon_e )	// last region is not a cds
				{	
					exon_token.e_s = exon_s;
					exon_token.e_e = exon_e;
					exon_token.c_s = cds_s;
					exon_token.c_e = cds_e;
					//// determine if a token is cds, utr, or intron
					exon_token.cds = 0;
					if ( 0 < cds_s + cds_e )
					{
						exon_token.cds = 2;
						if ( ( exon_s != cds_s) || ( exon_e != cds_e) )
						{	exon_token.cds = 1;	}
					}

					iso_token.exon.push_back(exon_token);	
				}
				//L("Finish Isoform %s with %d exons\n", trans_id, (int)iso_token.exon.size() ); 
				gstr = string(gene_id);				
				sort( iso_token.exon.begin(), iso_token.exon.end(), comp_exon);
				if ( 1 == cds_gene ){ iso_token.cds_iso = 1; }
				iso_gene_map[gstr].push_back( iso_token );

				if ( 0 == iso_token.exon.size()) { E("Warning: No Exon in %s\n", iso_token.id.c_str() ) ;}
				else{ adjust_gene( map_gene, gstr, gene_name, iso_token ); }
			}
			//L("Starting New Isoform %s\n", tid_str);
			strncpy( gene_id, gid_str, TOKEN_LENGTH);
			strncpy( gene_name, gn_str, TOKEN_LENGTH);
			strncpy( trans_id, tid_str, TOKEN_LENGTH);
			strncpy( trans_name, tn_str, TOKEN_LENGTH);
			iso_token.id = string(trans_id);
			iso_token.tname = string(trans_id);
			iso_token.gid = string(gene_id);
			iso_token.ref = string(seq);
			iso_token.src = string(src);
			iso_token.fea = string(fea);
			iso_token.strand = strand;
			iso_token.cds_iso = 0;
			iso_token.exon.clear();
			exon_s = 0; exon_e = 0; cds_gene = 0;
		}

		// CDS and Exon
		// Note in Ensembl GTF CDS comes after Exon records
		//
		if ( 0 == strncmp(fea, "exon", 4) )
		{	// Previous records
			exon_token.e_s = exon_s;
			exon_token.e_e = exon_e;
			exon_token.c_s = cds_s;
			exon_token.c_e = cds_e;
			//// determine if a token is cds, utr, or intron
			exon_token.cds = 0;
			if ( 0 < cds_s + cds_e )
			{
				exon_token.cds = 2;
				if ( ( exon_s != cds_s) || ( exon_e != cds_e) )
				{	exon_token.cds = 1;	}
			}
			if ( 0 < exon_s + exon_e )	
			{	iso_token.exon.push_back(exon_token);	}

			
			// Reading Current Records
			exon_s = start;
			exon_e = end;
			cds_s = 0;
			cds_e = 0;
		}
		else if ( 0 == strncmp(fea, "CDS", 3) )
		{
			cds_s = start;
			cds_e = end;
			cds_gene = 1; // reading a protein-coding transcript
		}
		
		count++;
		if ( 0 == count%1000000){E(".");}

	}
	//Last Record
	exon_token.e_s = exon_s;
	exon_token.e_e = exon_e;
	exon_token.c_s = cds_s;
	exon_token.c_e = cds_e;
	exon_token.cds = 0;
	if ( 0 < cds_s + cds_e )
	{
		exon_token.cds = 2;
		if ( ( exon_s != cds_s) || ( exon_e != cds_e))
		{	exon_token.cds = 1;	}
	}
	if ( 0 < exon_s + exon_e )	
	{	iso_token.exon.push_back(exon_token);	}
	//L("Finish Isoform %s with %d exon\n", trans_id, (int)iso_token.exon.size() );
	//strncpy( gene_name, gn_str, TOKEN_LENGTH);
	
	gstr = string(gene_id);
	sort( iso_token.exon.begin(), iso_token.exon.end(), comp_exon);
	if ( 1 == cds_gene ){ iso_token.cds_iso = 1;}
	iso_gene_map[gstr].push_back(iso_token);
	if ( 0 == iso_token.exon.size()) { E("Warning: No Exon in %s\n", iso_token.id.c_str() ) ;}
	else{adjust_gene( map_gene, gstr, gene_name, iso_token );}
	
	E("\n");
	free(readline);
	free(seq);
	free(src);
	free(fea);
	free(misc);
	free(gid_str);
	free(gn_str); 
	free(tid_str);
	free(tn_str);
	free(bio_str);
	free(gene_id);
	free(gene_name);
	free(trans_id);
	free(trans_name);

	//map<string, vector<isoform> >::iterator it;
	//for( it = iso_gene_map.begin(); it != iso_gene_map.end(); it++)
	//{
	//	int limit=(int)it->second.size();
	//	for( int i = 0; i < limit; i++)
	//	{
	//		int exon_size = it->second[i].exon.size();
	//		for( int j = 0; j < exon_size; j++)
	//		{	L("%s\t%s\t%lu\t%lu\t%d\t%d\n", it->first.c_str(), it->second[i].id.c_str(), it->second[i].exon[j].e_s, it->second[i].exon[j].e_e, it->second[i].exon[j].cds, it->second[i].cds_iso);	}
	//	}
	//}
	
	map<string, gene_data >::iterator it;
	for( it = map_gene.begin(); it != map_gene.end(); it++)
	{
		//L("Add_Gene\t%s\t%s\t%s\t%u\t%u\t%d\n", it->second.chr.c_str(), it->second.gene_id.c_str(), it->second.gene_name.c_str(), it->second.start, it->second.end, it->second.cds_gene );
		gene_sorted_map[it->second.chr].push_back(it->second);
	}
	E("Scanning total %d genes\n", (int)map_gene.size() );
}


/**********************************************/
// Output Gene/Isoform Boundary to outfile for masking purpose
void output_Isoform_Boundary( map<string, vector <isoform> > &iso_gene_map, char *outfile)
{
	//E("ok");
	FILE *out_fp = fopen( outfile, "w" );
	map<string, vector<isoform> >::iterator it;
	for ( it = iso_gene_map.begin(); it != iso_gene_map.end(); it++)
	{
		L("chr %s\n", it->first.c_str());
		int limit = it->second.size();
		L("chr_count %d\n", limit);
		//sort( it->second.begin(), it->second.end(), comp_isoform_start);
		for ( int i = 0; i < limit; i++)
		{
			L("start %d\n", i);
			int iso_l = it->second[i].exon.size();
			L("size %d\n", iso_l);
			
			fprintf(out_fp,"%s\t%u\t%u\tgene_id\"%s\";transcript_id\"%s\";src\"%s\";\n",it->second[i].ref.c_str(), it->second[i].exon[0].e_s,  it->second[i].exon[iso_l -1].e_e, 
				it->second[i].gid.c_str(), it->second[i].id.c_str(), it->second[i].src.c_str());
		}
		
	}

	fclose(out_fp);
	
}
/**********************************************/
// Reading Ensembl Version GTF File with the following properties
void masker_reader( char *gtf_file, char *outfile )
{
	char *readline = (char*)malloc(MAX_LINE);
	char *seq = (char*)malloc(TOKEN_LENGTH);
	char *src = (char*)malloc(TOKEN_LENGTH);
	char *fea = (char*)malloc(TOKEN_LENGTH);
	char *misc = (char*)malloc(TOKEN_LENGTH);
	char *gid_str = (char*)malloc(TOKEN_LENGTH);
	char *gn_str = (char*)malloc(TOKEN_LENGTH);
	char *tid_str = (char*)malloc(TOKEN_LENGTH);
	char *tn_str = (char*)malloc(TOKEN_LENGTH);
	
	uint32_t start =0, end = 0; 
	
	t_mask mask_item;
	map<string, vector<t_mask> > map_mask; // chr, vector of masker
	FILE *fp = fopen( gtf_file, "r" );
	while( NULL != fgets( readline, MAX_LINE, fp) )
	{
		sscanf(readline, "%s %u %u %s", seq, &start, &end, fea);
		copyToStringStrip( seq, mask_item.ref );
		copyToStringStrip( fea, mask_item.fea );
		mask_item.start = start;
		mask_item.end = end;
		map_mask[ mask_item.ref].push_back(mask_item);	
	}
	fclose(fp);
	FILE *out_fp =  fopen( outfile, "w" );
	map<string, vector<t_mask> >::iterator it;
	for( it = map_mask.begin(); it != map_mask.end(); it ++)
	{
		sort( it->second.begin(), it->second.end(), comp_mask);
		int limit = it->second.size();
		for (int i =0 ; i < limit; i++)
		{
			fprintf(out_fp, "%s\t%u\t%u\t%s\n", it->second[i].ref.c_str(), it->second[i].start, it->second[i].end, it->second[i].fea.c_str());
		}

	}
	fclose(out_fp);
}
/**********************************************/
// Output Gene/Isoform Boundary For Specfic Set of Genes
void output_Isoform_Boundary_Specific( map<string, vector <isoform> > &iso_gene_map, char *outfile, map<string, int> &gene_dict )
{
	//E("ok");
	FILE *out_fp = fopen( outfile, "w" );
	map<string, vector<isoform> >::iterator it;
	map<string, int>::iterator git;
	for ( it = iso_gene_map.begin(); it != iso_gene_map.end(); it++)
	{
		L("chr %s\n", it->first.c_str());
		int limit = it->second.size();
		L("chr_count %d\n", limit);
		//sort( it->second.begin(), it->second.end(), comp_isoform_start);
		for ( int i = 0; i < limit; i++)
		{
			L("start %d\n", i);
			int iso_l = it->second[i].exon.size();
			L("size %d\n", iso_l);
		
			git = gene_dict.find( it->second[i].gname);
			if ( git != gene_dict.end() )
			{
				fprintf(out_fp,"%s\t%s\t%s\t%s\t%c\t%s\t%u\t%u\n",
						it->second[i].gname.c_str(),
						it->second[i].gid.c_str(), it->second[i].id.c_str(), it->second[i].src.c_str(),
						it->second[i].strand,
						it->second[i].ref.c_str(), it->second[i].exon[0].e_s,  it->second[i].exon[iso_l -1].e_e 
					   );
			}

			//fprintf(out_fp,"%s\t%u\t%u\tgene_id\"%s\";transcript_id\"%s\";src\"%s\";\n",it->second[i].ref.c_str(), it->second[i].exon[0].e_s,  it->second[i].exon[iso_l -1].e_e, 
			//	it->second[i].gname.c_str(),
			//	it->second[i].gid.c_str(), it->second[i].id.c_str(), it->second[i].src.c_str());
		}
		
	}

	fclose(out_fp);
	
}
/**********************************************/
// Output Gene/Isoform Boundary For Specfic Set of Genes
void output_Gene_Boundary_Specific( map<string, vector <gene_data> > &gene_sorted_map, char *outfile, map<string, int> &gene_dict )
{
	char *gene_out=(char*)malloc(MAX_LINE);
    strcpy(gene_out, outfile);
	strcat(gene_out, ".gene");
	//E("ok");
	FILE *out_fp = fopen( gene_out, "w" );
	map<string, vector<gene_data> >::iterator it;
	map<string, int>::iterator git;

	for ( it = gene_sorted_map.begin(); it != gene_sorted_map.end(); it++)
	{
		L("chr %s\n", it->first.c_str());
		int limit = it->second.size();
		L("chr_count %d\n", limit);
		//sort( it->second.begin(), it->second.end(), comp_isoform_start);
		for ( int i = 0; i < limit; i++)
		{
			//fprintf(out_fp,"%s\t%s\t%d\t%c\t%s\t%u\t%u\n",
			//	it->second[i].gene_name.c_str(), it->second[i].gene_id.c_str(), it->second[i].cds_gene,
			//	it->second[i].strand, it->second[i].chr.c_str(), 
			//	it->second[i].start, it->second[i].end);
		
			git = gene_dict.find( it->second[i].gene_name);
			if ( git != gene_dict.end() )
			{
				fprintf(out_fp,"%s\t%s\t%d\t%c\t%s\t%u\t%u\n",
						it->second[i].gene_name.c_str(), it->second[i].gene_id.c_str(), it->second[i].cds_gene,
						it->second[i].strand, it->second[i].chr.c_str(), 
						it->second[i].start, it->second[i].end);
			}
		}
	}

	fclose(out_fp);
	free(gene_out);
	
}
/**********************************************/
// Original Format: (id, end , chr, strand, left, end)
void genename_reader( char *gene_file, map<string, int> &gene_dict)
{
	char *readline = (char*)malloc(MAX_LINE);
	//char *gid_str = (char*)malloc(TOKEN_LENGTH);
	string gid_str;
	
	
	FILE *fp = fopen( gene_file, "r" );
	while( NULL != fgets( readline, MAX_LINE, fp) )
	{
		copyToStringStrip( readline, gid_str );
		gene_dict[gid_str]=1;
	}
	fclose(fp);
	
	//
	int count = 1;
	map<string, int>::iterator it;
	for( it = gene_dict.begin(); it != gene_dict.end(); it++)
	{
		//E("Gene %d %s\n", count, it->first.c_str());
		count++;
	}
	//
	free(readline);
}
/**********************************************/
/**********************************************/
void SortGeneInContig( map<string, vector<gene_data> > &gene_sorted_map)
{
	map<string, vector<gene_data> >::iterator it;
	for( it = gene_sorted_map.begin(); it != gene_sorted_map.end(); it++)
	{
		sort( it->second.begin(), it->second.end(), comp_gene_in_contig);
	}
	
	
	//for( it = gene_sorted_map.begin(); it != gene_sorted_map.end(); it++)
	//{
	//	for( int i = 0; i < (int)it->second.size(); i++)
	//	L("G\t%s\t%s\t%s\t%u\t%u\n", it->first.c_str(), it->second[i].gene_id.c_str(), it->second[i].gene_name.c_str(), it->second[i].start, it->second[i].end );
	//}
}

/**********************************************/
void SortGeneInContigByEnd( map<string, vector<gene_data> > &gene_sorted_map)
{
	map<string, vector<gene_data> >::iterator it;
	for( it = gene_sorted_map.begin(); it != gene_sorted_map.end(); it++)
	{
		sort( it->second.begin(), it->second.end(), comp_gene_in_contig_end);
	}
	
	
	//for( it = gene_sorted_map.begin(); it != gene_sorted_map.end(); it++)
	//{
	//	for( int i = 0; i < (int)it->second.size(); i++)
	//	L("G\t%s\t%s\t%s\t%u\t%u\n", it->first.c_str(), it->second[i].gene_id.c_str(), it->second[i].gene_name.c_str(), it->second[i].start, it->second[i].end );
	//}
}

/**********************************************/
// Original Format: (id, end , chr, strand, left, end)
void LengthCalculator( char *gtf_file, map<string, vector<tr_length> > &length_map)
{
	vector<string> token_array, id_array;
	tr_length tmp_tr;

	int left = 0, right = 0, shift = 0, total = 0;
	string t_id = "";

	FILE *fp = fopen( gtf_file, "r" );

	char *readline = (char*)malloc (MAX_LINE);
	while( NULL != fgets( readline, 2000, fp) )
	{
		token_array = splitString( readline, '\t');
		id_array = splitStringOld(token_array[8], '"');
		if ( t_id != id_array[3]  )
		{
			if ( 0 < total )
			{
				tmp_tr.chr_id = token_array[0];
				tmp_tr.length = total;
				length_map[t_id].push_back(tmp_tr);
			}
			total = 0; // for the next transcript
			t_id = id_array[3];
		}

		left = atoi(token_array[3].c_str() );
		right= atoi(token_array[4].c_str() );
		shift = right - left + 1;
		if ( shift < 0 ){E("Warning:%s", readline );}
		total += shift;
		L("%d\t%d\t%d\t%s\n", left, right, total, id_array[3].c_str());		
	}

	if ( 0 < total ) // For the last trasncript
	{
		tmp_tr.chr_id = token_array[0];
		tmp_tr.length = total;
		length_map[t_id].push_back(tmp_tr);
		//length_map[t_id] = total;
	}
	E(" %d records\n", (int)length_map.size() );		
	fclose(fp);

}
/**********************************************/
// Original Format: (id, end , chr, strand, left, end)
void regionReader( char *pred_file, vector<fusion> &fusion_vec )
{
	//Same prediction are grouped together

	string cur_id = "";

	string id="",
			chr;
	char end;//,  strand;
	int left, right;
	int num_pred = 0;

	FILE *fp = fopen( pred_file, "r" );
	vector<int> vec1, vec2;
	fusion t_fus;

	vector<string> token_array;
	char *readline = (char*)malloc (MAX_LINE);
	while (NULL != fgets(readline, MAX_LINE, fp) )
	{
		token_array = splitString(readline, '\t');
		id = token_array[0];

		if ( id != cur_id ) // finalize reading cur_id
		{
			if ( 0 < num_pred )
			{
				t_fus.id = cur_id;				
				t_fus.group = -1;
				fusion_vec.push_back(t_fus);
				//E("ADD\t%s", t_fus.id.c_str());
				//num_pred++;
			} 	 
			t_fus.coor1.clear();
			t_fus.coor2.clear();
			cur_id = id;
			num_pred++; // number of change:= number of record +1
		}
		end = token_array[1][0];
		left = atoi(token_array[4].c_str());
		right = atoi(token_array[5].c_str());
		switch(end)
		{
			case '0':
				t_fus.chr1 = token_array[2];
				t_fus.strand1 = token_array[3][0];
				t_fus.coor1.push_back(left);
				t_fus.coor1.push_back(right);
				break;
			case '1':
				t_fus.chr2 = token_array[2];
				t_fus.strand2 = token_array[3][0];
				t_fus.coor2.push_back(left);
				t_fus.coor2.push_back(right);
				break;
			default:
				E("Error End Record:%c\n", end);
				break;
		}

	}

	t_fus.id = id;
	t_fus.group = -1;
	fusion_vec.push_back(t_fus);
	//E("ADD\t%s", t_fus.id.c_str());
	E("%d fusions in the prediction\n", num_pred);
	//OutputFusionPrediction(fusion_vec);
	fclose(fp);
}


/**********************************************/
void read_uniq_region( map<string, vector<Region> > &region_map, char *uniq_file )
{
	//suppose regions in a transcript are grouped together

	map<string, vector<Region> >::iterator mit;
	vector<string> token_array;
	vector<Region> region_array;
	Region r_token;

	string t_id = "";

	FILE *fp = fopen(uniq_file, "r");

	char *readline = (char*)malloc (MAX_LINE);
	while (NULL != fgets(readline, MAX_LINE, fp) )
	{
		token_array = splitString(readline, '\t');
		if ( 0 < region_array.size() && 0 != t_id.compare(token_array[3]) )
		{
			region_map[t_id] = region_array;
			//fprintf(stdout, "AUQ\t%s\t%d\n", t_id.c_str(), (int)region_array.size());
			region_array.clear();
			region_array.reserve(16);
			//t_id = token_array[3];
		}
		t_id = token_array[3];
		r_token.start = atoi(token_array[4].c_str());
		r_token.end = atoi(token_array[5].c_str());
		region_array.push_back(r_token);

	}

	region_map[t_id] = region_array;
	//fprintf(stdout, "AUQ\t%s\t%d\n", t_id.c_str(), (int)region_array.size());
	fclose(fp);
}

/**********************************************/
void LoadCoordinate( char *info_file, map<string, coor_record> &coor_map )
{
	vector<string> token_array;
	string trans_name;
	coor_record coor_token;

	FILE *fp = fopen(info_file, "r");

	char *readline = (char*)malloc (2000);
	while( NULL != fgets(readline, 2000, fp) )
	{
		token_array = splitString(readline, '\t');
		trans_name = token_array[2];
		coor_token.chr_name = token_array[0];
		coor_token.gene_name = token_array[1];

		coor_token.length = atoi(token_array[3].c_str());
		coor_token.start = atoi(token_array[4].c_str());
		coor_token.end = atoi(token_array[5].c_str());

		coor_map[ trans_name ] = coor_token;
	}

	fclose(fp);
}


/**********************************************/
void mapping_transcript(string id_str, string n_str, map<string, string> &enst_table)
{
	map<string, string>::iterator it;
	map<string, string>::iterator t_it;
	it=enst_table.find(n_str);
	if ( enst_table.end() == it)
	{
		enst_table[n_str] = id_str;
		//L("ADD\t%s\t%s\n", n_str.c_str(), id_str.c_str() );
	}
	else 
	{
		// ( id_str != it->second )// duplicate name
		//
		string tmp_index = n_str + "SOAPfuse2SOAPfuse";
		t_it = enst_table.find(tmp_index);
		if ( enst_table.end() == t_it  )
		{
			enst_table[tmp_index] = id_str;
			//L("ADD\t%s\t%s\n", tmp_index.c_str(), id_str.c_str() );
		}
		// TODO: automatically generate index for duplicated gene name
		// Now we only take care for the second duplication entry
		//	
		//else 
		//{
		//	L("TRANSE\t%s\t%s\n", id_str.c_str(), n_str.c_str() );
		//}
		//}

	}
}


/**********************************************/
map<string, GENEPosition > gtfReader_PT( char *gtfFileName, map<string, vector<string> > &gene_table, map<string, string> &enst_table, map<string, string> &ensg_table )
{

	map<string, GENEPosition > PT_map;
	FILE *fp = fopen(gtfFileName, "r");
	GTFRecord gtf_item;

	vector<string> token_array;
	//token_array.reserve(20);
	int count = 0;
	int t_count=0;
	string chr_augment = "";
	string gene_augment = "";
	string gn_augment = ""; // for ambiguous gene name in gtf
	string trans_augment = "";
	string trans_name = "";// the name for transcript NAME in ensembl gtf

	vector<GTFRecord> exon_vector;
	exon_vector.reserve(100);
	map<string, GENEPosition> single_map;
	GENEPosition single_pos;

	map <string,int> freq_map;
	int previous = 0, acc = 0;
	char *mystr = (char*)malloc (2000);
	while( NULL != fgets(mystr, 2000, fp) )
	{

		token_array = splitString(mystr, '\t');
		gtf_item = MakeGtfItem(token_array);
		if (trans_augment != gtf_item.attr.trans_id) //gene_id)//ene_name)
		{

			if (0 < exon_vector.size()) //switch to different transcript
			{ 
				single_pos = generate_genepos(exon_vector);
				single_pos.chr_name = chr_augment;
				single_pos.gene_name = gene_augment;
				single_pos.gn_name = gn_augment;
				//L("TABLE\t%s\t%s\n", single_pos.gene_name.c_str(), single_pos.gn_name.c_str());
				gene_table[gn_augment].push_back(trans_augment);
				//L("Insert %d to %s\n", (int)gene_table[gn_augment].size(), gn_augment.c_str());
				t_count++;
				PT_map[trans_augment] = single_pos;
				mapping_transcript(trans_augment, trans_name, enst_table);
				//L("M\t%s\t%s\n", trans_augment.c_str(), trans_name.c_str() );
				acc++;
				//single_map[trans_augment] = single_pos;				
				exon_vector.clear();
				exon_vector.reserve(100);

			}
			trans_augment = gtf_item.attr.trans_id;//gene_id;//name;
			trans_name = gtf_item.attr.trans_name;
		}

		if (chr_augment != gtf_item.seqname)
		{
			//fprintf(stderr, "CH_stat%s: %d (%d %d) \n",chr_augment.c_str(), acc- previous, acc, previous);
			previous = acc;
			//if (0 < chr_augment.size() )
			//{
			//	

			////	GeneMap[chr_augment] = single_map;
			////	single_map.clear();
			//}
			chr_augment = gtf_item.seqname;
		}
		
		if("exon" == gtf_item.feature)
		{
			exon_vector.push_back(gtf_item);
		}

		count +=1;
		if (0 == count%100000)
		{
			fprintf(stderr, ".");
		}
		gene_augment = gtf_item.attr.gene_id;
		gn_augment = gtf_item.attr.gene_name;
		ensg_table[gene_augment] =  gn_augment;
		//L("F\t%s\t%s\n", gene_augment.c_str(), gn_name.c_str() );
	}
	//fprintf(stderr, "CH_stat%s: %d (%d %d) \n",chr_augment.c_str(), acc- previous, acc, previous);
	fprintf(stderr, "\n");
	fclose(fp);
	single_pos = generate_genepos(exon_vector);
	single_pos.chr_name = chr_augment;
	single_pos.gene_name = gene_augment;
	single_pos.gn_name = gn_augment;
	//L("TABLE\t%s\t%s\n", single_pos.gene_name.c_str(), single_pos.gn_name.c_str());
	gene_table[gn_augment].push_back(trans_augment);
	//L("Insert %d to %s\n", (int)gene_table[gn_augment].size(), gn_augment.c_str());
	t_count++;
	PT_map[trans_augment] = single_pos;
	mapping_transcript(trans_augment, trans_name, enst_table);
	//L("M\t%s\t%s\n", trans_augment.c_str(), trans_name.c_str() );
	//GeneMap[chr_augment] = single_map;
	acc++;


	E("Total Transcript %d\n", (int)PT_map.size());
	E("Total Gene %d\n", (int)ensg_table.size());
	E("Record in GeneTrans Table\t%d\n", t_count);
	return PT_map;

}

/**********************************************/
map<string, map<string, GENEPosition> > gtfReader( char *gtfFileName )
{


	map<string, map<string, GENEPosition> > GeneMap;
	FILE *fp = fopen(gtfFileName, "r");
	GTFRecord gtf_item;

	vector<string> token_array;
	//token_array.reserve(20);
	int count = 0;

	string chr_augment = "";
	string gene_augment = "";

	vector<GTFRecord> exon_vector;
	exon_vector.reserve(100);
	map<string, GENEPosition> single_map;
	GENEPosition single_pos;

	char *mystr = (char*)malloc (2000);
	while( NULL != fgets(mystr, 2000, fp) )
	{

		token_array = splitString(mystr, '\t');
		gtf_item = MakeGtfItem(token_array);
		if (gene_augment != gtf_item.attr.gene_id)//ene_name)
		{

			if (0 < exon_vector.size()) 
			{ 
				single_pos = generate_genepos(exon_vector);
				single_map[gene_augment] = single_pos;				
				exon_vector.clear();
				exon_vector.reserve(100);

			}
			gene_augment = gtf_item.attr.gene_id;//name;
		}

		if (chr_augment != gtf_item.seqname)
		{
			if (0 < single_map.size())
			{

				GeneMap[chr_augment] = single_map;
				single_map.clear();
			}
			chr_augment = gtf_item.seqname;
		}
		
		if("exon" == gtf_item.feature)
		{
			exon_vector.push_back(gtf_item);
		}

		count +=1;
		if (0 == count%100000)
		{
			fprintf(stderr, ".");
		}
	}
	fprintf(stderr, "\n");
	fclose(fp);
	single_pos = generate_genepos(exon_vector);
	single_map[gene_augment] = single_pos;
	GeneMap[chr_augment] = single_map;
	return GeneMap;

}

/**********************************************/
GENEPosition generate_genepos(vector<GTFRecord> exon_vector)
{
	GENEPosition new_pos;
	new_pos.strand = 0;
	new_pos.start = 0;
	new_pos.end = 0;
    vector<ExonPosition> epos_vector;
	epos_vector.reserve(exon_vector.size());
	ExonPosition ep;
	int ep_count = 0;
	sort(exon_vector.begin(), exon_vector.end(), comp_gtf);

	int group = 0;
	int upper_bound = 0;

	vector<GTFRecord>::iterator it;

	for(it = exon_vector.begin(); it < exon_vector.end(); it++)
	{	
		if(	0 == new_pos.strand) // first exon in a gene
		{
			new_pos.start = (*it).start;
			new_pos.end = (*it).end;
			new_pos.strand = (*it).strand;
			ep.start = (*it).start;
			ep.end = (*it).end;
			ep.id = ep_count;
			ep.status = 0;
			//ep.status = (char*)malloc (_sample_num);
			//ep.count = (int*)malloc (_sample_num);
			//memset();

			ep.group_id = 0;
			upper_bound = (*it).end;

			ep_count++;
			epos_vector.push_back(ep);
		}
		else
		{
			if ((*it).start < new_pos.start)
			{
				new_pos.start = (*it).start;
			}
			if( new_pos.end < (*it).end)
			{
				new_pos.end = (*it).end;
			}

			if ((ep.start != (*it).start) ||(ep.end != (*it).end) )
			{
				ep.start =(*it).start;
				ep.end = (*it).end;
				ep.id = ep_count;
				ep.status = 0;
				//memset( ep.status, 0, _sample_num * sizeof(char) );
				//memset( ep.count, 0, _sample_num * sizeof(int) );
		
				/********************/ 
				if ( upper_bound < ep.start )
				{
					upper_bound = ep.end;
					group++;//ep.group_id = group;
				}
				else
				{
					if ( upper_bound < ep.end) 
					{
						upper_bound = ep.end;
					}
				}
				ep.group_id = group;
				/********************/

				ep_count++;
				epos_vector.push_back(ep);
			}
		}
	}
	new_pos.epos = epos_vector;
	return new_pos;
}
/**********************************************/

GTFRecord MakeGtfItem(vector<string> token_array)
{
	GTFRecord gtf_item;
	GTFAttr gtf_attr;  
	gtf_item.seqname = token_array[0];
	gtf_item.source = token_array[1];
	gtf_item.feature = token_array[2];
	gtf_item.start = atoi(token_array[3].c_str());
	gtf_item.end = atoi(token_array[4].c_str());
	//gtf_item.score = token_array[5];
	gtf_item.strand = token_array[6][0];
	gtf_item.frame = atoi(token_array[7].c_str());
	gtf_item.attr = parse_gtf_attr(token_array[8]);
	return gtf_item;  
}


/**********************************************/

GTFAttr parse_gtf_attr(string attr_string)
{
	GTFAttr attr;
	vector<string> attr_array = splitStringOld(attr_string.c_str(), '\"');
	//char *temp = (char*)attr_string.c_str();
	//vector<string> attr_array = splitString(temp, '\"');
	attr.gene_id = attr_array[1];
	attr.trans_id = attr_array[3];
	attr.exon_num = atoi(attr_array[5].c_str());
	attr.gene_name = convert_genename(attr_array[7]);
	attr.trans_name = attr_array[11];
	return attr;
}
/**********************************************/
string convert_genename(string raw_str)
{
	size_t found;
	found = raw_str.find("/");
	if (found != string::npos)
	{
		raw_str.replace(found, 1, "_");
	}
	return raw_str;
}

/**********************************************/
string extract_attr(string raw_string)
{
	vector<string> content_array = splitStringOld(raw_string, '\"');
	//char *temp = (char*)raw_string.c_str();
	//vector<string> content_array = splitString(temp, '\"');  	
	string attr_content = content_array[1];
	return attr_content;
}

/**********************************************/
map<string, vector<GENEBoundary> > convertGeneBoundary(map<string, map<string, GENEPosition> > GeneMap, int s_num )
{
	_sample_num = s_num;

	map<string, vector<GENEBoundary> > GBMap;
	map<string, map<string, GENEPosition> >::iterator it;
	map<string, GENEPosition> local_map;
	map<string, GENEPosition>::iterator l_it;

	string chr_name = "";
	chr_name.reserve(1000);
	for (it = GeneMap.begin(); it != GeneMap.end(); it++)
	{
		vector<GENEBoundary> local_vec;
		local_vec.reserve(20000);
		chr_name = (*it).first;
		local_map = (*it).second;
		for (l_it = local_map.begin(); l_it != local_map.end(); l_it++)
		{
			GENEPosition temp_pos = (*l_it).second;
			GENEBoundary gb_item;
			gb_item.gene_name = (*l_it).first;
			gb_item.start = temp_pos.start;
			gb_item.end = temp_pos.end;
			gb_item.strand = temp_pos.strand;
			//gb_item.status = 0;
			gb_item.status = (char*) malloc (_sample_num);
			memset(gb_item.status, 0, _sample_num*sizeof(char) );
			//gb_item.count = 0;
			gb_item.count = (int*) malloc (_sample_num);
			memset(gb_item.count, 0, _sample_num * sizeof(int) );

			gb_item.finish_sample = 0;
			gb_item.flag = 0;

			local_vec.push_back(gb_item);
		}

		sort(local_vec.begin(), local_vec.end(), comp_gene);

		GBMap[chr_name] = local_vec;

	}
	return GBMap;
}


/**********************************************/
void AnalyzeAllMapping( char *map_file, map<string, coor_record> &coor_map, int read_length, char *log_file )
{
	// input sam file is 1-based, but output log is 0-based IN TRANSCRIPT!
	
	FILE *fp = fopen(map_file, "r");
	FILE *log_fp = fopen(log_file, "w");
	vector<string> token_array;
	vector<string> id_array;
	vector<int> mapping_count;

	int line = 0;
	int prev_index = 0;
	int pos = 0, 
		end_pos = 0,
		max_pos = 0;
	string trans_name = "";
	string chr_name = "";
	string gene_name = "";

	map<string, coor_record>::iterator it;

	char *readline = (char*)malloc (2000);
	while( NULL != fgets(readline, 2000, fp) && '@'== readline[0] );

	do
	{
		token_array = splitString(readline, '\t');
		id_array = splitStringOld(token_array[0], '_');

		if (  0 != trans_name.compare(id_array[1]) )
		{
			if ( 0 < line )
			{
				if (1 == ana_flag ) {fprintf(stdout, "analyzing %s\n", trans_name.c_str() );}
				// finalize the vector
				check_mapping(trans_name, mapping_count, max_pos, log_fp, chr_name, gene_name);
			}

			trans_name = id_array[1];
			// initialize the vector
			it = coor_map.find(trans_name);
			if ( it != coor_map.end() )
			{
				if (1 == ana_flag) {fprintf(stdout, "Init\t%s\t%d\n", trans_name.c_str(), coor_map[trans_name].length);}
				max_pos = coor_map[trans_name].length;
				chr_name = coor_map[trans_name].chr_name;
				gene_name = coor_map[trans_name].gene_name;
				mapping_count.clear();
				mapping_count.reserve(max_pos);
				for( int i =0; i < max_pos; i++)
				{
					mapping_count[i] = 0;
				}
			}
			prev_index = 0;
			// End initialization
		}
		
		pos = atoi( id_array[2].c_str() );
		if ( pos == prev_index ) 
		{
			if (1 == ana_flag) {fprintf(stdout, "multi_read\t%s\t%d\n", trans_name.c_str() , pos);}
			mapping_count[pos-1] = -1; // why pos-1? since sam is 1-based, and we convert to 0-based here
			end_pos = pos + read_length - 1;
			if ( end_pos < max_pos )
			{
				mapping_count[end_pos-1] = -1;
			}
		}

		prev_index = pos;
		line++;
		if ( 0 == line%100000 )
		{
			fprintf(stderr, ".");
		}


	}while( NULL != fgets(readline, 2000, fp) );

	if ( 1 == ana_flag) {fprintf(stdout, "analyzing %s\n", trans_name.c_str() );}
	// finalize the vector
	check_mapping(trans_name, mapping_count, max_pos, log_fp, chr_name, gene_name);

	fclose(fp);
	fclose(log_fp);
} 


/**********************************************/
void check_mapping(string &trans_name, vector<int> &mapping_count, int max_pos, FILE *log_fp, string &chr_name, string &gene_name)
{
	int seg_length = 0;
	int start =0,
		end = 0;

	for( int i = 0; i < max_pos; i++ )
	{
		if ( 0 == mapping_count[i] )
		{
			if ( 0 == seg_length )
			{
				start = i;
			}
			seg_length ++;
			end = i;
		}

		else
		{
			if ( 0 < seg_length )
			{
				// output from start to end
				fprintf(log_fp, "uniq\t%s\t%s\t%s\t%d\t%d\t%d\n", chr_name.c_str(), gene_name.c_str(), trans_name.c_str(), start, end, seg_length);
				seg_length = 0;
			}
		}
	}

	if ( 1 < seg_length )
	{
		fprintf(log_fp, "uniq\t%s\t%s\t%s\t%d\t%d\t%d\n", chr_name.c_str(), gene_name.c_str(), trans_name.c_str(), start, end, seg_length);
	}
}


/**********************************************/
// Assume the sam file is sorted by read name, and multi-mapping reads are filtered (-n 1)
// we only want to filter the Starting Positions (instrad of full region) of multi-mappings for transcripts
void AnalyzeUniqMapping( char *map_file, map<string, coor_record> &coor_map, int read_length, char *log_file )
{
	// input sam file is 1-based, coor_map are also 1-based 
	// but output log will be 0-based IN TRANSCRIPT!
	
	FILE *fp = fopen(map_file, "r");
	FILE *log_fp = fopen(log_file, "w");
	vector<string> token_array;
	vector<string> id_array;
	vector<int> mapping_count;

	int line = 0;
	int prev_index = 0;
	int pos = 0, //end_pos = 0,
		max_pos = 0;
	string trans_name = "";
	string chr_name = "";
	string gene_name = "";

	map<string, coor_record>::iterator it;

	char *readline = (char*)malloc (2000);
	while( NULL != fgets(readline, 2000, fp) && '@'== readline[0] );

	do
	{
		token_array = splitString(readline, '\t');
		id_array = splitStringOld(token_array[0], '_');

		if (  0 != trans_name.compare( id_array[1] ) )// starts a new transcript
		{
			if ( 0 < line )//output
			{
				if ( 1 == ana_flag ) { fprintf(stdout, "analyzing\t%s\n", trans_name.c_str() ); }
				// finalize the vector
				check_uniq_mapping(trans_name, mapping_count, max_pos, log_fp, chr_name, gene_name);
			}

			trans_name = id_array[1];
			// Initialize the vector
			it = coor_map.find(trans_name);
			if ( it != coor_map.end() )
			{
				if ( 1 == ana_flag ) {fprintf(stdout, "Init\t%s\t%d\n", trans_name.c_str(), coor_map[trans_name].length);}
				max_pos = coor_map[trans_name].length; // total length 
				chr_name = coor_map[trans_name].chr_name;
				gene_name = coor_map[trans_name].gene_name;
				mapping_count.clear();
				mapping_count.reserve(max_pos);
				for( int i =0; i < max_pos; i++)
				{
					mapping_count[i] = 0;
				}
			}
			else 
			{
				fprintf(stderr, "Warning: %s not found in coordinate map\n", trans_name.c_str() );
			}
			prev_index = 0;
			// End initialization
		}
		
		pos = atoi( id_array[2].c_str() );
		mapping_count[ pos - 1 ] = 1; // uniquely mappable
		//if ( pos == prev_index ) 
		//{
		//	if (1 == ana_flag) {fprintf(stdout, "multi_read\t%s\t%d\n", trans_name.c_str() , pos);}
		//	mapping_count[pos-1] = -1; // why pos-1? since sam is 1-based, and we convert to 0-based here
		//	end_pos = pos + read_length - 1;
		//	if ( end_pos < max_pos )
		//	{
		//		mapping_count[end_pos-1] = -1;
		//	}
		//}

		prev_index = pos;
		line++;
		if ( 0 == line%100000 )
		{
			fprintf(stderr, ".");
		}


	}while( NULL != fgets(readline, 2000, fp) );

	if ( 1 == ana_flag) {fprintf(stdout, "analyzing %s\n", trans_name.c_str() );}
	// finalize the vector
	check_uniq_mapping(trans_name, mapping_count, max_pos, log_fp, chr_name, gene_name);

	fclose(fp);
	fclose(log_fp);
} 


/**********************************************/
void check_uniq_mapping(string &trans_name, vector<int> &mapping_count, int max_pos, FILE *log_fp, string &chr_name, string &gene_name)
{ // 1 for unique mapped position, 0 otherwise 
	int seg_length = 0;
	int start = 0, // starting position of current interval
		end = 0;

	for( int i = 0; i < max_pos; i++ )
	{
		if ( 1 == mapping_count[i] )
		{
			if ( 0 == seg_length )
			{
				start = i;
			}
			seg_length ++;
			end = i;
		}

		else
		{
			if ( 0 < seg_length )
			{
				// output from start to end
				fprintf(log_fp, "uniq\t%s\t%s\t%s\t%d\t%d\t%d\n", chr_name.c_str(), gene_name.c_str(), trans_name.c_str(), start, end, seg_length);
				seg_length = 0;
			}
		}
	}

	if ( 0 < seg_length )
	{
		fprintf(log_fp, "uniq\t%s\t%s\t%s\t%d\t%d\t%d\n", chr_name.c_str(), gene_name.c_str(), trans_name.c_str(), start, end, seg_length);
	}
}

/**********************************************/

void printGTF( vector<GENEBoundary > gb_vector)
{
	vector<GENEBoundary>::iterator vit;                                                                                             
	GENEBoundary gb_item; 
	for(vit = gb_vector.begin(); vit < gb_vector.end(); vit++)   
	{                    
		gb_item = (*vit);                                     
		printf(" %s %d %d %c\n", gb_item.gene_name.c_str(), gb_item.start, gb_item.end, gb_item.strand);                           
	}                                                                                                                                      
}

/**********************************************/
void printPTGeneMap(map<string, GENEPosition>  PTMap)
{
	map<string, GENEPosition>::iterator it;
	for( it = PTMap.begin(); it != PTMap.end(); it++)
	{
		printf("%s ", (*it).first.c_str());
		GENEPosition g_pos = (*it).second;
		//map<string, GENEPosition>::iterator mit;
		//for(mit = pos_map.begin(); mit != pos_map.end(); mit++)
		//{
		printf("%s %s %s %d %d %d\n", g_pos.chr_name.c_str(), g_pos.gene_name.c_str(), g_pos.gn_name.c_str(),  g_pos.start, g_pos.end, (int)g_pos.epos.size());
		int limit = (int)g_pos.epos.size();
		for( int i =0; i < limit; i++ )
		{
			printf(" %d %d %d\n", g_pos.epos[i].id, g_pos.epos[i].start, g_pos.epos[i].end );
		}
		//}
	}
}
/**********************************************/
void printGeneMap(map<string, map<string, GENEPosition> > GeneMap)
{
	map<string, map<string, GENEPosition> >::iterator it;
	for( it = GeneMap.begin(); it != GeneMap.end(); it++)
	{
		printf(">%s\n", (*it).first.c_str());
		map<string, GENEPosition> pos_map = (*it).second;
		map<string, GENEPosition>::iterator mit;
		for(mit = pos_map.begin(); mit != pos_map.end(); mit++)
		{
			printf(" %s %d %d\n", (*mit).first.c_str(), (*mit).second.start, (*mit).second.end);
		}
	}
}
/**********************************************/
void printExonPosition( vector<ExonPosition> epos)
{	
	vector<ExonPosition>::iterator it;
	for(it = epos.begin(); it < epos.end(); it++)
	{
		printf(" %d %d %d\n",(*it).id, (*it).start, (*it).end);
	}
}

/**********************************************/
void printExonPositionRange(vector<ExonPosition> epos, int start, int end)
{
	if (start < 0 || (int)epos.size() <= end )
	{
		printf(" error in printExonPositionRange\n");
	}
	else
	{
		//int i;
		//vector<ExonPosition>::iterator it;
		for(int i = start; i <= end; i++)
		{	
			ExonPosition tep = epos[i];
			printf("\t%d: %d %d\n", i, tep.start, tep.end);
		}		
	}
}


/**********************************************/
void OutputGeneStat( map<string, GENEPosition> &PTMap)
{
	
	FILE *fp = fopen("gtf_stat.txt", "w");

	map<string, GENEPosition>::iterator it;

	//for( it = PTMap.begin(); it != PTMap.end(); it++)
	//{
	//	fprintf(fp, "%s\t%s\t%s\t%d");
	//}
	fclose(fp);
}
/**********************************************/
void OutputFusionPrediction( vector<fusion> &fusion_vec)
{
	int limit= (int)fusion_vec.size();
	E("%d Fusions in the prediction\n", limit);
	fusion t_f;
	for( int i =0; i < limit; i++)
	{
		//E("See %d\n", i);
		t_f = fusion_vec[i];
		int lim_1= (int)t_f.coor1.size();
		//E("See %d-%d-%d\n", limit, i, lim_1);
		int j1 =0;
		while( j1 < lim_1)
		{
			//fprintf(out_fp,"%s\t0\t%s\t%c\t%d\t%d\n", t_f.id.c_str(), t_f.chr1.c_str(), t_f.strand1, t_f.coor1[j1], t_f.coor1[j1+1]);
			L("%s\t0\t%s\t%c\t%d\t%d\n", t_f.id.c_str(), t_f.chr1.c_str(), t_f.strand1, t_f.coor1[j1], t_f.coor1[j1+1]);
			j1+=2;
		}
		int lim_2= (int)t_f.coor2.size();
		//E("See %d-%d-%d\n", limit, i, lim_2);
		int j2 =0;
		while( j2 < lim_2)
		{
			//fprintf(out_fp,"%s\t1\t%s\t%c\t%d\t%d\n", t_f.id.c_str(), t_f.chr2.c_str(), t_f.strand2, t_f.coor2[j2], t_f.coor2[j2+1]);
			L("%s\t1\t%s\t%c\t%d\t%d\n", t_f.id.c_str(), t_f.chr2.c_str(), t_f.strand2, t_f.coor2[j2], t_f.coor2[j2+1]);
			j2+=2;
		}
	}
}

/**********************************************/
void OutputLengthSummary( map<string, vector<tr_length> > &length_map)
{
	int limit= (int)length_map.size();
	E("Outputing %d Records for Transcripts\n", limit);
	FILE *out_fp = fopen("summary.txt", "w");
	map<string, vector<tr_length> >::iterator it;
	for( it = length_map.begin(); it != length_map.end(); it++)
	{
		int reg_num = (int)(*it).second.size();
		for( int i = 0; i < reg_num; i ++)
		{
			fprintf(out_fp, "%s\t%s\t%d\n", (*it).first.c_str(), (*it).second[i].chr_id.c_str(), (*it).second[i].length);
		}
	}
	fclose(out_fp);
}
