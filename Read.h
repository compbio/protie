#ifndef __READ__
#define __READ__
#include <vector>
#include <map>
#include <string>
//#include "GeneAnnotation.h"
void ExtractReads( char *fastq_file, char *gene_file , char *out_file);
void OutputSplitSeq( char *pred_file , char *out_file);
// for mode 2
void ExtractMappings( char *fastq_file, char *gene_file , char *out_file);
// for mode 3
void ClassifyMappings( char *sam_file );
// for mode 4
void ClassifyBadMappings( char *sam_file );
// for mode 7
void CountBadMappings( char *sam_file );
// for mode 8
void CountMappings( char *sam_file );
// for mode 5
void GetFastqFromSam( char *sam_file, char *output );
// for mode 6
void GetFastqFromSam_STDOUT( char *sam_file, char *output );
// for mode 9
void CheckClipping( char *sam_file, char *output );
// for mode 10
void CheckBowtieMappings( char *sam_file, char *output , int max_e );
// for mode 11
void CountReadOcc( char *sam_file, char *output , int max_e );
// for mode 12
void CheckBWAMappings( char *sam_file, char *output , int max_e );
// for mode 13
void SelectMappings( char *pred_file, char *sam_file, char *output );
// for mode 14
void MergeMates( char *first_file, char *second_file, char *output );


// misc tools
int calculate_ml( char *cigar);
void copy_string(char *src, char *dest);
void copy_string_reverse(char *src, char *dest);
void copy_string_rc(char *src, char *dest);
int clip_length(char *cigar);
int get_pair_quality( const std::vector<int> &vec_clip, const std::vector<int> &vec_tlen, int max_e);
//void DNATranslate( const string &g_seq, vector<string> &trasn_vec, int flag );
//int bp_dist(int left, int right, int bp);
//void make_rc( const string &raw_str, string &new_str);
//char ToProtein( const string &trigram);
//void SummarizeFasta( char *pred_file, char *out_file );
//void ExtractPeptideFromFasta( char *pred_file, char *out_file );
//void ExtractPeptideFromFasta_Mis( char *pred_file, char *out_file );
//int generate_peptide(string &final_str, int left, int right, char *raw_str);

#endif
