#ifndef __FUSION__
#define __FUSION__
//#include <vector>
#include <map>
#include <string>
//#include "GeneAnnotation.h"
void ConvertToProteins( char *pred_file, char *out_file, char *lib_name, int pep_length, int mode);
void DNATranslate( const string &g_seq, vector<string> &trasn_vec, int flag );
int bp_dist(int left, int right, int bp);
//void make_rc( const string &raw_str, string &new_str);
char ToProtein( const string &trigram);
void SummarizeFasta( char *pred_file, char *out_file );
void ExtractPeptideFromFasta( char *pred_file, char *out_file );
void ExtractPeptideFromFasta_Mis( char *pred_file, char *out_file );
int generate_peptide(string &final_str, int left, int right, char *raw_str);


#endif
