/*
 * Copyright (c) 2012 - 2013, Simon Fraser University
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 *   
 * Redistributions of source code must retain the above copyright notice, this list
 * of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or other
 *   materials provided with the distribution.
 * - Neither the name of the Simon Fraser University nor the names of its contributors may be
 *   used to endorse or promote products derived from this software without specific
 *   prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * Author         : Yen-Yi Lin
 * Email          : yenyil AT sfu DOT ca
 * Last Update    : June 28, 2013.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>

#include "CommandLineParser.h"
#include "Common.h"
using namespace std;

int Mode_GetFusionPep = 0;
int Mode_CheckProtID = 0;
int Mode_CheckPepSEQ = 0;
int Mode_CheckDtaID = 0;
int Mode_CheckMultiHit = 0;
int Mode_CountEvent = 0;
int Mode_GetFusionEvent_0 = 0;
int Mode_GetFusionEvent = 0;
int Mode_GetSVEvent = 0;
int Mode_deFuseInfo = 0;
int Mode_AdjustPvalue = 0;
int Mode_CheckVCF = 0;
int Mode_ConvertVCF = 0;
int Mode_GetSVPep = 0;
int Mode_MergePep = 0;


//
int mode_count = 1;
int Op_Mode = 1;
int Join_Mode = 0;
int Reads_Model = 1;
int Decoy_Mode = 1; // Ignore Decoy
int Mix_Mode = 1; // Ignore Decoy
int Library_Type = 0;
int Orman_Flag = 50;
int Reads_Buffer = 100;
int Abe_Mode = 1; // what kinds of event does protie need to consider when expanding peptide header
char GtfFilename[FILE_NAME_LENGTH];
char RepeatFilename[FILE_NAME_LENGTH];
char FastqFilename[FILE_NAME_LENGTH];
char SeqFilename[FILE_NAME_LENGTH]="\0";
char SamFilename[FILE_NAME_LENGTH]="\0";
char SamListname[FILE_NAME_LENGTH];
char Predictname[FILE_NAME_LENGTH];
char Libraryname[FILE_NAME_LENGTH]="CPTAC";
char TargetFilename[FILE_NAME_LENGTH]="";
char Outputname[FILE_NAME_LENGTH]="new";
char OutputFolder[FILE_NAME_LENGTH] = "./temp/";
float Error_Bound = 0.01;

/**********************************************/
int parseCommandLine( int argc, char *argv[] )
{
	int opt;
	int opt_index;
	int gtf_flag = 0;//sam_flag = 0;

	static struct option long_opt[] = 
	{
		// Binary Options which do not take parameters
		{"help", no_argument, 0, 'h'},
		{"version", no_argument, 0, 'v'},
		{"getfusionpep", no_argument, &Mode_GetFusionPep, 1}, // polish later
		{"checkprotid", no_argument, &Mode_CheckProtID, 1}, // polish later
		{"checkpepseq", no_argument, &Mode_CheckPepSEQ, 1}, // polish later
		{"checkdtaid", no_argument, &Mode_CheckDtaID, 1}, // polish later
		{"checkmultihit", no_argument, &Mode_CheckMultiHit, 1}, // polish later
		{"countevent", no_argument, &Mode_CountEvent, 1}, // polish later
		{"getfusionevent_0", no_argument, &Mode_GetFusionEvent_0, 1}, // polish later
		{"getfusionevent", no_argument, &Mode_GetFusionEvent, 1}, // polish later
		{"defuseinfo", no_argument, &Mode_deFuseInfo, 1}, // polish later
		{"getsvevent", no_argument, &Mode_GetSVEvent, 1}, // polish later
		{"adjustp", no_argument, &Mode_AdjustPvalue, 1}, // polish later
		{"checkvcf", no_argument, &Mode_CheckVCF, 1}, // polish later
		{"convertvcf", no_argument, &Mode_ConvertVCF, 1}, // polish later
		{"getsvpep", no_argument, &Mode_GetSVPep, 1}, // polish later
		{"mergepep", no_argument, &Mode_MergePep, 1}, // polish later
		////////////////////////////////////////////////////////////////
		{"global", required_argument, 0, 'g'},
		{"fastq", required_argument, 0, 'f'},
		{"predict", required_argument, 0, 'p'},
		//{"joint", required_argument, 0, 'j'},
		{"seq", required_argument, 0, 's'},
		//{"sam-list", required_argument, 0, 'S'},
		{"mode", required_argument, 0, 'm'},
		{"error", required_argument, 0, 'e'},
		{"output", required_argument, 0, 'o'},
		{"repeat", no_argument, 0, 'r' },
		//{"type", required_argument, 0, 't'},
		{"target", required_argument, 0, 't'},
		{"decoy", required_argument, 0, 'd'},
		{"mix", required_argument, 0, 'x'},
		{"abe", required_argument, 0, 'b'},
		{"lib-name", required_argument, 0, 'l'},
		{0,0,0,0},
	};

	while ( -1 !=  (opt = getopt_long( argc, argv, "hr:vg:f:p:s:S:t:m:e:o:d:b:l:x:", long_opt, &opt_index )  ) ) 
	{
		switch(opt)
		{
			case 'h':
				printHELP();
				return 0;
				break;
			case 'v':
				L("%s.%s\n", versionNumber, versionNumberF);
				return 0;
				break;
			case 'g':
				strncpy(GtfFilename, optarg, FILE_NAME_LENGTH );
				//gtf_flag = 1;
				break;
			case 'f':
				strncpy(FastqFilename, optarg, FILE_NAME_LENGTH );
				//gtf_flag = 1;
				break;
			case 'p':
				strncpy(Predictname, optarg, FILE_NAME_LENGTH);
				gtf_flag = 1;
				break;
			case 'l':
				strncpy(Libraryname, optarg, FILE_NAME_LENGTH);
				break;
			case 't':
				strncpy(TargetFilename, optarg, FILE_NAME_LENGTH);
				//Join_Mode = 1;
				break;
			case 's':
				strncpy(SeqFilename, optarg, FILE_NAME_LENGTH);
				break;
			case 'S':
				strncpy(SamListname, optarg, FILE_NAME_LENGTH);
				break;
			case 'm':
				Op_Mode = atoi(optarg);
				break;
			//case 't':
			//	Library_Type = atoi(optarg);
			//	break;
			case 'd':
				Decoy_Mode = atoi(optarg);
				break;
			case 'x':
				Mix_Mode = atoi(optarg);
				break;
			case 'e':
				Error_Bound = atof(optarg);
				if ( 0 > Error_Bound){Error_Bound = 100.00;}
				break;
			case 'b':
				Abe_Mode = atoi(optarg);
				break;
			case 'o':
				strncpy(Outputname, optarg, FILE_NAME_LENGTH);
				break;		
			case 'r':
				strncpy(RepeatFilename, optarg, FILE_NAME_LENGTH);
				break;		
			case '?': 
				fprintf(stderr, "Unknown parameter: %s\n", long_opt[opt_index].name);
				abort();
				return 0;
				break;
			//default:
				//printHELP();
		}
	}
	if ( !( Mode_GetFusionPep + Mode_CheckProtID + Mode_CheckPepSEQ + Mode_CheckDtaID + Mode_CountEvent +  Mode_GetFusionEvent_0 + Mode_GetFusionEvent + Mode_deFuseInfo
		+ Mode_CheckMultiHit + Mode_GetSVEvent + Mode_AdjustPvalue  + Mode_CheckVCF + Mode_ConvertVCF + Mode_GetSVPep + Mode_MergePep ) )
	{
		fprintf(stdout, "Unknown mode for protie_tools. See help for details.\n");
		printMODELHELP();
		exit(1);
	}

	return 1;
}
/**********************************************/
void printHELP()
{
	fprintf(stdout, "\nGeneral Options:\n");
	fprintf(stdout, "-h|--help:\tShows help message.\n");
	fprintf(stdout, "-v|--version:\tShows current version.\n");
	fprintf(stdout, "-p|--predict:\tSpecify the read id (sorted) of interested.\n");
	fprintf(stdout, "-s|--seq:\tSequence File Containing ALL ORIGINAL Peptide Contents. (FASTA Format)\n");
	fprintf(stdout, "-g|--global:\t*peptable for microSV predictions\n");
	//fprintf(stdout, "-l|--lib-name:\tSpecify library name for a MERGED fasta file. (default: CPTAC.)\n");
	fprintf(stdout, "\nAdvanced Options:\n");
	//fprintf(stdout, "-m|--mode:\tCategory of input files (default = 1, see details below).\n");
	fprintf(stdout, "-o|--output:\tname of converted output. ( default = new )\n");
	//fprintf(stdout, "-t|--target:\tname of small set of relevant peptide file. (from output of -m 3)\n");
	//fprintf(stdout, "-r|--repeat:\trepeat file information from RepeatMasker. E.g., ~/Annotation/Masked/hg19.fa.out\n");
	fprintf(stdout, "-e|--error:\tmaximum qvalue allowed for confident PSM search, inclusive. (default = 0.01). -1 for all records.\n");
	fprintf(stdout, "-d|--decoy:\tAsk protie to include/exclude decoy records in analysis. 1 for excluding, 0 otherwsie (default = 1).\n");
	fprintf(stdout, "-x|--mix:\tIf protie should output events from incorrect mix. 1 for only correct mix ones. 0 otherwise. (default = 1, correct mix only).\n");
	fprintf(stdout, "-b|--abe:\tAberration type in getfusionevent or getsvevent. 1 for fusion, 2 for sv (default = 1).\n");
	printMODELHELP();
	fprintf(stdout, "\nExample Commands:\n");
	fprintf(stdout, "\n./protie_tools --getfusionpep -p defuse.tsv -o output.fa\n\n");
	fprintf(stdout, "\n./protie_tools --checkprotid -f protein.fasta -p tsv -o output\n\n");
	fprintf(stdout, "\n./protie_tools --checkdtaid -p tsv -r base_tsv -o output -e 0.01\n\n");
	fprintf(stdout, "\n./protie_tools --checkpepseq -f protein.fasta -p tsv -o output -e 0.01 -d 1\n\n");
	
	fprintf(stdout, "\n./protie_tools --checkmultihit -p tsv -o output -e 0.01 -d 1\n\n");

	fprintf(stdout, "\n./protie_tools --countevent -p tsv -f seq.freq -o output\n\n");
	
	fprintf(stdout, "\n./protie_tools --getfusionevent -s all_seq.fa -f seq.freq -p tsv -o output -x 1\n\n");
	
	fprintf(stdout, "\n./protie_tools --defuseinfo -f results.classify.tsv -p tsv -o output \n\n");
	
	fprintf(stdout, "\n./protie_tools --getsvevent -f seq.freq -s patient.pep -g patient.peptable -p tsv -o output -b 2 -x 1\n\n");

	fprintf(stdout, "\n./protie_tools --adjustp -p tsv -f protein.fastq -o prefix\n\n");
	
	fprintf(stdout, "\n./protie_tools --checkvcf -g ensembl.gtf -p mistrvar.vcf -o prefix\n\n");
	
	fprintf(stdout, "\n./protie_tools --convertvcf -t mistrvar.align -p mistrvar.vcf -f genome.fa -o prefix\n\n");
	
	fprintf(stdout, "\n./protie_tools --getsvpep -p mistrvar.transcript -o output.prefix\n\n");
	
	fprintf(stdout, "\n./protie_tools --mergepep -f input.fa -o output.prefix -l libname\n\n");

	//fprintf(stdout, "./msgf_tools -m 0 -p any.textfile\n");
	//fprintf(stdout, "./msgf_tools -m 1 -p sniper.tsv -o output\n");
	//fprintf(stdout, "./msgf_tools -m 2 -s seq.fa -f seq.freq -p sniper.tsv -o output\n");
	//fprintf(stdout, "./msgf_tools -m 3 -s seq.fa -f seq.freq -p sniper.tsv -g patient.peptable -o target\n");
	//fprintf(stdout, "./msgf_tools -m 4 -t target.pepinfo -s seq.fa -f seq.freq -p sniper.tsv -g patient.peptable -o output\n");
	//fprintf(stdout, "./msgf_tools -m 6 -p rnaseq.sam -o output\n");
	//fprintf(stdout, "./msgf_tools -m 7 -p sth.merged -o output\n");
	//fprintf(stdout, "./msgf_tools -m 8 -p sth.pass -o output\n");
	//fprintf(stdout, "./msgf_tools -m 15 -p sniper.tsv -o output\n");


	fprintf(stdout, "\n");
}
/**********************************************/
void printMODELHELP()
{

	fprintf(stdout, "\nOptions for interpretation:\n");
	fprintf(stdout, "--getfusionpep:\tGet Fusion Peptide from deFuse prediction tsv file.\n");
	fprintf(stdout, "--checkprotid:\tClassify TSV Based on protein header from a fasta file.\n");
	fprintf(stdout, "--checkdtaid:\tClassify the corresponding entry in known search results.\n");
	fprintf(stdout, "--checkpepseq:\tClassify TSV Based on Raw Peptide sequence content.\n\n");
	fprintf(stdout, "--checkmultihit:\tClassify TSV Based on Raw Peptide sequence content.\n\n");
	fprintf(stdout, "--getfusionevents:\tExtract all relevant events for fusion-relevant spectra.\n\n");
	fprintf(stdout, "--defuseinfo:\tExtract defuse information events of interests.\n\n");
	//fprintf(stdout, "0:\tReporting Maximum Line Length in a File.\n");
	//fprintf(stdout, "1:\tCounting Number of SV Calls in a MSGF TSV File ( Only to CPTAC Peptides ).\n");
	//fprintf(stdout, "2:\tGetting Original Peptide Information for Good Calls ( Containing CPTAC Peptides ).\n");
	//fprintf(stdout, "\t\t-f should be the frequency results from stools mode 34.\n");
	//fprintf(stdout, "3:\tExtracting Limited Amounts of Interesting Peptides containing SVs.\n");
	//fprintf(stdout, "4:\tExpand the TSV File with Actual SV Information.\n");
	//fprintf(stdout, "6:\tChecking Reads From RNA-Seq Mappings Results.\n");
	//fprintf(stdout, "7:\tExtracting and Merging Event with RNA-Seq Crossing At Least One Side.\n");
	//fprintf(stdout, "8:\tSummarizing For a Patient.\n");
	//fprintf(stdout, "15\tCollecting Number of Matched Peptides/Peoteins from a TSV file.\n");
}
/**********************************************/
