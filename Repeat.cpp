#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <iostream>
#include <fstream>
#include <string>
#include <map>
#include <algorithm>

#include "Repeat.h"
#include "Common.h"
//#include "GeneAnnotation.h"
#include "Fasta.h"

#define FASTA_LINE 60

using namespace std;
/**********************************************/
void convert_ensembl_ref(char *chr_char, string &chr_str)
{
	chr_str = "";
	chr_str = string(chr_char).substr(3);
	if ("M" == chr_str ){chr_str += "T";} // from M to MT
}

/**********************************************/
void convert_repeat_info(char *fea, string &repeat_info)
{
	repeat_info = "REPEAT";

	if ( !strcmp("DNA", fea))
	{
		repeat_info = "DNA";
	}
	else if ( !strcmp("RNA", fea))
	{
		repeat_info = "RNA";
	}
	else if ( !strcmp("LINE", fea))
	{
		repeat_info = "LINE";
	}
	else if ( !strcmp("SINE", fea))
	{
		repeat_info = "SINE";
	}
	else if ( !strcmp("LTR", fea))
	{
		repeat_info = "LTR";
	}
}

/**********************************************/
bool sortRepeatByStart( const repeat_t &r1, const repeat_t &r2 )
{
	return r1.s < r2.s;
}
/**********************************************/
bool sortRepeatByEnd( const repeat_t &r1, const repeat_t &r2 )
{
	return r1.e < r2.e;
}
/**********************************************/
void parse_Repeat( char *repeat_file , map<string, vector <repeat_t> > &map_repeat )
{
	string total_seq="", total_rc="";
	
	char *readline=(char*)malloc(MAX_LINE);
	char *misc=(char*)malloc(TOKEN_LENGTH);
	char *ref=(char*)malloc(TOKEN_LENGTH);
	char *fea=(char*)malloc(TOKEN_LENGTH);
	char *sim=(char*)malloc(TOKEN_LENGTH);

	//char header;
	int count = 0;
	int offset =0;
	uint32_t start = 0, end = 0,
			score =0, rid = 0;
	//float sim = 0.0; // similarity
	repeat_t tmp_repeat;
	string new_ref;// formatted reference without chr prefix
	string repeat_info;// formatted reference without chr prefix
	FILE *fp = fopen( repeat_file, "r");
	//FILE *out_fp= fopen( out_file, "w");
	//int cur = 0, cur_gene = 0;
	int ret;
	while( NULL != fgets(readline, MAX_LINE, fp) )
	{
		ret = sscanf( readline, "%u %s %s %s %s %u %u %s %s %s %s %s %s %s %u %n\n", 
			&score, misc, misc, misc, ref,
			&start, &end, misc, misc, misc,
			fea, misc, misc, misc,
			&rid, &offset);
		if ( 0 == strlen(ref)){continue;} // Ignore header by skipping weird reference
		//L("%d\t%u\t%s\t%u\t%u\t%s\t%u\n", ret, score, ref, start, end, fea, rid);

		if ( 7 >= ret ){ E("Undefined Format:%s", readline);}

		convert_ensembl_ref( ref, new_ref);
		convert_repeat_info( fea, repeat_info);

		tmp_repeat.rid = rid;
		tmp_repeat.score = score;
		tmp_repeat.s = start;
		tmp_repeat.e = end;
		tmp_repeat.ref = new_ref;
		tmp_repeat.r_info = repeat_info;
		map_repeat[new_ref].push_back(tmp_repeat);
		count++;

	}

	map<string, vector<repeat_t> >::iterator it;
	for(it = map_repeat.begin(); it != map_repeat.end(); it++)
	{
		//sort( it->second.begin(), it->second.end(), sortRepeatByStart);
		sort( it->second.begin(), it->second.end(), sortRepeatByEnd);
	}

	E("Total Prediction Record\t%d\n", count);
	E("Total References with Predictions:\t%d\n", (int)map_repeat.size());
	fclose(fp);
	free(fea);
	free(ref);
	free(misc);
	free(readline);
}

///**********************************************/
//void copyToString( char *src, string &new_str)
//{
//	int limit=strlen(src);
//	new_str.clear();
//	new_str.reserve(limit);
//	for( int i = 0; i < limit; i ++)
//	{
//		new_str.push_back(src[i]);
//	}
//}

///**********************************************/
//bool sortByStart( const unit_pred &p1, const unit_pred &p2 )
//{
//	return p1.s < p2.s;
//}
///**********************************************/
///**********************************************/
//void SortPredictionOnly( map<string, vector<unit_pred> > &new_record, map<string, vector<unit_pred> > &map_record  )
//{
//	int limit=0;
//	map<string, vector<unit_pred> >::iterator it;
//	for( it = map_record.begin(); it != map_record.end(); it++)
//	{
//		// sorting by the starting coordinate
//		sort( it->second.begin(), it->second.end(), sortByStart);
//		limit=(int)it->second.size();
//		int diff = 0;
//
//		vector<unit_pred> tmp_pred;
//		tmp_pred.reserve( limit );
//		tmp_pred.push_back(it->second[0]);
//		L( "==%d\t%s\t%u\t%u\t%u\n", it->second[0].id, it->first.c_str(), it->second[0].s, it->second[0].e, it->second[0].s);
//		for( int i =1; i < limit; i ++)
//		{
//			tmp_pred.push_back( it->second[i] );
//		}
//
//		new_record[it->first] = tmp_pred;
//	}
//	
//
//}
///**********************************************/
//void analyze_record( unit_pred &pred)
//{
//	// determine the boundary of inversion calls
//	int a =0;
//}
///**********************************************/
//// To Compute the extended boundary of contigs on the reference
//void MergePrediction( map<string, vector<unit_pred> > &sorted_record,  char *outfile  )
//{
//	int limit=0;
//	map<string, vector<unit_pred> >::iterator it;
//	for( it = sorted_record.begin(); it != sorted_record.end(); it++)
//	{
//		// Reading sorted record for a chromosome
//		limit=(int)it->second.size();
//		int diff = 0;
//
//		//vector<unit_pred> tmp_pred;
//		//tmp_pred.reserve( limit );
//		//tmp_pred.push_back(it->second[0]);
//		L( "==%d\t%s\t%u\t%u\t%u\n", it->second[0].id, it->first.c_str(), it->second[0].s, it->second[0].e, it->second[0].s);
//		for( int i =1; i < limit; i ++)
//		{
//			// calculate the boundary of this record
//			 analyze_record( it->second[i]);
//			//if ( ( it->second[i].s != it->second[i-1].s) ||  
//			//	(it->second[i].e != it->second[i-1].e) 
//			//	)
//			//{
//			//	tmp_pred.push_back( it->second[i] );
//			//	diff = it->second[i].s - it->second[i-1].e;
//			//	if ( 0 >= diff)
//			//	{
//			//		E("Warning\tOverlap %s %d %u %u with %d\n", it->first.c_str(), it->second[i].id, it->second[i].s, it->second[i].e, diff) ;
//			//	}
//			//	L( "==%d\t%s\t%u\t%u\t%d\n", it->second[i].id, it->first.c_str(), it->second[i].s, it->second[i].e, diff);
//
//			//}
//			//else
//			//{
//			//	E("Warning\tduplicate %s %d %u %u\n", it->first.c_str(), it->second[i].id, it->second[i].s, it->second[i].e);
//			//}
//		}
//
//		//new_record[it->first] = tmp_pred;
//	}
//	
//
//}
///**********************************************/
//void SortPrediction( map<string, vector<unit_pred> > &new_record, map<string, vector<unit_pred> > &map_record  )
//{
//	int limit=0;
//	map<string, vector<unit_pred> >::iterator it;
//	for( it = map_record.begin(); it != map_record.end(); it++)
//	{
//		// sorting by the starting coordinate
//		sort( it->second.begin(), it->second.end(), sortByStart);
//		limit=(int)it->second.size();
//		int diff = 0;
//
//		vector<unit_pred> tmp_pred;
//		tmp_pred.reserve( limit );
//		tmp_pred.push_back(it->second[0]);
//		L( "==%d\t%s\t%u\t%u\t%u\n", it->second[0].id, it->first.c_str(), it->second[0].s, it->second[0].e, it->second[0].s);
//		for( int i =1; i < limit; i ++)
//		{
//			if ( ( it->second[i].s != it->second[i-1].s) ||  
//				(it->second[i].e != it->second[i-1].e) 
//				)
//			{
//				tmp_pred.push_back( it->second[i] );
//				diff = it->second[i].s - it->second[i-1].e;
//				if ( 0 >= diff)
//				{
//					E("Warning\tOverlap %s %d %u %u with %d\n", it->first.c_str(), it->second[i].id, it->second[i].s, it->second[i].e, diff) ;
//				}
//				L( "==%d\t%s\t%u\t%u\t%d\n", it->second[i].id, it->first.c_str(), it->second[i].s, it->second[i].e, diff);
//
//			}
//			else
//			{
//				E("Warning\tduplicate %s %d %u %u\n", it->first.c_str(), it->second[i].id, it->second[i].s, it->second[i].e);
//			}
//		}
//
//		new_record[it->first] = tmp_pred;
//	}
//	
//
//}
///**********************************************/
//void OutputPrediction( map<string, vector<unit_pred> > &new_record, char *output  )
//{
//	char *out_name=(char*)malloc(MAX_LINE);
//	strcpy(out_name, output);
//	strcat(out_name, ".predict");
//	FILE *out_fp=fopen( out_name, "w");
//	int count = 0;
//
//	map<string, vector<unit_pred> >::iterator it;
//	for( it = new_record.begin(); it != new_record.end(); it++)
//	{
//		int limit = it->second.size();
//		for ( int i=0; i < limit; i++)
//		{
//			fprintf( out_fp, "%d\t%d\t%s\t%u\t%u\n", count, it->second[i].id, it->first.c_str(), it->second[i].s, it->second[i].e);
//			fprintf( out_fp, "%s%s%s", it->second[i].ref.c_str(), it->second[i].align.c_str(), it->second[i].contig.c_str());
//			count++;
//		}
//	}
//	fclose(out_fp);
//	
//}
