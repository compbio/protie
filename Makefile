CC=g++
CCOPT = -m64 -O -fPIC -fexceptions -DNDEBUG -DIL_STD
CFLAGS = -c -Wall -g $(CCOPT)
LDFLAGS = -lz -lm -g 
SOURCES = protie_tools.cpp Common.cpp CommandLineParser.cpp Fasta.cpp Msgf.cpp Flag.cpp Repeat.cpp Read.cpp GeneAnnotation.cpp Compare.cpp Prediction.cpp Peptide.cpp 
#SOURCES = protie_tools.cpp Common.cpp CommandLineParser.cpp Fasta.cpp Msgf.cpp Flag.cpp Repeat.cpp Read.cpp GeneAnnotation.cpp Compare.cpp Peptide.cpp 
OBJECTS = $(SOURCES:.cpp=.o)
EXECUTABLE = protie

all: $(SOURCES) $(EXECUTABLE)
	rm -rf *.o

$(EXECUTABLE): $(OBJECTS)
	$(CC) $(OBJECTS) -o $@ $(LDFLAGS)

.cpp.o:
	$(CC) $(CFLAGS) $< -o $@ 
clean:
	rm -f *.o *~ #*
