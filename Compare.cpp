#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <iostream>
#include <fstream>
#include <string>
#include <map>

#include "Compare.h"
//#include "Prediction.h"
#include "Common.h"
#include "GeneAnnotation.h"

#define FASTA_LINE 50
#define SEG_LENGTH 14

using namespace std;


/**********************************************/
int overlap( const uint32_t s1, const uint32_t e1, const uint32_t s2, const uint32_t e2)
{
	int flag = 0;
	if ( (s2 <= s1) && ( e1 <= e2) )
	{
		flag = 1;
	}
	else if ( (s1 <= s2) && ( e2 <= e1) )
	{
		flag = 2;
	}
	else if ( (s2 <= s1) && ( s1 <= e2))
	{
		flag = 3;
	}
	else if ( (s2 <= e1) && ( e1 <= e2))
	{
		flag = 4;
	}
	return flag;

}
/**********************************************/
int overlap_length( const uint32_t s1, const uint32_t e1, const uint32_t s2, const uint32_t e2)
{
	uint32_t length = 0;
	if ( (s2 <= s1) && ( e1 <= e2) )
	{
		length = e1 - s1 + 1;
	}
	else if ( (s1 <= s2) && ( e2 <= e1) )
	{
		length = e2 - s2 + 1;
	}
	else if ( (s2 <= s1) && ( s1 <= e2))
	{
		length = e2 - s1 + 1;
	}
	else if ( (s2 <= e1) && ( e1 <= e2))
	{
		length = e1 - s2 + 1;
	}
	return (int)length;

}
/**********************************************/
// check if inversion or duplication breakpoint is in a specific region
int vcf_overlap( const unit_vcf &t_vcf, const uint32_t s2, const uint32_t e2)
{
	int flag = 0;
	if ( "INV" == t_vcf.sv)
	{
		flag = overlap(t_vcf.s, t_vcf.e, s2, e2); // VCF and GTF coordinate system
	}
	else if ( (s2 <= t_vcf.pos) && ( t_vcf.pos <= e2))
	{
		flag = 1; // Insertion of Duplication or Other SV located in the corresponding regions
	}

	return flag;

}
/**********************************************/
// check overlapping length for inversion or duplication breakpoint is in a specific region
int vcf_overlap_length( const unit_vcf &t_vcf, const uint32_t s2, const uint32_t e2)
{
	int flag = 0;
	if ( "INV" == t_vcf.sv)
	{
		flag = overlap_length(t_vcf.s, t_vcf.e, s2, e2); // VCF and GTF coordinate system
	}
	else if ( (s2 <= t_vcf.pos) && ( t_vcf.pos <= e2))
	{
		flag = 1; // 1 bp overlapping
	}
	return flag;

}

/**********************************************/
// check if a region contains the left breakpoint of an inversion or duplication
int vcf_overlap_left( const unit_vcf &t_vcf, const uint32_t s2, const uint32_t e2)
{
	int flag = 0;
	if ( "INV" == t_vcf.sv)
	{
		if ( ( s2 <= t_vcf.s-1) && (t_vcf.s-1 <=  e2 ) )
		{ flag = 1;}
	}
	else if ( (s2 <= t_vcf.pos) && ( t_vcf.pos <= e2))
	{
		flag = 1; // Insertion of Duplication or Other SV located in the corresponding regions
	}

	return flag;

}
/**********************************************/
// check if a region contains the right breakpoint of an inversion or duplication
int vcf_overlap_right( const unit_vcf &t_vcf, const uint32_t s2, const uint32_t e2)
{
	int flag = 0;
	if ( "INV" == t_vcf.sv)
	{
		if ( ( s2 <= t_vcf.e-1) && (t_vcf.e-1 <=  e2 ) )
		{ flag = 1;}
	}
	else if ( (s2 <= t_vcf.pos) && ( t_vcf.pos <= e2))
	{
		flag = 1; // Insertion of Duplication or Other SV located in the corresponding regions
	}

	return flag;

}
/**********************************************/
void copy_match(match_info &m1, match_info &m2)
{
	m2.tname = m1.tname;
	m2.gname = m1.gname;
	m2.gid = m1.gid;
	m2.tid = m1.tid;
	m2.src = m1.src;
	m2.fea = m1.fea;
	m2.e_s = m1.e_s;
	m2.e_e = m1.e_e;
	m2.c_s = m1.c_s;
	m2.c_e = m1.c_e;
	m2.overlap = m1.overlap;
	m2.e_overlap = m1.e_overlap;
	m2.cds = m1.cds;
	m2.cds_trans = m1.cds_trans;
	m2.cds_gene = m1.cds_gene;
	m2.strand = m1.strand;
}
/**********************************************/
int better_candidate( int old_l, int new_l, const string &old_src, const string &new_str)
{
	int flag = 0;
	if ( "protein_coding" == old_src)
	{
		if ( ("protein_coding" == new_str) && (old_l < new_l)){ flag = 1;}
	}
	else if ( old_l < new_l) {flag = 1;}
	return flag;
}
/**********************************************/
void locate_pred_in_isoform( const unit_pred &pred, const vector<isoform> &vec_isoform, match_info &mi_item )
{
	match_info m_item;

	int num_iso=(int)vec_isoform.size();
	int i, j,
		best_i;
	// if it's UTR or intronic
	int flag_utr = 0; // 0 for UTR, 1 after seeing CDS, 2 after seeing CDS end
	int tmp_l = 0, max_overlap = 0; // pick the isoform with max overlapping when multiple choices
	int tmp_cds = 0, best_cds = 0; // overlap with cds or not
	int flag_hit = 0, // 0 for left UTR, 1 for right UTR, 2 for intronic, 3 for cds
		best_hit = -1;
	string best_source="";
	L("searching \t%u\t%u\n", pred.s, pred.e);
	for( i =0; i< num_iso; i++)
	{
		int n_exon = vec_isoform[i].exon.size();
		L("cmp\t%s\t%s in isoform %d\n", vec_isoform[i].src.c_str(), vec_isoform[i].fea.c_str(), i);

		flag_utr = 0;
		max_overlap = 0;
		best_cds = 0;
		flag_hit = 0;
		for( j = 0; j < n_exon; j++)
		{
			L("== %s\t%s\tisoform %d:%d %d\t%d\n", vec_isoform[i].src.c_str(), vec_isoform[i].fea.c_str(), i, j, vec_isoform[i].exon[j].e_s, vec_isoform[i].exon[j].e_e);
			if ( vec_isoform[i].exon[j].e_e < pred.s)
			{
				if ( ( 0 == flag_utr) && ( 0 < vec_isoform[i].exon[j].cds ) ){ flag_utr = 1;}
				if ( ( 1 == flag_utr) && ( 2 > vec_isoform[i].exon[j].cds ) ){ flag_utr = 2;}
				L("skipping \t%s\t%s in isoform %d:%d %d\t%d\n", vec_isoform[i].src.c_str(), vec_isoform[i].fea.c_str(), i, j, vec_isoform[i].exon[j].e_s, vec_isoform[i].exon[j].e_e);
				continue;
			}
			else if ( vec_isoform[i].exon[j].e_s > pred.e )
			{
				if ( (0 == flag_hit) && ( 1 == flag_utr)){flag_hit=2;}
				else if ( (0 == flag_hit) && ( 2 == flag_utr)){flag_hit=1;}
				L("Passing \t%s\t%s in isoform %d:%d %d\t%d\n", vec_isoform[i].src.c_str(), vec_isoform[i].fea.c_str(), i, j, vec_isoform[i].exon[j].e_s, vec_isoform[i].exon[j].e_e);
				break;
			}
			else if ( 0 <  overlap( pred.s, pred.e, vec_isoform[i].exon[j].e_s, vec_isoform[i].exon[j].e_e ) )
			{
				flag_hit = 3;
				//L("matched:\t%d\t%u\t%u\t%s\t%s\t%u\t%u\t%d\n", pred.id, pred.s, pred.e, vec_isoform[i].ref.c_str(), vec_isoform[i].tname.c_str(),
				//		vec_isoform[i].exon[j].e_s, vec_isoform[i].exon[j].e_e, tmp_l);

				tmp_l = overlap_length( pred.s, pred.e, vec_isoform[i].exon[j].e_s, vec_isoform[i].exon[j].e_e );
				L("matched:%s\t%d\t%u\t%u\t%s\t%s\t%u\t%u\t%d\n", vec_isoform[i].src.c_str(), pred.id, pred.s, pred.e, vec_isoform[i].ref.c_str(), vec_isoform[i].tname.c_str(),
						vec_isoform[i].exon[j].e_s, vec_isoform[i].exon[j].e_e, tmp_l);


				//if ( (max_overlap < tmp_l) ||
				//( (max_overlap == tmp_l) && ( vec_isoform[i].src=="protein_coding"))
				//)
				if ( better_candidate( max_overlap, tmp_l, best_source, vec_isoform[i].src))
				{
					best_source = vec_isoform[i].src;
					m_item.tname = vec_isoform[i].tname;
					m_item.src = vec_isoform[i].src;
					m_item.fea = vec_isoform[i].fea;
					m_item.e_s = vec_isoform[i].exon[j].e_s;
					m_item.e_e = vec_isoform[i].exon[j].e_e;
					m_item.c_s = vec_isoform[i].exon[j].c_s;
					m_item.c_e = vec_isoform[i].exon[j].c_e;
					m_item.e_overlap = tmp_l;
					if ( 2 == vec_isoform[i].exon[j].cds)
					{
						tmp_cds = overlap_length( pred.s, pred.e, vec_isoform[i].exon[j].c_s, vec_isoform[i].exon[j].c_e );
						//m_item.overlap = tmp_l;
						//m_item.cds = 2;
						if ( pred.e - pred.s + 1 == tmp_cds )
						{
							m_item.overlap = pred.e - pred.s + 1;
							m_item.cds = 2;
						}
						else
						{
							m_item.overlap = tmp_cds;
							m_item.cds = 3;
						}
					}
					else if ( 1 == vec_isoform[i].exon[j].cds) // state changing exon
					{
						tmp_cds = overlap_length( pred.s, pred.e, vec_isoform[i].exon[j].c_s, vec_isoform[i].exon[j].c_e );
						if ( 0 == tmp_cds )
						{
							m_item.overlap = 0;
							if ( pred.e < vec_isoform[i].exon[j].c_s)
							{ m_item.cds = 0;}
							else
							{ m_item.cds = 1;}
						}
						else if ( pred.e - pred.s + 1 == tmp_cds )
						{
							m_item.overlap = pred.e - pred.s + 1;
							m_item.cds = 2;
						}
						else
						{
							m_item.overlap = tmp_cds;
							m_item.cds = 3;
						}
					}
					else // Non-coding
					{
						m_item.overlap = 0;
						if ( 0 == flag_utr)
						{ m_item.cds = 0;}
						else
						{ m_item.cds = 1;}
					}

					m_item.overlap = tmp_l;//m_item.cds = flag_utr;
					max_overlap = tmp_l;
					L("update_exon:\t%s\t%d\t%d\t%d\t%u\t%u\t%d\t%f\t", best_source.c_str(), tmp_cds, vec_isoform[i].exon[j].cds, pred.id, pred.s, pred.e, m_item.cds, m_item.overlap*1.0/(pred.e - pred.s + 1) );
					L("%u\t%u\t%u\t%u\n", m_item.e_s, m_item.e_e, m_item.c_s, m_item.c_e);
				}

			}

		}
		if ( best_hit <  flag_hit)
		{
			best_hit = flag_hit; // The best we achieve for this gene
			best_i = i;
		}
	}

	if ( 2 == best_hit)
	{
		m_item.tname = vec_isoform[best_i].tname;
		m_item.src = vec_isoform[best_i].src;
		m_item.fea = vec_isoform[best_i].fea;
		m_item.e_s = 0;
		m_item.e_e = 0;
		m_item.c_s = 0;
		m_item.c_e = 0;
		m_item.overlap = 0;
		m_item.e_overlap = 0;
		m_item.cds = 4;
		L("final:\tIntronic\n" );
	}
	else if ( 2 > best_hit)
	{
		m_item.tname = vec_isoform[best_i].tname;
		m_item.src = vec_isoform[best_i].src;
		m_item.fea = vec_isoform[best_i].fea;
		m_item.e_s = 0;
		m_item.e_e = 0;
		m_item.c_s = 0;
		m_item.c_e = 0;
		m_item.overlap = 0;
		m_item.e_overlap = 0;
		m_item.cds = best_hit;
		L("final:\tUTR\t%d\n", best_hit );
	}
	else
	{
		L("final:\t%d\t%u\t%u\t%d\t%f\t", pred.id, pred.s, pred.e, m_item.cds, m_item.overlap*1.0/(pred.e - pred.s + 1) );
		L("%u\t%u\t%u\t%u\t%s\t%s\n", m_item.e_s, m_item.e_e, m_item.c_s, m_item.c_e, m_item.src.c_str(), m_item.fea.c_str());
	}

	copy_match(m_item, mi_item);
}
/**********************************************/
void locate_single_pred( const unit_pred &pred, const vector<gene_data> &vec_gene, const map<string, vector<isoform> > &iso_gene_map, match_info &mi_item)
{
	map<string, vector<isoform> >::const_iterator it;
	int limit = vec_gene.size();
	int match_gene = 0;
	int max_overlap = -1;
	int g_hit = 0;// default: no overlapped gene detected
	match_info g_mitem;
	for( int i = 0; i < limit; i++ )
	{
		if ( vec_gene[i].end < pred.s)
		{
			continue;
		}
		else if ( vec_gene[i].start > pred.e )
		{
			L("Stop: boundary %u is crossed at %u\n", pred.e, vec_gene[i].start);
			break;
		}
		else
		{
			if ( 0 < overlap( pred.s, pred.e, vec_gene[i].start, vec_gene[i].end ) )
			{
				match_gene++;
				L("GeneMatched_%d:\t%s\t%d\t%u\t%u\t%s\t%s\t%u\t%u\n", match_gene, vec_gene[i].gene_id.c_str(), pred.id, pred.s, pred.e, vec_gene[i].chr.c_str(), vec_gene[i].gene_name.c_str(), vec_gene[i].start, vec_gene[i].end);
				it = iso_gene_map.find( vec_gene[i].gene_id);

				clear_match_info(g_mitem);
				locate_pred_in_isoform( pred, it->second, g_mitem);
				if ( max_overlap < g_mitem.overlap)
				{
					L("Update Gene %s for %d\n", vec_gene[i].gene_id.c_str(), g_mitem.overlap);
					max_overlap = g_mitem.overlap;
					copy_match( g_mitem, mi_item);
					mi_item.gname = vec_gene[i].gene_name.c_str();
					mi_item.gid = vec_gene[i].gene_id.c_str();
					mi_item.cds_gene = vec_gene[i].cds_gene;
					mi_item.strand = vec_gene[i].strand;
				}
				g_hit = 1;
			}
			else
			{
				L("GeneDisjoint\t%d\t%u\t%u\t%s\t%s\t%u\t%u\n", pred.id, pred.s, pred.e, vec_gene[i].chr.c_str(), vec_gene[i].gene_name.c_str(), vec_gene[i].start, vec_gene[i].end);
			}
		}

	}
	// Once no genes detected
	if ( 0 == g_hit )
	{
		mi_item.tname = "";
		mi_item.gname = "";
		mi_item.gid = "";
		mi_item.src = "";
		mi_item.fea = "";
		mi_item.e_s = 0;
		mi_item.e_e = 0;
		mi_item.c_s = 0;
		mi_item.c_e = 0;
		mi_item.overlap = -1;
		mi_item.e_overlap = -1;
		mi_item.cds = -1;
		mi_item.cds_gene = -1;
		mi_item.cds_trans = -1;
	}
}

/**********************************************/
void clear_match_info( match_info &match_item)
{
	match_item.tname = "";
	match_item.gname = "";
	match_item.gid = "";
	match_item.tid = "";
	match_item.src = "";
	match_item.fea = "";
	match_item.e_s = 0;
	match_item.e_e = 0;
	match_item.c_s = 0;
	match_item.c_e = 0;
	match_item.overlap = 0;
	match_item.e_overlap = 0;
	match_item.cds = -1;
	match_item.cds_gene = 0;
	match_item.cds_trans = 0;
	match_item.strand = '\0';
}
/**********************************************/
void LocatePrediction( const map<string, vector<unit_pred> > &sort_record,  const map<string, vector<gene_data> > &gene_sorted_map, const map<string, vector<isoform> > &iso_gene_map, char *out_file)
{
	map<string, vector<unit_pred> >::const_iterator p_it;
	map<string, vector<gene_data> >::const_iterator g_it;

	int i, limit;
	match_info mi_item;
	char *all_out=(char*)malloc(MAX_LINE);
	char *anno=(char*)malloc(MAX_LINE);
	strcpy(all_out, out_file);
	strcat(all_out, ".all");
	strcpy(anno, out_file);
	strcat(anno, ".anno");

	FILE *out_fp = fopen(all_out, "w");
	FILE *anno_fp = fopen(anno, "w");

	int leng_pred = 0;
	int flag_anno = 0;
	string p_type ="";
	for( p_it = sort_record.begin(); p_it != sort_record.end(); p_it++)
	{
		g_it = gene_sorted_map.find(p_it->first);
		if ( g_it != gene_sorted_map.end() )
		{
			E("Compare chromosome %s\n", p_it->first.c_str());
			limit = p_it->second.size();
			for( i = 0; i < limit; i++)
			{
				clear_match_info(mi_item);
				leng_pred = p_it->second[i].e - p_it->second[i].s+1;

				locate_single_pred( p_it->second[i], g_it->second, iso_gene_map, mi_item);
				flag_anno = 0;
				fprintf(out_fp,  ">\t%d\t%d\t%s\t%u\t%u\t%s\t%s", p_it->second[i].id, p_it->second[i].sup, p_it->second[i].ref_id.c_str(), p_it->second[i].s-1, p_it->second[i].e, p_it->second[i].sim.c_str(), p_it->second[i].fea.c_str() );
				if ( 0 <= mi_item.cds )
				{
					// for a protein coding gene
					if ( 1 == mi_item.cds_gene)
					{
						if (2 == mi_item.cds)
						{
							p_type = "CDS";
							flag_anno = 1;
						}
						else if ( 3 == mi_item.cds)
						{
							p_type = "PARTIAL";
							flag_anno = 1;
						}
						else if ( 4 == mi_item.cds)
						{
							p_type = "INTRON";
						}
						else if ( 0 == mi_item.cds)
						{
							p_type = "5UTR";
							if ('-' == mi_item.strand){p_type = "3UTR";}
						}
						else
						{
							p_type = "5UTR";
							if ('+' == mi_item.strand){p_type = "3UTR";}
						}
						fprintf( out_fp, "\tCODING_GENE\t%s", p_type.c_str() );
					}
					else
					{
						if ( mi_item.overlap == leng_pred)
						{
							p_type = "EXON";
						}
						else if ( 0 < mi_item.overlap )
						{
							p_type = "PARTIAL";
						}
						else
						{
							p_type = "INTRON";
						}
						fprintf( out_fp, "\tNONCODING_GENE\t%s", p_type.c_str() );
					}
					fprintf( out_fp, "\t%s\t%s\t%s\t%s\t%s\t%d\t%c\t%d\t%d\t%d\t%d\n", mi_item.gname.c_str(), mi_item.gid.c_str(), mi_item.src.c_str(), mi_item.fea.c_str(), mi_item.tname.c_str(), mi_item.cds, mi_item.strand, mi_item.overlap, mi_item.e_overlap, leng_pred, mi_item.cds_gene );
					if ( 1 == flag_anno )
					{
					fprintf( anno_fp,  ">\t%d\t%d\t%s\t%u\t%u\t%s\t%s", p_it->second[i].id, p_it->second[i].sup, p_it->second[i].ref_id.c_str(), p_it->second[i].s-1, p_it->second[i].e, p_it->second[i].sim.c_str(), p_it->second[i].fea.c_str() );
					fprintf( anno_fp, "\tCODING_GENE\t%s", p_type.c_str() );
					fprintf( anno_fp, "\t%s\t%s\t%s\t%s\t%s\t%d\t%c\t%d\t%d\t%d\t%d\n", mi_item.gname.c_str(), mi_item.gid.c_str(), mi_item.src.c_str(), mi_item.fea.c_str(), mi_item.tname.c_str(), mi_item.cds, mi_item.strand, mi_item.overlap, mi_item.e_overlap, leng_pred, mi_item.cds_gene );
					fprintf( anno_fp, "%s%s%s", p_it->second[i].ref.c_str(), p_it->second[i].align.c_str(), p_it->second[i].contig.c_str() );
					}
				}
				else
				{
					fprintf( out_fp, "\n");
				}

				fprintf( out_fp, "%s%s%s", p_it->second[i].ref.c_str(), p_it->second[i].align.c_str(), p_it->second[i].contig.c_str() );
			}

		}
		else
		{
			E("Missing Chromosome: %s\n", p_it->first.c_str());
		}
	}
	fclose(out_fp);
	fclose(anno_fp);
	free(all_out);
	free(anno);
}
// Assumption: Prediction is sorted by starting position. Repeat Annotation is sorted by Ending Position
/**********************************************/
void LocateRepeat( const map<string, vector<unit_pred> > &sort_record,  const map<string, vector<repeat_t> > &repeat_map, char *out_file)
{
	map<string, vector<unit_pred> >::const_iterator p_it; // prediction: starting by starting position
	map<string, vector<repeat_t> >::const_iterator r_it; // repeat model: starting by ending position

	int i =0 , j = 0, p_size =0 , r_size = 0;
	int left_b=0;
	FILE *out_fp = fopen(out_file, "w");

	int leng_pred = 0, leng_match=0;
	int flag_uniq = 1;
	int best_match = 0, best_idx = 0; // max overlength and the repeat index
	string p_type ="";

	for( p_it = sort_record.begin(); p_it != sort_record.end(); p_it++)
	{
		r_it = repeat_map.find(p_it->first);
		if ( r_it != repeat_map.end() )
		{
			E("Compare repeats in chromosome %s\n", p_it->first.c_str());
			p_size = (int)p_it->second.size();
			r_size = (int)r_it->second.size();
			left_b = 0;
			for( i = 0; i < p_size; i++)
			{
				flag_uniq = 1; // default: not in repeat region
				leng_pred = p_it->second[i].e - p_it->second[i].s+1;
				best_idx = -1; // assume the max
				best_match = -1;

				// from left_b till the starting position cross end position of i
				j = left_b;
				L("CMP_START\t%d\t(%u, %u)\t(%u, %u)\n", left_b, p_it->second[i].s, p_it->second[i].e, r_it->second[j].s, r_it->second[j].e);

				while(  p_it->second[i].e > r_it->second[j].s )
				{
					if ( r_it-> second[j].e < p_it->second[i].s )
					{
						left_b = j;
						L("SET_LB\t(%u, %u)\t(%u, %u)\n", p_it->second[i].s, p_it->second[i].e, r_it->second[j].s, r_it->second[j].e);
					}
					else
					{
						leng_match = overlap_length(p_it->second[i].s, p_it->second[i].e, r_it->second[j].s, r_it->second[j].e);
						if ( 0 < leng_match )
						{
							flag_uniq = 0;
							L("MATCH\t(%u, %u)\t(%u, %u)\n", p_it->second[i].s, p_it->second[i].e, r_it->second[j].s, r_it->second[j].e);
							if ( best_match < leng_match)
							{
								best_match = leng_match;
								best_idx = j;
							}
						}
					}
					j++;
				}

				if ( flag_uniq)
				{
					L("UNIQUE\t%d\t(%u, %u)\n", j, p_it->second[i].s, p_it->second[i].e);
					fprintf(out_fp, "%s\tUNIQUE\t0\n", p_it->second[i].header.c_str() );
				}
				else
				{
					fprintf(out_fp, "%s\t%s\t%f\n", p_it->second[i].header.c_str(), r_it->second[best_idx].r_info.c_str(), best_match*1.0/leng_pred );
				}

				L("ITERATION\t%d\t%d\t%d\n", j-left_b+1, left_b, j);

				fprintf(out_fp, "%s%s%s", p_it->second[i].ref.c_str(), p_it->second[i].align.c_str(), p_it->second[i].contig.c_str());

			}

		}
		else
		{
			E("Missing Chromosome: %s\n", p_it->first.c_str());
		}
	}
	fclose(out_fp);
}

/**********************************************/
int skip_interval( const unit_vcf &pred, uint32_t exon_end )
{
	int flag = 0;// 1 indicates the interval can be skipped
	if ( exon_end < pred.pos ){ flag = 1;}
	return flag;
}
/**********************************************/
int final_interval( const unit_vcf &pred, uint32_t exon_start)
{
	int flag = 0; // 1 indicates the interval already pass the SV region
	if  ("INV" == pred.sv)
	{
		if ( pred.e < exon_start) { flag = 1;}
	}
	else if ( pred.pos < exon_start ){ flag = 1;}
	return flag ;
}
/**********************************************/
// Pick the transcript with maximum overlapping length with pred from vec_isoform
// On the other hand, DUP only need to test one single point
// Let's focus for INVERSION first
// mi_item is about the best one among all possible m_item
void locate_vcf_in_isoform( const unit_vcf &pred, const vector<isoform> &vec_isoform, match_info &mi_item )
{
	match_info m_item;

	int num_iso=(int)vec_isoform.size();
	int i, j, best_i;
	// if it's UTR or intronic
	int flag_utr = 0; // 0 for UTR, 1 after seeing CDS start, 2 after seeing CDS end
	int tmp_l = 0, max_overlap = 0; // pick the isoform with max overlapping when multiple choices
	int tmp_cds = 0, best_cds = 0; // overlap with cds or not
	int flag_hit = 0, // 0 for left UTR, 1 for right UTR, 2 for intronic, 3 for cds/exon/partial
		best_hit = -1;
	string best_source="";
	L("searching \t%u\t%u\n", pred.s, pred.e);

	for( i = 0; i< num_iso; i++)
	{
		int n_exon = vec_isoform[i].exon.size();
		L("cmp\t%s\t%s of isoform %d\n", vec_isoform[i].src.c_str(), vec_isoform[i].fea.c_str(), i);

		flag_utr = 0;
		max_overlap = 0;
		best_cds = 0;
		flag_hit = 0;

		for( j = 0; j < n_exon; j++)
		{
			L("== %s\t%s\tisoform %d:%d %d\t%d\n", vec_isoform[i].src.c_str(), vec_isoform[i].fea.c_str(), i, j, vec_isoform[i].exon[j].e_s, vec_isoform[i].exon[j].e_e);
			if ( skip_interval( pred, vec_isoform[i].exon[j].e_e ) )	//if ( vec_isoform[i].exon[j].e_e < pred.s)
			{
				if ( ( 0 == flag_utr) && ( 0 < vec_isoform[i].exon[j].cds ) ){ flag_utr = 1;}
				if ( ( 1 == flag_utr) && ( 2 > vec_isoform[i].exon[j].cds ) ){ flag_utr = 2;}
				L("skipping \t%s\t%s in isoform %d:%d %d\t%d\n", vec_isoform[i].src.c_str(), vec_isoform[i].fea.c_str(), i, j, vec_isoform[i].exon[j].e_s, vec_isoform[i].exon[j].e_e);
				continue;
			}
			else if ( final_interval( pred, vec_isoform[i].exon[j].e_s ) ) //vec_isoform[i].exon[j].e_s > pred.e )
			{
				if ( (0 == flag_hit) && ( 1 == flag_utr)){flag_hit=2;}
				else if ( (0 == flag_hit) && ( 2 == flag_utr)){flag_hit=1;}
				L("Passing \t%s\t%s in isoform %d:%d %d\t%d\n", vec_isoform[i].src.c_str(), vec_isoform[i].fea.c_str(), i, j, vec_isoform[i].exon[j].e_s, vec_isoform[i].exon[j].e_e);
				break;
			}
			else if ( 0 <  vcf_overlap( pred, vec_isoform[i].exon[j].e_s, vec_isoform[i].exon[j].e_e ) )
			{
				flag_hit = 3;
				//L("matched:\t%d\t%u\t%u\t%s\t%s\t%u\t%u\t%d\n", pred.id, pred.s, pred.e, vec_isoform[i].ref.c_str(), vec_isoform[i].tname.c_str(),
				//		vec_isoform[i].exon[j].e_s, vec_isoform[i].exon[j].e_e, tmp_l);

				tmp_l = vcf_overlap_length( pred, vec_isoform[i].exon[j].e_s, vec_isoform[i].exon[j].e_e );
				L("matched:%s\t%s\t%u\t%u\t%u\t%s\t%s\t%u\t%u\t%d\n", vec_isoform[i].src.c_str(), pred.sv.c_str(), pred.pos, pred.s, pred.e, vec_isoform[i].ref.c_str(), vec_isoform[i].tname.c_str(),
						vec_isoform[i].exon[j].e_s, vec_isoform[i].exon[j].e_e, tmp_l);


				//if ( (max_overlap < tmp_l) ||
				//( (max_overlap == tmp_l) && ( vec_isoform[i].src=="protein_coding"))
				//)
				if ( better_candidate( max_overlap, tmp_l, best_source, vec_isoform[i].src))
				{
					best_source = vec_isoform[i].src;
					m_item.tname = vec_isoform[i].tname;
					m_item.src = vec_isoform[i].src;
					m_item.fea = vec_isoform[i].fea;
					m_item.e_s = vec_isoform[i].exon[j].e_s;
					m_item.e_e = vec_isoform[i].exon[j].e_e;
					m_item.c_s = vec_isoform[i].exon[j].c_s;
					m_item.c_e = vec_isoform[i].exon[j].c_e;
					m_item.e_overlap = tmp_l;
					if ( 2 == vec_isoform[i].exon[j].cds)
					{
						tmp_cds = vcf_overlap_length( pred, vec_isoform[i].exon[j].c_s, vec_isoform[i].exon[j].c_e );
						//m_item.overlap = tmp_l;
						//m_item.cds = 2;
						if ("INV" == pred.sv )
						{
							if ( pred.e - pred.s == tmp_cds ) //pred is vcf now
							{
								m_item.overlap = pred.e - pred.s;
								m_item.cds = 2;
							}
							else
							{
								m_item.overlap = tmp_cds;
								m_item.cds = 3;
							}
						}
						else
						{
							m_item.overlap = 1;
							m_item.cds = 2; // Always fully containment
						}
					}
					else if ( 1 == vec_isoform[i].exon[j].cds) // state changing exon
					{
						tmp_cds = vcf_overlap_length( pred, vec_isoform[i].exon[j].c_s, vec_isoform[i].exon[j].c_e );
						if ( 0 == tmp_cds )
						{
							m_item.overlap = 0;
							if ( final_interval( pred, vec_isoform[i].exon[j].c_s)) //( pred.e < vec_isoform[i].exon[j].c_s)
							{ m_item.cds = 0;}
							else
							{ m_item.cds = 1;}
						}
						else if ("INV" == pred.sv )
						{
							if ( pred.e - pred.s == tmp_cds ) //pred is vcf now
							{
								m_item.overlap = pred.e - pred.s;
								m_item.cds = 2;
							}
							else
							{
								m_item.overlap = tmp_cds;
								m_item.cds = 3;
							}
						}
						else
						{
							m_item.overlap = 1;
							m_item.cds = 2; // Always fully containment
						}
					}
					else // Non-coding
					{
						m_item.overlap = 0;
						if ( 0 == flag_utr)
						{ m_item.cds = 0;}
						else
						{ m_item.cds = 1;}
					}

					m_item.overlap = tmp_l;//m_item.cds = flag_utr;
					max_overlap = tmp_l;
					L("update_exon:\t%s\t%d\t%d\t%u\t%u\t%u\t%d\t", best_source.c_str(), tmp_cds, vec_isoform[i].exon[j].cds, pred.pos, pred.s, pred.e, m_item.cds );
					if ("INV"== pred.sv)
					{	L("\t%f\t", m_item.overlap*1.0/(pred.e - pred.s ) );}
					else
					{ 	L("\t%f\t", m_item.overlap*1.0/1);}
					L("%u\t%u\t%u\t%u\n", m_item.e_s, m_item.e_e, m_item.c_s, m_item.c_e);
				}

			}

		}
		if ( best_hit <  flag_hit)
		{
			best_hit = flag_hit; // The best we achieve for this gene
			best_i = i;
		}
	}

	if ( 2 == best_hit)
	{
		m_item.tname = vec_isoform[best_i].tname;
		m_item.src = vec_isoform[best_i].src;
		m_item.fea = vec_isoform[best_i].fea;
		m_item.e_s = 0;
		m_item.e_e = 0;
		m_item.c_s = 0;
		m_item.c_e = 0;
		m_item.overlap = 0;
		m_item.e_overlap = 0;
		m_item.cds = 4;
		L("final:\tIntronic\n" );
	}
	else if ( 2 > best_hit)
	{
		m_item.tname = vec_isoform[best_i].tname;
		m_item.src = vec_isoform[best_i].src;
		m_item.fea = vec_isoform[best_i].fea;
		m_item.e_s = 0;
		m_item.e_e = 0;
		m_item.c_s = 0;
		m_item.c_e = 0;
		m_item.overlap = 0;
		m_item.e_overlap = 0;
		m_item.cds = best_hit;
		L("final:\tUTR\t%d\n", best_hit );
	}
	else
	{
		L("final:\tGOOD\t%u\t%u\t%u\t%d\t", pred.pos, pred.s, pred.e, m_item.cds );
		if ("INV"== pred.sv)
		{	L("\t%f\t", m_item.overlap*1.0/(pred.e - pred.s) );}
		else
		{ 	L("\t%f\t", m_item.overlap*1.0/1);}
		L("%u\t%u\t%u\t%u\t%s\t%s\n", m_item.e_s, m_item.e_e, m_item.c_s, m_item.c_e, m_item.src.c_str(), m_item.fea.c_str());
	}

	copy_match(m_item, mi_item);
}
/**********************************************/
// Select the transcript in a gene with best matching criteria with a variation
// On the other hand, DUP only need to test one single point
// Let's focus for INVERSION first
// mi_item is about the best one among all possible m_item
/**********************************************/
void locate_vcf_in_isoform2( const unit_vcf &pred, const vector<isoform> &vec_isoform, match_info &best_match )
{
	match_info t_match;

	int num_iso=(int)vec_isoform.size();
	int i, j, best_i;
	// if it's UTR or intronic
	int flag_utr = 0; // 0 for UTR, 1 after seeing CDS start, 2 after seeing CDS end
	int tmp_l = 0, max_overlap = 0, max_cds=0; // pick the isoform with max overlapping when multiple choices
	int tmp_cds = 0, best_cds = 0; // overlap with cds or not
	int flag_hit = 0, // 0 for left UTR, 1 for right UTR, 2 for intronic, 3 for cds/exon/partial
		best_hit = -1;	// 0 for crossing multiple cds. 1 for full in cds. 2 for partial overlap
	int num_bp = 0, num_cds = 0;
	string best_source="";
	L("Check CDS for %s\t%d\t\t%u\t%u\t%u\n", pred.patient.c_str(), pred.id, pred.pos, pred.s, pred.e);

	for( i = 0; i< num_iso; i++ )
	{
		int n_exon = vec_isoform[i].exon.size();
		if ( 0 == vec_isoform[i].cds_iso)
		{	continue;	}
		L(">>>cmp\t%s\t%s(%d) of isoform %d\n", vec_isoform[i].src.c_str(), vec_isoform[i].fea.c_str(), vec_isoform[i].cds_iso, i);

		flag_utr = 0;
		best_cds = 0;
		num_bp = 0;num_cds = 0;

		for( j = 0; j < n_exon; j++)
		{
			if ( skip_interval( pred, vec_isoform[i].exon[j].e_e ) )
			{
				//if ( ( 0 == flag_utr) && ( 0 < vec_isoform[i].exon[j].cds ) ){ flag_utr = 1;}
				//if ( ( 1 == flag_utr) && ( 2 > vec_isoform[i].exon[j].cds ) ){ flag_utr = 2;}
				//L("skipping \t%s\t%s in isoform %d:%d %d\t%d\n", vec_isoform[i].src.c_str(), vec_isoform[i].fea.c_str(), i, j, vec_isoform[i].exon[j].e_s, vec_isoform[i].exon[j].e_e);
				continue;
			}
			else if ( final_interval( pred, vec_isoform[i].exon[j].e_s ) )
			{
				//if ( (0 == flag_hit) && ( 1 == flag_utr)){flag_hit=2;}
				//else if ( (0 == flag_hit) && ( 2 == flag_utr)){flag_hit=1;}
				L("done: \t%s\t%s in isoform %d:%d %d\t%d\n", vec_isoform[i].src.c_str(), vec_isoform[i].fea.c_str(), i, j, vec_isoform[i].exon[j].e_s, vec_isoform[i].exon[j].e_e);
				break;
			}
			else if ( 0 <  vcf_overlap( pred, vec_isoform[i].exon[j].e_s, vec_isoform[i].exon[j].e_e ) )
			{
				tmp_l = vcf_overlap_length( pred, vec_isoform[i].exon[j].c_s, vec_isoform[i].exon[j].c_e );
				if ( tmp_l )
				{
					//L("CDSHIT\t%s\t%s\t%d in isoform %d:%d %d\t%d\t%d\t%d\n", vec_isoform[i].src.c_str(), vec_isoform[i].fea.c_str(), tmp_l, i, j, vec_isoform[i].exon[j].c_s, vec_isoform[i].exon[j].c_e, pred.id, pred.pos);
					L("CDSHIT\t%d\t%d\t%u\t%u\t%u\t with %u %u %d in isoform %s %s %s\n", pred.id, tmp_l, pred.pos, pred.s, pred.e,  vec_isoform[i].exon[j].c_s, vec_isoform[i].exon[j].c_e, vec_isoform[i].exon[j].cds, vec_isoform[i].src.c_str(), vec_isoform[i].fea.c_str(), vec_isoform[i].id.c_str() );
					num_bp += tmp_l;
					num_cds ++;
				}
			}
		}
		if ( max_overlap < num_bp )
		{
			max_overlap = num_bp; max_cds = num_cds;
			best_hit = 1;
			best_i = i;
			if ( 1 < max_cds){	best_hit = 0; }
			else if ( ("INV" == pred.sv) && ( max_overlap < (int)pred.e-pred.s+1)) { best_hit = 2;}
		}
	}

	if ( -1 < best_hit)
	{
		t_match.tname = vec_isoform[best_i].tname;
		t_match.tid = vec_isoform[best_i].id;
		t_match.src = vec_isoform[best_i].src;
		t_match.fea = vec_isoform[best_i].fea;
		t_match.e_s = 0;
		t_match.e_e = 0;
		t_match.c_s = 0;
		t_match.c_e = 0;
		t_match.overlap = max_overlap;
		t_match.e_overlap = max_cds;
		t_match.cds = best_hit;
		copy_match( t_match, best_match );
		L("CodingGene\t%s\t%d\t%s\t%u\t%u\t%u\t%d\t%d\t%d\n", pred.sv.c_str(), pred.id, vec_isoform[best_i].ref.c_str(), pred.pos, pred.s, pred.e, max_overlap, max_cds, best_hit);
	}


}
// To Determine if a VCF Record is in geneic region or not
/**********************************************/
int locate_single_vcf( const unit_vcf &pred, const vector<gene_data> &vec_gene, const map<string, vector<isoform> > &iso_gene_map, match_info &vcf_item)
{
	map< string, vector<isoform> >::const_iterator it;
	int limit = vec_gene.size();
	int num_gene= 0; // for what?
	int num_cds_gene = 0;
	int max_overlap  = 0;//-1;
	int g_hit = 0;// default: no cds-overlapped gene detected

	match_info gene_match;	// selection among multiple overlapping entries

	for( int i = 0; i < limit; i++ )
	{
		if ( skip_interval( pred, vec_gene[i].end ) )
		{	continue;	}
		else if ( final_interval( pred, vec_gene[i].start) )
		{
			L("<<<<: vcf (%lu, %lu, %lu) stops at %u\n", pred.s, pred.e, pred.pos, vec_gene[i].start);
			break;
		}
		else
		{
			if ( 0 < vcf_overlap( pred, vec_gene[i].start, vec_gene[i].end ) )
			{
				L("%s\t%d\tMatchGene %d:\t%s\t%u\t%u\t%u\t%s\t%s\t%u\t%u\n", pred.patient.c_str(), pred.id, num_gene++, vec_gene[i].gene_id.c_str(), pred.pos, pred.s, pred.e, vec_gene[i].chr.c_str(), vec_gene[i].gene_name.c_str(), vec_gene[i].start, vec_gene[i].end);
				//L("CheckGene_%d:\t%s\t%u\t%u\t%u\t%s\t%s\t%u\t%u\n", num_gene++, vec_gene[i].gene_id.c_str(), pred.pos, pred.s, pred.e, vec_gene[i].chr.c_str(), vec_gene[i].gene_name.c_str(), vec_gene[i].start, vec_gene[i].end);
				it = iso_gene_map.find( vec_gene[i].gene_id );

				clear_match_info(gene_match);
				locate_vcf_in_isoform2( pred, it->second, gene_match );
				if ( max_overlap < gene_match.overlap)
				{
					L("Update Gene %s: %d over %d from %s\n", vec_gene[i].gene_id.c_str(), gene_match.overlap, max_overlap, gene_match.tid.c_str());
					max_overlap = gene_match.overlap;
					copy_match( gene_match, vcf_item );
					vcf_item.gname    = vec_gene[i].gene_name.c_str();
					vcf_item.gid      = vec_gene[i].gene_id.c_str();
					vcf_item.cds_gene = vec_gene[i].cds_gene;
					vcf_item.strand   = vec_gene[i].strand;
				}
				if ( gene_match.overlap ){ g_hit++; }
			}
			else
			{
				L("SkipGene\t%u\t%u\t%u\t%s\t%s\t%u\t%u\n", pred.pos, pred.s, pred.e, vec_gene[i].chr.c_str(), vec_gene[i].gene_name.c_str(), vec_gene[i].start, vec_gene[i].end);
			}
		}

	}
	// Once no genes detected
	//L("REPORT\t%s\t%d\t%d\n", pred.patient.c_str(), pred.id, num_gene );
	if ( 0 == g_hit )
	{
		clear_match_info(vcf_item);
		vcf_item.overlap = -1;
		vcf_item.e_overlap = -1;
		//mi_item.cds = -1;
		vcf_item.cds_gene = -1;
		vcf_item.cds_trans = -1;
	}
	//else
	//{	L("REPORT\t%d\t%d\t%d\n", pred.id, max_overlap, vcf_item.overlap);	}

	return g_hit;
}

// Locate VCF Records of INV and DUP based on The Gene Model
// sort_record: ( chromosome, vector of microSVs )
// gene_sorted_map: ( chromosome, gene boundary)
// iso_gene_map: (gene ID, vector of isoforms)
/**********************************************/
// Step 1: classify if a call is genic or not
// Step 2: determine if it overlaps with at least one coding region
//// Step 3: extract transcript sequence
//// Step 4: extract 6 reading frames from the above transcripts
void LocateVCF_GeneModel( const map<string, vector<unit_vcf> > &sort_record,  const map<string, vector<gene_data> > &gene_sorted_map, const map<string, vector<isoform> > &iso_gene_map, char *out_file)
{
	map<string, vector<unit_vcf> >::const_iterator v_it; // prediction
	map<string, vector<gene_data> >::const_iterator g_it; // gene interval

	match_info mi_item;

	char *all_out	=	(char*)malloc(MAX_LINE);
	char *anno		=	(char*)malloc(MAX_LINE);
	char *extend	=	(char*)malloc(MAX_LINE);
	strcpy(all_out, out_file);
	strcat(all_out, ".all");
	strcpy(anno, out_file);
	strcat(anno, ".anno");
	strcpy(extend, out_file);
	strcat(extend, ".extend");
	FILE *out_fp = fopen(all_out, "w");
	FILE *anno_fp = fopen(anno, "w");
	FILE *extend_fp = fopen(extend, "w");

	int i, limit;
	int leng_pred = 0;
	int flag_anno = 0;
	string p_type ="";
	int num_gene = 0;
	for( v_it = sort_record.begin(); v_it != sort_record.end(); v_it++)
	{
		g_it = gene_sorted_map.find(v_it->first);	// For each chromosome
		if ( g_it != gene_sorted_map.end() )
		{
			E( "Locating Intervals In Chromosome %s\n", v_it->first.c_str() );
			num_gene = 0;
			limit = v_it->second.size();
			for( i = 0; i < limit; i++)
			{
				clear_match_info( mi_item );

				//if ( "INV"== v_it->second[i].sv)
				//{ leng_pred = v_it->second[i].e - v_it->second[i].s; }// Since VCF is end position exclusive
				leng_pred = v_it->second[i].e - v_it->second[i].s + 1;// Since VCF is both inclusive (at least for inv and dup)

				num_gene = locate_single_vcf( v_it->second[i], g_it->second, iso_gene_map, mi_item);

				fprintf( out_fp, "%s\t%u\t.\t%s\t%s\t", v_it->second[i].chr.c_str(), v_it->second[i].pos, v_it->second[i].ref.c_str(), v_it->second[i].alt.c_str() );
				fprintf( out_fp, "%f\t%s\t", v_it->second[i].qual, v_it->second[i].filter.c_str() );
				fprintf( out_fp, "%s", v_it->second[i].info.c_str());

				if ( 0 < num_gene )
				{
					fprintf( out_fp, ";GeneID=%s;TranscriptID=%s;NumGene=%d;CDS=%d;Overlap=%d", mi_item.gid.c_str(), mi_item.tid.c_str(), num_gene, mi_item.cds, mi_item.overlap);
					fprintf( anno_fp, "%s\t%u\t.\t%s\t%s\t", v_it->second[i].chr.c_str(), v_it->second[i].pos, v_it->second[i].ref.c_str(), v_it->second[i].alt.c_str() );
					fprintf( anno_fp, "%f\t%s\t", v_it->second[i].qual, v_it->second[i].filter.c_str() );
					fprintf( anno_fp, "%s", v_it->second[i].info.c_str());
					fprintf( anno_fp, ";GeneID=%s;TranscriptID=%s;NumGene=%d;CDS=%d;Overlap=%d\n", mi_item.gid.c_str(), mi_item.tid.c_str(), num_gene, mi_item.cds, mi_item.overlap);
				}
				fprintf( out_fp, "\n");
			}
		}
		else
		{	E("Missing Chromosome: %s\n", v_it->first.c_str());	}
	}
	fclose(out_fp);// all results
	fclose(anno_fp);// calls in coding regions
	fclose(extend_fp);// calls in genetic regions + UTR
	free(all_out);
	free(anno);
	free(extend);
}

// Check if a SV is in specific repeat region
// For duplication, I currently mark based on the SOURCE  ( instead of the BP section)
// Assumption: Prediction is sorted by starting position. Repeat Annotation is sorted by Ending Position
/**********************************************/
void LocateVCF_Repeat( const map<string, vector<unit_vcf> > &sort_record,  const map<string, vector<repeat_t> > &repeat_map, char *out_file)
{
	map<string, vector<unit_vcf> >::const_iterator v_it; // prediction: starting by starting position
	map<string, vector<repeat_t> >::const_iterator r_it; // repeat model: starting by ending position

	int i =0 , j = 0, v_size =0 , r_size = 0;
	int left_b = 0; // The leftmost entry we need to check
	FILE *out_fp = fopen(out_file, "w");

	int leng_pred = 0, leng_match=0;
	int flag_uniq = 1;
	int best_match = 0, best_idx = 0; // max overlength and the repeat index
	string p_type ="";

	for( v_it = sort_record.begin(); v_it != sort_record.end(); v_it++)
	{
		r_it = repeat_map.find(v_it->first);
		if ( r_it != repeat_map.end() )
		{
			E("Compare repeats in chromosome %s\n", v_it->first.c_str());
			v_size = (int)v_it->second.size();
			r_size = (int)r_it->second.size();
			left_b = 0;
			for( i = 0; i < v_size; i++)
			{
				flag_uniq = 1; // default: not in repeat region
				leng_pred = v_it->second[i].e - v_it->second[i].s; // Coordinate is BED-style system
				best_idx = -1; // assume the max
				best_match = -1;

				// from left_b till the starting position cross end position of i
				j = left_b;
				L("CMP_START\t%d\t(%u, %u)\t(%u, %u)\n", left_b, v_it->second[i].s, v_it->second[i].e, r_it->second[j].s, r_it->second[j].e);

				while(  v_it->second[i].e > r_it->second[j].s )
				{
					if ( r_it-> second[j].e < v_it->second[i].s )
					{
						left_b = j;
						L("INC_LB\t(%u, %u) to \t(%u, %u)\n", v_it->second[i].s, v_it->second[i].e, r_it->second[j].s, r_it->second[j].e);
					}
					else
					{
						leng_match = overlap_length(v_it->second[i].s+1, v_it->second[i].e, r_it->second[j].s, r_it->second[j].e);
						if ( 0 < leng_match )
						{
							flag_uniq = 0;
							L("MATCH\t(%u, %u)\t(%u, %u)\n", v_it->second[i].s, v_it->second[i].e, r_it->second[j].s, r_it->second[j].e);
							if ( best_match < leng_match)
							{
								best_match = leng_match;
								best_idx = j;
							}
						}
					}
					j++;
				}

				fprintf( out_fp, "%s\t%u\t.\t%s\t%s\t", v_it->second[i].chr.c_str(), v_it->second[i].pos, v_it->second[i].ref.c_str(), v_it->second[i].alt.c_str() );
				fprintf( out_fp, "%f\t%s\t", v_it->second[i].qual, v_it->second[i].filter.c_str() );
				fprintf( out_fp, "%s", v_it->second[i].info.c_str());

				if ( flag_uniq)
				{
					L("UNIQUE\t%d\t(%u, %u)\n", j, v_it->second[i].s, v_it->second[i].e);
					fprintf(out_fp, "RClass=UNIQUE;RType=NO;RInfo=0,0,%d;\n", leng_pred );
				}
				else
				{
					L("REPEAT\t%d\t%s\t(%u, %u)\t%f\n", j, r_it->second[best_idx].r_info.c_str(), v_it->second[i].s, v_it->second[i].e, best_match*1.0/leng_pred);
					fprintf(out_fp, "RClass=REPEAT;RType=%s;RInfo=%f,%d,%d\n", r_it->second[best_idx].r_info.c_str(), best_match*1.0/leng_pred, best_match, leng_pred );
				}

				L("ITERATION\t%d\t%d\t%d\n", j-left_b+1, left_b, j);

				//fprintf(out_fp, "%s%s%s", p_it->second[i].ref.c_str(), p_it->second[i].align.c_str(), p_it->second[i].contig.c_str());

			}

		}
		else
		{
			E("Missing Chromosome: %s\n", v_it->first.c_str());
		}
	}
	fclose(out_fp);
}

// Check how a SV overlaps a repeat region: how many remains if we only check source, only destination, or both
// Assumption: Prediction is sorted by starting position. Repeat Annotation is sorted by Ending Position
/**********************************************/
int check_repeat( const unit_vcf &vcf_t, uint32_t r_start)
{	// Either one condition holds means we need to check
	int flag = 0;
	if ( ( vcf_t.e > r_start) || ( vcf_t.pos > r_start) ){ flag =1;}
	return flag;
}
/**********************************************/
int increment_repeatidx( const unit_vcf &vcf_t, uint32_t r_end)
{
	int flag = 0;
	if ( ( vcf_t.s > r_end ) && ( vcf_t.pos > r_end ) ){ flag =1;}
	return flag;
}
/**********************************************/
int overlap_repeat_length( const unit_vcf &vcf_t, uint32_t r_start, uint32_t r_end, vector<int> &t_vec)
{
	int match_l = 0;
	int src_l = 0, dest_flag = 0, adj_l = 0;;
	t_vec.clear();
	if  ("INV" == vcf_t.sv)
	{
		match_l = overlap_length( vcf_t.s+1, vcf_t.e, r_start, r_end );
		t_vec.push_back(match_l);
		t_vec.push_back(match_l);
		t_vec.push_back(match_l);
	}
	else
	{
		// Source
		src_l = overlap_length( vcf_t.s+1, vcf_t.e, r_start, r_end );
		// DEST
		if ( (r_start <= vcf_t.pos) && ( vcf_t.pos <= r_end)) {dest_flag = 1;}
		if ( dest_flag ){ adj_l = src_l;}
		t_vec.push_back(src_l); // source part
		t_vec.push_back(dest_flag);// dest part
		t_vec.push_back(adj_l); // consider both
		// If we need source
		match_l = src_l;
		// If we need dest
	}
	return match_l;
}
/**********************************************/
int initial_repeat_vec( vector<int> &new_vec)
{
	new_vec.clear();
	for( int i=0;  i <3; i++){new_vec.push_back(0); }
}
/**********************************************/
int adj_repeat_vec( vector<int> &new_vec, vector<int> &t_vec)
{
	for( int i=0;  i <3; i++)
	{
		if ( new_vec[i] < t_vec[i]) { new_vec[i] = t_vec[i];}
	}
}
/**********************************************/
void CountVCF_Repeat( const map<string, vector<unit_vcf> > &sort_record,  const map<string, vector<repeat_t> > &repeat_map, char *out_file)
{
	map<string, vector<unit_vcf> >::const_iterator v_it; // prediction: starting by starting position
	map<string, vector<repeat_t> >::const_iterator r_it; // repeat model: starting by ending position
	char *all_out=(char*)malloc(MAX_LINE);
	strcpy(all_out, out_file);
	strcat(all_out, ".all");


	int i =0 , j = 0, v_size =0 , r_size = 0;
	int left_b = 0; // The leftmost entry we need to check
	FILE *out_fp = fopen(out_file, "w");
	FILE *all_fp = fopen(all_out, "w");

	int leng_pred = 0, leng_match=0;
	int flag_uniq = 1;
	int best_match = 0, best_idx = 0; // max overlength and the repeat index
	string p_type ="";
	vector<int> final_vec, tmp_vec; // length of source, length of dest, length of both

	for( v_it = sort_record.begin(); v_it != sort_record.end(); v_it++)
	{
		r_it = repeat_map.find(v_it->first);
		if ( r_it != repeat_map.end() )
		{
			E("Compare repeats in chromosome %s\n", v_it->first.c_str());
			v_size = (int)v_it->second.size();
			r_size = (int)r_it->second.size();
			left_b = 0;
			for( i = 0; i < v_size; i++)
			{
				flag_uniq = 1; // default: not in repeat region
				leng_pred = v_it->second[i].e - v_it->second[i].s; // Coordinate is BED-style system
				best_idx = -1; // assume the max
				best_match = -1;
				best_match = 0;
				initial_repeat_vec( final_vec ); // empty record

				// from left_b till the starting position cross end position of i
				j = left_b;
				L("CMP_START\t%d\t(%u, %u)\t(%u, %u)\n", left_b, v_it->second[i].s, v_it->second[i].e, r_it->second[j].s, r_it->second[j].e);

				while( check_repeat( v_it->second[i], r_it->second[j].s))// v_it->second[i].e > r_it->second[j].s )
				{
					if ( increment_repeatidx( v_it->second[i], r_it->second[j].e))// ( r_it-> second[j].e < v_it->second[i].s )
					{
						left_b = j;
						L("INC_LB\t(%u, %u) to \t(%u, %u)\n", v_it->second[i].s, v_it->second[i].e, r_it->second[j].s, r_it->second[j].e);
					}
					else
					{
						leng_match = overlap_repeat_length(v_it->second[i], r_it->second[j].s, r_it->second[j].e, tmp_vec);
						if ( 0 < leng_match )
						{
							flag_uniq = 0;
							L("MATCH\t(%u, %u)\t(%u, %u)\n", v_it->second[i].s, v_it->second[i].e, r_it->second[j].s, r_it->second[j].e);
							if ( best_match < leng_match)
							{
								best_match = leng_match;
								best_idx = j;
							}
						}
						adj_repeat_vec( final_vec, tmp_vec ); // empty record
					}
					j++;
				}

				fprintf( out_fp, "%s\t%u\t.\t%s\t%s\t", v_it->second[i].chr.c_str(), v_it->second[i].pos, v_it->second[i].ref.c_str(), v_it->second[i].alt.c_str() );
				fprintf( out_fp, "%f\t%s\t", v_it->second[i].qual, v_it->second[i].filter.c_str() );
				fprintf( out_fp, "%s", v_it->second[i].info.c_str());

				fprintf( all_fp, "%s\t%d\t", v_it->second[i].patient.c_str(), v_it->second[i].id);
				fprintf( all_fp, "%s\t%s\t%u\t%u\t%u\t%s\t%s\t", v_it->second[i].sv.c_str(), v_it->second[i].chr.c_str(), v_it->second[i].pos, v_it->second[i].s, v_it->second[i].e, v_it->second[i].ref.c_str(), v_it->second[i].alt.c_str() );
				fprintf( all_fp, "%f\t%s\t", v_it->second[i].qual, v_it->second[i].filter.c_str() );
				fprintf( all_fp, "%d\t%d\t%f\t", v_it->second[i].min, v_it->second[i].max, v_it->second[i].sim );

				if ( flag_uniq )
				{
					L("UNIQUE\t%d\t(%u, %u)\n", j, v_it->second[i].s, v_it->second[i].e);
					fprintf(out_fp, "RClass=UNIQUE;RType=NO;RInfo=0,0,%d;\n", leng_pred );
					fprintf( all_fp, "UNIQUE\t%d\t%d\t%d\t%d\t%d\n", leng_pred, best_match, final_vec[0], final_vec[1], final_vec[2] );
				}
				else
				{
					L("REPEAT\t%d\t%s\t(%u, %u)\t%f\n", j, r_it->second[best_idx].r_info.c_str(), v_it->second[i].s, v_it->second[i].e, best_match*1.0/leng_pred);
					fprintf(out_fp, "RClass=REPEAT;RType=%s;RInfo=%f,%d,%d\n", r_it->second[best_idx].r_info.c_str(), best_match*1.0/leng_pred, best_match, leng_pred );
					fprintf( all_fp, "REPEAT\t%d\t%d\t%d\t%d\t%d\n", leng_pred, best_match, final_vec[0], final_vec[1], final_vec[2] );
				}

				L("ITERATION\t%d\t%d\t%d\n", j-left_b+1, left_b, j);

				//fprintf(out_fp, "%s%s%s", p_it->second[i].ref.c_str(), p_it->second[i].align.c_str(), p_it->second[i].contig.c_str());

			}

		}
		else
		{
			E("Missing Chromosome: %s\n", v_it->first.c_str());
		}
	}
	fclose(out_fp);
	fclose(all_fp);
}
/**********************************************/
int HammingDist( const string &q_str, const string &tmp_str)
{
	int q_l = (int)q_str.size();
	int t_l = (int)tmp_str.size();
	int limit = q_l, diff = t_l - q_l;
	if ( t_l < q_l ){limit = t_l; diff = q_l - t_l;} // prevent from accessing invalid position
	int dist = 0;
	for( int i = 0; i < limit ; i++ )
	{
		if ( q_str[i]!=tmp_str[i])
		{	dist++;	}
	}
	return dist + diff;
}
/**********************************************/
// Assumption: q_str is SHORTER than tmp_str
int Best_HammingDist( const string &q_str, const string &tmp_str, int &best_loc)
{
	int q_l = q_str.size();
	int t_l = tmp_str.size();
	int best_dist = q_l;
	int t_dist = 0;
	for( int i = 0; i < t_l - q_l + 1 ; i++ )
	{	// hamming distance between q_str and tmp_str
		t_dist = 0;
		for( int j = 0; j < q_l ; j++)
		{
			if ( q_str[j]!=tmp_str[i+j])
			{	t_dist++;	}
		}

		if ( t_dist < best_dist)
		{
			best_dist = t_dist;
			best_loc = i; // location provides best hamming distance
		}
	}
	return best_dist;
}
/**********************************************/
void calibrate_info( FILE *out, const char *option, uint32_t shift, int cali_flag)
{
	char *info=(char*)malloc(MAX_LINE);
	char *tag=(char*)malloc(MAX_LINE);
	char *misc=(char*)malloc(MAX_LINE);
	char c;
	int t_offset=0, offset=0;
	uint32_t s, e;
	int ret=4;
	//fprintf(out,"\n==%s\n", option);
	while( *(option+t_offset))
	{
		ret = sscanf( option+t_offset, "%[^\=]=%[^\;]%c%n", info, tag, &c, &offset);
		if ( 0 == strncmp("END", info, 3 )) // END. for inversion
		{
			sscanf(tag, "%u", &e );
			fprintf(out,"%s=%u;", info, e+shift);
		}
		else if ( 0 == strncmp("ME", info, 2 ))
		{
			sscanf(tag, "%[^\,],%u,%u,%c;%n", misc, &s, &e, &c,  &offset );
			fprintf(out,"%s=%s,%u,%u,%c;", info, misc,  s+shift, e+shift, c);
		}
		else
		{
			fprintf(out,"%s=%s;", info, tag);
		}
		t_offset += offset;
		if ( 3 != ret){break;} // for the last record without colon
	}
	fprintf(out,"CAL=%d;\n", cali_flag);
	free(misc);
	free(tag);
	free(info);
}
/**********************************************/
int get_flank(const string &chr, string &flank, uint32_t start, size_t length )
{
	int f_size=3;
	if ( start + length + f_size > chr.size() ){ f_size = chr.size() - start - length; }
	flank = chr.substr(start - f_size, length + 2*f_size); // 2 copies of flanking!
	return f_size;
}
/**********************************************/
// To return FLAG value for new VCF: 1 for the first ININVERTED base. 4 for the first INVERTED base. 5 Otherwise
int check_longinv( const string &report, const string &left, const string &left_rc)
{
	int flag = 5;
	if ( report == left){ flag = 1;}
	else if ( report == left_rc){flag = 4;}
	return flag;
}
/**********************************************/
int compeql_str( const string &s1, const string &s2)
{
	int flag = 1;
	if (s1 == s2){ flag = 0;}
	return flag;
}
/**********************************************/
// VCF calls, reference sequence, output, final location shifting if needed
int cmp_sv_seq( const unit_vcf &vcf_t, const string &chr, FILE *log, uint32_t &shift )
{
	int flag = 0; // If the SV match the reference sequence or not
	string ref=""; // contents in reference seuqence
	string left = "", left_rc = "", left_shift="";
	string flank = ""; // length of flanking regions. Default 3bp
	string ca_ref = ""; // sequences after calibration
	string dup_src=""; // source of duplication
	size_t pred_leng, found;
	int f_size;
	int dist = 0, best_dist = 0, best_loc = 0;
	int long_inv = 0, pos_cmp1 = 0, pos_cmp2 = 0;

	shift = 0;
	pred_leng = vcf_t.e - vcf_t.s;
	ref = chr.substr( vcf_t.s, pred_leng ); // vcf is bed-style format

	if ( "<INV>" == vcf_t.alt )
	{
		//pred_leng = vcf_t.e - vcf_t.s;ref= chr.substr( vcf_t.s, pred_leng); // vcf is bed-style format
		left = chr.substr(vcf_t.s,1);
		make_rc( chr.substr(vcf_t.e-1, 1) , left_rc );
		long_inv = check_longinv( vcf_t.ref, left, left_rc );
		if ( long_inv ) //if ( 0 == vcf_t.ref.compare( left))
		{
			fprintf( log, "S\tINV0\t%u\t%u\t%u\t%d\t%s\t%s\t%s\t%s\n", vcf_t.pos, vcf_t.s, vcf_t.e, long_inv - 1 , vcf_t.ref.c_str(), vcf_t.alt.c_str(), left.c_str(), left_rc.c_str() );
			flag = long_inv;
		}
		else
		{
			fprintf( log, "F\tINV0\t%u\t%u\t%u\t5\t%s\t%s\t%s\t%s\t%s\n", vcf_t.pos, vcf_t.s, vcf_t.e, vcf_t.ref.c_str(), vcf_t.alt.c_str(), left.c_str(), left_rc.c_str(), ref.c_str() );
		}
	}
	else if ("INV" == vcf_t.sv ) // Check if vcf.REF equals to reference since alt might contain sequencing error
	{
		//pred_leng = vcf_t.e - vcf_t.s;ref = chr.substr( vcf_t.s, pred_leng ); // vcf is bed-style format
		f_size = get_flank( chr, flank, vcf_t.s, pred_leng );
		dist = HammingDist( vcf_t.ref, ref );

		if ( 1 >= dist ) //if ( 0 == t_vcf.ref.compare( ref ) )
		{
			fprintf( log, "S\tINV\t%u\t%u\t%u\t%d\t%s\t%s\t%s\n", vcf_t.pos, vcf_t.s, vcf_t.e, dist, vcf_t.ref.c_str(), vcf_t.alt.c_str(), ref.c_str() );
			flag = 1;
		}
		else
		{
			found = flank.find( vcf_t.ref);
			if ( found != string::npos ) // only coordinate error
			{
				fprintf( log, "R0\tINV\t%u\t%u\t%u\t%d\t%s\t%s\t%s\n", vcf_t.pos, vcf_t.s, vcf_t.e, dist, vcf_t.ref.c_str(), vcf_t.alt.c_str(), ref.c_str() );
				shift = found - f_size;
				ca_ref= chr.substr( vcf_t.s + shift, pred_leng);
				fprintf( log, "R1\tINV\t%u\t%u\t%u\t0\t%s\t%s\t%s\n", vcf_t.pos + shift, vcf_t.s + shift, vcf_t.e + shift, vcf_t.ref.c_str(), vcf_t.alt.c_str(), ca_ref.c_str() );
				fprintf( log, "RC\t%d\t%s\t%s\n", vcf_t.ref.compare( ca_ref ),  vcf_t.ref.c_str(), ca_ref.c_str() ) ;
				flag = 2;
			}
			else
			{	// Find Best Hamming Distance in flanking region
				best_dist = Best_HammingDist(vcf_t.ref, flank, best_loc);
				if ( 2 >= best_dist )
				{
					fprintf( log, "R0\tINV\t%u\t%u\t%u\t%d\t%s\t%s\t%s\n", vcf_t.pos, vcf_t.s, vcf_t.e, dist, vcf_t.ref.c_str(), vcf_t.alt.c_str(), ref.c_str() );
					shift = best_loc - f_size;
					ca_ref= chr.substr( vcf_t.s + shift, pred_leng);
					fprintf( log, "R3\tINV\t%u\t%u\t%u\t%d\t%s\t%s\t%s\n", vcf_t.pos + shift, vcf_t.s + shift, vcf_t.e + shift, best_dist, vcf_t.ref.c_str(), vcf_t.alt.c_str(), ca_ref.c_str() );
					fprintf( log, "RF\t%d\t%s\t%s\n", HammingDist( vcf_t.ref, ca_ref ),  vcf_t.ref.c_str(), ca_ref.c_str() ) ;
					flag = 3;
				}
				else
				{
					fprintf( log, "F\tINV\t%u\t%u\t%u\t%d\t%s\t%s\t%s\t%s\n", vcf_t.pos, vcf_t.s, vcf_t.e, best_dist, vcf_t.ref.c_str(), vcf_t.alt.c_str(), ref.c_str(), flank.c_str() );
				}
			}
		}
	}
	else if ("DUP" == vcf_t.sv )
	{
		left = chr.substr(vcf_t.pos-1,1); // the base before duplication
		//pred_leng = vcf_t.e - vcf_t.s;ref= chr.substr( vcf_t.s, pred_leng ); // vcf is bed-style format
		f_size = get_flank( chr, flank, vcf_t.s, pred_leng);
		dup_src = vcf_t.alt.substr(1);
		dist = HammingDist(dup_src, ref);
		pos_cmp1 = compeql_str( vcf_t.ref, left);//vcf_t.ref.compare( left );

		if ( (1 >= dist) && ( 0 == pos_cmp1 ) ) //if ( 0 == t_vcf.alt.substr(1).compare( ref ))
		{
			fprintf(log, "S\tDUP\t%u\t%u\t%u\t%d\t%s\t%s\t%s\n", vcf_t.pos, vcf_t.s, vcf_t.e, dist, vcf_t.ref.c_str(), vcf_t.alt.c_str(), ref.c_str() );
			flag = 1;
		}
		else
		{
			found = flank.find( dup_src );// t_vcf.alt.substr(1) );
			if ( found != string::npos ) // only coordinate error
			{
				shift = found - f_size;
				ca_ref= chr.substr( vcf_t.s + shift, pred_leng);
				left_shift = chr.substr( vcf_t.pos + shift -1, 1 ); // the base before duplication
				pos_cmp2 = compeql_str( vcf_t.ref, left_shift );//vcf_t.ref.compare( left_shift );
				if ( 0 == pos_cmp2)
				{
					fprintf( log, "R0\tDUP\t%u\t%u\t%u\t%d\t%s\t%s\t%s\n", vcf_t.pos, vcf_t.s, vcf_t.e, dist, vcf_t.ref.c_str(), vcf_t.alt.c_str(), ref.c_str() );
					//fprintf( log, "R1\tDUP\t%u\t%s\t%s\t%s\n", found, t_vcf.ref.c_str(), t_vcf.alt.c_str(), flank.substr( found, pred_leng ).c_str() );
					fprintf( log, "R1\tDUP\t%u\t%u\t%u\t0\t%s\t%s\t%s\t%d\t%d\n", vcf_t.pos, vcf_t.s+ shift, vcf_t.e+ shift, vcf_t.ref.c_str(), vcf_t.alt.c_str(), ca_ref.c_str(), pos_cmp1, pos_cmp2 );
					fprintf( log, "RC\t%d\t%s\t%s\n", vcf_t.alt.substr(1).compare( ca_ref ),  vcf_t.alt.substr(1).c_str(), ca_ref.c_str() ) ;
					flag = 2;
				}
				dist = 0; // for tracking purpose
			}
			else
			{	// Find Best Hamming Distance
				best_dist = Best_HammingDist( dup_src, flank, best_loc );//t_vcf.alt.substr(1), flank);
				if (  2 >= best_dist )
				{
					shift = best_loc - f_size;
					ca_ref= chr.substr( vcf_t.s + shift, pred_leng);
					left_shift = chr.substr(vcf_t.pos + shift -1,1); // the base before duplication
					pos_cmp2 = compeql_str( vcf_t.ref, left_shift);//vcf_t.ref.compare( left_shift );
					if ( 0 == pos_cmp2)
					{
						fprintf( log, "R0\tDUP\t%u\t%u\t%u\t%d\t%s\t%s\t%s\n", vcf_t.pos, vcf_t.s, vcf_t.e, dist, vcf_t.ref.c_str(), vcf_t.alt.c_str(), ref.c_str() );
						fprintf( log, "R3\tDUP\t%u\t%u\t%u\t%d\t%s\t%s\t%s\t%d\t%d\n", vcf_t.pos, vcf_t.s+ shift, vcf_t.e+ shift, best_dist, vcf_t.ref.c_str(), vcf_t.alt.c_str(), ca_ref.c_str(),
								pos_cmp1, pos_cmp2 );
						fprintf( log, "RF\t%d\t%s\t%s\n", HammingDist( vcf_t.alt.substr(1), ca_ref ),  vcf_t.alt.substr(1).c_str(), ca_ref.c_str() ) ;
						flag = 3;
					}
				}
				dist = best_dist;
			}

			if ( 0 == flag )
			{
				fprintf( log, "F\tDUP\t%u\t%u\t%u\t%d\t%s\t%s\t%s\t%s\t%d\t%d\n", vcf_t.pos, vcf_t.s, vcf_t.e, dist, vcf_t.ref.c_str(), vcf_t.alt.c_str(), ref.c_str(), flank.c_str(), pos_cmp1, pos_cmp2 );
			}
		}
	}
	else
	{	E("Undefined Format %s\n", vcf_t.info.c_str()); }
	return flag;
}
/**********************************************/
void VCF_calibration( const map<string, vector<unit_vcf> > &sort_record,  const map<string, string > &chr_map, char *out_file)
{
	map<string, vector<unit_vcf> >::const_iterator v_it; // prediction: starting by starting position, not POS
	map<string, string >::const_iterator chr_it; // reference genome

	char *vcf_out=(char*)malloc(MAX_LINE);
	char *vcf_log=(char*)malloc(MAX_LINE);
	char *vcf_miss=(char*)malloc(MAX_LINE);
	strcpy(vcf_out, out_file);
	strcat(vcf_out, ".vcf");
	strcpy(vcf_log, out_file);
	strcat(vcf_log, ".calibrate");
	strcpy(vcf_miss, out_file);
	strcat(vcf_miss, ".notfix");
	FILE *out_fp = fopen(vcf_out, "w");
	FILE *log_fp = fopen(vcf_log, "w");
	FILE *miss_fp = fopen(vcf_miss, "w");

	int i = 0 , j = 0, v_size = 0, chr_size = 0;
	uint32_t shift = 0;
	int cali_flag = 0;
	int count = 0, n_cor = 0, n_cali = 0;

	for( v_it = sort_record.begin(); v_it != sort_record.end(); v_it++)
	{
		chr_it = chr_map.find(v_it->first);
		if ( chr_it != chr_map.end() )
		{
			E("Calibrate VCF Calls in chromosome %s\n", v_it->first.c_str());
			v_size = (int)v_it->second.size();
			chr_size = (int)chr_it->second.size();
			for( i = 0; i < v_size; i++)
			{
				//cmp_sv_seq( v_it->second[i], chr_it->second );
				cali_flag = cmp_sv_seq( v_it->second[i], chr_it->second, log_fp, shift );
				if ( 1 <= cali_flag)
				{
					fprintf( out_fp, "%s\t", v_it->second[i].chr.c_str());
					fprintf( out_fp, "%u\t", v_it->second[i].pos + shift);
					//if ( "DUP" == v_it->second[i].sv)
					//{	fprintf( out_fp, "%u\t", v_it->second[i].pos);	}
					//else
					//{	fprintf( out_fp, "%u\t", v_it->second[i].pos + shift);	}
					fprintf( out_fp, ".\t%s\t%s\t", v_it->second[i].ref.c_str(), v_it->second[i].alt.c_str() );
					fprintf( out_fp, "%f\t%s\t", v_it->second[i].qual, v_it->second[i].filter.c_str() );
					calibrate_info( out_fp, v_it->second[i].info.c_str(), shift, cali_flag);
					n_cor ++;
					if ( 2 <= cali_flag){ n_cali++;}
				}
				else
				{
					fprintf( miss_fp, "%s\t%u\t.\t%s\t%s\t", v_it->second[i].chr.c_str(), v_it->second[i].pos, v_it->second[i].ref.c_str(), v_it->second[i].alt.c_str() );
					fprintf( miss_fp, "%f\t%s\t", v_it->second[i].qual, v_it->second[i].filter.c_str() );
					fprintf( miss_fp, "%s\n", v_it->second[i].info.c_str() );
				}

			}
			count+=v_size;
		}
		else
		{
			E("Missing Chromosome: %s\n", v_it->first.c_str());
		}
	}
	//E("Number of Correct/Calibrate/Missing Records:\t%u\t%u\t%u\n", n_cor, n_cali, count - n_cor - n_cali);
	E("Number of Valid/Calibrated/Missing Records:\t%d\t%d\t%d\n", n_cor, n_cali, count - n_cor);
	fclose(out_fp);
	fclose(log_fp);
	fclose(miss_fp);
	free(vcf_out);
	free(vcf_log);
	free(vcf_miss);
}
/**********************************************/
int pass_VCF_filter( const unit_vcf &vcf_t)
{
	return 1; // TO Be FILLED
}
/**********************************************/
// extend gene boundary when long invesion exceeds the gene boundary
int get_correct_bd( uint32_t &lb, uint32_t &rb, const unit_vcf &vcf_t, const uint32_t g_s, const uint32_t g_e, uint32_t &l_d, uint32_t &r_d )
{
	int bd_flag = 0; // 0 for gene bound, 1 for left, 2 for eight, 3 for both ends
	lb = g_s - 1;
	rb = g_e - 1;
	if ( "INV" == vcf_t.sv)
	{
		if ( g_e < vcf_t.e )
		{	// exceed right end
			rb = vcf_t.e - 1;
			r_d = vcf_t.e - g_e ;
			bd_flag = 2;
		}
		if ( vcf_t.s < g_s - 1 )
		{	// exceed left end
			lb = vcf_t.s;
			l_d = g_s - 1 - vcf_t.s;
			bd_flag++;
		}
	}
	return bd_flag;
}
/**********************************************/
// Extract Gene Sequences from lb to rb just in case when long SV exceeds gene boundary
int	get_SV_region( const unit_vcf &vcf_t, const gene_data &gene_item, const string &chr_seq, string &new_seq, vector<uint32_t> &vec_coor, sv_region_t &sv_bd )
{	//gene are in 1-based
	new_seq.clear();
	vec_coor.clear();
	uint32_t seq_l =0, dup_l=0, inv_l = 0, ref_l=0;// final length, duplication length, original length
	//ref_l = gene_item.end - gene_item.start + 1;
	int i = 0;
	uint32_t count = 0;
	uint32_t lb = 0, rb = 0, l_diff = 0, r_diff = 0;
	int bd_flag = get_correct_bd( lb, rb, vcf_t, gene_item.start, gene_item.end, l_diff, r_diff );
	L("SeqCHECK\t%s\t%d\t%s\t%s\t%s\t", vcf_t.patient.c_str(), vcf_t.id, gene_item.gene_id.c_str(), vcf_t.sv.c_str(), vcf_t.chr.c_str());
	L("%u\t%u\t%u\t%u\t%u\t%u\t%u\t%u\t%u\t%d\n", vcf_t.pos, vcf_t.s, vcf_t.e, gene_item.start, gene_item.end, lb, rb, l_diff, r_diff, bd_flag );
	ref_l = rb - lb + 1;
	new_seq.reserve( ref_l );
	vec_coor.reserve( ref_l );
	string inv_str;

	sv_bd.lb = lb;
	sv_bd.rb = rb;
	sv_bd.l_diff = l_diff;
	sv_bd.r_diff = r_diff;
	sv_bd.flag = bd_flag;

	if ( "INV" == vcf_t.sv)	// Sequence for Inversion should be (lb, s-1), ALT, (e, rb)
	{
		seq_l = ref_l;
		//L("SeqLength\tINV\t%s\t%u\t%u\t%u\t%u\n", vcf_t.chr.c_str(), vcf_t.pos, vcf_t.s, vcf_t.e, seq_l);
		// Before Inversion
		if ( lb < vcf_t.s )
		{
			for( i = lb; i < vcf_t.s ; i++)
			{	new_seq.push_back(chr_seq[i]);
				vec_coor.push_back(count ); count++;
			}
		//L("B4\t%u\t%u\t%u\n", lb, vcf_t.s-1, count);
		}

		// ALT
		inv_l = vcf_t.e - vcf_t.s;
		if ( "<INV>" == vcf_t.alt)
		{
			make_rc( chr_seq.substr( vcf_t.s, inv_l), inv_str );
			new_seq.append( inv_str );
		}
		else
		{	new_seq.append(vcf_t.alt);	}

		for( i = 0; i < inv_l; i++)
		{	vec_coor.push_back(count ); count++;	}
		//L("ALT\t%u\t%u\t%u\n", vcf_t.s, vcf_t.e-1, count);

		// After Inversion
		//L("Hm\t%u\t%u\n", vcf_t.e, rb);
		if ( vcf_t.e <= rb ) // ( vcf_t.e < rb  )
		{
			for( i = vcf_t.e; i <= rb ; i++)
			{	new_seq.push_back(chr_seq[i]);
				vec_coor.push_back(count ); count++;
			}
		//	L("AF\t%u\t%u\t%u\n", vcf_t.e, rb, count);
		}

	}
	else if ("DUP" == vcf_t.sv)	// Sequence for duplication should be (lb, pos-1), ALT, (pos, rb)
	{
		dup_l = vcf_t.e - vcf_t.s;
		seq_l = ref_l + dup_l;
		//L("SeqLength\tDUP\t%s\t%u\t%u\t%u\t%u\t%u\n", vcf_t.chr.c_str(), vcf_t.pos, vcf_t.s, vcf_t.e, seq_l, ref_l);
		// Before Duplication: ( lb, pos-1 )
		if ( lb < vcf_t.pos )
		{
			for( i = lb; i < vcf_t.pos ; i++)
			{	new_seq.push_back(chr_seq[i]);
				vec_coor.push_back(count ); count++;
			}
		}

		new_seq.append(vcf_t.alt.substr(1));
		for( i = 1; i <vcf_t.alt.size(); i++)
		{	count++;	} // increment only

		// After Duplication: (pos, rb)
		if ( vcf_t.pos <= rb  )
		{
			for( i = vcf_t.pos; i <= rb ; i++)
			{	new_seq.push_back(chr_seq[i]);
				vec_coor.push_back(count ); count++;
			}
		}
	}

	L("GENOME\t%s\t%d\t%s\t%u\t%u\t%u\t%u\t%u\t%u\t%lu\t%lu\n", vcf_t.patient.c_str(), vcf_t.id, vcf_t.sv.c_str(), count, seq_l, ref_l, dup_l, count-seq_l, seq_l -ref_l-dup_l, new_seq.size(), vec_coor.size());
	return seq_l;
}
/**********************************************/
// extract SV isoform sequence from ref_seq w.r.t sv_bd boundary and vec_coor position
uint32_t get_SV_transcript( const unit_vcf &vcf_t, string &trans_sv, const string &ref_seq, const vector<uint32_t> &vec_coor, const gene_data &gene_item, const isoform &iso_item,
	const sv_region_t &sv_bd, int before_sv, int total_sv)
{
	//E(" Getting Isoform for %s in %u\n", iso_item.id.c_str(), ref_seq.size());
	int exon_num = (int)iso_item.exon.size();
	uint32_t old_s = 0, old_e = 0,
			idx_s = 0, idx_e = 0,
			new_s = 0, new_e = 0;
	trans_sv.clear();
	trans_sv.reserve(4096);
	for( int i = 0; i < exon_num; i++ ) // seq for exon i
	{
		if ( 0 < iso_item.exon[i].cds) // CDS or Codon
		{
			old_s = iso_item.exon[i].c_s -1; // 0-based
			old_e = iso_item.exon[i].c_e -1; // 0-based
			idx_s = old_s - sv_bd.lb;
			idx_e = old_e - sv_bd.lb;
			if ( idx_e < vec_coor.size())
			{
				new_s = vec_coor[idx_s];
				new_e = vec_coor[idx_e];
				for( int j = new_s; j <= new_e; j ++ )
				{
					if ( j < ref_seq.size() )
					{	trans_sv.push_back( ref_seq[j]); }
					else	{ E("WHAT0 %d %lu\n", j , ref_seq.size());}
				}
			}
			else	{ E("WHAT1 %s %d %u %u %u %lu\n", vcf_t.patient.c_str(), vcf_t.id, vcf_t.pos, idx_s, idx_e, vec_coor.size());}
		}
	}
	//E(" Finish Isoform for %s of %u\n", iso_item.id.c_str(), trans_sv.size());
	return trans_sv.size();
}

/**********************************************/
// Output All Transcripts in a Corresponding Gene Overlapping With An Inversion/Duplication Call
// Generate the corresponding gene region based on gene_item: Now
// And output all transcript sequences intersecting CDS part
void OverlappingIsoform_vcf( const unit_vcf &vcf_t, const gene_data &gene_item, const vector<isoform> &vec_isoform, const string &chr_seq, match_info &mi_item, vector<int> &summary, FILE *seq_fp )
{
	match_info m_item; // Can we safely remove it in this module

	int num_iso=(int)vec_isoform.size();
	int num_exon;
	int i, j, best_i;

	// if it's UTR or intronic
	int flag_utr = 0; // 0 for leftmost UTR, 1 after seeing CDS start, 2 after seeing CDS end. Only Region 1 contains CDS
	int tmp_l = 0, max_overlap = 0; // tmp_l is the overlapping length wrt an exon. max_overlap is the best one among all exons
	int tmp_cds = 0; // overlap with cds or not. Check: length or flag?
	int flag_hit = 0, // 0 for left UTR, 1 for right UTR, 2 for intronic, 3 for cds/exon/partial
		best_hit = -1;
	string best_source="";
	int before_sv = 0, // bases before reaching a SV
		total_sv = 0; // total overlap length with a transcript. As a result the trasncript look like (before_sv, then total_sv, then remaining parts)

	// gene sequence and corresponding transcript sequences
	string seq_sv = "", trans_sv = "";
	vector<uint32_t> vec_coor; // new coordinates on modified seq_sv
	uint32_t seq_size, iso_size;
	sv_region_t sv_bd;
	// PURPOSE: get correct gene region boundaries before generating transcripts
	seq_size = get_SV_region( vcf_t, gene_item, chr_seq, seq_sv, vec_coor, sv_bd);

	L("ISO_STA\t%s\t%d\t%s\t%u\t%u\t%u\n", vcf_t.patient.c_str(), vcf_t.id, gene_item.gene_id.c_str(), vcf_t.pos, vcf_t.s, vcf_t.e);

	int ct_exon, ct_cds; // number of overlapping exon and cds in a transcript
	int i_flag = 0, e_flag = 0, c_flag = 0; // if an isoform/exon/cds overlaps a VCF call
	int possible = 0, e_hit = 0, c_hit = 0, p_hit = 0; // the number of possible isoform/ exon_hit/ cds_hit/ problematic_hit
	int e_iso=0, c_iso = 0, p_iso=0;
	int cross_exon = 0, cross_cds = 0;
	initial_summary( summary, 6 );

	// I already have a region and sv_bd
	for( i = 0; i< num_iso; i++) // For isoform i in this gene
	{
		num_exon = vec_isoform[i].exon.size();
		L("cmp_iso\t%s\t%d\t%d\t%s\t%s\n", vcf_t.patient.c_str(), vcf_t.id, i, vec_isoform[i].src.c_str(), vec_isoform[i].fea.c_str() );

		flag_utr = 0;
		max_overlap = 0;
		flag_hit = 0;
		ct_exon = 0; ct_cds = 0; // number of ovelapping exon and cds in this isoform
		i_flag = 0;
		e_flag = 0;
		c_flag = 0;
		e_iso = 0;
		c_iso = 0;
		p_iso = 0;
		before_sv = 0; total_sv = 0;

		for( j = 0; j < num_exon; j++ )
		{
			tmp_cds = 0;
			L(" cmp_exon\t%d\t%d\t%d\t%u\t%u\t%u\t%u\n", i, j, vec_isoform[i].exon[j].cds, vec_isoform[i].exon[j].e_s, vec_isoform[i].exon[j].e_e, vec_isoform[i].exon[j].c_s, vec_isoform[i].exon[j].c_e );
			if ( skip_interval( vcf_t, vec_isoform[i].exon[j].e_e ) )	//if ( vec_isoform[i].exon[j].e_e < pred.s)
			{
				// otherwise we will add 1 instead of 0
				if ( 0 < vec_isoform[i].exon[j].c_s ) {	before_sv += vec_isoform[i].exon[j].c_e - vec_isoform[i].exon[j].c_s + 1;}

				if ( ( 0 == flag_utr) && ( 0 < vec_isoform[i].exon[j].cds ) ){ flag_utr = 1; }
				else if ( ( 1 == flag_utr) && ( 2 > vec_isoform[i].exon[j].cds ) ){ flag_utr = 2; }
				L("skip\t%d\t%d\t%d\n", vec_isoform[i].exon[j].cds, flag_utr, flag_hit ); //vec_isoform[i].src.c_str(), vec_isoform[i].fea.c_str(), i, j, vec_isoform[i].exon[j].e_s, vec_isoform[i].exon[j].e_e);
				continue;
			}
			else if ( final_interval( vcf_t, vec_isoform[i].exon[j].e_s ) ) //vec_isoform[i].exon[j].e_s > pred.e )
			{
				if ( (0 == flag_hit) && ( 1 == flag_utr)){flag_hit=2;}
				else if ( (0 == flag_hit) && ( 2 == flag_utr)){flag_hit=1;}
				L("pass\t%d\t%d\t%d\n", vec_isoform[i].exon[j].cds, flag_utr, flag_hit); // vec_isoform[i].src.c_str(), vec_isoform[i].fea.c_str(), i, j, vec_isoform[i].exon[j].e_s, vec_isoform[i].exon[j].e_e);
				break;
			}
			else if ( 0 <  vcf_overlap( vcf_t, vec_isoform[i].exon[j].e_s, vec_isoform[i].exon[j].e_e ) ) // overlapping an exon
			{
				flag_hit = 3;
				i_flag = 1;	ct_exon++; e_flag = 1;

				tmp_l = vcf_overlap_length( vcf_t, vec_isoform[i].exon[j].e_s, vec_isoform[i].exon[j].e_e );
				//if ( 0 < tmp_l){cross_exon++;e_flag = 1;}
				L("E_matched:%s\t%s\t%u\t%u\t%u\t%s\t%s\t%u\t%u\t%d\n",
						vec_isoform[i].src.c_str(), vcf_t.sv.c_str(), vcf_t.pos, vcf_t.s, vcf_t.e, vec_isoform[i].ref.c_str(), vec_isoform[i].tname.c_str(),
						vec_isoform[i].exon[j].e_s, vec_isoform[i].exon[j].e_e, tmp_l);

				if ( 2 == vec_isoform[i].exon[j].cds ) // CDS
				{
					tmp_cds = vcf_overlap_length( vcf_t, vec_isoform[i].exon[j].c_s, vec_isoform[i].exon[j].c_e );
					if ( 0 < tmp_cds){ct_cds++; c_flag += 1;}
					if ("INV" == vcf_t.sv )
					{
						if ( vcf_t.e - vcf_t.s == tmp_cds )// fully containment
						{
							before_sv += vcf_t.pos - vec_isoform[i].exon[j].c_s + 1;
							total_sv = vcf_t.e - vcf_t.s;
							//m_item.overlap = vcf_t.e - vcf_t.s;	m_item.cds = 2;
						}
						else // partial
						{
							if ( vec_isoform[i].exon[j].c_s -1 < vcf_t.pos)
							{	before_sv += vcf_t.pos - vec_isoform[i].exon[j].c_s + 1;	}
							total_sv += tmp_cds;
							//m_item.overlap = tmp_cds;m_item.cds = 3;
						}
					}
					else
					{
						before_sv += vcf_t.pos - vec_isoform[i].exon[j].c_s + 1;
						total_sv = vcf_t.e - vcf_t.s;
						//m_item.overlap = 1;		m_item.cds = 2; // Always fully containment
					}
				}
				else if ( 1 == vec_isoform[i].exon[j].cds) // state changing exon
				{
					tmp_cds = vcf_overlap_length( vcf_t, vec_isoform[i].exon[j].c_s, vec_isoform[i].exon[j].c_e );
					if ( 0 < tmp_cds){ct_cds++; c_flag += 1;}
					if ( 0 == tmp_cds ) // 5 UTR or 3 UTR
					{
						m_item.overlap = 0;
						if ( final_interval( vcf_t, vec_isoform[i].exon[j].c_s)) //( pred.e < vec_isoform[i].exon[j].c_s)
						{ m_item.cds = 0;}
						else
						{ m_item.cds = 1;}
					}
					else if ("INV" == vcf_t.sv )
					{
						if ( vcf_t.e - vcf_t.s == tmp_cds ) //pred is vcf now
						{
							before_sv += vcf_t.pos - vec_isoform[i].exon[j].c_s + 1;
							total_sv = vcf_t.e - vcf_t.s;
							//m_item.overlap = vcf_t.e - vcf_t.s;	m_item.cds = 2;
						}
						else
						{
							if ( vec_isoform[i].exon[j].c_s -1 < vcf_t.pos)
							{	before_sv += vcf_t.pos - vec_isoform[i].exon[j].c_s + 1;	}
							total_sv += tmp_cds;
							//m_item.overlap = tmp_cds;	m_item.cds = 3;
						}
					}
					else
					{
						before_sv += vcf_t.pos - vec_isoform[i].exon[j].c_s + 1;
						total_sv = vcf_t.e - vcf_t.s;
						//m_item.overlap = 1;	m_item.cds = 2; // Always fully containment
					}
				}
				else // Non-coding
				{
					m_item.overlap = 0;
					if ( 0 == flag_utr){ m_item.cds = 0;}
					else{ m_item.cds = 1;}
				}

				// For This Exon
				m_item.overlap = tmp_l;//m_item.cds = flag_utr;
				max_overlap = tmp_l;
				//L("f_exon:\t%d\t%d\t%d\t%d\t%d\t%u\t%u\t%u\t", tmp_l, tmp_cds, e_flag, c_flag, vec_isoform[i].exon[j].cds, vcf_t.pos, vcf_t.s, vcf_t.e);//, m_item.cds );
				L("f_cds:%d\t%d\t%d\t%d\t%d\t%d\t%u\t%u\t%u\t%u\t%u\t%d\t%d\n", i, j,
				tmp_cds, vec_isoform[i].exon[j].cds, flag_utr, flag_hit, vcf_t.pos, vcf_t.s, vcf_t.e, vec_isoform[i].exon[j].c_s, vec_isoform[i].exon[j].c_e, before_sv, total_sv);
				if( e_flag ){ e_iso=1; }
				if( c_flag ){ c_iso=1; }
				if( 1 < ct_cds){ p_iso=1; }
			}
		}

		L("iso_%d:%d %d %d\n\n", i, e_iso, c_iso, p_iso); // Summary for THIS transcript
		if ( 1 == i_flag ){ possible++;}
		if ( 1 == e_iso ){ e_hit++;}
		if ( 1 == c_iso ){ c_hit++;}
		if ( 1 == p_iso ){ p_hit++;}
		// Convert Sequence Here
		if ( 1 == c_iso ) // contain coding region
		{
			iso_size = get_SV_transcript( vcf_t, trans_sv, seq_sv, vec_coor, gene_item, vec_isoform[i], sv_bd, before_sv, total_sv );
			if ( iso_size ) // should always triggered
			{
				fprintf(seq_fp,"%s\t%d\t%s\t%s\t%u\t%u\t%u\t", vcf_t.patient.c_str(), vcf_t.id, vcf_t.sv.c_str(), vcf_t.chr.c_str(), vcf_t.pos, vcf_t.s, vcf_t.e);
				fprintf(seq_fp,"%s\t%s\t%c\t%d\t%d\t%d\t", gene_item.gene_id.c_str(), vec_isoform[i].id.c_str(), vec_isoform[i].strand, iso_size, before_sv, total_sv);
				//fprintf(seq_fp,">%s_%d_%s_%s_%u_%u_%u_%s_%u\n", vcf_t.patient.c_str(), vcf_t.id, vcf_t.chr.c_str(), vcf_t.sv.c_str(), vcf_t.pos, vcf_t.s, vcf_t.e,  vec_isoform[i].id.c_str(), iso_size);
				fprintf(seq_fp,"%s\n", trans_sv.c_str());
			}
		}
		//if ( ( 0< iso_size) && ( 0 == c_hit)){E("HECK %d %d\n", iso_size, c_hit);}

	}
	L("ISO_END\t%s\t%d\t%s\t%d\t%d\t%d\n", vcf_t.patient.c_str(), vcf_t.id, gene_item.gene_id.c_str(), e_hit, c_hit, p_hit);
	summary[0]=num_iso;
	summary[1]=possible;
	summary[2]=e_hit;
	summary[3]=c_hit;
	summary[4]=p_hit;
	summary[5]=(int)seq_size;

	copy_match(m_item, mi_item);
}
/**********************************************/
void initial_summary( vector<int> &vec_sum, int n_item)
{
	vec_sum.clear();
	for(int i= 0; i < n_item; i++){ vec_sum.push_back(0);}
}
void adj_summary( vector<int> &f_sum, const vector<int> &t_sum, int n_item)
{
	for(int i = 0; i < n_item-1; i++){ f_sum[i] += t_sum[i];}
	//f_sum[n_item-1] = t_sum[n_item-1];
}
// Find All ( Protein Coding ) Genes Overlapping With The Corresponding Inversions/Duplications Entry
/**********************************************/
void Overlapping_vcf( const unit_vcf &vcf_t, const vector<gene_data> &vec_gene, const map<string, vector<isoform> > &iso_gene_map, const string &chr_seq, FILE *out_fp, FILE *seq_fp )//, match_info &mi_item)
{
	map<string, vector<isoform> >::const_iterator it;
	int limit = vec_gene.size();
	int check_gene = 0, // genes which we cannot skip
		match_gene = 0; // number of genes with overlapping EXONIC part. Should we change to CDS part?

	int match_iso = 0;
	int max_overlap = -1;	//int g_hit = 0;// default: no overlapped gene detected

	match_info g_mitem;	// We use g_mitem to pick among multiple overlapping entries
	vector<int> vec_sum, vec_sum_gene;
	initial_summary( vec_sum, 6 );
	initial_summary( vec_sum_gene, 6 );

	for( int i = 0; i < limit; i++ )
	{
		if ( skip_interval( vcf_t, vec_gene[i].end ) )
		{	//L("Skip: boundary %u/%u is crossed at %u\n", vcf_t.s, vcf_t.pos, vec_gene[i].end);
			continue;
		}
		else if ( final_interval( vcf_t, vec_gene[i].start) )
		{
			L("Stop: vcf %u/%u stops at %u\n", vcf_t.e, vcf_t.pos, vec_gene[i].start);
			break;
		}
		else
		{
			if ( 0 < vcf_overlap( vcf_t, vec_gene[i].start, vec_gene[i].end ) )
			{
				L("Gene1\t%d\t%s\t%u\t%u\t%u\t%s\t%s\t%u\t%u\n", match_gene, vec_gene[i].gene_id.c_str(), vcf_t.pos, vcf_t.s, vcf_t.e, vec_gene[i].chr.c_str(), vec_gene[i].gene_name.c_str(), vec_gene[i].start, vec_gene[i].end);
				it = iso_gene_map.find( vec_gene[i].gene_id );

				clear_match_info(g_mitem);
				OverlappingIsoform_vcf( vcf_t, vec_gene[i], it->second, chr_seq, g_mitem, vec_sum_gene, seq_fp );
				adj_summary( vec_sum, vec_sum_gene, 6 );// only increment to n_item-1 one
				if ( 0 < vec_sum_gene[2]){match_gene++;}
				//
				//if ( 0 < g_mitem.overlap)//if ( max_overlap < g_mitem.overlap)
				//{
				//	if ( max_overlap < g_mitem.overlap){ max_overlap = g_mitem.overlap; }
				//}
				//g_hit = 1;
			}
			else
			{
				L("Gene0\t%u\t%u\t%u\t%s\t%s\t%u\t%u\n", vcf_t.pos, vcf_t.s, vcf_t.e, vec_gene[i].chr.c_str(), vec_gene[i].gene_name.c_str(), vec_gene[i].start, vec_gene[i].end);
			}
			check_gene++;
		}
	}	//L("Finalize: vcf %u/%u/%u stops with %d item\n", vcf_t.pos, vcf_t.e, vcf_t.s, check_gene);

	vec_sum[5] = vec_sum_gene[5]; // DANGEROUS sometimes
	fprintf(out_fp, "%s\t%d\t%s\t%s\t%u\t%u\t%u\t", vcf_t.patient.c_str(), vcf_t.id, vcf_t.chr.c_str(), vcf_t.sv.c_str(), vcf_t.pos, vcf_t.s, vcf_t.e);
	fprintf(out_fp, "%d\t%d\t%d\t%d\t%d\t%d\t%d\t%u\n", check_gene, match_gene, vec_sum[0], vec_sum[1], vec_sum[2], vec_sum[3], vec_sum[4], vec_sum[5]);
}
/**********************************************/
void PeptideDB_VCF( const map<string, vector<unit_vcf> > &sort_record,  const map<string, vector<gene_data> > &gene_sorted_map, const map<string, vector<isoform> > &iso_gene_map, const map<string, string> &chr_map, char *out_file)
{
	map<string, vector<unit_vcf> >::const_iterator v_it; // vcf
	map<string, vector<gene_data> >::const_iterator g_it; // gene interval
	map<string, vector<isoform> >::const_iterator i_it; // isoform interval
	map<string, string>::const_iterator r_it;	// chromosome
	match_info mi_item;

	char *all_out=(char*)malloc(MAX_LINE);
	char *seq_out=(char*)malloc(MAX_LINE);
	//char *anno=(char*)malloc(MAX_LINE);
	//char *extend=(char*)malloc(MAX_LINE);
	strcpy(all_out, out_file);
	strcat(all_out, ".gene");
	strcpy(seq_out, out_file);
	strcat(seq_out, ".transcript");
	//strcpy(anno, out_file);
	//strcat(anno, ".anno");
	//strcpy(extend, out_file);
	//strcat(extend, ".extend");
	FILE *out_fp = fopen(all_out, "w");
	FILE *seq_fp = fopen(seq_out, "w");
	//FILE *extend_fp = fopen(extend, "w");

	int i, limit;
	int leng_pred = 0;
	int flag_anno = 0;
	string p_type ="";

	for( v_it = sort_record.begin(); v_it != sort_record.end(); v_it++)
	{
		// For all VCF in a single chromosome
		g_it = gene_sorted_map.find(v_it->first);
		r_it = chr_map.find(v_it->first);
		if ( (g_it != gene_sorted_map.end() ) && (r_it != chr_map.end() ) )
		{
			E("Start Locating VCFs In Chromosome %s\n", v_it->first.c_str());
			limit = v_it->second.size();
			for( i = 0; i < limit; i ++ )
			{
				if ( pass_VCF_filter(v_it->second[i]) )
				{
					//E("Converting %d %s %s %u %u %u\n", i, v_it->second[i].sv.c_str(), v_it->second[i].chr.c_str(), v_it->second[i].pos, v_it->second[i].s, v_it->second[i].e);
					Overlapping_vcf( v_it->second[i], g_it->second,  iso_gene_map, r_it->second, out_fp, seq_fp );
				}
			}
		}
		else
		{
			E("Missing Chromosome: %s\n", v_it->first.c_str());
		}
	}
	fclose(out_fp);// all results
	fclose(seq_fp);// calls in genetic regions
	free(seq_out);
	free(all_out);
}

/*************** RNA-Seq Relevant Module ***************/
// For the given vcf in a chromosome, output the conflicting one (i.e., the records can NOT exist at the same time)
//void ClusterVCF( const vector<unit_vcf> &vec_vcf )
//{
//	int limit = (int)vec_vcf.size();
//	// Get Overlapping Clusters
//	int i = 0, j = 0, chunk = 0, chunk_size = 0;
//
//	while( i < limit )
//	{
//		// get left and right boundary
//		// grouping the cluster starting from i. j should be the NEXT start position
//		L( "Record\t%d %s:\t%u\t%u\t%u\n", i, vec_vcfpi].chr.c_str(), vec_vcf[i].pos, vec_vcf[i].s, vec_vcf[i].e );
//		while( ( j < limit ) && ( equal_bp( it->second[i], it->second[j]) ) ) // Duplicate
//		{
//			L( "Merge\t%d %d:\t%u\t%u\t%u\t%d\n",j , i, it->second[i].pos, it->second[i].s, it->second[i].e, it->second[j].cluster);
//			//L("Increment %d\n", j);
//			min += it->second[j].min;
//			max += it->second[j].max;
//			if ( sim < it->second[j].sim){ sim = it->second[j].sim;}
//			j++;
//			merge++;
//		}
//		//now from i to j-1
//	}
//}
/**********************************************/
void TranscriptDB_VCF( const map<string, vector<unit_vcf> > &sort_record,  const map<string, vector<gene_data> > &gene_sorted_map, const map<string, vector<isoform> > &iso_gene_map, const map<string, string> &chr_map, char *out_file)
{
	map<string, vector<unit_vcf> >::const_iterator v_it; // vcf calls
	map<string, vector<gene_data> >::const_iterator g_it; // gene interval
	map<string, vector<isoform> >::const_iterator i_it; // isoform interval
	map<string, string>::const_iterator r_it;	// chromosome
	match_info mi_item;

	char *all_out=(char*)malloc(MAX_LINE);
	char *seq_out=(char*)malloc(MAX_LINE);
	strcpy(all_out, out_file);
	strcat(all_out, ".gene");
	strcpy(seq_out, out_file);
	strcat(seq_out, ".transcript");
	FILE *out_fp = fopen(all_out, "w");
	FILE *seq_fp = fopen(seq_out, "w");

	int i, limit,
		leng_pred = 0,
		flag_anno = 0;
	string p_type ="";

	for( v_it = sort_record.begin(); v_it != sort_record.end(); v_it++)
	{
		// For all VCF in a single chromosome
		g_it = gene_sorted_map.find(v_it->first);
		r_it = chr_map.find(v_it->first);
		if ( (g_it != gene_sorted_map.end() ) && (r_it != chr_map.end() ) )
		{
			E("Start Clustering Calls In Chromosome %s\n", v_it->first.c_str());
			//ClusterVCF( v_it->second[i]);
			//limit = v_it->second.size();
			//for( i = 0; i < limit; i ++ )
			//{
			//	if ( pass_VCF_filter(v_it->second[i]) )
			//	{
			//		//E("Converting %d %s %s %u %u %u\n", i, v_it->second[i].sv.c_str(), v_it->second[i].chr.c_str(), v_it->second[i].pos, v_it->second[i].s, v_it->second[i].e);
			//		Overlapping_vcf( v_it->second[i], g_it->second,  iso_gene_map, r_it->second, out_fp, seq_fp );
			//	}
			//}
		}
		else
		{
			E("Missing Chromosome: %s\n", v_it->first.c_str());
		}
	}
	fclose(out_fp);
	fclose(seq_fp);
	free(seq_out);
	free(all_out);
}
/**********************************************/
// Obsolete

/**********************************************/
void ConvertToCount( char *pred_file, map<string, vector<tr_length> > &length_map , char *out_file )
{
	vector<string> token_array;
	map<string, vector<tr_length> >::iterator it;
	string t_id;
	int reg_num = 0, // the number of gene regions for a single id
		f_id, // the index for multi-region transcript
		f_length; // length of the transcript
	FILE *fp = fopen(pred_file, "r");
	FILE *out_fp = fopen(out_file, "w");
	int f_count=0;
	float tmp_rpkm =0.0, f_rpkm = 0.0;
	int seq_depth[12]= {0,0,0, 80000000, 81000000, 82000000, 80001000, 80999000, 82001000, 79999000,81001000, 81999000 };
	//
	float factor_vec[12];
	for( int i = 0; i < 12; i++)
	{
		factor_vec[i] = seq_depth[i]*1.0/1000000000;
	}



	char *readline  = (char*)malloc(MAX_LINE);
	while( NULL != fgets(readline, MAX_LINE, fp) )
	{
		if( 0 == strncmp("T", readline, 1 ) )
		{
			continue;
		}
		token_array = splitString(readline, '\t');
		t_id = token_array[0];
		it = length_map.find(t_id);
		if ( it != length_map.end())
		{
			reg_num = (int)(*it).second.size();
			if ( 1 == reg_num)
			{
				f_id = 0;
				f_length = (*it).second[0].length;
			}
			else
			{
				f_id = select_longest_region((*it).second );
				f_length = (*it).second[f_id].length;
			}
			L("OK\t%s\t%d\t%d\t%d\n", t_id.c_str(), f_id, reg_num, f_length );
			fprintf(out_fp, "%s", t_id.c_str());
			for( int i = 3; i < 12; i ++)
			{
				tmp_rpkm = atof(token_array[i].c_str());
				f_rpkm = tmp_rpkm * f_length * factor_vec[i];
				//fprintf(out_fp, "\t%f", f_rpkm);
				f_count = (int)(floor(f_rpkm+0.5f));
				fprintf(out_fp, "\t%d", f_count);

			}
			fprintf(out_fp, "\n");
		}
		else
		{
			L("NOT_FOUND\t%s\n", t_id.c_str() );
		}


	}

	fclose( out_fp );
	fclose( fp );
}

/**********************************************/
int select_longest_region( vector<tr_length> &tr_vec)
{
	int id = 0,
		max_l = 0,
		limit;
	limit = (int)tr_vec.size();

	for( int i = 0; i < limit; i ++)
	{
		if ( tr_vec[i].length > max_l )
		{
			max_l = tr_vec[i].length;
			id = i;
		}
	}
	return id;
}

/**********************************************/
void OutputJoinedFusion( char *out_file, vector<fusion> &fusion_vec )
{
	int limit=(int)fusion_vec.size();
	int limit_1, limit_2;
	int i = 0, j = 0,
		j1 = 0, j2 =0,
		loc1 = 0, loc2 = 0,
		end1, end2; // end1 indicates the smaller location
	string join_id;
	vector<int> v_1, v_2;

	FILE *out_fp= fopen(out_file, "w");
	for( i =0; i < limit; i++ )
	{
		j = fusion_vec[i].group;
		join_id = fusion_vec[j].id;
		v_1 = fusion_vec[i].coor1;
		v_2 = fusion_vec[i].coor2;

		// Make sure the ends match in grouped predictions
		loc1 = v_1[0];
		loc2 = v_2[0];
		if (loc1 <= loc2)
		{
			end1 = 0;
			end2 = 1;
		}
		else
		{
			end1 = 1;
			end2 = 0;
		}

		limit_1 = (int)v_1.size();
		j1 = 0;
		while( j1 < limit_1 )
		{
			fprintf(out_fp, "%s\t%d\t%s\t%c\t%d\t%d\n", join_id.c_str(), end1,  fusion_vec[i].chr1.c_str(),  fusion_vec[i].strand1, v_1[j1], v_1[j1+1] );
			L("%s\t%d\t%s\t%c\t%d\t%d\t%s\t0\n", join_id.c_str(), end1,  fusion_vec[i].chr1.c_str(),  fusion_vec[i].strand1, v_1[j1], v_1[j1+1], fusion_vec[i].id.c_str() );
			j1 += 2;
		}
		limit_2 = (int)v_2.size();
		j2 = 0;
		while( j2 < limit_2 )
		{
			fprintf(out_fp, "%s\t%d\t%s\t%c\t%d\t%d\n", join_id.c_str(), end2,  fusion_vec[i].chr2.c_str(),  fusion_vec[i].strand2, v_2[j2], v_2[j2+1] );
			L( "%s\t%d\t%s\t%c\t%d\t%d\t%s\t1\n", join_id.c_str(), end2, fusion_vec[i].chr2.c_str(),  fusion_vec[i].strand2, v_2[j2], v_2[j2+1], fusion_vec[i].id.c_str() );
			j2 += 2;
		}


		//L("F\t%s\t%s\n", fusion_vec[i].id.c_str(), join_id.c_str() );
	}
	fclose(out_fp);
}
/**********************************************/
void joinFusion( vector<fusion> &fusion_vec )
{
	int limit=(int)fusion_vec.size();
	int j = 0;
	string join_id;
	for( int i =0; i < limit; i++ )
	{
		j = fusion_vec[i].group;
		join_id = fusion_vec[j].id;
		//L("F\t%s\t%s\n", fusion_vec[i].id.c_str(), join_id.c_str() );
	}
}
/**********************************************/
void clusterFusion( vector<fusion> &fusion_vec )
{
	int limit=(int)fusion_vec.size();
	int hit;
	for( int i =0; i < limit; i++ )
	{
		if ( -1 == fusion_vec[i].group )
		{
			fusion_vec[i].group = i;
			for( int j = i+1; j < limit; j++)
			{
				//L("COMP\t%d\t%d\t%d\n", i, j, limit);
				if ( -1 == fusion_vec[j].group )
				{
					hit = compare_fusion( fusion_vec[i], fusion_vec[j]) ;
					if ( 2 == hit )
					{
						fusion_vec[j].group = i;
					}

				}
			}
		}
	}
}


/**********************************************/
// For prediction intervals (A1,B1) and (A2,B2)
int compare_fusion( fusion &f_1, fusion &f_2)
{
	int hit = 0,
		s_hit =0; // second round of comparison

	if ( (f_1.chr1 == f_2.chr1) && (f_1.chr2 == f_2.chr2) ) //A1-A2 and B1-B2
	{
		if ( f_1.coor1[0] <= f_2.coor1[0] )// A1-A2
		{
			hit +=	compare_seg( f_1.coor1, f_2.coor1 );
		}
		else
		{
			hit +=	compare_seg( f_2.coor1, f_1.coor1 );
		}

		if ( 1 == hit ) // B1-B2
		{
			if ( f_1.coor2[0] <= f_2.coor2[0] )// A1-B2
			{
				hit +=	compare_seg( f_1.coor2, f_2.coor2 );
			}
			else
			{
				hit +=	compare_seg( f_2.coor2, f_1.coor2 );
			}
		}
	}
	if ( (2 > hit) && (f_1.chr1 == f_2.chr2) && (f_1.chr2 == f_2.chr1) ) //A1-B2 and A1-B2
	{
		hit = 0; // Just in case only one end matched
		if ( f_1.coor1[0] <= f_2.coor2[0] )// A1-B2
		{
			hit +=	compare_seg( f_1.coor1, f_2.coor2 );
		}
		else
		{
			hit +=	compare_seg( f_2.coor2, f_1.coor1 );
		}

		if ( 1 == s_hit ) // A2-B1
		{
			if ( f_1.coor2[0] <= f_2.coor1[0] )
			{
				hit +=	compare_seg( f_1.coor2, f_2.coor1 );
			}
			else
			{
				hit +=	compare_seg( f_2.coor1, f_1.coor2 );
			}
		}
	}

	if ( 2 == hit )
	{
		//L("MATCHED\t%s\t%s\n", f_1.id.c_str(), f_2.id.c_str() );
	}

	return hit;
}
/**********************************************/
// Assumption: starting point of v_1 os less than that of v_2
int compare_seg( vector<int> &v_1, vector<int> &v_2)
{
	int hit = 0;
	int limit_1 = (int)v_1.size();
	int limit_2 = (int)v_2.size();

	int i = 0,
		j = 0;
	//OutputIntVec(v_1);
	//OutputIntVec(v_2);
	while( (i < limit_1) && ( j < limit_2))
	{
		if ( v_1[i] <= v_2[j] )
		{
			if ( v_1[i+1] >= v_2[j] )
			{
				hit = 1;
				//L("M_1\t%d\t%d\t%d\t%d\t%d\t%d\n", i, v_1[i], v_1[i+1], j, v_2[j+1], v_2[j+2]);
				break;// Overlap
			}
			else
			{
				i += 2;
			}
		}
		else
		{
			if ( v_1[i] <= v_2[j+1] )
			{
				//L("M_3\t%d\t%d\t%d\t%d\t%d\t%d\n", i, v_1[i], v_1[i+1], j, v_2[j+1], v_2[j+2]);
				hit = 1;
				break;
			}
			else
			{
				j += 2;
			}
		}
	}

	return hit;
}
/**********************************************/
void OutputIntVec( vector<int> &v_1)
{
	int limit = (int)v_1.size();
	L("CO");
	for( int i =0; i < limit; i++)
	{
		L("\t%d", v_1[i]);
	}
	L("\n");
}
/**********************************************/
/***   Original Prediction Conversion Part  ***/
/**********************************************/

/**********************************************/
void ParseTopHatFusion( char *fm_file, char *out_file, map<string, GENEPosition> &PTMap, map<string, vector<string> > &gene_table, map<string, string> &ensg_table, int read_length)
{
	vector<string> token_array;

	string strand;
	string chr1, chr2,
		gene1, gene2,
		g_in1, g_in2, // in case gene id was reported instead of gene name
		trans1, trans2,
		trans_name1, trans_name2,
		strand1, strand2;
	string current_fusion,
			new_fusion;
	vector<string> vec_trans1, vec_trans2;
	map<string, string>::iterator t1_it, t2_it;
	map<string, vector<string> >::iterator t_it;
	map<string, GENEPosition>::iterator p1_it, p2_it;
	vector<ExonPosition> vec_ep1, vec_ep2;
	vector<int> end_point;

	string check_ori;

	int pos1=0, pos2=0;
	int h1, h2, // if gene_id exist
		t1, t2; // if we find compatible transcript
	int ghit1, ghit2, thit1, thit2;
	int count = 1;
	//int dir_flag = 0;	//int exon1 = -1, exon2= -1;
	int limit, i;
	FILE *out_fp= fopen(out_file, "w");
	FILE *fp = fopen(fm_file, "r");
	char *readline  = (char*)malloc(MAX_LINE);
	while( NULL != fgets(readline, MAX_LINE, fp) )
	{
		token_array = splitString(readline, '\t');
		chr1 = token_array[2];
		chr2 = token_array[5];
		gene1 = token_array[1];
		gene2= token_array[4];
		//trans_name1 = token_array[1];
		//trans_name2 = token_array[8];
		// Does TopHat-Fusion work in 0-based coordinate system?
		pos1 = atoi(token_array[3].c_str());
		pos2 = atoi(token_array[6].c_str());
		//strand1 = token_array[3];
		//strand2 = token_array[10];
		// Strategy 1: Find the first COMPATIBLE transcript
		L("Read %s", readline );
		h1 = 1;
		h2 = 1;
		ghit1=0;
		ghit2=0;
		g_in1 = gene1;
		if ( 0 == strncmp( gene1.c_str(), "ENSG", 4 ) )	// reported via gene_id
		{
			t1_it = ensg_table.find(gene1);
			if ( ensg_table.end() == t1_it )
			{
				L("WRONG GENE1 ID\t%s\n", gene1.c_str());
				h1 = 0;	//exit (EXIT_FAILURE);
			}
			else
			{	g_in1 = t1_it -> second;	}
		}
		if ( 1 == h1 )
		{
			t_it = gene_table.find(g_in1);
			if ( gene_table.end() == t_it)
			{
				L("WRONG GENE1 NAME\t%s\n", g_in1.c_str());
				//exit (EXIT_FAILURE);
			}else
			{
				vec_trans1 = t_it->second; // locate transcript
				ghit1=1;
			}
		}

		g_in2 = gene2;
		if ( 0 == strncmp( gene2.c_str(), "ENSG", 4 ) )	// report by gene_id in TopHat-fusion
		{
			t2_it = ensg_table.find(gene2);
			if ( ensg_table.end() == t2_it )
			{
				L("WRONG GENE2 ID\t%s\n", gene2.c_str());
				h2 = 0;	//exit (EXIT_FAILURE);
			}
			else
			{	g_in2 = t2_it -> second;	}
		}
		if ( 1 == h2 )
		{
			t_it = gene_table.find(g_in2);
			if ( gene_table.end() == t_it)
			{
				L("WRONG GENE2 NAME\t%s\n", g_in2.c_str());
				//exit (EXIT_FAILURE);
			}
			else
			{
				vec_trans2 = t_it->second;
				ghit2 =1;
			}
		}

		// Transcript Level Searching
		thit1 = 0;
		thit2 = 0;

		if (  1 == ghit1 )
		{
			//L("Candidates\t%d\t%d\n", (int)vec_trans1.size(), (int)vec_trans2.size() );
			t1 = locate_trans( trans1, strand1, vec_trans1, PTMap, chr1, pos1 ); // 1 means there exists compatible transcripts
			if ( 1 == t1 )	{	thit1 = 1;	}
			else{	L("MISSING T1 of %s for %d\n", g_in1.c_str(), pos1); }
		}
		if ( 1 == ghit2 )
		{
			t2 = locate_trans( trans2, strand2, vec_trans2, PTMap, chr2, pos2 ); // 1 means there exists compatible transcript
			if ( 1 == t2 ) {	thit2 = 1;	}
			else{ L("MISSING T2 of %s for %d\n", g_in2.c_str(), pos2); }
		}
		L("Summary\t%s-%d\t%d%d%d%d\n", token_array[0].c_str(), count, ghit1, thit1, ghit2, thit2 );
		// 5' gene
		if ( 1 == thit1 )
		{
			p1_it = PTMap.find(trans1);
			if ( '+' == strand1[0]) //go to smaller coordinate
			{	locate_segment(trans1, pos1, read_length, p1_it->second, end_point, 0);	//L("P_1+\n");
			}
			else
			{	locate_segment(trans1, pos1, read_length, p1_it->second, end_point, 1);	//L("P_1-\n");
			}
		}
		else
		{
			if ( 0 == ghit1) {check_strand( gene1, strand1 ); }
			if ( '+' == strand1[0]) //go to smaller coordinate
			{	approx_segment( pos1, read_length, end_point, 0);	//L("P_1+\n");
			}
			else
			{	approx_segment( pos1, read_length, end_point, 1);	//L("P_1+\n");
			}

		}
		// output 5' gene
		limit = (int)end_point.size();
		if (  '+' == strand1[0])
		{
			i = limit - 1;
			while( 0 <= i )
			{   fprintf(out_fp, "%s-%d\t0\t%s\t+\t%d\t%d\n", token_array[0].c_str(), count, chr1.c_str(), end_point[i], end_point[i-1] );
			    i -= 2;
			}L("O_1+\n");
		}
		else
		{
			i = 0;
			while( i < limit )
			{   fprintf(out_fp, "%s-%d\t0\t%s\t+\t%d\t%d\n", token_array[0].c_str(), count, chr1.c_str(), end_point[i], end_point[i+1] );
			    i += 2;
			}L("O_1-\n");
		}

		// 3' gene
		if ( 1 == thit2 )
		{
			p2_it = PTMap.find(trans2);
			if ( '+' == strand2[0]) //go to smaller coordinate
			{	locate_segment(trans2, pos2, read_length, p2_it->second, end_point, 1);	//L("P_2+\n");
			}
			else
			{	locate_segment(trans2, pos2, read_length, p2_it->second, end_point, 0);	//L("P_2-\n");
			}
		}
		else
		{
			if ( 0 == ghit2) {check_strand( gene2, strand2 ); }
			if ( '+' == strand1[0]) //go to smaller coordinate
			{	approx_segment( pos2, read_length, end_point, 1);	//L("P_1+\n");
			}
			else
			{	approx_segment( pos2, read_length, end_point, 0);	//L("P_1+\n");
			}

		}
		// output 3' gene
		limit = (int)end_point.size();
		if ( '+' == strand[0] )
		{
			i = 0;
			while( i < limit )
			{   fprintf(out_fp, "%s-%d\t1\t%s\t+\t%d\t%d\n", token_array[0].c_str(), count, chr2.c_str(), end_point[i], end_point[i+1] );
			    i += 2;
			}L("O_2+\n");
		}
		else
		{
			i = limit - 1;
			while( 0 <= i )
			{   fprintf(out_fp, "%s-%d\t1\t%s\t+\t%d\t%d\n", token_array[0].c_str(), count, chr2.c_str(), end_point[i], end_point[i-1] );
			    i -= 2;
			}L("O_2-\n");
		}
		count++;
	}
	fclose(fp);
	fclose(out_fp);
}

/**********************************************/
// Hardcoding here: SANP25 and GUCD1 are reverse-stranded
void check_strand( string &gene_id, string &strand)
{
	strand[0]= '+';
	if ( ('S' == gene_id[0] ) || ( 'G' == gene_id[0]) )
	{
		strand[0]= '-';	//L("CHECK\t%s\t%s\n", gene_id.c_str(), strand.c_str());
	}//	{L("CHECK\t%s\t%s\n", gene_id.c_str(), strand.c_str() );}

}
/**********************************************/
int locate_trans( string &trans_id, string &strand,  vector<string> &vec_id, map<string, GENEPosition> &PTMap, string &chr_str, int pos )
{
	int limit=(int)vec_id.size();
	string t_id;
	GENEPosition tmp_gp;
	map<string, GENEPosition>::iterator pit;

	int hit=0, loc = -1;
	for( int i = 0; i < limit; i++ )
	{
		t_id = vec_id[i];
		pit = PTMap.find(t_id);
		tmp_gp = pit->second;
		strand[0] = tmp_gp.strand; // no matter if we have compatible transcript or not

		//L("NOW\t%d\t%d\t%d\t%s\t%s\t%s\n", pos, i, limit, t_id.c_str(), tmp_gp.chr_name.c_str(), chr_str.c_str() );
		if ( tmp_gp.chr_name == chr_str )
		{
			L("Checking %s of %d in %s\n", t_id.c_str(), (int)tmp_gp.epos.size(), chr_str.c_str() );
			loc = find_exon( pos, tmp_gp.epos);
			if ( 0 <= loc )
			{
				trans_id = vec_id[i];
				//strand[0] = tmp_gp.strand;
				hit = 1;
				break; // got a compatible transcript
			}
		}

	}
	return hit;
}

/**********************************************/
void approx_segment( int pos, int read_length, vector<int> &end_point, int direction)
{	//direction: 0 for going to leftside, 1 for going to rightside
	end_point.clear();
	if ( 1 == direction )
	{
		end_point.push_back( pos );
		end_point.push_back( pos + read_length -1 );
		L("APP\t%d\t%d\t%d\n", direction, end_point[0], end_point[1]);
	}
	else
	{
		end_point.push_back( pos );
		end_point.push_back( pos - read_length + 1 );
		L("APP\t%d\t%d\t%d\n", direction, end_point[1], end_point[0]);
	}
}
/**********************************************/
void ParseSOAPfuse( char *fm_file, char *out_file, map<string, GENEPosition> &PTMap, map<string, vector<string> > &gene_table, map<string, string> &enst_table, int read_length)
{
	vector<string> token_array;

	string strand;
	string chr1, chr2,
		gene1, gene2,
		trans1, trans2,
		trans_name1, trans_name2,
		strand1, strand2;
	string current_fusion,
			new_fusion;
	//vector<string> vec_trans1, vec_trans2;
	map<string, string>::iterator t1_it, t2_it;
	map<string, vector<string> >::iterator t_it;
	map<string, GENEPosition>::iterator p1_it, p2_it;
	vector<ExonPosition> vec_ep1, vec_ep2;
	vector<int> end_point;

	string check_ori;

	int pos1=0, pos2=0;
	int count = 1;
	//int dir_flag = 0;	//int exon1 = -1, exon2= -1;
	int limit, i;
	FILE *out_fp= fopen(out_file, "w");
	FILE *fp = fopen(fm_file, "r");
	char *readline  = (char*)malloc(MAX_LINE);
	while( NULL != fgets(readline, MAX_LINE, fp) )
	{
		if ( 0 == strncmp(readline, "up_", 3) )	// first entry of header: "up_gene" in *.trans
			{continue;}
		token_array = splitString(readline, '\t');
		//rand = token_array[4];
		chr1 = token_array[2];
		chr2 = token_array[9];
		gene1 = token_array[0];
		gene2= token_array[7];
		trans_name1 = token_array[1];
		trans_name2 = token_array[8];
		pos1 = atoi(token_array[5].c_str());
		pos2 = atoi(token_array[12].c_str());
		strand1 = token_array[3];
		strand2 = token_array[10];
		//new_fusion = chr1 + "_" + token_array[5] + "_" + chr2 + "_" + token_array[12];
		new_fusion = gene1 + "_" + token_array[5] + "_" + gene2 + "_" + token_array[12];
		if ( new_fusion != current_fusion )
		{
			L("New Record:%d %s %s \n%s", count, current_fusion.c_str() , new_fusion.c_str(), readline);
			t1_it = enst_table.find(trans_name1);
			if ( enst_table.end() == t1_it)
			{ L("Error in Trans1_Name:%s\n", trans1.c_str());}
			trans1 = t1_it->second;
			t2_it = enst_table.find(trans_name2);
			if ( enst_table.end() == t2_it)
			{ L("Error in Trans2_Name:%s\n", trans2.c_str());}
			trans2 = t2_it->second;

			p1_it = PTMap.find(trans1);
			p2_it = PTMap.find(trans2);
			if ( PTMap.end() == p1_it || PTMap.end() == p2_it)
			{
				E("Error in %s: cannot locate transcript %s or %s\n", token_array[0].c_str(), trans1.c_str(), trans2.c_str());
				continue;
			}
			// 5' gene
			//if ( 0 == strcmp( '+', strand[0]) ) // go to left
			if ( "+" == strand1 ) // go to smaller coor
			{
				locate_segment(trans1, pos1, read_length, p1_it->second, end_point, 0);
				limit = (int)end_point.size();
				i = limit-1;
				while(0 <= i)
				{
					fprintf(out_fp, "%s\t0\t%s\t+\t%d\t%d\n", new_fusion.c_str(), chr1.c_str(), end_point[i], end_point[i-1] );
					i -= 2;
				}

			}
			else
			{
				locate_segment(trans1, pos1, read_length, p1_it->second, end_point, 1);
				limit = (int)end_point.size();
				i = 0;
				while( i < limit )
				{
					fprintf(out_fp, "%s\t0\t%s\t-\t%d\t%d\n", new_fusion.c_str(), chr1.c_str(), end_point[i], end_point[i+1] );
					i += 2;
				}
			}
			// 3' gene
			//if ( 0 == strcmp( '+', strand[1] ))
			if ( "+"== strand2 ) // go to larger coor
			{
				locate_segment(trans2, pos2, read_length, p2_it->second, end_point, 1);
				limit = (int)end_point.size();
				i = 0;
				while( i < limit )
				{
					fprintf(out_fp, "%s\t1\t%s\t-\t%d\t%d\n", new_fusion.c_str(), chr2.c_str(), end_point[i], end_point[i+1] );
					i += 2;
				}
			}
			else
			{
				locate_segment(trans2, pos2, read_length, p2_it->second, end_point, 0);
				limit = (int)end_point.size();
				i = limit-1;
				while(0 <= i)
				{
					fprintf(out_fp, "%s\t1\t%s\t+\t%d\t%d\n", new_fusion.c_str(), chr2.c_str(), end_point[i], end_point[i-1] );
					i -= 2;
				}
			}


			current_fusion = new_fusion;
		}

		//check_ori = gene1+"->"+gene2;
		//if (check_ori != token_array[18])
		//{
		//	E("Warning: Wrong Orientation\t%sn", token_array[0].c_str());
		//}

		//trans1 = vec_trans1[0];
		//trans2 = vec_trans2[0];

		//p1_it = PTMap.find(trans1);
		//p2_it = PTMap.find(trans2);
		//if ( PTMap.end() == p1_it || PTMap.end() == p2_it)
		//{
		//	E("Error in %s: cannot locate transcript %s or %s\n", token_array[0].c_str(), trans1.c_str(), trans2.c_str());
		//	continue;
		//}
		//// 5' gene
		////if ( 0 == strcmp( '+', strand[0]) ) // go to left
		//if ( '+' == strand[0] ) // go to smaller coor
		//{
		//	locate_segment(trans1, pos1, read_length, p1_it->second, end_point, 0);
		//	limit = (int)end_point.size();
		//	i = limit-1;
		//	while(0 <= i)
		//	{
		//		fprintf(out_fp, "%s\t0\t%s\t+\t%d\t%d\n", token_array[0].c_str(), chr1.c_str(), end_point[i], end_point[i-1] );
		//		i -= 2;
		//	}

		//}
		//else
		//{
		//	locate_segment(trans1, pos1, read_length, p1_it->second, end_point, 1);
		//	limit = (int)end_point.size();
		//	i = 0;
		//	while( i < limit )
		//	{
		//		fprintf(out_fp, "%s\t0\t%s\t-\t%d\t%d\n", token_array[0].c_str(), chr1.c_str(), end_point[i], end_point[i+1] );
		//		i += 2;
		//	}
		//}
		//// 3' gene
		////if ( 0 == strcmp( '+', strand[1] ))
		//if ( '+'== strand[1] ) // go to larger coor
		//{
		//	locate_segment(trans2, pos2, read_length, p2_it->second, end_point, 1);
		//	i = 0;
		//	while( i < limit )
		//	{
		//		fprintf(out_fp, "%s\t1\t%s\t+\t%d\t%d\n", token_array[0].c_str(), chr2.c_str(), end_point[i], end_point[i+1] );
		//		i += 2;
		//	}
		//}
		//else
		//{
		//	locate_segment(trans2, pos2, read_length, p2_it->second, end_point, 0);
		//	limit = (int)end_point.size();
		//	i = limit-1;
		//	while(0 <= i)
		//	{
		//		fprintf(out_fp, "%s\t1\t%s\t+\t%d\t%d\n", token_array[0].c_str(), chr2.c_str(), end_point[i], end_point[i-1] );
		//		i -= 2;
		//	}
		//}


		// Sanity Checking
		//t_it = gene_table.find(gene1);
		//if (gene_table.end() != t_it)
		//{
		//	if ( vec_trans1.size() != t_it->second.size() )
		//	{
		//		E("Warning: Inconsist Trans Num for %s in %s:%d\t%d\n",gene1.c_str(),  token_array[0].c_str(), (int)vec_trans1.size(),  (int)t_it->second.size());
		//	}
		//}
		//else
		//{
		//	E("Warning: Gene Missing:%s\n", gene1.c_str());
		//}
		//E("Num_Entry\t%d\n", (int)token_array.size());
		count++;
	}
	fclose(fp);
	fclose(out_fp);
}
/**********************************************/
void ParseFusionMap( char *fm_file, char *out_file, map<string, GENEPosition> &PTMap, map<string, vector<string> > &gene_table , int read_length)
{
	vector<string> token_array;

	string strand;
	string chr1, chr2,
		gene1, gene2,
		trans1, trans2;
	vector<string> vec_trans1, vec_trans2;
	map<string, vector<string> >::iterator t_it;
	map<string, GENEPosition>::iterator p1_it, p2_it;
	vector<ExonPosition> vec_ep1, vec_ep2;
	vector<int> end_point;

	string check_ori;
	int pos1=0, pos2=0;
	//int dir_flag = 0;	//int exon1 = -1, exon2= -1;
	int limit=0, i=0;
	FILE *out_fp= fopen(out_file, "w");
	FILE *fp = fopen(fm_file, "r");
	char *readline  = (char*)malloc(MAX_LINE);
	while( NULL != fgets(readline, MAX_LINE, fp) )
	{
		if ( 0 == strncmp(readline, "Fus", 3) )
			{continue;}
		token_array = splitString(readline, '\t');
		strand = token_array[4];
		chr1 = token_array[5];
		chr2 = token_array[7];
		gene1 = token_array[9];
		gene2= token_array[13];
		vec_trans1 = splitStringOld( token_array[10], ',');
		vec_trans2 = splitStringOld( token_array[14], ',');
		pos1 = atoi(token_array[6].c_str());
		pos2 = atoi(token_array[8].c_str());
		check_ori = gene1+"->"+gene2;
		if (check_ori != token_array[18])
		{
			E("Warning: Wrong Orientation\t%sn", token_array[0].c_str());
		}

		trans1 = vec_trans1[0];
		trans2 = vec_trans2[0];

		p1_it = PTMap.find(trans1);
		p2_it = PTMap.find(trans2);
		if ( PTMap.end() == p1_it || PTMap.end() == p2_it)
		{
			E("Error in %s: cannot locate transcript %s or %s\n", token_array[0].c_str(), trans1.c_str(), trans2.c_str());
			continue;
		}
		// 5' gene
		//if ( 0 == strcmp( '+', strand[0]) ) // go to left
		if ( '+' == strand[0] ) // go to smaller coor
		{
			locate_segment(trans1, pos1, read_length, p1_it->second, end_point, 0);
			limit = (int)end_point.size();
			i = limit-1;
			while(0 <= i)
			{
				fprintf(out_fp, "%s\t0\t%s\t+\t%d\t%d\n", token_array[0].c_str(), chr1.c_str(), end_point[i], end_point[i-1] );
				i -= 2;
			}

		}
		else if ('-' == strand[0])
		{
			locate_segment(trans1, pos1, read_length, p1_it->second, end_point, 1);
			limit = (int)end_point.size();
			i = 0;
			while( i < limit )
			{
				fprintf(out_fp, "%s\t0\t%s\t-\t%d\t%d\n", token_array[0].c_str(), chr1.c_str(), end_point[i], end_point[i+1] );
				i += 2;
			}
		}
		else
		{
			E("WrongStrand:%s\n", token_array[0].c_str());
		}
		// 3' gene
		//if ( 0 == strcmp( '+', strand[1] ))
		if ( '+'== strand[1] ) // go to larger coor
		{
			locate_segment(trans2, pos2, read_length, p2_it->second, end_point, 1);
			limit = (int)end_point.size();
			i = 0;
			while( i < limit )
			{
				fprintf(out_fp, "%s\t1\t%s\t-\t%d\t%d\n", token_array[0].c_str(), chr2.c_str(), end_point[i], end_point[i+1] );
				i += 2;
			}
		}
		else if ('-'==strand[1])
		{
			locate_segment(trans2, pos2, read_length, p2_it->second, end_point, 0);
			limit = (int)end_point.size();
			i = limit-1;
			while(0 <= i)
			{
				fprintf(out_fp, "%s\t1\t%s\t+\t%d\t%d\n", token_array[0].c_str(), chr2.c_str(), end_point[i], end_point[i-1] );
				i -= 2;
			}
		}
		else
		{
			E("WrongStrand:%s\n", token_array[0].c_str());
		}


		// Sanity Checking
		//t_it = gene_table.find(gene1);
		//if (gene_table.end() != t_it)
		//{
		//	if ( vec_trans1.size() != t_it->second.size() )
		//	{
		//		E("Warning: Inconsist Trans Num for %s in %s:%d\t%d\n",gene1.c_str(),  token_array[0].c_str(), (int)vec_trans1.size(),  (int)t_it->second.size());
		//	}
		//}
		//else
		//{
		//	E("Warning: Gene Missing:%s\n", gene1.c_str());
		//}
		//E("Num_Entry\t%d\n", (int)token_array.size());

	}
	fclose(fp);
	fclose(out_fp);
}



// Determine the segments upstream
// TODO: check if it make sense for ext_length > 0
void locate_segment( string &trans, int pos, int read_length, GENEPosition &gp, vector<int> &end_point, int direction)
{
	// direction: 0 for going left, 1 for going right
	end_point.clear();

	vector<ExonPosition> vec_ep = gp.epos;
	L("Exon Num for %s is %d\n", trans.c_str(), (int)gp.epos.size());
	int e_i = find_exon( pos, vec_ep );
	if ( -1 == e_i)
	{
		E("Cannot Find Exon for %d in %s\n", pos, trans.c_str());
		exit(EXIT_FAILURE);
	}
	//int l_diff = pos-vec_ep[e_i].start;
	//int r_diff = vec_ep[e_i].end-pos;
	int ext_length = read_length;
	int tmp_ext = 0;
	int limit = (int)vec_ep.size();
	L("S:\t%d in %d of %s:%d %d\n", pos, e_i, trans.c_str(), vec_ep[e_i].start, vec_ep[e_i].end);
	//if ( (read_length >l_diff) || (read_length>r_diff))
	//{
	//	L("D:\t%d in %d of %s:%d %d\n", pos, e_i, trans.c_str(), vec_ep[e_i].start, vec_ep[e_i].end);
	//}
	int cur_i = e_i; // index of splitting exon
	int cur_c = 0; // counter of STEP
	// go forward rl
	int l_end, r_end;
	if ( 1 == direction )
	{
		l_end = pos;
		//while( 0 < ext_length ) // need to extend so many bases
		while( (0 < ext_length) && (cur_i < limit) ) // need to extend so many bases
		{
			r_end = l_end + ext_length - 1;
			if ( r_end <= vec_ep[cur_i].end ) // end in this exon
			{
				ext_length = 0;
			}
			else
			{
				r_end = vec_ep[cur_i].end;
				tmp_ext = r_end - l_end + 1;
				ext_length -= tmp_ext;
			}
			end_point.push_back(l_end);
			end_point.push_back(r_end);
			L("Add %d_%d: %d %d from %d %d; %d to extend\n", direction,cur_c, l_end, r_end, vec_ep[cur_i].start, vec_ep[cur_i].end, ext_length);
			cur_i++;
			cur_c++;
			l_end = vec_ep[cur_i].start;
		}

		if ( 0 < ext_length){L("NotEnd %s %d %d: ends in %d %d; %d to extend\n", trans.c_str(), pos, direction, l_end, r_end, ext_length);}

	}
	else // go upstream; coordinates will be in decreasing/REVERSED order
	{
		r_end = pos;
		//while( 0 < ext_length ) //still need to decrease so many bases
		while( ( 0 < ext_length) && ( 0 <= cur_i ) ) //still need to decrease so many bases
		{
			l_end = r_end - ext_length + 1;
			if ( vec_ep[cur_i].start <= l_end) // end in this exon
			{
				ext_length = 0;
			}
			else
			{
				l_end = vec_ep[cur_i].start;
				tmp_ext = r_end - l_end + 1;
				ext_length -= tmp_ext;
			}
			//end_point.push_back(l_end);
			end_point.push_back(r_end);
			end_point.push_back(l_end);
			L("Add %d_%d: %d %d from %d %d; %d to extend\n", direction,cur_c, l_end, r_end, vec_ep[cur_i].start, vec_ep[cur_i].end, ext_length);
			cur_i--; // To fit the structure in gtf reader
			cur_c++;
			r_end = vec_ep[cur_i].end;
		}
		if ( 0 < ext_length){L("NotEnd %s %d %d: ends in %d %d; %d to extend\n", trans.c_str(), pos, direction, l_end, r_end, ext_length);}
	}

}
/**********************************************/
// Determine the exon containing breakpoint
int find_exon( int pos, vector<ExonPosition> &vec_ep )
{
	int limit = (int)vec_ep.size();
	int i = 0;
	int hit = -1;
	while( i < limit)
	{
		if ( (vec_ep[i].start <= pos) && ( pos <= vec_ep[i].end))
		{
			hit = i;
		}
		++i;
	}
	return hit;
}


/**********************************************/
/***   For Integrating VCF and Alignment Files ***/

/**********************************************/
void clear_alt( unit_alt &t_alt)
{
	t_alt.status = 1;
	t_alt.bp_var = 0; t_alt.l_var = 0; t_alt.r_var = 0;
	t_alt.bp_s = -1; t_alt.bp_e = -1; 
	t_alt.al_s = -1; t_alt.al_e = -1;
	t_alt.rp_s = -1; t_alt.rp_e = -1;
	t_alt.anchor = -1;
	t_alt.v_ct = "";
	t_alt.l_s = 0; t_alt.l_e = 0; t_alt.l_ref = ""; t_alt.l_ct = "";
	t_alt.r_s = 0; t_alt.r_e = 0; t_alt.r_ref = ""; t_alt.r_ct = "";
}

/**********************************************/
void get_cntstring( string &cnt, size_t t_limit)
{
	cnt.clear(); cnt.reserve(t_limit);
	int limit = (int)t_limit;
	for (int i=1; i<=limit; i++)
	{
		if (i%10==0)
			cnt+='0';
		else if(i%5==0)
			cnt+='5';
		else
			cnt+=' ';
	}
}

/**********************************************/
// Get corresponding string of src[s:e-1] to dest without dash
// 0-based, end-exclusive!!!
void get_alignstr( const string &src, string &dest, int s, int e)
{
	dest.clear();
	if ( (e > s) && (e <= (int)src.size() ) )
	{
		int limit = e - s;
		dest.reserve(limit);
		for(int i=s; i <e; i++)
		{
			if ( '-' != src[i]){ dest.push_back( src[i]);}
		}
	}
}
/**********************************************/
void get_rawstr( const string &src, string &dest, int s, int e)
{
	dest.clear();
	if ( (e > s) && (e <= (int)src.size() ) )
	{
		int limit = e - s;
		dest.reserve(limit);
		for(int i=s; i <e; i++)
		{
			dest.push_back( src[i]);
		}
	}
}
/**********************************************/
void get_cleanstr( const string &src, string &dest )
{
	dest.clear();
	int limit = (int)src.size();
	dest.reserve(limit);
	for(int i=0; i < limit; i++)
	{
		if ( ( '-' != src[i]) && (' ' != src[i]) )
		{ dest.push_back( src[i]);}
	}
}
/**********************************************/
int detect_anchor( const string &align_str, int bp_s, int bp_e)
{
	int flag = -1; // anchor on the left
	int limit = (int)align_str.size();
	int i = 0, num_bar = 0;
	for( i = 0; i <= bp_s; i++ )
	{
		if('|' == align_str[i]){num_bar++;}else{ num_bar = 0;}
		if ( 22 <= num_bar ){ flag = 0; break;}
	}
	if ( -1 == flag)
	{
		for( i = bp_e; i < limit; i++)
		{
			if('|' == align_str[i]){num_bar++;}else{ num_bar = 0;}
			if ( 22 <= num_bar ){ flag = 1; break;}
		}
	}

	return flag;
}

/**********************************************/
int update_anchor_from_ref0( unit_alt &t_alt, const unit_vcf &t_vcf, const vector<string> &vec_str, const string  &chr_str, FILE *log)
{
	fprintf(log, " + anchor side : %d\n", t_alt.anchor);

	// Get real anchor from genome
	string segment;
	size_t found;

	if ( 1 == t_alt.anchor )
	{
		if ( 15 > (int)t_alt.r_ref.size())
		{
			fprintf( log, "Conflict Right Anchor\n");
		}
		else
		{
			segment = chr_str.substr( t_vcf.e, SEG_LENGTH);
			found = t_alt.r_ref.find(segment);
			if ( found == string::npos)
			{
				fprintf(log, " + No RSegment : %u\t%s\n", t_vcf.e, segment.c_str() );
			}
		}
	}
	else if ( 0 == t_alt.anchor )
	{
		if ( 15 > (int)t_alt.l_ref.size())
		{
			fprintf( log, "Conflict Left Anchor\n");
		}
		else
		{
			segment = chr_str.substr( t_vcf.s-1-SEG_LENGTH, SEG_LENGTH);
			found = t_alt.l_ref.find(segment);
			if ( found == string::npos)
			{
				fprintf(log, " + No LSegment : %u\t%s\n", t_vcf.s-1-SEG_LENGTH, segment.c_str() );
			}
		}
	}
}
/**********************************************/
int update_anchor_from_ref1( unit_alt &t_alt, const unit_vcf &t_vcf, const vector<string> &vec_str, const string  &chr_str, FILE *log)
{
	fprintf(log, " + anchor side : %d\n", t_alt.anchor);

	// Get real anchor from genome
	string segment;
	string new_ref;
	string t_str;
	size_t found;
	int bp_s, bp_e, al_s, al_e, raw_shift, shift;
	int l_ref, l_ct;
	uint32_t new_bp_s, new_bp_e;
	if ( 1 == t_alt.anchor )
	{
		bp_s = 1;
	//	bp_e = t_alt.bp_e + t_alt.bp_s - 1;
		raw_shift = t_alt.bp_s - 0;
		get_alignstr( vec_str[1], t_str, 0, t_alt.bp_s );
		l_ref = (int)t_str.size();
		get_alignstr( vec_str[3], t_str, 0, t_alt.bp_s );
		l_ct  = (int)t_str.size();
		shift = ( l_ref > l_ct )? l_ref : l_ct ;
		bp_e = t_alt.bp_e + shift - 1;

		fprintf(log, " + Raw Exten   : %d\n", raw_shift );
		fprintf(log, " + Extension   : %d\n", shift );
		if ( raw_shift < shift){fprintf(log, "Kidding\n");}
		if ( bp_e > (int)vec_str[1].size() )
		{
			fprintf(log, " + Error: %d Exceeds Alignment Boundary %d\n", bp_e, (int)vec_str[1].size() );
			return 0;
		}
		new_bp_e = t_vcf.e + shift;
		fprintf(log, " + New bp start: %u\n", t_alt.bp_s - shift );
		fprintf(log, " + New bp end  : %u\n", t_alt.bp_e + shift);
		fprintf(log, " + New Genomic : %u\t%u\n", t_vcf.s - shift, t_vcf.e + shift );
		segment = chr_str.substr( new_bp_e, SEG_LENGTH);
		get_alignstr( vec_str[1], new_ref, bp_e, t_alt.al_e + 1);
		fprintf(log, " + New RightRef: %s\n", new_ref.c_str() );
		found = new_ref.find(segment);
		if ( found == string::npos )
		{
			fprintf(log, " + New RFlank  : %s\n", segment.c_str() );
			fprintf(log, " + No RSegment\n" );
		}
		else
		{	
			string t_cnt;
			for(int i=0;i<(int)found;i++){t_cnt.push_back(' ');}
			fprintf(log, " + New RFlank  : %s%s\n", t_cnt.c_str(), segment.c_str() );
			fprintf(log, " + DIFF        : %lu\n", found);
		}
	}
	else if ( 0 == t_alt.anchor )
	{
		bp_e = (int)vec_str[1].size();
		//shift = bp_e - t_alt.bp_e;
		//bp_s = t_alt.bp_s - ( shift);
		raw_shift = bp_e - 1 - t_alt.bp_e;

		get_alignstr( vec_str[1], t_str, t_alt.bp_e+1, bp_e);
		l_ref = (int)t_str.size();
		get_alignstr( vec_str[3], t_str, t_alt.bp_e+1, bp_e );
		l_ct  = (int)t_str.size();
		shift = ( l_ref > l_ct )? l_ref : l_ct ;
		bp_s = t_alt.bp_s - ( shift);

		fprintf(log, " + Raw Exten   : %d\n", raw_shift );
		fprintf(log, " + Extension   : %d\n", shift );
		if ( raw_shift < shift){fprintf(log, "Kidding\n");}
		
		if ( 0 > bp_s )
		{
			fprintf(log, " + Error %d Exceeds Alignment Boundary 0\n", bp_s );
			return 0;
		}
		new_bp_s = t_vcf.s - shift;
		fprintf(log, " + New bp start: %u\n", t_alt.bp_s - shift );
		fprintf(log, " + New bp end  : %u\n", t_alt.bp_e + shift );
		fprintf(log, " + New Genomic : %u\t%u\n", t_vcf.s - shift, t_vcf.e + shift );
		segment = chr_str.substr( new_bp_s - 1 - SEG_LENGTH, SEG_LENGTH);
		get_alignstr( vec_str[1], new_ref, t_alt.al_s, bp_s );
		fprintf(log, " + New LeftRef : %s\n", new_ref.c_str() );

		found = new_ref.find(segment);
		if ( found == string::npos )
		{
			fprintf(log, " + New LFlank  : %s\n", segment.c_str() );
			fprintf(log, " + No LSegment\n" );
		}
		else
		{
			string t_cnt;
			for(int i=0;i<(int)found;i++){t_cnt.push_back(' ');}
			fprintf(log, " + New LFlank  : %s%s\n", t_cnt.c_str(), segment.c_str() );
			fprintf(log, " + DIFF        : %lu\n", new_ref.size() - found - SEG_LENGTH);
		}
	}
}
/**********************************************/
int update_anchor_from_ref2( unit_alt &t_alt, const unit_vcf &t_vcf, const vector<string> &vec_str, const string  &chr_str, FILE *log)
{
	int clean_case = clean_alignment( t_alt, vec_str  ); // 1 for no space in effective regions. 2 for no space in whole alignment. 0 otherwise.
	fprintf(log, " + anchor side : %d\n", t_alt.anchor);
	fprintf(log, " + clean       : %d\n", clean_case  );

	int ori_diff = 0, new_diff = 0;
	// Get real anchor from genome
	string segment;
	string new_ref;
	string t_str;
	size_t found;
	int bp_s, bp_e, al_s, al_e, raw_shift, shift;
	int l_ref, l_ct;
	uint32_t new_bp_s, new_bp_e;
	// Inversions before Anchor 
	if ( 1 == t_alt.anchor )
	{
		if ( 2 == clean_case )
		{
			segment = chr_str.substr( t_alt.bp_e, SEG_LENGTH);
			get_alignstr( vec_str[1], new_ref, bp_e, t_alt.al_e + 1);
			fprintf(log, " * New RightRef: %s\n", new_ref.c_str() );
			found = new_ref.find(segment);
			if ( found == string::npos )
			{
				fprintf(log, " * New RFlank  : %s\n", segment.c_str() );
				fprintf(log, " * No RSegment\n" );
				ori_diff = -1;
			}
			else
			{	
				string t_cnt;
				for(int i=0;i<(int)found;i++){t_cnt.push_back(' ');}
				fprintf(log, " * New RFlank  : %s%s\n", t_cnt.c_str(), segment.c_str() );
				fprintf(log, " * DIFF        : %lu\n", found);
				ori_diff = (int)found;
			}			
		}

		bp_s = 1;
	//	bp_e = t_alt.bp_e + t_alt.bp_s - 1;
		raw_shift = t_alt.bp_s - 0;
		get_alignstr( vec_str[1], t_str, 0, t_alt.bp_s );
		l_ref = (int)t_str.size();
		get_alignstr( vec_str[3], t_str, 0, t_alt.bp_s );
		l_ct  = (int)t_str.size();
		shift = ( l_ref > l_ct )? l_ref : l_ct ;
		bp_e = t_alt.bp_e + shift - 1;

		fprintf(log, " + Raw Exten   : %d\n", raw_shift );
		fprintf(log, " + Extension   : %d\n", shift );
		if ( raw_shift < shift){fprintf(log, "Kidding\n");}
		if ( bp_e > (int)vec_str[1].size() )
		{
			fprintf(log, " + Error: %d Exceeds Alignment Boundary %d\n", bp_e, (int)vec_str[1].size() );
			return 0;
		}
		new_bp_e = t_vcf.e + shift;
		fprintf(log, " + New bp start: %u\n", t_alt.bp_s - shift );
		fprintf(log, " + New bp end  : %u\n", t_alt.bp_e + shift);
		fprintf(log, " + New Genomic : %u\t%u\n", t_vcf.s - shift, t_vcf.e + shift );
		segment = chr_str.substr( new_bp_e, SEG_LENGTH);
		get_alignstr( vec_str[1], new_ref, bp_e, t_alt.al_e + 1);
		fprintf(log, " + New RightRef: %s\n", new_ref.c_str() );
		found = new_ref.find(segment);
		if ( found == string::npos )
		{
			fprintf(log, " + New RFlank  : %s\n", segment.c_str() );
			fprintf(log, " + No RSegment\n" );
			new_diff = -1;
		}
		else
		{	
			string t_cnt;
			for(int i=0;i<(int)found;i++){t_cnt.push_back(' ');}
			fprintf(log, " + New RFlank  : %s%s\n", t_cnt.c_str(), segment.c_str() );
			fprintf(log, " + DIFF        : %lu\n", found);
			new_diff = (int)found;
		}
	}
	else if ( 0 == t_alt.anchor )
	{
		if ( 2 == clean_case )
		{
			segment = chr_str.substr( t_alt.bp_s - 1 - SEG_LENGTH, SEG_LENGTH);
			get_alignstr( vec_str[1], new_ref, t_alt.al_s, bp_s );
			fprintf(log, " * New LeftRef : %s\n", new_ref.c_str() );

			found = new_ref.find(segment);
			if ( found == string::npos )
			{
				fprintf(log, " * New LFlank  : %s\n", segment.c_str() );
				fprintf(log, " * No LSegment\n" );
				ori_diff = -1;
			}
			else
			{
				string t_cnt;
				for(int i=0;i<(int)found;i++){t_cnt.push_back(' ');}
				fprintf(log, " * New LFlank  : %s%s\n", t_cnt.c_str(), segment.c_str() );
				fprintf(log, " * DIFF        : %lu\n", new_ref.size() - found - SEG_LENGTH);
				ori_diff = (int)found;
			}
		}
		bp_e = (int)vec_str[1].size();
		//shift = bp_e - t_alt.bp_e;
		//bp_s = t_alt.bp_s - ( shift);
		raw_shift = bp_e - 1 - t_alt.bp_e;

		get_alignstr( vec_str[1], t_str, t_alt.bp_e+1, bp_e);
		l_ref = (int)t_str.size();
		get_alignstr( vec_str[3], t_str, t_alt.bp_e+1, bp_e );
		l_ct  = (int)t_str.size();
		shift = ( l_ref > l_ct )? l_ref : l_ct ;
		bp_s = t_alt.bp_s - ( shift);

		fprintf(log, " + Raw Exten   : %d\n", raw_shift );
		fprintf(log, " + Extension   : %d\n", shift );
		if ( raw_shift < shift){fprintf(log, "Kidding\n");}
		
		if ( 0 > bp_s )
		{
			fprintf(log, " + Error %d Exceeds Alignment Boundary 0\n", bp_s );
			return 0;
		}
		new_bp_s = t_vcf.s - shift;
		fprintf(log, " + New bp start: %u\n", t_alt.bp_s - shift );
		fprintf(log, " + New bp end  : %u\n", t_alt.bp_e + shift );
		fprintf(log, " + New Genomic : %u\t%u\n", t_vcf.s - shift, t_vcf.e + shift );
		segment = chr_str.substr( new_bp_s - 1 - SEG_LENGTH, SEG_LENGTH);
		get_alignstr( vec_str[1], new_ref, t_alt.al_s, bp_s );
		fprintf(log, " + New LeftRef : %s\n", new_ref.c_str() );

		found = new_ref.find(segment);
		if ( found == string::npos )
		{
			fprintf(log, " + New LFlank  : %s\n", segment.c_str() );
			fprintf(log, " + No LSegment\n" );
			new_diff = -1;
		}
		else
		{
			string t_cnt;
			for(int i=0;i<(int)found;i++){t_cnt.push_back(' ');}
			fprintf(log, " + New LFlank  : %s%s\n", t_cnt.c_str(), segment.c_str() );
			fprintf(log, " + DIFF        : %lu\n", new_ref.size() - found - SEG_LENGTH);
			new_diff = (int)found;
		}
	}
}
/**********************************************/
int clean_alignment( const unit_alt &t_alt, const vector<string> &vec_str )
{
	int flag = 1;
	int l = t_alt.al_s, r = t_alt.al_e;
	if ( t_alt.bp_s < l) l = t_alt.bp_s;
	if ( r < t_alt.bp_e ) r = t_alt.bp_e;
	for( int i = l; i<= r;i++)
	{
		if ( ' '==vec_str[2][i] )
		{ flag = 0; }
	}

	if ( ( 0 == l ) && ( vec_str[2].size() == r+1 ) && ( flag ) )
	{ flag =2; }
	return flag;
}
/**********************************************/
// -1: not found
int select_best_step( int d0, int d1, int d2, int d3, int &best_d)
{
	int flag = 0;
	int min = ( d0 > 0)? d0 : -1*d0 ;
	if ( -4 == d0 + d1 + d2 + d3 )
	{
		flag = -1;
		min = -1;
	}
	if ( (0<=d3) and ( d3<=min) )
	{	
		flag = 3;
		min = d3;
	}
	if ( (0<=d2) and ( d2<=min) )
	{	
		flag = 2;
		min = d2;
	}
	if ( (0<=d1) and ( d1<=min) )
	{	
		flag = 1;
		min = d1;
	}
	if ( (0<=d0) and ( d0<=min) )
	{	
		flag = 0;
		min = d0;
	}
	best_d = min;
	return flag;
}

// Resolve Issues of Breakpoint Differences in MiStrVar
// genomics start will be 1-based since it's from vcf. ref_s and ref_e are 0-based for get_alignstr.
// ref_e is EXCLUSIVE
/**********************************************/
int calculate_bpdiff( int gen_start, int ref_s, int ref_e, const string &chr_str, const string &local_ref, int dist_to_end )
{
	int diff = -1;

	string ref, segment;
	size_t found;
	get_alignstr( local_ref, ref, ref_s, ref_e );
	segment = chr_str.substr( gen_start - 1, SEG_LENGTH);
	//L("Check 1-based start position %d\n", gen_start);
	//L("Check %d %d\n", ref_s, ref_e);
	//L("dist_to_end\n", dist_to_end);
	//L("genomic %s\n", segment.c_str() );

	found = ref.find( segment );
	if ( found != string::npos)
	{
		diff = (int)found;
		if ( dist_to_end )
		{
			diff = (int)ref.size() - (int)found - SEG_LENGTH;
		}
	}
	//L("Result %d\n", diff );
	
	return diff;
}
/**********************************************/
int get_effect_seglen( const string &ref, int start, uint32_t seg_len)
{
	//L(" Checking %d in %lu, %s\n", start, ref.size(), ref.c_str() );
	int len = 0;
	int num_char = 0;
	for ( int i = start; i < (int)ref.size(); i++)
	{
		if ( (' ' != ref[i]) && ('-' != ref[i]) )
		{ num_char ++; }
		len++;
		//L(" now in %c %d %d\n", ref[i], num_char, len);
		if ( num_char == (int) seg_len )
		{ break;}
	}
	return len;
}
/**********************************************/
int get_effect_seglen_suffix( const string &ref, int end, uint32_t seg_len)
{
	//L(" Checking %d in %lu, %s\n", end, ref.size(), ref.c_str() );
	int len = 0;
	int num_char = 0;
	if ( end > (int) seg_len)
	{
		len = (int)seg_len;
	}
	else
	{
	for ( int i = end; i >= 0; i--)
	{
		if ( (' ' != ref[i]) && ('-' != ref[i]) )
		{	num_char ++;	}
		len++;
		//L(" now in %c %d %d\n", ref[i], num_char, len);
		if ( num_char == (int) seg_len )
		{ break;}

	}
	}

	return len;
}
/**********************************************/
int update_anchor_from_ref( unit_alt &t_alt,  unit_vcf &t_vcf, const vector<string> &vec_str, const string  &chr_str, FILE *log)
{
	int clean_case = clean_alignment( t_alt, vec_str  ); // 1 for no space in effective regions. 2 for no space in whole alignment. 0 otherwise.
	fprintf(log, " + anchor side : %d\n", t_alt.anchor);
	fprintf(log, " + clean       : %d\n", clean_case  );

	int d0=0, d1=0, d2=0, d3=0;
	int best_idx, best_diff = -1;
	
	string segment;	// real sequences from genome
	string ref;
	string t_str, tmp_ref, tmp_ct, clean_str;
	size_t found;
	int bp_s, bp_e, al_s, al_e, raw_shift, shift;
	int l_ref, l_contig;
	int left_end, right_start;
	uint32_t new_bp_s, new_bp_e;
	
	// Inversions locates before Anchor 
	if ( 1 == t_alt.anchor )
	{
		//L(" Checking for %u %u\n", t_vcf.s, t_vcf.e);
		// predicted breakpoints with 1 shift
		d0 = calculate_bpdiff( t_vcf.e + 2, t_alt.bp_e + 1, t_alt.al_e + 1, chr_str, vec_str[1], 0 );
		d1 = calculate_bpdiff( t_vcf.e + 1, t_alt.bp_e + 1, t_alt.al_e + 1, chr_str, vec_str[1], 0 );

		// attempt to adjust breakpoints		
		bp_s = 0;
		raw_shift = t_alt.bp_s; // bp_s is 0-based inclusive
		get_alignstr( vec_str[1], t_str, 0, t_alt.bp_s );
		l_ref = (int)t_str.size();
		get_alignstr( vec_str[3], t_str, 0, t_alt.bp_s );
		l_contig  = (int)t_str.size();
		shift = ( l_ref > l_contig )? l_ref : l_contig ;
		bp_e = t_alt.bp_e + shift;
		new_bp_e = t_vcf.e + shift;

		//if ( bp_e > (int)vec_str[1].size()-1 )
		if ( bp_e > t_alt.al_e )
		{
			fprintf(log, " * Error: Right End %d Exceeds Alignment Boundary %d\n", bp_e, t_alt.al_e );
			t_alt.status = -1;
			return 0;
		}

		d2 = calculate_bpdiff( new_bp_e + 2, bp_e + 1, t_alt.al_e + 1, chr_str, vec_str[1], 0 );
		d3 = calculate_bpdiff( new_bp_e + 1, bp_e + 1, t_alt.al_e + 1, chr_str, vec_str[1], 0 );
		
		
		// Decide Coordinate
		best_idx = select_best_step(d0, d1, d2, d3, best_diff);
		if ( (0 == best_idx) || ( 1 == best_idx)  )
		{
			t_vcf.s += (1 - best_idx);
			t_vcf.e += (1 - best_idx);
			t_vcf.pos+= (1 - best_idx);
			fprintf(log, " + final   bp1 : %u\n", t_vcf.s );
			fprintf(log, " + final   bp2 : %u\n", t_vcf.e );
		}
		else if ( ( 2 == best_idx) || ( 3 == best_idx) )
		{
			t_vcf.s = t_vcf.s - shift + (3 - best_idx);
			t_vcf.e = t_vcf.e + shift + (3 - best_idx);
			t_vcf.pos = t_vcf.pos - shift + (3 - best_idx);
			t_alt.status = 0;
			
			// Clear Left Hand Side
			t_alt.l_var = 0; 
			t_alt.l_s = 0;
			t_alt.l_e = 0;
			t_alt.l_ref.clear();
			t_alt.l_ct.clear();
			fprintf(log, " * left contig : clear\n" );
			
			// Update Right Hand Side Ref and Contig
			t_alt.bp_e += shift;
			//get_alignstr( vec_str[1], clean_str, t_alt.bp_e + 1, vec_str[1].size() );
			//get_rawstr( vec_str[1], tmp_ref, t_alt.bp_e+1, vec_str[1].size() );//t_alt.rp_e+1);
			//get_rawstr( vec_str[3], tmp_ct,  t_alt.bp_e+1, vec_str[3].size() );//t_alt.rp_e+1);
			get_alignstr( vec_str[1], clean_str, t_alt.bp_e + 1, t_alt.al_e + 1 );
			get_rawstr( vec_str[1], tmp_ref, t_alt.bp_e+1, t_alt.al_e + 1 );//t_alt.rp_e+1);
			get_rawstr( vec_str[3], tmp_ct,  t_alt.bp_e+1, t_alt.al_e + 1 );//t_alt.rp_e+1);
			t_alt.r_var = ( tmp_ref==tmp_ct ) ? 0 : 1;
			t_alt.r_ref = tmp_ref;
			t_alt.r_ct = tmp_ct;

			right_start = t_vcf.e + 1;
			t_alt.r_s = t_alt.r_e = 0;
			if ( 0 < tmp_ref.size() )
			{
				t_alt.r_s = right_start;
				t_alt.r_e = right_start + clean_str.size() -1;
			}

			fprintf(log, " * bp calibrate: %d\n", shift );
			fprintf(log, " * final   bp1 : %u\n", t_vcf.s );
			fprintf(log, " * final   bp2 : %u\n", t_vcf.e );
			fprintf(log, " * bp diff     : %d\n", best_diff );
			fprintf(log, " * right ref   : %s\n", tmp_ref.c_str() );
			fprintf(log, " * right contig: %s\n", tmp_ct.c_str()  );
			fprintf(log, " * right var   : %d\n", t_alt.r_var);
			fprintf(log, " * right start : %u\n", t_alt.r_s);
			fprintf(log, " * right end   : %u\n", t_alt.r_e);
		}

	}
	else if ( 0 == t_alt.anchor )
	{
		// predicted breakpoints with 1 shift
		d0 = calculate_bpdiff( t_vcf.s + 1 - SEG_LENGTH, t_alt.al_s, t_alt.bp_s, chr_str, vec_str[1], 1 );
		d1 = calculate_bpdiff( t_vcf.s - SEG_LENGTH,     t_alt.al_s, t_alt.bp_s, chr_str, vec_str[1], 1 );

		// attempt to adjust breakpoints		
		bp_e = (int)vec_str[1].size() -1;
		raw_shift = bp_e - t_alt.bp_e;
		get_alignstr( vec_str[1], t_str, t_alt.bp_e + 1, bp_e + 1);
		l_ref = (int)t_str.size();
		get_alignstr( vec_str[3], t_str, t_alt.bp_e + 1, bp_e + 1);
		l_contig  = (int)t_str.size();
		shift = ( l_ref > l_contig )? l_ref : l_contig ;
		bp_s = t_alt.bp_s - shift;
		new_bp_s = t_vcf.s - shift;
		if ( raw_shift < shift ){fprintf(log, "Kidding\n");}
		
		if ( t_alt.al_s > bp_s )
		{
			fprintf(log, " * Error: Left Start %d Exceeds Alignment Boundary 0\n", bp_s, t_alt.al_s );
			t_alt.status = -1;
			return 0;
		}

		d2 = calculate_bpdiff( new_bp_s + 1 - SEG_LENGTH, t_alt.al_s, bp_s, chr_str, vec_str[1], 1 );
		d3 = calculate_bpdiff( new_bp_s - SEG_LENGTH,     t_alt.al_s, bp_s, chr_str, vec_str[1], 1 );
		// Decide Coordinate
		best_idx = select_best_step(d0, d1, d2, d3, best_diff);
		if ( (0 == best_idx) || ( 1 == best_idx)  )
		{
			t_vcf.s += (1 - best_idx);
			t_vcf.e += (1 - best_idx);
			t_vcf.pos+= (1 - best_idx);
			fprintf(log, " + final   bp1 : %u\n", t_vcf.s );
			fprintf(log, " + final   bp2 : %u\n", t_vcf.e );
		}
		else if ( ( 2 == best_idx) || ( 3 == best_idx) )
		{
			t_vcf.s = t_vcf.s - shift + (3 - best_idx);
			t_vcf.e = t_vcf.e + shift + (3 - best_idx);
			t_vcf.pos = t_vcf.pos - shift + (3 - best_idx);
			t_alt.status = 0;
		
			// Clear Right
			t_alt.r_var = 0; 
			t_alt.r_s = 0;
			t_alt.r_e = 0;
			t_alt.r_ref.clear();
			t_alt.r_ct.clear();
			
			// Set Left
			t_alt.bp_s -= shift;
			//get_rawstr( vec_str[1], tmp_ref, t_alt.rp_s, t_alt.bp_s );
			//get_rawstr( vec_str[3], tmp_ct,  t_alt.rp_s, t_alt.bp_s );
			get_alignstr( vec_str[1], clean_str, t_alt.al_s, t_alt.bp_s );
			get_rawstr( vec_str[1], tmp_ref, t_alt.al_s, t_alt.bp_s );
			get_rawstr( vec_str[3], tmp_ct,  t_alt.al_s, t_alt.bp_s );
			t_alt.l_var = ( tmp_ref==tmp_ct ) ? 0 : 1;
			t_alt.l_ref = tmp_ref;
			t_alt.l_ct = tmp_ct;

			left_end = t_vcf.pos - 1;
			t_alt.l_s = t_alt.l_e = 0;
			if ( 0 < tmp_ref.size() )
			{
				t_alt.l_s = left_end - clean_str.size() + 1;
				t_alt.l_e = left_end;
			}

			fprintf(log, " * bp calibrate: %d\n", shift );
			fprintf(log, " * final   bp1 : %u\n", t_vcf.s );
			fprintf(log, " * final   bp2 : %u\n", t_vcf.e );
			fprintf(log, " * bp diff     : %d\n", best_diff );
			fprintf(log, " * left ref    : %s\n", tmp_ref.c_str() );
			fprintf(log, " * left contig : %s\n", tmp_ct.c_str()  );
			fprintf(log, " * left var    : %d\n", t_alt.l_var);
			fprintf(log, " * left start  : %u\n", t_alt.l_s);
			fprintf(log, " * left end    : %u\n", t_alt.l_e);
			fprintf(log, " * right contig: all clear\n" );

		}
		
	}
	// Ignore All The Garbage Cases
	if ( -1 == best_idx)
	{
		t_alt.status = -1;
	}
	fprintf(log, " <>CMP         : c%d\t%d\t%d\t%d\t%d\t%d\t%d\n", clean_case, best_idx, best_diff, d0, d1, d2, d3);

}
//vec_bp: 0-based position of bp and bar. both inclusive
/**********************************************/
int get_align_interval( const vector<string> &vec_str, unit_alt &t_alt)
{
	int limit = (int)vec_str[2].size();
	int bp_s = -1, bp_e = -1, al_s = -1, al_e = -1, rp_s = -1, rp_e = -1;
	for(int i=0; i < limit; i++)
	{
		if ( '|' == vec_str[2][i] )
		{
			al_e = i;
			if ( -1 == al_s){ al_s = i;}
		}
		else if (' ' != vec_str[2][i])
		{
			bp_e = i;
			if ( -1 == bp_s){ bp_s = i;}
		}
	}
	
	if ( al_s < bp_s )
	{
		rp_s = al_s;
		while( '|' == vec_str[2][rp_s] && rp_s <= bp_s){rp_s++;}
	}
	if ( bp_e < al_e )
	{
		rp_e = al_e;
		while( '|' == vec_str[2][rp_e] && rp_e >= bp_e){rp_e--;}
	}

	if (  (-1 == bp_s) || (-1 == bp_e) || (-1 == al_s) ||(-1 == al_e)  )
	{ L("Warning: Suspicious Alignment Display: %s\n%d\t%d\t%d\t%d\n", vec_str[0].c_str(), bp_s, bp_e, al_e, al_e);}

	t_alt.bp_s = bp_s;
	t_alt.bp_e = bp_e;
	t_alt.al_s = al_s;
	t_alt.al_e = al_e;
	t_alt.rp_s = rp_s;
	t_alt.rp_e = rp_e;
	return 0;
}

// A Hack For Extracting Variations and Contigs from Alignment with indels/mismatches 
/**********************************************/
int extract_align( unit_alt &t_alt, unit_vcf &t_vcf, const vector<string> &vec_str, const string &chr_str, FILE *log)
{
	string tmp_ref, tmp_ct, tmp_var, clean_str;

	get_align_interval( vec_str, t_alt );
	get_alignstr( vec_str[3], tmp_var, t_alt.bp_s, t_alt.bp_e + 1 );
	t_alt.v_ct = tmp_var;
	uint32_t left_end, right_start;

	fprintf(log, " + bp start    :%6d\n", t_alt.bp_s + 1 );
	fprintf(log, " + bp end      :%6d\n", t_alt.bp_e + 1 );
	fprintf(log, " + align start :%6d\n", t_alt.al_s + 1 );
	fprintf(log, " + align end   :%6d\n", t_alt.al_e + 1 );
	fprintf(log, " + diff start  :%6d\n", t_alt.rp_s + 1 );
	fprintf(log, " + diff end    :%6d\n", t_alt.rp_e + 1 );
	fprintf(log, " + variation   : %s\n", t_alt.v_ct.c_str() );


	// Updating Left Side Donor Genome
	if ( t_alt.al_s > t_alt.bp_s )
	{ t_alt.l_var = 0; }
	else
	{
		get_alignstr( vec_str[1], clean_str, 0, t_alt.bp_s );
		//get_rawstr( vec_str[3], tmp_ct,  t_alt.rp_s, t_alt.bp_s );
		get_rawstr( vec_str[1], tmp_ref, 0, t_alt.bp_s );
		get_rawstr( vec_str[3], tmp_ct,  0, t_alt.bp_s );
		t_alt.l_var = ( tmp_ref==tmp_ct ) ? 0 : 1;
		t_alt.l_ref = tmp_ref;
		t_alt.l_ct = tmp_ct;

		left_end = t_vcf.pos;
		if ("INV" == t_vcf.sv)
		{	left_end--;	}
		t_alt.l_s = t_alt.l_e = 0;
		if ( 0 < tmp_ref.size() )
		{
			t_alt.l_s = left_end - clean_str.size() + 1;
			t_alt.l_e = left_end;
		}

		fprintf(log, " + left ref    : %s\n", tmp_ref.c_str() );
		fprintf(log, " + left contig : %s\n", tmp_ct.c_str()  );
		fprintf(log, " + left var    : %d\n", t_alt.l_var);
		fprintf(log, " + left start  : %u\n", t_alt.l_s);
		fprintf(log, " + left end    : %u\n", t_alt.l_e);
	}

	// Updating Right Side Donor Genome
	if ( t_alt.al_e < t_alt.bp_e )
	{ t_alt.r_var = 0; }
	else
	{
		get_alignstr( vec_str[1], clean_str, t_alt.bp_e+1, vec_str[1].size() );
		get_rawstr( vec_str[1], tmp_ref, t_alt.bp_e+1, vec_str[1].size() );//t_alt.rp_e+1);
		get_rawstr( vec_str[3], tmp_ct,  t_alt.bp_e+1, vec_str[3].size() );//t_alt.rp_e+1);
		t_alt.r_var = ( tmp_ref==tmp_ct ) ? 0 : 1;
		t_alt.r_ref = tmp_ref;
		t_alt.r_ct = tmp_ct;

		right_start = t_vcf.pos + 1 ;
		if ("INV" == t_vcf.sv)
		{	right_start = t_vcf.e + 1;}
		t_alt.r_s = t_alt.r_e = 0;
		if ( 0 < tmp_ref.size() )
		{
			t_alt.r_s = right_start;
			t_alt.r_e = right_start + clean_str.size() -1;
		}

		fprintf(log, " + right ref   : %s\n", tmp_ref.c_str() );
		fprintf(log, " + right contig: %s\n", tmp_ct.c_str()  );
		fprintf(log, " + right var   : %d\n", t_alt.r_var);
		fprintf(log, " + right start : %u\n", t_alt.r_s);
		fprintf(log, " + right end   : %u\n", t_alt.r_e);
	}

	// A Hack for Long MicroInversion Coordinates
	if ( "<INV>" == t_vcf.alt )
	{
		t_alt.anchor = detect_anchor( vec_str[2], t_alt.bp_s, t_alt.bp_e ) ;
		//fprintf(log, " + anchor side :%6d\n", t_alt.anchor);
		if ( -1 == t_alt.anchor ) 
		{ 
			t_alt.status = -1;
			fprintf(log, "Can Not Detect Anchor Side\n"); 
		}
		else
		{
			update_anchor_from_ref( t_alt, t_vcf, vec_str, chr_str, log);
		}
	}

	// Set Up Actual MicroSV Parts
	return 0;
}
/**********************************************/
int find_Overlap_Isoform( const unit_vcf &t_vcf, const vector<isoform> vec_isoform, vector<isoform> &iso_cand, FILE *log)
{
	int i, j, num_iso=(int)vec_isoform.size();	
	int tmp_l, num_bp = 0, num_cds = 0;
	int valid_gene = 0 ; // if the gene contains any isoforms with max overlap length
	for( i = 0; i< num_iso; i++ )
	{
		int n_exon = vec_isoform[i].exon.size();
		num_bp = 0;num_cds = 0;

		for( j = 0; j < n_exon; j++)
		{
			if ( skip_interval( t_vcf, vec_isoform[i].exon[j].e_e ) )
			{	continue;	}
			else if ( final_interval( t_vcf, vec_isoform[i].exon[j].e_s ) )
			{	break;	}
			else if ( 0 <  vcf_overlap( t_vcf, vec_isoform[i].exon[j].e_s, vec_isoform[i].exon[j].e_e ) )
			{
				tmp_l = vcf_overlap_length( t_vcf, vec_isoform[i].exon[j].c_s, vec_isoform[i].exon[j].c_e );
				if ( tmp_l )
				{
					num_bp += tmp_l;
					num_cds ++;
				}
			}
			if ( num_bp >= t_vcf.ol )
			{
				iso_cand.push_back( vec_isoform[i] );
				valid_gene = 1;
				fprintf(log, " + Isoform     : %s %s %d\n", vec_isoform[i].gid.c_str(), vec_isoform[i].id.c_str(), num_bp);
			}
		}
	}
	return valid_gene;
}
/**********************************************/
void find_Overlap_Gene( const unit_vcf &t_vcf, const map<string, vector<gene_data> > &gene_sorted_map, const map<string, vector<isoform> > &iso_gene_map, vector<isoform> &iso_cand, FILE *log)
{
	map<string, vector<gene_data> >::const_iterator chr_it;
	map<string, vector<isoform> >::const_iterator git;
	if ( 1 == t_vcf.num_gene )
	{
		git = iso_gene_map.find( t_vcf.gid );
		find_Overlap_Isoform( t_vcf, git->second, iso_cand, log);
	}
	else
	{
		chr_it = gene_sorted_map.find( t_vcf.chr );
		int limit = (int)chr_it->second.size();
		for( int i = 0; i < limit; i++ )
		{
			if ( skip_interval( t_vcf, chr_it->second[i].end ) )
			{       continue;       }
			else if ( final_interval( t_vcf, chr_it->second[i].start) )
			{
				break;
			}
			else
			{
				if ( 0 < vcf_overlap( t_vcf, chr_it->second[i].start, chr_it->second[i].end ) )
				{
					git = iso_gene_map.find( chr_it->second[i].gene_id );
					find_Overlap_Isoform( t_vcf, git->second, iso_cand, log);
				}
			}
		}
	}
}

// Extract predicted donor transcripts with SVs
/**********************************************/
void get_svtrans( const unit_alt &t_alt, const unit_vcf &t_vcf, const string &chr_str, const vector<isoform> &iso_cand, FILE *out, FILE *log )
{
	int n_exon, n_iso = (int) iso_cand.size();
	int i,j;
	
	int var_set = 0; // If variation is appended. Just in case the variation cross multiple CDS regions
	int var_s, var_e = 0;	
	// Boundary of Unchanged Parts
	uint32_t left_bd, right_bd;
	uint32_t effect_seg_len; // when sv regions cross exon boundary

	string trans_seq, ref_seq, exon_seq, tmp_str, z_seq;


	left_bd = ( 0 == t_alt.l_s)? t_vcf.s - 1: t_alt.l_s - 1;
	right_bd = ( 0 == t_alt.r_e)? t_vcf.e + 1: t_alt.r_e + 1;

	Exon t_exon;
	fprintf( log, "-<=*=>-*-<=*=>-*-<=*=>-*-<=*=>-*-<=*=>-*-<=*=>-*-<=*=>-*-<=*=>-*-<=*=>-*-<=*=>-\n");
	fprintf( log, "%s\t%d\t%s\t", t_vcf.patient.c_str(), t_vcf.id, t_vcf.chr.c_str() );
	fprintf( log, "%u\t%u\t%u\t", t_vcf.s, t_vcf.e, t_vcf.pos );
	fprintf( log, "%s\t%s\t%s\t", t_vcf.sv.c_str(), t_vcf.ref.c_str(), t_vcf.alt.c_str() );
	fprintf( log, "%s\t%s\t%d\n\n", t_vcf.gid.c_str(), t_vcf.tid.c_str() , t_vcf.ol);

	for( i = 0; i < n_iso; i++ )
	{
		n_exon = iso_cand[i].exon.size();
		var_set = 0;
		trans_seq.clear();
		trans_seq.reserve(1000000);

		for( j = 0; j < n_exon; j++)
		{
			t_exon = iso_cand[i].exon[j];

			if ( skip_interval( t_vcf, t_exon.e_e ) )
			{	
				exon_seq = get_gtf_str( chr_str, t_exon.e_s, t_exon.e_e );
				fprintf(log, " + exon %5d  : %u\t%u\t\n", j, t_exon.e_s, t_exon.e_e);
				fprintf(log, " + exonseq     : %s\n", exon_seq.c_str() );
				trans_seq += exon_seq;
			}
			else if ( final_interval( t_vcf, t_exon.e_s ) )
			{	
				exon_seq = get_gtf_str( chr_str, t_exon.e_s, t_exon.e_e );
				fprintf(log, " * exon %5d  : %u\t%u\n", j, t_exon.e_s, t_exon.e_e);
				fprintf(log, " * exonseq     : %s\n", exon_seq.c_str() );
				trans_seq += exon_seq;
			}

			if ( 0 <  vcf_overlap( t_vcf, t_exon.e_s, t_exon.e_e ) )
			{
				fprintf(log, " = effexon     : %u\t%u\n", t_exon.e_s, t_exon.e_e);
				// Left Flank Processing
				//if ( (t_exon.e_s <= t_vcf.s-1) && (t_vcf.s-1 <=  t_exon.e_e ) )
				if ( vcf_overlap_left( t_vcf, t_exon.e_s, t_exon.e_e ) )
				{
					if ( 0 == t_alt.l_s )
					{
						// Append all leading exons
						left_bd = t_vcf.s - 1;
						if ("DUP" == t_vcf.sv)
						{ left_bd = t_vcf.pos; }
						exon_seq = get_gtf_str( chr_str, t_exon.e_s, left_bd );
						fprintf(log, " (0 clean left : %u\t%u\n", t_alt.l_s, t_alt.l_e );
						fprintf(log, " (0 exonseq    : %s\n", exon_seq.c_str() );
						trans_seq += exon_seq;

					}
					else if ( t_exon.e_s <= t_alt.l_s )
					{
						left_bd = t_alt.l_s - 1;
						// Append leading exons
						exon_seq = get_gtf_str( chr_str, t_exon.e_s, left_bd);
						fprintf(log, " (1 start left : %u\t%u\n", t_exon.e_s, left_bd );
						fprintf(log, " (1 exonseq    : %s\n", exon_seq.c_str() );
						trans_seq += exon_seq;
						// Append l_ct
						get_cleanstr( t_alt.l_ref, ref_seq);
						get_cleanstr( t_alt.l_ct, exon_seq);
						fprintf(log, " (1 original   : %u\t%u\t%s\n", t_alt.l_s, t_alt.l_e, ref_seq.c_str() );
						fprintf(log, " (1 exonseq    : %s\n", exon_seq.c_str() );
						trans_seq += exon_seq;
					}
					else if ( t_exon.e_s <= t_alt.l_e )
					{
						uint32_t seg_len = t_alt.l_e - t_exon.e_s + 1;
						// Append last characters of contig
						fprintf(log, " (2 clip left  : %u\t%u\t%u\n", t_alt.l_s, t_alt.l_e,  seg_len );
						fprintf(log, " (2 all ref    : %s\n", t_alt.l_ref.c_str() );
						fprintf(log, " (2 all cont   : %s\n", t_alt.l_ct.c_str() );
						// Update New Ref
						// Find correct positions on the left part
						effect_seg_len = get_effect_seglen_suffix( t_alt.l_ref, t_alt.l_ref.size()-1, seg_len  );
						fprintf(log, " (2 seg_len    : %u\t%u\n", seg_len, effect_seg_len );



						fprintf(log, " (2 new ref    : %s\n", t_alt.l_ref.substr( t_alt.l_ref.size() - effect_seg_len, effect_seg_len).c_str()  );
						if ( effect_seg_len > t_alt.l_ct.size() )
						{
							get_cleanstr( t_alt.l_ct, exon_seq);
						}
						else
						{ 
							z_seq = t_alt.l_ct.substr( t_alt.l_ct.size() - effect_seg_len, effect_seg_len );
							get_cleanstr( z_seq, exon_seq);
						}
						fprintf(log, " (2 exonseq    : %s\n", exon_seq.c_str() );
						trans_seq += exon_seq;
					}
				}
				// For Variation Part
				if ("DUP" == t_vcf.sv)
				{
					fprintf(log, " = dvariation  : %s\n", t_alt.v_ct.c_str());
					var_s = (int)trans_seq.size()+1;
					trans_seq += t_alt.v_ct;
					var_e = (int)trans_seq.size();
					var_set = 1;
				}
				else if ( 0 == var_set ) // so we only append var once for events crossing multiple exons
				{
					var_s = (int)trans_seq.size()+1;
					if ( "<INV>" == t_vcf.alt )
					{
						tmp_str = get_gtf_str( chr_str, t_vcf.s, t_vcf.e);
						make_rc( tmp_str, exon_seq );
						fprintf(log, " = Ivariation  : %s\n", exon_seq.c_str() );
						trans_seq += exon_seq;
					}
					else
					{
						fprintf(log, " = ivariation  : %s\n", t_alt.v_ct.c_str());
						trans_seq += t_alt.v_ct;
					}
					var_e = (int)trans_seq.size();

					var_set = 1;
				}

				// Dup? Append
				// Inv? 
				// Not INV
				// Right Flank Processing
				fprintf(log, " = right flank : %d\n", t_alt.r_var);
				// Need to deal with Right?
				if ( vcf_overlap_right( t_vcf, t_exon.e_s, t_exon.e_e ) )
				{
					if ( 0 == t_alt.r_e )
					{
						// Append remaining  exons
						right_bd = t_vcf.e + 1;
						if ("DUP" == t_vcf.sv)
						{ right_bd = t_vcf.pos+1;}
						exon_seq = get_gtf_str( chr_str, right_bd, t_exon.e_e);
						fprintf(log, " <0 clean right: %u\t%u\n", t_alt.r_s, t_alt.r_e );
						fprintf(log, " <0 exonseq    : %s\n", exon_seq.c_str() );
						trans_seq += exon_seq;

					}
					else if ( t_alt.r_e <= t_exon.e_e )
					{
						right_bd = t_alt.r_e + 1;
						// Append r_ct
						get_cleanstr( t_alt.r_ref, ref_seq);
						get_cleanstr( t_alt.r_ct, exon_seq);
						fprintf(log, " <1 original   : %u\t%u\t%s\n", t_alt.r_s, t_alt.r_e, ref_seq.c_str() );
						fprintf(log, " <1 exonseq    : %s\n", exon_seq.c_str() );
						trans_seq += exon_seq;

						// Append remaining exons
						exon_seq = get_gtf_str( chr_str, right_bd, t_exon.e_e );
						fprintf(log, " <1 start right: %u\t%u\n", right_bd, t_exon.e_e );
						fprintf(log, " <1 exonseq    : %s\n", exon_seq.c_str() );
						trans_seq += exon_seq;
					}
					else if ( t_alt.r_s <= t_exon.e_e )
					{
						uint32_t seg_len = t_exon.e_e - t_alt.r_s + 1;
						// Append last characters of contig
						fprintf(log, " <2 clip right : %u\t%u\t%u\n", t_alt.r_s, t_alt.r_e, seg_len );
						fprintf(log, " <2 all ref    : %s\n", t_alt.r_ref.c_str() );
						fprintf(log, " <2 all cont   : %s\n", t_alt.r_ct.c_str() );

						// Find correct position on the right part
						effect_seg_len = get_effect_seglen( t_alt.r_ref, 0, seg_len  );
						fprintf(log, " <2 seg_len    : %u\t%u\n", seg_len, effect_seg_len );

						fprintf(log, " <2 new ref    : %s\n", t_alt.r_ref.substr( 0, effect_seg_len).c_str() );
						if ( effect_seg_len > t_alt.r_ct.size() )
						{
							get_cleanstr( t_alt.r_ct, exon_seq);
						}
						else
						{ 
							z_seq = t_alt.r_ct.substr( 0, effect_seg_len );
							get_cleanstr( z_seq, exon_seq);
						}
						fprintf(log, " <2 exonseq    : %s\n", exon_seq.c_str() );
						trans_seq += exon_seq;
					}
				}
			}
		}
		// Sequence Obtained
		if ( 0 < (int)trans_seq.size())
		{ 
			fprintf(log, "\n>%s %d %s %s %d %d %lu\n", t_vcf.patient.c_str(), t_vcf.id, iso_cand[i].gid.c_str(), iso_cand[i].id.c_str(), var_s -1 , var_e-1, trans_seq.size() );
			fprintf(log, "%s\n", trans_seq.c_str() );
			//fprintf( out, ">%lu %lu %lu\n", t_vcf.pos, t_vcf.s, t_vcf.e );
			//fprintf( out, ">%u %u %u\n", t_vcf.pos, t_vcf.s, t_vcf.e );
			//	fprintf( out, ">%s %d %s %s %lu %lu %lu %c %s %s %d %d %lu %d %d %d\n", 
			//		t_vcf.patient.c_str(), t_vcf.id, t_vcf.sv.c_str(), t_vcf.chr.c_str(),
			//		t_vcf.pos, t_vcf.s, t_vcf.e, iso_cand[i].strand, 
			//		iso_cand[i].gid.c_str(), iso_cand[i].id.c_str(), var_s, var_e, trans_seq.size(),
			//		t_alt.status, t_vcf.num_gene, t_vcf.cds);
			fprintf( out, ">%s %d %s %s ", t_vcf.patient.c_str(), t_vcf.id, t_vcf.sv.c_str(), t_vcf.chr.c_str() );
			fprintf( out, "%u %u %u %c ", t_vcf.pos, t_vcf.s, t_vcf.e, iso_cand[i].strand );
			fprintf( out, "%s %s %d %d %lu ", iso_cand[i].gid.c_str(), iso_cand[i].id.c_str(), var_s-1, var_e-1, trans_seq.size());
			fprintf( out, "%d %d %d\n", t_alt.status, t_vcf.num_gene, t_vcf.cds);
			//%lu %lu %lu %c %s %s %d %d %lu %d %d %d\n", 
			//	t_vcf.patient.c_str(), t_vcf.id, t_vcf.sv.c_str(), t_vcf.chr.c_str(),
			//	t_vcf.pos, t_vcf.s, t_vcf.e, iso_cand[i].strand, 
			//	iso_cand[i].gid.c_str(), iso_cand[i].id.c_str(), var_s, var_e, trans_seq.size(),
			//	t_alt.status, t_vcf.num_gene, t_vcf.cds);
			fold_output( out, trans_seq );
		}
		else
		{
			L("No Sequence %s %d %s\n", t_vcf.patient.c_str(), t_vcf.id, t_vcf.tid.c_str() );
		}
		
		
	}

}
// Extract the corresponding alignment contig to reflect donor genome and fixing breakpoints
/**********************************************/
void convert_VCF( const map<string, vector<unit_vcf> > &map_record, const map<int, vector<string> > &map_alignment,
	const map<string, string> &map_ref, const map<string, vector<gene_data> > &gene_sorted_map, const map<string, vector<isoform> > &iso_gene_map, char *out_file )
{

	string log_file    = add_extension( out_file, "clog");
	string expand_file = add_extension( out_file, "tlog");
	string fa_file     = add_extension( out_file, "trans");
	FILE *log_fp       = fopen( log_file.c_str(), "w");
	FILE *expand_fp    = fopen( expand_file.c_str(), "w");
	FILE *fa_fp        = fopen( fa_file.c_str(), "w");
	
	int i = 0 , limit = 0;
	unit_vcf t_vcf;
	
	map<string, vector<unit_vcf> >::const_iterator it;
	map<int, vector<string> >::const_iterator al_it;
	map<string, string>::const_iterator chr_it;

	vector<isoform> iso_candidate;

	string cnt;
	unit_alt t_alt;
	for( it = map_record.begin(); it != map_record.end(); it++ )
	{
		chr_it = map_ref.find( it->first );
		if ( chr_it != map_ref.end() )
		{
			limit = (int)it->second.size();
			for ( i = 0; i < limit; i++ )
			{
				t_vcf = it->second[i];
				fprintf( log_fp, "-<=*=>-*-<=*=>-*-<=*=>-*-<=*=>-*-<=*=>-*-<=*=>-*-<=*=>-*-<=*=>-*-<=*=>-*-<=*=>-\n");
				fprintf( log_fp, "%s\t%d\t%s\t", t_vcf.patient.c_str(), t_vcf.id, t_vcf.chr.c_str() );
				fprintf( log_fp, "%u\t%u\t%u\t", t_vcf.s, t_vcf.e, t_vcf.pos );
				fprintf( log_fp, "%s\t%s\t%s\t", t_vcf.sv.c_str(), t_vcf.ref.c_str(), t_vcf.alt.c_str() );
				fprintf( log_fp, "%s\t%s\t%d\n\n", t_vcf.gid.c_str(), t_vcf.tid.c_str() , t_vcf.ol);

				// Obtaining Donor Genome Information From Actual Contigs
				clear_alt( t_alt );
				al_it = map_alignment.find( t_vcf.id );

				if ( al_it == map_alignment.end() )
				{	E("Warning: Can not Find Alignment Records for SV %s %d\n", t_vcf.patient.c_str(), t_vcf.id);	}
				else
				{
					get_cntstring( cnt, al_it->second[2].size() );
					fprintf( log_fp, "Alignment:%s\n", al_it->second[0].substr(1, al_it->second[0].size()-1).c_str());
					fprintf( log_fp, ">>> %5lu  %s\nRef      : %s\n", al_it->second[2].size(), cnt.c_str(),  al_it->second[1].c_str() );
					fprintf( log_fp, "           %s\nCont     : %s\n", al_it->second[2].c_str(), al_it->second[3].c_str()  );
					extract_align( t_alt, t_vcf, al_it->second, chr_it->second, log_fp  );
				}
				fprintf(log_fp, "\n\n");

				// Searching Related Genes and Isoforms

				// Collect Get All Isoform Information with Matched Bases at least overlap_length nt
				iso_candidate.clear();
				find_Overlap_Gene( t_vcf, gene_sorted_map, iso_gene_map, iso_candidate, log_fp );

				if ( 0 <= t_alt.status ) // skip all evens with status -1, or garbage calls
				{
					get_svtrans( t_alt, t_vcf, chr_it->second, iso_candidate, fa_fp, expand_fp );

				}
			}
		}
		else
		{	E("Warning: Unknown Chromsome %s\n", it->first.c_str() );	}
	}

	fclose( log_fp );
	fclose( expand_fp );
	fclose( fa_fp );
}
