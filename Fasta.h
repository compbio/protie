#ifndef __FASTA__
#define __FASTA__
//#include <vector>
#include <map>
#include <string>
//#include "GeneAnnotation.h"

//void LoadFasta(char *fasta_file);

void LoadFasta(char *fasta_file, std::map<std::string, std::string> &chr_map);
void LoadFastaShort(char *fasta_file, std::map<std::string, std::string> &chr_map, size_t buffer_size);
void LoadFasta_Single(char *fasta_file, std::string &chr_name, std::string &chr_seq);

// Reading Protein Fasta ID into Map including Protein ID along with the property
int getProteinType( char *prot_type );
void readEnsemblProteinID( char *fasta_file, std::map<std::string, int> &prot_map);

// Reading Protein Fasta Seqeunce into Map including Protein seq along with the property
void readSeqEnsemblProteinID_0( char *fasta_file, std::map<std::string, int> &prot_map);
void readSeqEnsemblProteinID( char *fasta_file, std::map<std::string, int> &k_map, std::map<std::string, int> &n_map, std::map<std::string, int> &p_map);
#endif
