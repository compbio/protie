#ifndef __COMPARE__
#define __COMPARE__
//#include <vector>
#include <map>
#include <string>
#include "Prediction.h"
#include "Repeat.h"
#include "GeneAnnotation.h"
//#include "Fusion.h"
//using namespace std;

typedef struct
{
	string tname; // transcript name
	string gname; // gene name
	string gid; // gene id
	string tid; // transcript id
	string src; // protein_coding or not
	string fea; // exon, cds, or codon
	uint32_t e_s; // exon start
	uint32_t e_e; // exon end
	uint32_t c_s; // cds start
	uint32_t c_e; // cds end
	int overlap; // best CDS overlap
	int e_overlap; // exon overlap
	int cds; // -1 integenic, 0 left UTR, 1 right UTR, 2 full containment, 3 partial overlap, 4 intronic
	int cds_trans;
	int cds_gene;
	char strand;
}match_info;

typedef struct
{
	uint32_t lb; // sv_gene start. 0-based inclusive
	uint32_t rb; // sv_gene end. 0-based inclusive
	uint32_t l_diff; // gene start - lb
	uint32_t r_diff; // rb - gene end
	int flag;
}sv_region_t;

// Information for single microSV record based on its alignment
typedef struct
{
	int status; // -1: don't process this entry; 0: updated bp; 1: as predicted
	// 1 for different contents; 0 o.w. 0 for all 3 flags when alignment is missing
	int bp_var;
	int l_var;
	int r_var;
	// coordinate on alignment. 0-based inclusive
	int bp_s;
	int bp_e;
	int al_s;
	int al_e;
	int rp_s;
	int rp_e;
	int anchor; // only for long inversions. How to interpret?
	std::string v_ct;
	// l_s and l_e are genomic cooridnate. 1-based, both inclusive
	uint32_t l_s; 
	uint32_t l_e;
	std::string l_ref;
	std::string l_ct;
	//
	uint32_t r_s;
	uint32_t r_e;
	std::string r_ref;
	std::string r_ct;
}unit_alt;



int overlap( const uint32_t s1, const uint32_t e1, const uint32_t s2, const uint32_t e2);
int overlap_length( const uint32_t s1, const uint32_t e1, const uint32_t s2, const uint32_t e2);

int vcf_overlap( const unit_vcf &pred, const uint32_t s2, const uint32_t e2);
int vcf_overlap_length( const unit_vcf &pred, const uint32_t s2, const uint32_t e2);

int vcf_overlap_left( const unit_vcf &pred, const uint32_t s2, const uint32_t e2);
int vcf_overlap_right( const unit_vcf &pred, const uint32_t s2, const uint32_t e2);


void copy_match( match_info &m1, match_info &m2);
int better_candidate( int max_l, int new_l, const string &best_src, const string &new_stc);
void locate_pred_in_isoform( const unit_pred &pred, const vector<isoform> &vec_isoform, match_info &mi_item);
void locate_single_pred( const unit_pred &pred, const vector<gene_data> &vec_gene, const map<string, vector<isoform> > &iso_gene_map, match_info &mi_item);
void clear_match_info( match_info &match_item);
void LocatePrediction( const map<string, vector<unit_pred> > &sort_record, const  map<string, vector<gene_data> > &gene_sorted_map, const map<string, vector<isoform> > &iso_gene_map, char *out_file);

// Attempt to speed up comparison
//void LocatePrediction2( const map<string, vector<unit_pred> > &sort_record, const  map<string, vector<gene_data> > &gene_sorted_map, const map<string, vector<isoform> > &iso_gene_map, char *out_file);


// Comparison for Repeat Structure
void LocateRepeat( const map<string, vector<unit_pred> > &sort_record, const map<string, vector<repeat_t> > &repeat_map, char *out_file);

///////////////////////////////
int skip_interval(const unit_vcf &pred, uint32_t exon_end);
int final_interval(const unit_vcf &pred, uint32_t exon_start);
void locate_vcf_in_isoform( const unit_vcf &pred, const vector<isoform> &vec_isoform, match_info &mi_item);
int locate_single_vcf( const unit_vcf &vcf, const vector<gene_data> &vec_gene, const map<string, vector<isoform> > &iso_gene_map, match_info &mi_item);
// Annotating VCF based on Gene Model
void LocateVCF_GeneModel( const map<string, vector<unit_vcf> > &map_record, const  map<string, vector<gene_data> > &gene_sorted_map, const map<string, vector<isoform> > &iso_gene_map, char *out_file);
//
void LocateVCF_Repeat( const map<string, vector<unit_vcf> > &sort_record, const map<string, vector<repeat_t> > &repeat_map, char *out_file);
int check_repeat(const unit_vcf &pred, uint32_t r_start);
int increment_repeatidx(const unit_vcf &pred, uint32_t r_end);
int overlap_repeat_length(const unit_vcf &pred, uint32_t r_start, uint32_t r_end, vector<int> &t_vec);
int initial_repeat_vec( vector<int> &t_vec );
int adj_repeat_vec( vector<int> &new_vec, vector<int> &t_vec );
void CountVCF_Repeat( const map<string, vector<unit_vcf> > &sort_record, const map<string, vector<repeat_t> > &repeat_map, char *out_file);

// Calibrating VCF Coordinates by Sequence Content
//int cmp_sv_seq( const unit_vcf &t_vcf, const string &chr);
int HammingDist(const string &q_Str, const string &tmp_str);
int Best_HammingDist(const string &q_Str, const string &tmp_str, int &best_loc);
void calibrate_info(FILE *out, const char *option, uint32_t shift, int cali_flat);
int get_flank( const string &chr, string &flank, uint32_t start, size_t length, uint32_t shift);
int check_longinv( const string &report, const string &left, const string &left_rc);
int compeql_str( const string &s1, const string &s2);
int cmp_sv_seq( const unit_vcf &t_vcf, const string &chr, FILE *log, uint32_t &shift);
void VCF_calibration( const map<string, vector<unit_vcf> > &sort_record, const map<string, string > &chr_map, char *out_file);
//void VCF_RemoveDuplicate( const map<string, vector<unit_vcf> > &sort_record, char *out_file);
// Converting VCF records into Trancript Sequence
int pass_VCF_filter( const unit_vcf &vcf_t);
int get_correct_bd(uint32_t &lb, uint32_t &rb, const unit_vcf &vcf_t, const uint32_t g_s, const uint32_t g_e, uint32_t &l_d, uint32_t &r_d);
int get_SV_region(const unit_vcf &vcf_t, const gene_data &gene_item, const string &chr_seq, string &new_seq, vector<uint32_t> &vec_coor, sv_region_t &sv_bd);
uint32_t get_SV_transcript(const unit_vcf &vcf_t, string &trans_seq, const string &ref_seq, const vector<uint32_t> &vec_coor, const gene_data &gene_item,
	const isoform &iso_item, const sv_region_t &sv_bd, int before_sv, int total_sv);
void OverlappingIsoform_vcf( const unit_vcf &vcf, const gene_data &gene_item, const vector<isoform> &vec_isoform, const string &chr_seq, match_info &mi_item, vector<int> &summary, FILE *seq_fp);
void initial_summary( vector<int> &vec_sum, int n_item);
void adj_summary( vector<int> &f_sum, const vector<int> &t_sum, int n_item);
void Overlapping_vcf( const unit_vcf &vcf, const vector<gene_data> &vec_gene, const map<string, vector<isoform> > &iso_gene_map, const string &chr_seq, FILE *out_fp, FILE *seq_fp );//, match_info &mi_item);
void PeptideDB_VCF( const map<string, vector<unit_vcf> > &map_record, const  map<string, vector<gene_data> > &gene_sorted_map,
	const map<string, vector<isoform> > &iso_gene_map, const map<string, string> &chr_map, char *out_file);



// RNA-Seq Data Verification
//void ClusterVCF( const vector<unit_vcf> &vec_vcf);
void TranscriptDB_VCF( const map<string, vector<unit_vcf> > &map_record, const  map<string, vector<gene_data> > &gene_sorted_map,
	const map<string, vector<isoform> > &iso_gene_map, const map<string, string> &chr_map, char *out_file);
// Obsolete
// Prediction Result Cluster
void OutputJoinedFusion(char *out_file,  vector<fusion> &fusion_vec );
void joinFusion( vector<fusion> &fusion_vec );
void clusterFusion( vector<fusion> &fusion_vec );
int compare_fusion(fusion &f_1, fusion &f_2);
int compare_seg( vector<int> &v_1, vector<int> &v_2);
void OutputIntVec( vector<int> &v_1 );
// Conversion from FPKM to count
void ConvertToCount(char *pred_file, map<string, vector<tr_length> > &length_map, char *out_file);
int select_longest_region( vector<tr_length> &tr_vec);

// Prediction Result Converter
void ParseTopHatFusion(char *fm_file, char *out_file, map<string, GENEPosition> &PTMap, map<string, vector<string> > &gene_table, map<string, string> &ensg_table, int read_length);
void ParseSOAPfuse(char *fm_file, char *out_file, map<string, GENEPosition> &PTMap, map<string, vector<string> > &gene_table, map<string, string> &enst_table, int read_length);
void ParseFusionMap(char *fm_file, char *out_file, map<string, GENEPosition> &PTMap, map<string, vector<string> > &gene_table,  int read_length);

void check_strand( string &gene_id, string &strand);
int locate_trans( string &trans_id, string &strand, vector<string> & vec_id, map<string, GENEPosition> &PTMap, string &chr_str, int pos);
void approx_segment( int pos, int read_length, vector<int> &end_point, int direction);
void locate_segment(string &trans, int pos, int read_length, GENEPosition &gp, vector<int> &end_point, int direction);
int find_exon(int pos, vector<ExonPosition> &vec_ep);



// Integration of VCF and Alignment Files to Make Transcript Sequences
void clear_alt( unit_alt &t_alt);
void get_cntstring( std::string &cnt, size_t limit);
void get_alignstr( const std::string &src, std::string &dest, int s, int e);
void get_rawstr( const std::string &src, std::string &dest, int s, int e);
void get_cleanstr( const std::string &src, std::string &dest);
int detect_anchor( const std::string &anchor_str, int bp_s, int bp_e);
int clean_alignment( const unit_alt &t_alt, const vector<string> &vec_str);
int select_best_step( int d0, int d1, int d2, int d3, int &best_d);
int calculate_bpdiff( int gen_start, int ref_s, int ref_e, const string &chr_str, const string &local_ref, int dist_to_end);
int get_effect_seglen( const string &ref, int start, uint32_t seglen);
int get_effect_seglen_suffix( const string &ref, int end ,uint32_t seglen);
int update_anchor_from_ref( unit_alt &t_alt, unit_vcf &t_vcf, const std::vector <std::string> &vec_str, const std::string &chr_str, FILE *log);

int get_align_interval( const std::vector<std::string> &vec_str, unit_alt &t_alt);
int extract_align( unit_alt &t_alt, unit_vcf &t_vcf, const std::vector <std::string> &vec_str, const std::string &chr_str, FILE *log);
int find_Overlap_Transcript( const unit_vcf &t_vcf, const std::vector<isoform> &vec_isoform, vector<isoform> &iso_cand, char *out_file);
void find_Overlap_Gene( const unit_vcf &t_vcf, const std::map<std::string, std::vector<gene_data> > &gene_sorted_map, const std::map<std::string, std::vector<isoform> > &iso_gene_map, vector<isoform> &iso_cand, char *out_file);
void get_svtrans( const unit_alt &t_alt, const unit_vcf &t_vcf, const std::string &chr_str, const vector<isoform> &iso_cand, FILE *out, FILE *log);
void convert_VCF( const std::map< std::string, std::vector< unit_vcf > > &map_record, const std::map< int, std::vector<std::string> > &map_alignment,
	const std::map< std::string, std::string > &map_ref, const std::map<std::string, std::vector<gene_data> > &gene_sorted_map, const std::map<std::string, std::vector<isoform> > &iso_gene_map, char *out_file);

#endif
