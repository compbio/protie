**ProTIE**: Proteogenomics Integration Engine
===================
### What is ProTIE?
ProTIE (Proteogenomics  Integration Engine) is a computational tool to extract translated peptides from genomic and transcriptomic aberrations based on matching genomic, transcriptomic, and proteomic data from the same patients.

### How do I get ProTIE?
Just clone our repository and issue `make` command:
```
git clone git@bitbucket.org:compbio/protie.git
cd protie && make
```

### How do I run ProTIE?
You can use
```
./protie -h
```
to get basic ideas about parameters. For more details, please check doc/ProTIE_manual.pdf.



---


### Contact & Support

Feel free to drop any inquiry to [yenyil at sfu dot ca](mailto:).
