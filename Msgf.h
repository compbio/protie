#ifndef __MSGF__
#define __MSGF__
#include <map>
#include <vector>
#include <string>
#include "Read.h"
#include "Common.h"

typedef struct
{
	uint32_t count;
	std::string pep;
}pc_t;

typedef struct
{
	std::string pat;
	int id;
	int bp;
	int stop;
	int s;
	int e;
}dh_t;//defuse header

typedef struct
{
	std::string pat;
	int id;
	int ol;	// overlap
	int lp;	// l of pep
	int rp;	// r of pep
	int s;
	int e;
	int ori;
	int rf;
	int l_b4;
	int l_sv;
	char strand;
	std::string sv_type;
	std::string gid;
	std::string tid;
	std::string ref;
	uint32_t pos;
	uint32_t ps;
	uint32_t pe;
}mh_t;//mistrvar header

typedef struct
{
	std::string rid;
	uint32_t flag;
	std::string refid;
	uint32_t pos;
	std::string cigar;
	std::string rnext;
	uint32_t pnext;
	int tlen;
	int hd; // hamming distance
	std::string seq;
	std::string qual;
	std::string opt;
	std::string md;
	std::string info;
	int rl; //read length
}sam_t;


typedef struct
{
	std::string pep;
	std::string score; // original scores for ranking
	float e; // E-value
	int n_decoy; // number of decoy
	int num;	// number of all entries
	int decoy; // 0 for target, 1 for decoy
}rank_t;

// Common Utilities
int get_msgf_peptide( char *t_pep, std::string &pep_str);
// so we can pick the PSM with highest score and best sequence properties for every spectrum 
int augment_DTAMap( std::map < std::string, int> &map_dta, const std::string &dta_id, int prot_id);	




// Discard Sequences
int readDTA_from_TSV(char *tsv_file, std::map<std::string, int> &map_dta, float q_value);
void annotateDTA_by_KnownSearch( char *tsv_file, char *out_file, const std::map<std::string, int> &map_dta, float q_value);

// Annotate all search results as its 18th entry for a tsv file
int checkPepID_by_Protein( char *str, const std::map<std::string, int> &map_prot);
void classifyTSV_by_Protein( char *tsv_file, char *out_file, const std::map<std::string, int> &map_prot);


int find_substring( const std::string &target, const std::map< std::string, int > &map_str );
void classifyTSV_by_PeptideSeq( char *tsv_file, char *out_file, const std::map< std::string, int > &k_map, 
	const std::map< std::string, int > &n_map, const std::map< std::string, int > &p_map, float q_value, int decoy_flag);

void checkMultiHitTSV( char *tsv_file, char *out_file, float q_value, int decoy_flag);
// read peptide counts into a map
int getPepFreq(char *freq_file, std::map< int, pc_t > &map_freq);
// get the number of F_ or CPTAC_ for single dta record. abe_type 1 for fusion, 2 for sv
int getNumProtein( char *t_pep, std::vector<int> &vec_id, int abe_type);
uint32_t getNumEvent( const std::vector<int> &vec_id, const std::map<int, pc_t> &map_pepcount, std::map<std::string, int> &map_abepep);
void countEvent_in_TSV( char *tsv_file, char *out_file, const std::map<int,  pc_t > &map_pepcount, std::map< std::string, int > &map_abepep, int abe_type);


// We do NOT only count. Instead we extract those peptides of interests and focus on them
void collectPep_in_TSV( char *tsv_file, char *out_file, const std::map<int,  pc_t > &map_pepcount, std::map< std::string, int > &map_abepep, int abe_type);
void getFusionEvent_in_TSV( char *seq_file, char *out_file,	const std::map< std::string, int > &map_abepep, std::map< std::string, std::vector< std::string> > &map_event, int abe_type);

void getDTAEvent_in_TSV_0( char *tsv_file, char *out_file, const std::map< int, pc_t> &map_pep, const std::map<std::string, std::vector<std::string> > &map_event, int abe_type);

int IsDupFusion(const std::string &f1, const std::string &f2);
int IsCorrectMix( const std::string &f1, const char *mix);
int IsCorrectSVMix( const std::string &f1, const char *mix);	//1 for correct mixture. 3 for also in tumor
int augment_fusion_list_0( std::vector< std::string > &pat_vec, const std::vector< std::string> &vec_tmp);
int augment_fusion_list( std::vector< std::string > &pat_vec, const std::vector< std::string> &vec_tmp, std::vector<int> &flag_vec, const char *mix);
void getDTAEvent_in_TSV( char *tsv_file, char *out_file, const std::map< int, pc_t> &map_pep, const std::map<std::string, std::vector<std::string> > &map_event, int abe_type, int mix_type);

int parse_defuse_match( const std::string &e_str, const std::string &prot, const std::string &pep, dh_t &token );



// Not a Good Idea. Move it later
void get_defuse_table( char *tsv_file, std::map< std::string, std::vector< std::string> > &map_info);
void extract_defuse_entry( char *tsv_file, char *out_file, const std::map< std::string, std::vector< std::string> > &map_info);
int search_fusion_list( const int p_id, const std::vector<std::string > &pred_vec, std::string &fusion_str);


// For fusions
void collectSVInfo_in_TSV_0( char *pep_file, char *out_file, const std::map< std::string, int > &map_abepep);
void collectSVInfo_in_TSV( char *peptable, char *pep_file, char *out_file, const std::map< std::string, int > &map_abepep, std::map<std::string, std::vector<std::string> > &map_event);
int augment_fusion_list( std::vector< std::string > &pat_vec, const std::vector< std::string> &vec_tmp, std::vector<int> &flag_vec, const char *mix);
void getDTA_SVEvent_in_TSV( char *tsv_file, char *out_file, const std::map< int, pc_t> &map_pep, const std::map<std::string, std::vector<std::string> > &map_event, int abe_type, int mix_type);
int parse_mistrvar_match( const std::string &e_str, const std::string &prot, const std::string &pep, mh_t &token );
int IsDupSV( const mh_t &t1, const mh_t &t2);
int clear_mht( mh_t &t1);
int copy_mht( mh_t &dest, const mh_t &src);
//void getSVEvent_in_TSV( char *out_file,	const std::map< std::string, int > &map_abepep, std::map< std::string, std::vector< std::string> > &map_event, int abe_type);

/**************************************************/
// From sniper_msgf
void count_MSGF_TSV( char *tsv_file, char *out_file );
void parse_Seq_Freq( char *seq_file, char *freq_file, std::map< int, pc_t > &map_pepcount);
//
int get_pep_count( char *t_pep, std::vector<int> &vec_id);

uint32_t get_num_svp( const std::vector<int> &vec_id, const std::map<int, pc_t> &map_pepcount, std::map<std::string, int> &map_sv_pep);
void match_MSGF_TSV( char *tsv_file, char *out_file, const std::map<int,  pc_t > &map_pepcount, std::map< std::string, int > &map_sv_pep);

//int get_nth_word( char *target, const char *src, int n);
void extract_PepTable( char *pep_file, char *out_file, const std::map< std::string, int > &map_sv_pep);

void parse_TargetPeptides( char *pep_file, std::map< std::string, std::vector< std::string> > &map_sv_pep);
int adj_psm_summary( std::map<std::string, int> &map_summary, const std::vector<std::string> &vec_info );
int detect_mix( const std::string &p_str, const std::string &m_str);
uint32_t expand_svp( const std::vector<int> &vec_id, const std::map<int, pc_t> &map_pepcount, std::map<std::string, std::vector< std::string> > &map_target, std::map<std::string, int> &map_summary, 
 const std::string &mix_str, int &flag_mix );

void adj_out( const std::string &new_pep, const std::string &src, const std::string &mix_str, FILE *out);
int output_expansion( const std::vector<int> &vec_id, const std::map<int, pc_t> &map_pepcount, std::map<std::string, std::vector< std::string> > &map_target, const std::map<std::string, int> &map_summary, 
 const std::string &mix_str, int flag_mix, const std::string &new_psm, const std::string &new_pep, int num_mod, int num_svp, FILE *out );
void expand_MSGF_TSV( char *tsv_file, char *out_file, const std::map<int,  pc_t > &map_pepcount, const std::map< std::string, std::vector< std::string > > &map_target );
// Output information for debugging
void OutputSVPEP( char *out_file, const std::map< std::string, int > &map_sv_pep);


// Ok. I know the following do not belong to MSGF. I should put them somewehre later...
//
// For RNASeq Part
int get_sam_bound( const sam_t &tmp_sam, uint32_t &s1, uint32_t &e1, uint32_t &s2, uint32_t &e2);
int get_sv_bound( const std::string &ref_id, uint32_t &sv_s, uint32_t &sv_e );
void analyze_sampair( const std::vector< sam_t > &vec_sam, FILE *con_fp, FILE *dis_fp, int &n_con, int &n_cross );
//int calculate_ml( char *cigar);
//void get_MDField( char *option_tag, std::string &md_str);
void get_OPTField( char *option_tag, sam_t &tmp_sam);
void parse_RNASeq_SV( char *sam_file, char *outfile);



int count_hd( char *md, int left, int right);
int check_mapping( char *md, int left, int right, int &l_bad, int &r_bad);
int check_cross(uint32_t sv_s, uint32_t sv_e, uint32_t s, uint32_t e, const char *md, int &over_l, int &bad_l, int &le, int &re);
void extract_RNASeq_Read( char *sam_file, char *outfile);




void summarize_RNASeq_Read( char *sam_file, char *outfile);
//
void count_ProteinID_TSV( char *tsv_file, char *out_file, std::map< std::string, int > &map_sv_pep);



// Adjust Pvalue for MS-GF+ results
void getTwoClassPvalue( char *tsv_file, char *out_file, const std::map< std::string, int > &k_map, 	const std::map< std::string, int > &n_map, const std::map< std::string, int > &p_map);
void AdjPvalue( char *out_file );
void AdjPepQvalue( char *out_file );
//
//void convert_ensembl_ref( char *chr_char, std::string &chr_str );//, std::map< std::string, std::vector< unit_pred> > &map_record );
//void convert_repeat_ref( char *fea, std::string &repeat_info );//, std::map< std::string, std::vector< unit_pred> > &map_record );
//bool sortRepeatByStart(const repeat_t &p1, const repeat_t &p2);
//bool sortRepeatByEnd(const repeat_t &p1, const repeat_t &p2);
//void parse_Repeat( char *repeat_file, std::map<std::string, std::vector<repeat_t> > &repeat_map );//, std::map< std::string, std::vector< unit_pred> > &map_record );
//void parse_Repeat( char *predict_file, std::map< std::string, std::vector< unit_pred> > &map_record );


//void copyToString( char *src, std::string &new_str);
//void SortPredictionOnly( std::map< std::string, std::vector< unit_pred > > &new_record, std::map< std::string, std::vector< unit_pred> > &map_record );
//void OutputPrediction( std::map< std::string, std::vector< unit_pred > > &new_record, char *output);

//void analyze_record( unit_pred &pred );
//void MergePrediction( std::map< std::string, std::vector< unit_pred > > &sorted_record, char *output);

#endif
