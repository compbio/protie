#ifndef __PREDICTION__
#define __PREDICTION__
#include <map>
#include <string>
#include "Common.h"
typedef struct
{
	int id;
	int sup;
	std::string ref_id;
	uint32_t s;
	uint32_t e;
	std::string sim;
	std::string fea;
	std::string header;
	std::string ref;
	std::string align;
	std::string contig;
}unit_pred;


typedef struct
{
	int id;
	//string ref;
	uint32_t s;
	uint32_t e;
	int event; // 0 for inversion, 1 for insertion, 2 for deletion
}unit_abe;


// 0-based inclusive for s, e, and position
typedef struct
{
	int id;
	int sup;// obsolete
	int max;
	int min;
	int cluster;
	int contig;
	int num_gene;
	int cds;
	int ol;
	std::string chr;
	uint32_t s; // 0-based inclusive
	uint32_t e; // 0-based EXCLUSIVE
	uint32_t pos; // 0-based inclusive
	std::string sv; // type of sv
	std::string ref;
	std::string alt;
	float qual;
	float sim;
	std::string filter;
	std::string patient;
	std::string gid;
	std::string tid;
	std::string info;
}unit_vcf;

int align_of_interest( const std::map< std::string, std::vector< unit_vcf > > &map_record, int cluster, int contig, char *ref, uint32_t start, uint32_t end, uint32_t pos);

void parse_Alignment( char *align_file, const std::map< std::string, std::vector< unit_vcf > > &map_record, std::map< int, std::vector<std::string> > &map_alignment);

void parse_InversionPrediction( char *predict_file, std::map< std::string, std::vector< unit_pred> > &map_record );


//void copyToString( char *src, std::string &new_str);
void SortPredictionOnly( std::map< std::string, std::vector< unit_pred > > &new_record, std::map< std::string, std::vector< unit_pred> > &map_record );
void OutputPrediction( std::map< std::string, std::vector< unit_pred > > &new_record, char *output);

void analyze_record( unit_pred &pred );
void MergePrediction( std::map< std::string, std::vector< unit_pred > > &sorted_record, char *output);
// Obsolete
int get_vcf_info(char *option, unit_vcf &v_token);
void parse_SV_VCF( char *predict_file, std::map< std::string, std::vector< unit_vcf> > &map_record );

// Cluster VCF Records to See How Many Overlapping Ones
void parse_ShortSV_VCF( char *predict_file, std::map< std::string, std::vector< unit_vcf> > &map_record );
int compare_vcf( const unit_vcf &p1, const unit_vcf &p2);
//void sortVCF_WithOverlap_Group( std::map< std::string, std::vector< unit_vcf> > &map_record );
void sortVCF( std::map< std::string, std::vector<unit_vcf> >  &map_record);
void sortVCF_POS( std::map< std::string, std::vector<unit_vcf> >  &map_record);
void OutputVCF( std::map< std::string, std::vector< unit_vcf > > &map_record, char *output);

// Merge COMPLETE identical entry
int merge_vcf_item( FILE *out, const unit_vcf, int min, int max, float sim);
int equal_bp ( const unit_vcf &t1, const unit_vcf &t2);
void VCF_RemoveDuplicate( const std::map<std::string, std::vector<unit_vcf> > &sort_record, char *out_file);

bool sortVCFStart( const unit_vcf &p1, const unit_vcf &p2);
bool sortVCFPos( const unit_vcf &p1, const unit_vcf &p2);
bool cmpVCFPos( const unit_vcf &p1, const unit_vcf &p2);
int sv_type( const unit_vcf &t_vcf);
int cmp_vcf( const unit_vcf &p1, const unit_vcf &p2);
//void sortVCF_WithOverlap( char *predict_file, std::map< std::string, std::vector< unit_vcf> > &map_record );
void sortVCF_WithOverlap( std::map< std::string, std::vector< unit_vcf> > &map_record );
int overlap_vcf( const unit_vcf &p1, const unit_vcf &p2);
void cmp_patients_VCF(  std::map< std::string, std::vector< unit_vcf> > &base_record, std::map< std::string, std::vector< unit_vcf> > &map_record, char *outfile );

// Filtering VCF based on special condition
int valid_vcf( const unit_vcf &p1, float inv_id, int inv_sup, float dup_id, float dup_sup );
void FilterVCF( const std::map< std::string, std::vector< unit_vcf> > &map_record, char*outfile, float min_id, int min_sup );

// Cluster VCF Together
void create_interval_bd(unit_vcf &vcf_t, uint32_t &lb, uint32_t &rb);
void adj_interval_bd(unit_vcf &vcf_t, uint32_t &lb, uint32_t &rb);
void clusterVCF( const std::map< std::string, std::vector< unit_vcf> > &map_record, char *outfile );

//Attempt to Merge Fasta File
void MergeFasta( char *fasta_file, std::map<std::string, int> &seq_map, char *outfile, char *lib);

//// Integration of VCF and Alignment Files to Make Transcript Sequences
//void clear_alt( unit_alt &t_alt);
//void get_cntstring( std::string &cnt, size_t limit);
//void get_alignstr( const std::string &src, std::string &dest, int s, int e);
//int detect_anchor( const std::string &anchor_str, int bp_s, int bp_e);
//int update_anchor_from_ref( unit_alt &t_alt, const unit_vcf &t_vcf, const std::vector <std::string> &vec_str, const std::string &chr_str, FILE *log);
//
//int get_align_interval( const std::vector<std::string> &vec_str, unit_alt &t_alt);
//int extract_align( unit_alt &t_alt, const unit_vcf &t_vcf, const std::vector <std::string> &vec_str, const std::map<std::string, std::string> &map_ref, FILE *log);
//void convert_VCF( const std::map< std::string, std::vector< unit_vcf > > &map_record, const std::map< int, std::vector<std::string> > &map_alignment,
//	const std::map< std::string, std::string > &map_ref, const std::map<std::string, std::vector<gene_data> > &gene_sorted_map, const std::map<std::string, std::vector<isoform> > &iso_gene_map, char *out_file);
#endif
