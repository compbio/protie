#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <iostream>
#include <fstream>
#include <string>
#include <map>
#include <algorithm>

#include "Msgf.h"
#include "Flag.h"
#include "Compare.h"
#include "Read.h"
#include "Common.h"
//#include "GeneAnnotation.h"
#include "Fasta.h"

#define FASTA_LINE 60

using namespace std;

// Common Utilities for Processing MSGF File
/*********************************************/
int get_msgf_peptide( char *t_pep, string &pep_str)
{
	int mod = 0;
	pep_str.clear();
	while( *t_pep )
	{
		if (isalpha(*t_pep))
		{
			pep_str.append(1, *t_pep);
		}
		else if ('+' == *t_pep)
		{
			mod++;
		}
		t_pep++;
	}
	return mod;
}

/**********************************************/
int augment_DTAMap( map<string, int> &map_dta, const string &dta_id, int prot_flag )	//map_dta[ dta_id ]= prot_flag;
{
	int flag = 0;
	map<string, int>::iterator it;
	it = map_dta.find( dta_id );
	if (map_dta.end() == it)
	{ map_dta[dta_id] = prot_flag; flag = 1;}
	else if ( prot_flag > it->second )
	{ map_dta[dta_id] = prot_flag; flag = 2;}
	return flag;	
}
// Get Information of a dta from known search based on some threshold
// Here we need to get the BEST matching results for a DTA
/**********************************************/
int readDTA_from_TSV( char *tsv_file, map<string, int> &map_dta, float q_value )
{
	char *readline=(char*)malloc(MAX_LINE);
	char *match=(char*)malloc(MAX_LINE);

	int count = 0;
	int offset =0;
	
	int prot_flag = 0;
	string dta_id;
	float q_tsv;
	int t_flag=0;
	
	FILE *fp = fopen( tsv_file, "r");
	//FILE *out_fp = fopen( out_file, "w");
	int ret;

	while( NULL != fgets(readline, MAX_LINE, fp) )
	{
		if ( 19 != get_num_word(readline))
		{
			E("Abort: Format Inconsistency issue in Known Search Results %s\n", tsv_file);
			exit(1);
		}
		readline[strcspn(readline, "\n")] = 0; // Get Rid of Terminating New Line

		if ( 0 == strncmp("#", readline, 1))
		{	continue; 	}

		get_nth_word( match, readline, 4 );	// dta_id
		copyToString( match, dta_id );
		get_nth_word( match, readline, 16 );	// q_tsv
		sscanf(match, "%f", &q_tsv);
		get_nth_word( match, readline, 19 );	// prot_tsv
		sscanf(match, "%d", &prot_flag);	
		if ( q_value >= q_tsv )
		{ prot_flag += 8; }
		L("===\n%d\t%s\n", count, readline);
		L(">>>%s %d %f\n", dta_id.c_str(), prot_flag, q_tsv);
		// Just in case a dta can be matched to multiple entries
		t_flag = augment_DTAMap( map_dta, dta_id, prot_flag );	//map_dta[ dta_id ]= prot_flag;
		//if (1 == t_flag ){all_test++;} // Should be fine since we set 1 only when adding new entries
		count++;
		if ( 0 == count%10000){E(".");}
	}

	E("\n");
	fclose(fp);
	free(readline);
	return 0;
}
// For Aberrant Search Results, we mark the DTA Information Based on KNOWN Search Files
/**********************************************/
void annotateDTA_by_KnownSearch( char *tsv_file, char *out_file, const map<string, int> &map_dta, float q_value )
{
	char *readline=(char*)malloc(MAX_LINE);
	char *match=(char*)malloc(MAX_LINE);

	int count = 0;
	int offset =0;
	
	int prot_flag = 0;
	string dta_id, pep_str;
	float q_tsv;
	int n_mod;

	FILE *fp = fopen( tsv_file, "r");
	FILE *out_fp = fopen( out_file, "w");
	int ret;
	map<string, int>::const_iterator it;

	while( NULL != fgets(readline, MAX_LINE, fp) )
	{
		readline[strcspn(readline, "\n")] = 0; // Get Rid of Terminating New Line

		if ( 0 == strncmp("#", readline, 1))
		{
			fprintf(out_fp, "%s\tRawPep\tDTAInfo\n", readline);
			continue; 	
		}

		get_nth_word( match, readline, 4);	// dta_id
		copyToString( match, dta_id );
		get_nth_word( match, readline, 10 );	// peptide sequences
		n_mod = get_msgf_peptide( match, pep_str);
		get_nth_word( match, readline, 16);	// q_tsv
		sscanf(match, "%f", &q_tsv);

		it = map_dta.find(dta_id);
		if ( map_dta.end() != it ) // It's a known spec
		{ prot_flag = it->second; }
		else{ prot_flag = 0; }

		fprintf( out_fp, "%s\t%s\t%d\n", readline, pep_str.c_str(), prot_flag );


		L("===\n%d\t%s\n", count, readline);
		L(">>>%s %d %f\n", dta_id.c_str(), prot_flag, q_tsv);
		count++;
		if ( 0 == count%10000){E(".");}

	}

	E("\n");
	fclose(fp);
	fclose(out_fp);
	free(readline);
}
/**********************************************/
int checkPepID_by_Protein( char *str, const map<string, int> &map_prot)
{
	//L("Start %s\n", str);
	int flag_k = 0,	flag_n = 0,	flag_p = 0,	flag_d = 0,
		flag = 0;

	map<string,int>::const_iterator it;

	const char * delim="(;";

	char *str1=(char*)malloc(MAX_LINE);
	char *token1=(char*)malloc(TOKEN_LENGTH);
	char *save;

	char c;
	string prot_str;
	int offset, ret;
	int limit = (int)strlen(str);
	int i = 1;
	strncpy(str1, str, MAX_LINE);

	for (token1 = strtok_r(str1, delim, &save); token1; token1 = strtok_r(NULL, delim, &save))
	{
		if ((i++)%2)
		{	
			//L("chunk\t%d=%s\n", i-1, token1);
			if ( 0 == strncmp("XXX", token1, 3) )	// Decoy
			{	flag = 0;	flag_d = 1;	break;	}
			else
			{
				copyToString( token1, prot_str );
				it = map_prot.find( prot_str );
				if ( map_prot.end() != it )
				{
					//if (3 == it->second){ flag_k = 1;}//else if (2 == it->second){ flag_n = 1;}
					if ( IsKnownProtein( it->second ) ){ flag_k = 1;}
					else if ( IsNovelProtein( it->second) ){ flag_n = 1;}
					else{ flag_p = 1;}
				}
			}		
		}
	}

	// Consider only when a PSM does not match to decoy sequence
	if (!flag_d){ flag = 4*flag_k + 2*flag_n + flag_p;}


	//free(token1);
	return flag;
}
/**********************************************/
int checkPepID_by_Protein0( char *str, const map<string, int> &map_prot)
{
	L("Start %s\n", str);
	int flag_k = 0,
		flag_n = 0,
		flag_p = 0,
		flag_d = 0,
		flag = 0;

	map<string,int>::const_iterator it;

	char *tag=(char*)malloc(TOKEN_LENGTH);
	char *misc=(char*)malloc(TOKEN_LENGTH);
	char c;
	string prot_str;
	int offset, ret;
	int limit = (int)strlen(str);
	// while(*str) // since we kept using the same character arrays *str might get the previous records if new lines are shorter
	while( (*str) && ( 0 < limit) )
	{
		//L("WOW %d\n", (int)strlen(str));
		ret = sscanf( str, "%[^(](%[^;]%c%n", tag, misc, &c, &offset );
		//L("%s %d %d %d\n", tag, ret, limit, offset );
		if ( 0 == strncmp("XXX", tag, 3) )
		{	
			flag = 0;
			flag_d = 1;
			break;
		}
		else
		{
			copyToString( tag, prot_str);
			it = map_prot.find( prot_str);
			if ( map_prot.end() != it )
			{
				//if (3 == it->second){ flag = 4; flag_k = 1; break;}
				//if (3 == it->second){ flag_k = 1;}
				//else if (2 == it->second){ flag_n = 1;}
				if ( IsKnownProtein( it->second ) ){ flag_k = 1;}
				else if ( IsNovelProtein( it->second) ){ flag_n = 1;}
				else{ flag_p = 1;}
			}
		}
		str += offset;
		limit -= offset;
		//L("It's the limit now %d %d\n", limit, offset );
		if ( 3 != ret){break;} // for entry without ;
	}

	// Consider only when a PSM does not match to decoy sequence
	//if (0 == flag_k){ flag = 2*flag_n + flag_p;}
	if (0 == flag_d){ flag = 4*flag_k + 2*flag_n + flag_p;}


	//E("FLAG %d\n", flag);
	free(tag);
	free(misc);
	return flag;
}
/**********************************************/
void classifyTSV_by_Protein( char *tsv_file, char *out_file, const map<string, int> &map_prot )
{
	char *readline=(char*)malloc(MAX_LINE);
	char *match=(char*)malloc(MAX_LINE);

	int count = 0;
	int offset =0;
	uint32_t start = 0, end = 0,
			score =0, rid = 0;

	string pep_str;
	int n_mod;
	int prot_flag = 0;

	FILE *fp = fopen( tsv_file, "r");
	FILE *out_fp = fopen( out_file, "w");
	int ret;
	while( NULL != fgets(readline, MAX_LINE, fp) )
	{
		readline[strcspn(readline, "\n")] = 0; // Get Rid of Terminating New Line
		//E("start %d %s\n", count, readline);

		if ( 0 == strncmp("#", readline, 1))
		{ 
			fprintf(out_fp, "%s\tRawPep\tDTAInfo\n", readline);
			continue; 
		}
		//match[0] = 0;
		//E("hmmmm\n");
		get_nth_word( match, readline, 10 );
		n_mod = get_msgf_peptide( match, pep_str);
		get_nth_word( match, readline, 11);
		//E("%d %s\n", count, readline);
		//E("===%d\n%s\n%s\n===\n", count, readline, match);
		prot_flag = checkPepID_by_Protein(match, map_prot);
		fprintf(out_fp, "%s\t%s\t%d\n", readline, pep_str.c_str(), prot_flag );
		count++;
		if ( 0 == count%10000){E(".");}
		//E("finish %d %s\n", count, readline);

	}

	E("\n");
	//E("Total/Good TSVRecord\t%d\t%d\n", count, good_tsv);
	//E("Total References with Predictions:\t%d\n", (int)map_repeat.size());
	fclose(fp);
	fclose(out_fp);
	//	free(match); // since match is modified by strsep in checkPepID_by_Protein, we don't touch it
	free(readline);
}
/**********************************************/
int find_substring( const string &target, const map<string, int> &map_str )
{
	int flag = 0;
	size_t found;
	map<string, int>::const_iterator it;
	for( it = map_str.begin(); it!=map_str.end(); it++)
	{
		found = it->first.find(target);
		if ( found != string::npos ){ flag = 1; break;}
	}
	return flag;
}
/**********************************************/
// Since Filtration Step Starts Here We allows flag to specify including decoy not not
// decoy_flag 	1: ignore anything matching to decoy. 
// 				0: include everthing
void classifyTSV_by_PeptideSeq( char *tsv_file, char *out_file, const map<string, int> &k_map, const map<string, int> &n_map, const map<string, int> &p_map, float q_value, int decoy_flag )
{
	char *readline=(char*)malloc(MAX_LINE);
	char *match=(char*)malloc(MAX_LINE);

	int count = 0;
	int offset =0;
	//uint32_t start = 0, end = 0,	score =0, rid = 0;
	
	int pep_flag = 0;
	int n_mod = 0;
	string pep_str="";

	int file_type = -1; // 1 for 17-column, 2 for 19-column. 0 otherwise
	int num_token;
	float q_tsv = 0.0;
	const char *decoy_prefix = "XXX";
	char *pch;
	int valid_flag = 0; // valid 1 for passing all constraint including q_vlaue or decoy issues

	map<string, int> cache_map; // A cache once we met some strings that we visit before
	map<string, int>::iterator it;
	int n_sig = 0, n_dup = 0;

	FILE *fp = fopen( tsv_file, "r");
	FILE *out_fp = fopen( out_file, "w");
	int ret;
	while( NULL != fgets(readline, MAX_LINE, fp) )
	{
		if ( 0 > file_type)
		{
			num_token = get_num_word( readline);
			if ( 17 == num_token ){ file_type = 1;}
			else if ( 19 == num_token ){ file_type = 2;}
			else
			{
				E("Abort: Format Inconsistency issue in MS-GF+ Search Results %s: %d columns detected\n", tsv_file, num_token);
				exit(1);
			}
		}

		readline[strcspn(readline, "\n")] = 0; // Get Rid of Terminating New Line


		if ( 0 == strncmp("#", readline, 1))
		{ 
			if ( 1 == file_type )
			{	fprintf(out_fp, "%s\tRawPep\tDTAInfo\tPepInfo\n", readline);}
			else
			{	fprintf(out_fp, "%s\tPepInfo\n", readline);}
			continue; 
		}

		get_nth_word( match, readline, 11);
		pch = strstr( match, decoy_prefix);
		get_nth_word( match, readline, 16);	// q_tsv
		sscanf(match, "%f", &q_tsv);

		valid_flag = 1;
		//if ( (pch != NULL) || ( q_tsv > q_value) ){ valid_flag = 0;}
		if ( ( q_tsv > q_value) || ( ( decoy_flag ) && ( pch != NULL ) ) )
		{ valid_flag = 0;}


		if ( valid_flag ) // q-value less than threshold
		{
			if ( 1 == file_type )
			{
				get_nth_word( match, readline, 10 );
				n_mod = get_msgf_peptide( match, pep_str);
			}
			else
			{
				get_nth_word( match, readline, 18 );
				copyToString( match, pep_str );
			}

			pep_flag = 0;
			it = cache_map.find( pep_str);
			if ( cache_map.end() != it )
			{	pep_flag = it->second; n_dup++;}
			else
			{
				if (	find_substring( pep_str, k_map ) ) {pep_flag = 4;}
				else if (	find_substring( pep_str, n_map ) ) {pep_flag = 2;}
				else if (	find_substring( pep_str, p_map ) ) {pep_flag = 1;}
				cache_map[pep_str] = pep_flag; 
			}

			if ( 1 == file_type )
			{	fprintf(out_fp, "%s\t%s\t-1\t%d\n", readline, pep_str.c_str(), pep_flag ); }
			else 
			{	fprintf(out_fp, "%s\t%d\n", readline, pep_flag ); }
			n_sig++;
		}
		count++;
		if ( 0 == count%10000){E(".");}
	}

	E("\n");
	E("Total/Significant/Duplicative Peptide Record\t%d\t%d\t%d\n", count, n_sig, n_dup);
	//E("Total References with Predictions:\t%d\n", (int)map_repeat.size());
	fclose(fp);
	fclose(out_fp);
	free(readline);
}


/**********************************************/
// Since a dta might be mapped to multiple peptides with equal scores,
// We need to keep only dta records which map just to aberrations
// ASSUMPTIONS: TSV are sorted by dta id!
void checkMultiHitTSV( char *tsv_file, char *out_file, float q_value, int decoy_flag )
{
	char *readline=(char*)malloc(MAX_LINE);
	char *match=(char*)malloc(MAX_LINE);
	char *dta_id=(char*)malloc(MAX_LINE);

	int count = 0;
	int offset =0;
	//uint32_t start = 0, end = 0,	score =0, rid = 0;
	
	int pep_flag = 0;
	int n_mod = 0;
	string pep_str="";

	int file_type = -1; // 1 for 17-column, 2 for 19-column. 0 otherwise
	int num_token;
	int pep_info = 0;
	float q_tsv = 0.0;
	const char *decoy_prefix = "XXX";
	char *pch;
	int valid_flag = 0; // valid 1 for passing all constraint including q_vlaue or decoy issues

	vector<string> vec_tmp; // buffer before 
	string tmp_str;

	map<string, int> cache_map; // A cache once we met some strings that we visit before
	map<string, int>::iterator it;
	int n_sig = 0, n_dup = 0;

	FILE *fp = fopen( tsv_file, "r");
	FILE *out_fp = fopen( out_file, "w");
	int ret;
	while( NULL != fgets(readline, MAX_LINE, fp) )
	{
		if ( -1 == file_type )
		{
			num_token = get_num_word( readline);
			file_type = 1;
			if ( 20 != num_token )
			{
				E("Abort: Format Inconsistency issue in MS-GF+ Search Results %s: %d columns detected\n", tsv_file, num_token);
				exit(1);
			}

		}

		//readline[strcspn(readline, "\n")] = 0; // Get Rid of Terminating New Line


		if ( 0 == strncmp("#", readline, 1))
		{
			fprintf( out_fp, "%s", readline );
			continue; 
		}

		get_nth_word( match, readline, 4);	
		if ( 0 != strncmp( dta_id, match, MAX_LINE)	)	// starting a new dta
		{
			if ( 0 < (int)vec_tmp.size() )
			{
				if ( pep_flag )
				{
					for( int i = 0; i < (int)vec_tmp.size(); i++ )
					{	fprintf( out_fp, "%s", vec_tmp[i].c_str() );}	// output all records
					n_sig += (int)vec_tmp.size();
				}
				else{ n_dup += (int)vec_tmp.size(); }
			}
			
			vec_tmp.clear();
			pep_flag = 1;
			strncpy( dta_id, match, MAX_LINE);

		}
		get_nth_word( match, readline, 11);
		pch = strstr( match, decoy_prefix);
		get_nth_word( match, readline, 16);	// q_tsv
		sscanf(match, "%f", &q_tsv);
		get_nth_word( match, readline, 20);	// pep_info
		sscanf(match, "%d", &pep_info);

		valid_flag = 1;
		//if ( (pch != NULL) || ( q_tsv > q_value) ){ valid_flag = 0;}
		if ( ( q_tsv > q_value) || ( ( decoy_flag ) && ( pch != NULL ) ) )
		{ valid_flag = 0;}


		if ( valid_flag ) // q-value less than threshold
		{	
			copyToString( readline, tmp_str ); // put into buffer before making decisions
			vec_tmp.push_back( tmp_str );
			//if ( 4 <= pep_info ){ pep_flag = 0; } // Once any record is a known peptide the dta should be discarded
			if ( (pch != NULL ) || (4 <= pep_info) ){ pep_flag = 0; } // Once any record is a known peptide or decoy then the dta should be discarded
		}
		count++;
		if ( 0 == count%10000){E(".");}
	}

	// Last Record
	if ( 0 < (int)vec_tmp.size() )
	{
		if ( pep_flag )
		{
			for( int i = 0; i < (int)vec_tmp.size(); i++ )
			{	fprintf( out_fp, "%s", vec_tmp[i].c_str() );}	// output all records
			n_sig += (int)vec_tmp.size();
		}
		else{ n_dup += (int)vec_tmp.size(); }
	}

	E("\n");
	E("Total/Significant/Discarded DTA Record\t%d\t%d\t%d\n", count, n_sig, n_dup);
	//E("Total References with Predictions:\t%d\n", (int)map_repeat.size());
	fclose(fp);
	fclose(out_fp);
	free(dta_id);
	free(match);
	free(readline);
}
// Step: Sketch the duplicative entry for remaining aberrant peptide candidate
// Unfortunately we need to get original FASTA file for double checking.
// REASON: We NO LONGER ignore sequence contents for long peptides
/**********************************************/
int getPepFreq( char *freq_file, map< int, pc_t > &map_freq )
{
	char *readline  = (char*)malloc(MAX_LINE);
	char *misc      = (char*)malloc(TOKEN_LENGTH);
	char *match     = (char*)malloc(MAX_LINE);

	int count = 0, id = 0, fix = 0, offset =0;
	pc_t pc_item;
	FILE *fp = fopen( freq_file, "r");
	int ret;//int flag_psm = 0;
	while( NULL != fgets(readline, MAX_LINE, fp) )
	{
		if ( 0 == strncmp("#", readline, 1)){ continue; }
		ret = sscanf( readline, "%d %s %u", &id, match, &pc_item.count );
		//L("Echo\t%d\t%s\t%u\n", id, match, pc_item.count);
		if ( ret == 3 )
		{
			copyToString( match, pc_item.pep );
			map_freq[id] = pc_item;
		}
		else
		{	E("Cannot recognize Peptide Frequency Format:%s", readline);	}
	}
	fclose(fp);
	E("Total DB Entries:\t%lu\n", map_freq.size());

	free(match);
	free(misc);
	free(readline);
	return 0;
}

/**********************************************/
// WARNING: HARD CODED FOR FIXED Peptide Prefix so far
// OUR CPTAC PURPOSE!!!
// For further analysis you need to be careful about the items you put in vec_id!
// 6: microSV peptide
// 5: fusion peptide
int getNumProtein(char *matched_pep, vector<int> &vec_id, int abe_type)
{
	char *tag = (char*)malloc(TOKEN_LENGTH);
	char c;
	int offset, of2;
	int pep_id = 0;
	int flag_dta = 0, flag_decoy = 0,
		flag_fusion = 0, flag_microsv = 0;

	int count = 0, ret = 0;
	vec_id.clear();
	//L("%s\n", matched_pep);
	while( *matched_pep )
	{
		ret = sscanf( matched_pep, "%[^\;]%c%n", tag, &c, &offset);
		//L("%d\t%s\t%s\n", ret, matched_pep, tag);
		if ( 1 == abe_type) // Fusion Only
		{
			if ( 0 == strncmp("F_", tag, 2))
			{	
				sscanf( tag, "F_%d(%n", &pep_id, &of2);
				vec_id.push_back( pep_id);
				//L("%d\n", pep_id  );
				flag_fusion = 1;	
			}
			else if ( 0 == strncmp("XXX", tag, 3))
			{	flag_decoy = 1;	}	
			else	// Stop whenever you see a non-fusion sequence. Bad sign.
			{	
				flag_fusion = 0;	
				break;
			}
		}
		else if ( 2 == abe_type )// microSV Only
		{
			if ( 0 == strncmp("SV_", tag, 3))
			{	
				sscanf( tag, "SV_%d(%n", &pep_id, &of2);
				vec_id.push_back( pep_id );
				//L("%d\n", pep_id  );
				flag_microsv = 1;	
			}
			else if ( 0 == strncmp("XXX", tag, 3))
			{	flag_decoy = 1;	}	
			else	// Stop whenever reading a non-sv sequence. Bad sign.
			{	
				flag_microsv = 0;	
				break;
			}
		}
		if ( 3 == abe_type) // We only need fusion and ignore others
		{
			if ( 0 == strncmp("F_", tag, 2))
			{	
				sscanf( tag, "F_%d(%n", &pep_id, &of2);
				vec_id.push_back( pep_id);
				//L("%d\n", pep_id  );
				flag_fusion = 1;	
			}
			else if ( 0 == strncmp("XXX", tag, 3))
			{	flag_decoy = 1;	}	
			else if ( 0 == strncmp("SV_", tag, 3)) // Put a mark on a dta if it can also be matched to other aberrations
			{	
				flag_microsv = 1;	
			}
		}
		else if ( 4 == abe_type )// microSV Only
		{
			if ( 0 == strncmp("SV_", tag, 3))
			{	
				sscanf( tag, "SV_%d(%n", &pep_id, &of2);
				vec_id.push_back( pep_id);
				//L("%d\n", pep_id  );
				flag_microsv = 1;	
			}
			else if ( 0 == strncmp("XXX", tag, 3))
			{	flag_decoy = 1;	}	
			else if ( 0 == strncmp("F_", tag, 2))	// Put a mark on a dta if it can also be matched to other aberrations
			{	
				flag_fusion = 1;	
			}
		}
		
		matched_pep += offset;
		if ( 2 != ret){break;} // for the last record without colon
	}
	// We keep PSM with only special prefix (F_ or CPTAC_ plus XXX)
	//if ( !flag_decoy ){flag_sv+=4;} // 5 is the desired values 
	flag_dta = 4*(1- flag_decoy) + 2*flag_microsv + 1*flag_fusion; // values >= 5 is the desired values
	return flag_dta;
	
}
/**********************************************/
// map_pep: index (0, 1, ...) to a peptide content
// map_abepep: a peptide content to the number of DTA it is detected. By WHY do we need it here?
uint32_t getNumEvent( const vector<int> &vec_id, const map<int, pc_t> &map_pep, map<string, int> &map_abepep )
{
	map<int, pc_t>::const_iterator it;
	uint32_t n_svp = 0;
	int limit = (int)vec_id.size();
	for ( int i = 0; i < limit; i++ )
	{
		it = map_pep.find( vec_id[i] );
		if ( it!=map_pep.end() )
		{
			//L("Check\t%d\t%s\t%u\n", vec_id[i], it->second.pep.c_str(), it->second.count);
			n_svp += it->second.count;
			// Below we collect all peptides which really matter from the selected TSVs
			if ( map_abepep.find(it->second.pep) == map_abepep.end())
			{	map_abepep[ it->second.pep] = 1;	}
			else{ map_abepep[it->second.pep]++; }
		}
		else{ E("Missing Compact Peptide ID %d\n", vec_id[i] );}
	}
	return n_svp;
}
// Experimental Only: to sketch the event statistics for PSMs
// abe_type: 1 for fusion, 2 for microSVs
/**********************************************/
void countEvent_in_TSV( char *tsv_file, char *out_file, const map<int, pc_t> &map_pep, map<string, int> &map_abepep, int abe_type )
{
	string total_seq="", total_rc="";

	char *readline=(char*)malloc(MAX_LINE);
	char *misc=(char*)malloc(TOKEN_LENGTH);
	char *match=(char*)malloc(MAX_LINE);
	char *m_pep=(char*)malloc(TOKEN_LENGTH);
	
	int count = 0,
		good_tsv = 0;
	int offset =0;
	uint32_t n_svp = 0; // total number of sv peptides;
	//uint32_t start = 0, end = 0, score =0, rid = 0;

	int n_mod = 0;
	string new_psm, new_pep;
	string pep_str;
	vector<int> vec_id;
	string new_ref;// formatted reference without chr prefix
	string repeat_info;// formatted reference without chr prefix

	int file_type = -1; // 1 for 17 column, 2 otherwise
	int num_token;


	FILE *fp = fopen( tsv_file, "r");
	FILE *out_fp = fopen( out_file, "w");
	
	int ret;
	int flag_psm = 0;
	while( NULL != fgets(readline, MAX_LINE, fp) )
	{
		if ( 0 > file_type)
		{
			num_token = get_num_word( readline);
			if ( 17 == num_token ){ file_type = 1;}
			else if ( 17 < num_token ){ file_type = 2;}
			else
			{
				E("Abort: Format Inconsistency issue in MS-GF+ Search Results %s: %d columns detected\n", tsv_file, num_token);
				exit(1);
			}
		}
		
		readline[strcspn(readline, "\n")] = 0; // Get Rid of Terminating New Line

		if ( 0 == strncmp("#", readline, 1))
		{ 
			if ( 1 == file_type )
			{       fprintf(out_fp, "%s\tRawPep\tDTAInfo\tPepInfo\tNumProt\tNumEvent\n", readline);}
			else
			{       fprintf(out_fp, "%s\tNumProt\tNumEvent\n", readline);}
			continue; 
		}

		get_nth_word( match, readline, 11 );
		flag_psm = getNumProtein( match, vec_id, abe_type );
		if ( 4 < flag_psm ) // The PSM contains no decoy, and only the types we want
		{
			// Calculate number of relevant Entries for the spectra
			n_svp = getNumEvent( vec_id, map_pep, map_abepep);
			
			if ( 1 == file_type )
			{
				get_nth_word( match, readline, 10 );
				n_mod = get_msgf_peptide( match, pep_str);
				fprintf(out_fp, "%s\t%s\t-1\t-1\t%d\t%d\n", readline, pep_str.c_str(), vec_id.size(), n_svp );
			}
			else
			{
				get_nth_word( match, readline, 18 );
				copyToString( match, pep_str );
				fprintf(out_fp, "%s\t%d\t%d\n", readline, vec_id.size(), n_svp );
			}	
			good_tsv++;
		} 
		count++;
	}
	E("Total/Good TSVRecord\t%d\t%d\n", count, good_tsv);
	fclose(fp);
	fclose(out_fp);
	free(m_pep);//	free(ref);
	free(misc);
	free(readline);
}


// abe_type: 1 for fusion, 2 for microSVs
/**********************************************/
void collectPep_in_TSV( char *tsv_file, char *out_file, const map<int, pc_t> &map_pep, map<string, int> &map_abepep, int abe_type )
{
	//string total_seq="", total_rc="";

	char *readline = (char*)malloc(MAX_LINE);
	char *match    = (char*)malloc(MAX_LINE);
	char *misc     = (char*)malloc(TOKEN_LENGTH);
	char *m_pep    = (char*)malloc(TOKEN_LENGTH);
	
	int count = 0,	good_tsv = 0,	offset =0;
	uint32_t n_svp = 0; // total number of sv peptides;

	int n_mod = 0;
	string new_psm, new_pep;
	string pep_str;
	vector<int> vec_id;
	string new_ref;		// formatted reference without chr prefix
	string repeat_info;	// formatted reference without chr prefix


	FILE *fp = fopen( tsv_file, "r");
	//FILE *out_fp = fopen( out_file, "w");
	
	int ret;
	int flag_psm = 0;
	while( NULL != fgets(readline, MAX_LINE, fp) )
	{
		//if ( 0 > file_type)
		//{
		//	num_token = get_num_word( readline);
		//	if ( 17 == num_token ){ file_type = 1;}
		//	else if ( 17 < num_token ){ file_type = 2;}
		//	else
		//	{
		//		E("Abort: Format Inconsistency issue in MS-GF+ Search Results %s: %d columns detected\n", tsv_file, num_token);
		//		exit(1);
		//	}
		//}
		//
		//readline[strcspn(readline, "\n")] = 0; // Get Rid of Terminating New Line

		//if ( 0 == strncmp("#", readline, 1))
		//{ 
		//	if ( 1 == file_type )
		//	{       fprintf(out_fp, "%s\tRawPep\tDTAInfo\tPepInfo\tNumProt\tNumEvent\n", readline);}
		//	else
		//	{       fprintf(out_fp, "%s\tNumProt\tNumEvent\n", readline);}
		//	continue; 
		//}

		get_nth_word( match, readline, 11 );
		flag_psm = getNumProtein( match, vec_id, abe_type );
		if ( 4 < flag_psm ) // The PSM contains no decoy, and only the types we want
		{
			//if ( 6 != flag_psm){L("Inconsistent Protein Header in \t%s\n", readline);}
			// Collecting Information for Important Peptides
			// Calculate number of relevant Entries for the spectra
			n_svp = getNumEvent( vec_id, map_pep, map_abepep);	
			good_tsv++;
		} 
		count++;
	}
	E("Total/Good TSVRecord\t%d\t%d\n", count, good_tsv);
	fclose(fp);
	//fclose(out_fp);
	free(m_pep);//	free(ref);
	free(misc);
	free(readline);
}



// Extract Event List for each spectra
// abe_type: 1 for fusion, 2 for microSVs
// map_pep: from protein header to actual protein sequence
// map_abepep: collection of all important sequences
// map_event: from protein sequence to a list of event id
/**********************************************/
void getFusionEvent_in_TSV( char *seq_file, char *out_file, const map<string, int> &map_abepep, map<string, vector<string> > &map_event, int abe_type )
{
	char *readline=(char*)malloc(MAX_LINE);
	char *match=(char*)malloc(MAX_LINE);
	
	int count = 0,
		good_tsv = 0;
	int offset =0;
	uint32_t n_svp = 0; // total number of sv peptides;

	string pep_id = "", pep_str = "";

	//map<string, int>::const_iterator pit; // peptide iterator
	map<string, vector<string> >::iterator it; // event iterator

	FILE *fp = fopen( seq_file, "r");
	
	char *log_file=(char*)malloc(TOKEN_LENGTH);
	strcpy( log_file, out_file);
	strcat( log_file, ".info");
	FILE *out_fp = fopen( log_file, "w");
	
	int ret;
	while( NULL != fgets(readline, MAX_LINE, fp) )
	{
		readline[strcspn(readline, "\n")] = 0; // Get Rid of Terminating New Line
		if( 0 == strncmp(">", readline, 1 ) )
		{
			if ( 0 < (int)pep_str.size() ) // actual entry
			{
				//L("Target_%s_%s_\n", pep_id.c_str(), pep_str.c_str());
				if ( map_abepep.end() != map_abepep.find(pep_str) )
				{
					it = map_event.find( pep_str);
					if ( map_event.end() == it ) // add new entry
					{
						vector<string> tmp_vec;
						tmp_vec.push_back( pep_id );
						map_event[pep_str] = tmp_vec;
					}
					else {	map_event[pep_str].push_back(pep_id);	}
				}
				pep_str.clear();
			}

			ret = sscanf(readline, ">%s", match);
			copyToString(match, pep_id);
		}
		else{ copyToString( readline, pep_str);}

		count++;
		if (!(count%500000)) {E(".");}
	}
	// Last Entry
	if ( 0 < (int)pep_str.size() ) // actual entry
	{
		//L("Target_%s_%s_\n", pep_id.c_str(), pep_str.c_str());
		if ( map_abepep.end() != map_abepep.find(pep_str) )
		{
			it = map_event.find( pep_str);
			if ( map_event.end() == it ) // add new entry
			{
				vector<string> tmp_vec;
				tmp_vec.push_back( pep_id );
				map_event[pep_str] = tmp_vec;
			}
			else {	map_event[pep_str].push_back(pep_id);	}
		}
		pep_str.clear();
	}

	E("\nTotal/Good TSVRecord\t%d\t%d\n", count, good_tsv);
	
	fclose(fp);
	for( it = map_event.begin(); it != map_event.end(); it++){ fprintf(out_fp, "%s\t%ld\n", it->first.c_str(), it->second.size());}
	fclose(out_fp);
	free(log_file);
	free(readline);
}


// Extract Event List for each spectra
// abe_type: 1 for fusion, 2 for microSVs
// map_pep: from protein header to actual protein sequence
// map_abepep: collection of all important sequences
// map_event: from protein sequence to a list of event id
/**********************************************/
void collectSVInfo_in_TSV_0( char *pep_file, char *out_file, const map<string, int> &map_abepep )
{
	char *readline=(char*)malloc(MAX_LINE);
	char *match=(char*)malloc(MAX_LINE);
	
	int count = 0,
		good_tsv = 0;
	int offset =0;
	uint32_t n_svp = 0; // total number of sv peptides;

	string new_pep = "";

	map<string, int >::const_iterator it; // peptide iterator

	FILE *fp = fopen( pep_file, "r");
	
	char *log_file=(char*)malloc(TOKEN_LENGTH);
	strcpy( log_file, out_file);
	strcat( log_file, ".pepinfo");
	FILE *out_fp = fopen( log_file, "w");
	
	int ret;
	while( NULL != fgets(readline, MAX_LINE, fp) )
	{
		if ( 0 == strncmp("#", readline, 1)){ continue; }
		if ( get_nth_word( match, readline, 25) )
		{
			copyToString( match, new_pep);
			it = map_abepep.find( new_pep );
			if ( it != map_abepep.end() )
			{
				fprintf( out_fp, "%s", readline);
			}
		}
		count++;
		if ( 0 == count%100000){E(".");}
	}

	E("\nTotal/Good TSVRecord\t%d\t%d\n", count, good_tsv);
	
	fclose(fp);
	fclose(out_fp);
	free(log_file);
	free(readline);
}
/**********************************************/
void collectSVInfo_in_TSV1( char *pep_file, char *out_file, const map<string, int> &map_abepep, map<string, vector<string> > &map_event )
{
	char *readline  = (char*)malloc(MAX_LINE);
	char *match     = (char*)malloc(MAX_LINE);
	
	int count = 0,	good_tsv = 0,	offset =0;
	uint32_t n_svp = 0; // total number of sv peptides;

	string new_pep = "";
	string new_str = "";
	map<string, int >::const_iterator it; // peptide iterator
	map<string, vector<string> >::iterator e_it; // peptide iterator

	
	char *log_file=(char*)malloc(TOKEN_LENGTH);
	strcpy( log_file, out_file);
	strcat( log_file, ".svinfo");
	FILE *out_fp = fopen( log_file, "w");
	
	int ret;
	FILE *fp = fopen( pep_file, "r");
	while( NULL != fgets(readline, MAX_LINE, fp) )
	{
		if ( 0 == strncmp("#", readline, 1)){ continue; }
		if ( get_nth_word( match, readline, 25) )
		{
			copyToString( match, new_pep);
			copyToString( readline, new_str);
			it = map_abepep.find( new_pep );
			if ( it != map_abepep.end() ) // Only care those peptides relevant to interested dta
			{
				fprintf( out_fp, "%s", readline);
				e_it = map_event.find( new_pep);
				if ( e_it != map_event.end() )
				{
					map_event[new_pep].push_back( new_str);
				}
				else
				{
					vector<string> tmp_vec;
					tmp_vec.push_back(new_str);
					map_event[new_pep] = tmp_vec;
				}
			}
		}
		count++;
		if ( 0 == count%100000){E(".");}
	}

	E("\nTotal/Good TSVRecord\t%d\t%d\n", count, good_tsv);
	fclose(fp);
	fclose(out_fp);

	char *info_file=(char*)malloc(TOKEN_LENGTH);
	strcpy( info_file, out_file);
	strcat( info_file, ".pepinfo");
	FILE *info_fp = fopen( info_file, "w");
	for( e_it = map_event.begin(); e_it != map_event.end(); e_it++){ fprintf(info_fp, "%s\t%ld\n", e_it->first.c_str(), e_it->second.size());}
	fclose(info_fp);

	free(log_file);
	free(info_file);
	free(readline);
}
// map_event: peptide sequences -> vector of information from peptable
// for example, see new_pepinfo for peptide and single entry
/**********************************************/
void collectSVInfo_in_TSV( char *peptable, char *pep_file, char *out_file, const map<string, int> &map_abepep, map<string, vector<string> > &map_event )
{
	char *readline  = (char*)malloc(MAX_LINE);
	char *match     = (char*)malloc(MAX_LINE);
	char *token1    = (char*)malloc(TOKEN_LENGTH);
	char *token2    = (char*)malloc(TOKEN_LENGTH);
	
	int count = 0,	good_tsv = 0,	offset =0;
	uint32_t n_svp = 0; // total number of sv peptides;

	string new_pep = "", new_str = "", id_str = "", seq_str="";
	map<string, int >::const_iterator it; // peptide iterator
	map<string, vector<string> >::iterator e_it; // peptide iterator

	
	char *log_file=(char*)malloc(TOKEN_LENGTH);
	strcpy( log_file, out_file);
	strcat( log_file, ".svinfo");
	FILE *out_fp = fopen( log_file, "w");
	
	// Load peptide file with header patient_id
	map<string, string> map_pepstr;
	LoadFastaShort( pep_file, map_pepstr, 64);
	map<string, string>::iterator pep_it; // peptide iterator


	int ret;
	FILE *fp = fopen( peptable, "r");
	while( NULL != fgets(readline, MAX_LINE, fp) )
	{
		if ( 0 == strncmp("#", readline, 1)){ continue; }
		sscanf( readline, "%s %s %d", token1, token2, &offset);
		copyToString( token1, id_str);
		attachToString( "_", id_str);
		attachToString( token2, id_str);

		pep_it = map_pepstr.find( id_str );
		new_pep = pep_it->second;
		it = map_abepep.find( new_pep );
		if ( it != map_abepep.end() ) // Only care those peptides relevant to interested dta
		{
			copyToString( readline, new_str);
			//fprintf( out_fp, "%s\t%s", new_pep.c_str(), new_str.c_str() );
			
			e_it = map_event.find( new_pep);
			if ( e_it != map_event.end() )
			{
				map_event[new_pep].push_back( new_str);
			}
			else
			{
				vector<string> tmp_vec;
				tmp_vec.push_back(new_str);
				map_event[new_pep] = tmp_vec;
			}
		}
		count++;
		if ( 0 == count%100000){E(".");}
	}

	E("\nTotal/Good TSVRecord\t%d\t%d\n", count, good_tsv);
	fclose(fp);
	fclose(out_fp);

	char *info_file=(char*)malloc(TOKEN_LENGTH);
	strcpy( info_file, out_file);
	strcat( info_file, ".pepinfo");
	FILE *info_fp = fopen( info_file, "w");
	for( e_it = map_event.begin(); e_it != map_event.end(); e_it++){ fprintf(info_fp, "%s\t%ld\n", e_it->first.c_str(), e_it->second.size());}
	fclose(info_fp);

	free(log_file);
	free(info_file);
	free(readline);
}

/**********************************************/
void getDTAEvent_in_TSV_0( char *tsv_file, char *out_file, const map<int, pc_t> &map_pep, const map<string, vector<string> > &map_event, int abe_type )
{
	char *readline=(char*)malloc(MAX_LINE);
	char *misc=(char*)malloc(TOKEN_LENGTH);
	char *match=(char*)malloc(MAX_LINE);
	char *m_pep=(char*)malloc(TOKEN_LENGTH);
	
	int count = 0,
		good_tsv = 0;
	int offset =0;
	uint32_t n_svp = 0; // total number of sv peptides;

	int n_mod = 0;
	string pep_str;
	vector<int> vec_id;
	//string new_psm, new_pep;
	//string pep_str;
	//vector<int> vec_id;
	//string new_ref;// formatted reference without chr prefix
	//string repeat_info;// formatted reference without chr prefix

	int file_type = -1; // 1 for 17 column, 2 otherwise
	int num_token;


	FILE *fp = fopen( tsv_file, "r");
	FILE *out_fp = fopen( out_file, "w");

	map<int, pc_t>::const_iterator pit;// iterator for protein id
	string seq; // sequence for accessing map_event
	map<string, vector<string> >::const_iterator it; // iterator for peptide sequence
	int ret;
	int flag_psm = 0;

	while( NULL != fgets(readline, MAX_LINE, fp) )
	{
		if ( 0 > file_type )
		{
			num_token = get_num_word( readline);
			if ( 17 == num_token ){ file_type = 1;}
			else if ( 17 < num_token ){ file_type = 2;}
			else
			{
				E("Abort: Format Inconsistency issue in MS-GF+ Search Results %s: %d columns detected\n", tsv_file, num_token);
				exit(1);
			}
		}
		
		readline[strcspn(readline, "\n")] = 0; // Get Rid of Terminating New Line

		if ( 0 == strncmp("#", readline, 1))
		{ 
			if ( 1 == file_type )
			{       fprintf(out_fp, "%s\tRawPep\tDTAInfo\tPepInfo\tProtSeq\tEvent\n", readline);}
			else
			{       fprintf(out_fp, "%s\tProtSeq\tEvent\n", readline);}
			continue; 
		}

		get_nth_word( match, readline, 11 );
		flag_psm = getNumProtein( match, vec_id, abe_type );
		n_svp = 0;
		if ( 4 < flag_psm ) // The PSM contains no decoy, and only the types we want
		{
			L("=== New DTA ====\n");
			for( int i = 0; i < (int)vec_id.size(); i ++ )
			{ 
				pit = map_pep.find( vec_id[i]);
				if ( map_pep.end() != pit )
				{
					seq = pit->second.pep;
					it = map_event.find( seq );
					if ( map_event.end() != it )
					{
						L( "=== New Prot %s ====\n", seq.c_str() );
						n_svp += (int)it->second.size();
						for( int j =0; j <(int)it->second.size(); j++)
						{
						fprintf(out_fp, "%s\t%s\t%s\n", readline, seq.c_str(), it->second[j].c_str() );
						}
					}
					else{ E("Error: missing Peptide %s\n", seq.c_str()) ; }
				}
				else{ E("Error: missing sequence ID %d\n", vec_id[i]);}
			}
			
			//if ( 1 == file_type )
			//{
			//	get_nth_word( match, readline, 10 );
			//	n_mod = get_msgf_peptide( match, pep_str);
			//	fprintf(out_fp, "%s\t%s\t-1\t-1\t%d\t%d\n", readline, pep_str.c_str(), vec_id.size(), n_svp );
			//}
			//else
			//{
			//	get_nth_word( match, readline, 18 );
			//	copyToString( match, pep_str );
			//	fprintf(out_fp, "%s\t%d\t%d\n", readline, vec_id.size(), n_svp );
			//}	
			good_tsv++;
		} 
		count++;
	}
	E("Total/Good TSVRecord\t%d\t%d\n", count, good_tsv);
	fclose(fp);
	fclose(out_fp);
	free(m_pep);//	free(ref);
	free(misc);
	free(readline);
}
/**********************************************/
//each string is Pat_ID_Strand_ORF_bBP_sSTOP_pX-Y
int IsDupFusion(const string &f1, const string &f2 )
{
	int flag = 0;// not identical
	char *pat1=(char*)malloc(TOKEN_LENGTH);
	char *pat2=(char*)malloc(TOKEN_LENGTH);
	char *id1=(char*)malloc(TOKEN_LENGTH);
	char *id2=(char*)malloc(TOKEN_LENGTH);
	char *s_str=(char*)malloc(TOKEN_LENGTH);
	char *e_str=(char*)malloc(TOKEN_LENGTH);

	char *misc=(char*)malloc(MAX_LINE);
	char c;
	char ori1, ori2, rf1, rf2;
	
	int s_1 =0, e_1 = 0 ;
	int s_2 =0, e_2 = 0 ;
	int ol_length = 0;

	int ret1=0, ret2 =0;
	ret1 = sscanf( f1.c_str(),"%[^_]_%[^_]_%c_%c_%[^_]_%[^_]_%c%[^-]-%s", pat1, id1, &ori1, &rf1, misc, misc, &c, s_str, e_str );
	sscanf( s_str, "%d", &s_1 );
	sscanf( e_str, "%d", &e_1 );
	ret2 = sscanf( f2.c_str(),"%[^_]_%[^_]_%c_%c_%[^_]_%[^_]_%c%[^-]-%s", pat2, id2, &ori2, &rf2, misc, misc, &c, s_str, e_str );
	sscanf( s_str, "%d", &s_2 );
	sscanf( e_str, "%d", &e_2 );

	//L("NEW %s %s %d %d\tPAT %s %s %d %d\n", pat1, id1, s_1, e_1, pat2, id2, s_2, e_2);
	ol_length = overlap_int( s_1, e_1, s_2, e_2);
	if ( !strncmp( pat1, pat2, TOKEN_LENGTH) && !strncmp(id1, id2, TOKEN_LENGTH) && ( ori1==ori2 ) && (rf1 == rf2 ) && ol_length )
	{	//L("HIT %d\t%s\t%s\n", ol_length, f1.c_str(), f2.c_str() );	
		flag = 1;
	}

	free(pat1);
	free(pat2);
	free(id1);
	free(id2);
	free(s_str);
	free(e_str);
	free(misc);
	return flag;
}
/**********************************************/
// Hardcoded for now. Terrible!
int IsCorrectMix(const string &f1, const char *mix )
{
	int flag = 0;// f1 does not come not mix

	string mix_str=string(mix);
	string pat_str= f1.substr(0,4);
	
	if ( string::npos != mix_str.find(pat_str) )
	{
		//L("MIX\t%s %s\n", f1.c_str(), mix);
		flag++;
	}

	mix_str.clear();
	pat_str.clear();
	return flag;
}
/**********************************************/
// Hardcoded for now. Terrible!
// 1 for normal SV in the mix; 3 for cancer SV
int IsCorrectSVMix(const string &f1, const char *mix )
{
	int flag = 0;// f1 does not come not mix

	char *match=(char*)malloc(MAX_LINE);
	get_nth_word( match, f1.c_str(), 1 ); // sth like A09I-01A
	string tmp_f;
	copyToString(match, tmp_f);

	string mix_str=string(mix);
	string pat_str= tmp_f.substr(0,4);

	//L("WHAT%s\n", f1.c_str());
	//L("PAT %ld %s\n", tmp_f.size(), tmp_f.c_str());
	if ( string::npos != mix_str.find(pat_str) )
	{
		//L("MIX\t%s %s\n", f1.c_str(), mix);
		flag++;
	}

	// Events from Tumor Sample gets flag bit 2 beingset
	if ( (flag) && ("01"== tmp_f.substr(5,2) ) ){ flag +=2;}

	L("DetectSV: %s %s %d\n", tmp_f.c_str(), mix_str.c_str(), flag);

	mix_str.clear();
	pat_str.clear();
	return flag;
}
/**********************************************/
int augment_fusion_list_0( vector<string> &pat_vec, const vector<string> &vec_tmp)
{
	int n_token = (int)vec_tmp.size();
	int limit = pat_vec.size();
	int flag_dup = 0;
	for ( int i = 0; i < n_token; i++)
	{
		flag_dup = 0;
		for( int j = 0; j < limit; j++ )
		{
			//L("CMP %d_%d %s %s\n", i, j, vec_tmp[i].c_str(), pat_vec[j].c_str());
			if ( IsDupFusion( vec_tmp[i], pat_vec[j]))
			{
				flag_dup = 1;
				break;
			}
		}

		if ( !flag_dup){	pat_vec.push_back( vec_tmp[i] );	}
	}
	return 1;
}
/**********************************************/
int augment_fusion_list( vector<string> &pat_vec, const vector<string> &vec_tmp, vector<int> &flag_vec, const char *mix)
{
	int diff = 0;

	int n_token = (int)vec_tmp.size();
	int limit = pat_vec.size();
	int flag_dup = 0;
	int flag_mix = 0; // If the event is from correct mixture
	for ( int i = 0; i < n_token; i++)
	{
		flag_dup = 0;
		for( int j = 0; j < limit; j++ )// all-vs-all? stupid. Need to do something to speed up
		{
			//L("CMP %d_%d %s %s\n", i, j, vec_tmp[i].c_str(), pat_vec[j].c_str());
			if ( IsDupFusion( vec_tmp[i], pat_vec[j]))
			{
				flag_dup = 1;
				break;
			}
		}

		if ( !flag_dup)
		{	
			pat_vec.push_back( vec_tmp[i] );	
			diff++;	
			flag_mix = IsCorrectMix( vec_tmp[i], mix);
			flag_vec.push_back(flag_mix);
		}
	}
	return diff; // Number of elements added
}
// Let's try to remove duplicative items
// For results from --checkpepseq
/**********************************************/
void getDTAEvent_in_TSV( char *tsv_file, char *out_file, const map<int, pc_t> &map_pep, const map<string, vector<string> > &map_event, int abe_type, int mix_type )
{
	char *readline  = (char*)malloc(MAX_LINE);
	char *match     = (char*)malloc(MAX_LINE);
	char *misc      = (char*)malloc(TOKEN_LENGTH);
	char *mix       = (char*)malloc(TOKEN_LENGTH);
	char *m_pep     = (char*)malloc(TOKEN_LENGTH);
	
	int count = 0,	good_tsv = 0,	offset =0;
	uint32_t n_svp = 0; // total number of sv peptides;

	int n_mod = 0;
	string pep_str;
	vector<int> vec_id;

	int file_type = -1; // 1 for 17 column, 2 otherwise
	int num_token;

	vector<string> event_vec;
	vector<string> seq_vec; // for printing protein sequence
	int seq_idx,
		n_add;	// number of elements added
	vector<int> prot_vec; // idx for merged event list;
	vector<int> flag_vec; // flag for each entry;
	int s_flag = 0, 	// summary for single entry
		final_flag 	= 0, // summary based on all entries in flag_vec
		othermix_flag 	= 0,
		samemix_flag	= 0;

	FILE *fp = fopen( tsv_file, "r");
	FILE *out_fp = fopen( out_file, "w");

	char *log_file=(char*)malloc(TOKEN_LENGTH);
	strcpy(log_file, out_file);
	strcat(log_file, ".alltsv");
	FILE *log_fp = fopen( log_file, "w");

	map<int, pc_t>::const_iterator pit;// iterator for protein id
	string seq; // sequence for accessing map_event
	map<string, vector<string> >::const_iterator it; // iterator for peptide sequence
	int ret;
	int flag_psm = 0;

	dh_t tmp_event; // storing fusion header information
	int event_flag = 0;

	while( NULL != fgets(readline, MAX_LINE, fp) )
	{
		if ( 0 > file_type )// Assure that we will not have invalid access problems
		{
			num_token = get_num_word( readline);
			if ( 17 == num_token ){ file_type = 1;}
			else if ( 17 < num_token ){ file_type = 2;}
			else
			{
				E("Abort: Format Inconsistency issue in MS-GF+ Search Results %s: %d columns detected\n", tsv_file, num_token);
				exit(1);
			}
		}
		
		readline[strcspn(readline, "\n")] = 0; // Get Rid of Terminating New Line

		if ( 0 == strncmp("#", readline, 1))
		{ 
			if ( 1 == file_type )
			{      
				fprintf(out_fp, "%s\tRawPep\tDTAInfo\tPepInfo\tProtSeq\tEventID\tMixInfo\t", readline);
				fprintf(out_fp, "PSMInfo\tPatient\tID\tBP\tSTOP\tP_Start\tP_End\n" );
				fprintf(log_fp, "%s\tRawPep\tDTAInfo\tPepInfo\tProtSeq\tEventID\tMixInfo\n", readline);
			} 
			else
			{       
				fprintf(out_fp, "%s\tProtSeq\tEventID\tMixInfo\t", readline);
				fprintf(out_fp, "PSMInfo\tPatient\tID\tBP\tSTOP\tP_Start\tP_End\n" );
				fprintf(log_fp, "%s\tProtSeq\tEventID\tMixInfo\n", readline);
			}
			continue; 
		}

		get_nth_word( mix, readline, 1 ); // For detecting same sample or not
		get_nth_word( match, readline, 11 );
		flag_psm = getNumProtein( match, vec_id, abe_type );
		n_svp = 0;
		event_vec.clear();
		seq_vec.clear();	seq_idx = -1;
		prot_vec.clear();
		flag_vec.clear();
		n_add = 0;

		// Ok. Two Questions need to be answered. 
		// First, can it bematched to at least one patients? 
		// Second, how can we only output the corresponding one?


		// For future analysis, we need to maintain two attributes whenever adding new elements
		// which protein is this
		// Is this one the correct mixture/patient
		if ( 4 < flag_psm ) // The PSM contains no decoy, and only the types we want
		{
			//L("===NewDTA===\n");
			for( int i = 0; i < (int)vec_id.size(); i ++ )
			{ 
				pit = map_pep.find( vec_id[i]);
				if ( map_pep.end() != pit )
				{
					seq = pit->second.pep;
					seq_vec.push_back(seq);
					seq_idx++; // Event in this round connects to seq_vec[seq_idx];

					it = map_event.find( seq );
					if ( map_event.end() != it )
					{
						L( "===NewProt\t%d\t%s===\n", i, seq.c_str() );
						n_svp += (int)it->second.size();
						n_add = augment_fusion_list( event_vec, it->second, flag_vec, mix );
						for (int k = 0; k < n_add; k++){ prot_vec.push_back(seq_idx);}
						//for( int j =0; j <(int)it->second.size(); j++)
						//{	fprintf(out_fp, "%s\t%s\t%s\n", readline, seq.c_str(), it->second[j].c_str() );		}
					}
					else{ E("Error: missing Peptide %s\n", seq.c_str()) ; }
				}
				else{ E("Error: missing sequence ID %d\n", vec_id[i]);}
			}
			//L("=== SANITY %ld %ld %ld===\n", event_vec.size(), prot_vec.size(), flag_vec.size());
			//L("=== END DTA %ld in %d ===\n", event_vec.size(), n_svp);	
			
			final_flag 	= 0;	othermix_flag 	= 0;	samemix_flag	= 0;
			for(int k = 0; k < (int)flag_vec.size(); k++)
			{
				if ( flag_vec[k])	{	samemix_flag = 1;	}
				else	{ othermix_flag = 1;}
			}


			for(int k = 0; k < (int)flag_vec.size(); k++)
			{
				final_flag = 4*flag_vec[k] + 2*(1-othermix_flag ) + samemix_flag;
				if ( final_flag && (!mix_type || flag_vec[k]) ) // Output when mix_type == 0 or flag == 1
				{
					if ( 1 == file_type )
					{
						get_nth_word( match, readline, 10 );
						n_mod = get_msgf_peptide( match, pep_str);
						event_flag = parse_defuse_match( event_vec[k], seq_vec[ prot_vec[k] ], pep_str, tmp_event);
						fprintf(out_fp, "%s\t%s\t-1\t-1\t%s\t%s\t%d\t", readline, pep_str.c_str(), seq_vec[ prot_vec[k] ].c_str(),  event_vec[k].c_str(), final_flag);
						fprintf(out_fp, "%d\t%s\t%d\t%d\t%d\t%d\t%d\n", event_flag, tmp_event.pat.c_str(), tmp_event.id, tmp_event.bp, tmp_event.stop, tmp_event.s, tmp_event.e);
					}
					else
					{
						get_nth_word( match, readline, 18 );
						copyToString( match, pep_str );
						event_flag = parse_defuse_match( event_vec[k], seq_vec[ prot_vec[k] ], pep_str, tmp_event);
						fprintf(out_fp, "%s\t%s\t%s\t%d\t", readline, seq_vec[ prot_vec[k] ].c_str(), event_vec[k].c_str(), final_flag);	
						fprintf(out_fp, "%d\t%s\t%d\t%d\t%d\t%d\t%d\n", event_flag, tmp_event.pat.c_str(), tmp_event.id, tmp_event.bp, tmp_event.stop, tmp_event.s, tmp_event.e);
					}
				}
			}

			// Summary for matchinfo
			s_flag = 2*(1-othermix_flag ) + samemix_flag;
			if ( 1 == file_type )
			{
				get_nth_word( match, readline, 10 );
				n_mod = get_msgf_peptide( match, pep_str);
				fprintf(log_fp, "%s\t%s\t-1\t-1\tNA\tNA\t%d\n", readline, pep_str.c_str(), s_flag);
			}
			else
			{	fprintf(log_fp, "%s\tNA\tNA\t%d\n", readline,  s_flag);	}
			good_tsv++;			
		} 
		count++;
	}
	E("Total/Good TSVRecord\t%d\t%d\n", count, good_tsv);

	flag_vec.clear();
	prot_vec.clear();
	seq_vec.clear();
	event_vec.clear();

	fclose(fp);
	fclose(out_fp);
	fclose(log_fp);
	free(log_file);//	free(ref);
	free(m_pep);//	free(ref);
	free(misc);
	free(mix);
	free(readline);
}

/**********************************************/
// We have to parse later ...
int augment_sv_list( vector<string> &pat_vec, const vector<string> &vec_tmp, vector<int> &flag_vec, const char *mix)
{
	int diff = 0;

	int n_token = (int)vec_tmp.size(); // to be added
	int limit = pat_vec.size();	//existing
	int flag_dup = 0;
	int flag_mix = 0; // If the event is from correct mixture
	for ( int i = 0; i < n_token; i++)
	{
		flag_dup = 0;
		//for( int j = 0; j < limit; j++ )// all-vs-all? stupid. Need to do something to speed up
		//{
		//	L("CMP %d_%d %s %s\n", i, j, vec_tmp[i].c_str(), pat_vec[j].c_str());
		//	//if ( IsDupFusion( vec_tmp[i], pat_vec[j]))
		//	//{
		//	//	flag_dup = 1;
		//	//	break;
		//	//}
		//}

		if ( !flag_dup )
		{	
			pat_vec.push_back( vec_tmp[i] );	
			diff++;	
			flag_mix = IsCorrectSVMix( vec_tmp[i], mix);	//A09I-01A v.s. TCGA_A8-A09I_
			//L("Results of %s %s: %d", vec_tmp[i].c_str(), mix, flag_mix);
			flag_vec.push_back(flag_mix);
		}
	}
	return diff; // Number of elements added
}
/**********************************************/
int clear_mht( mh_t &t1)
{
	t1.pat = "";
	t1.id = 0;	t1.ol=0;
	t1.lp = 0; t1.rp=0;
	t1.s = 0; t1.e = 0;
	t1.ori = 0; t1.rf = 0;
	t1.strand='\0';
	t1.l_b4 = 0; t1.l_sv=0;
	t1.sv_type= "";
	t1.gid = "";
	t1.tid="";
	t1.ref = "";
	t1.pos = 0; t1.ps = 0; t1.pe = 0;
}
/**********************************************/
int copy_mht( mh_t &t1, const mh_t &t2)
{
	t1.pat = t2.pat;
	t1.id = t2.id;	t1.ol=t2.id;
	t1.lp = t2.lp; t1.rp=t2.rp;
	t1.s = t2.s; t1.e = t2.e;
	t1.ori = t2.ori; t1.rf = t2.rf;
	t1.strand=t2.strand;
	t1.l_b4 = t2.l_b4;
	t1.l_sv = t2.l_sv;
	t1.sv_type=t2.sv_type;
	t1.gid = t2.gid;
	t1.tid=t2.tid;
	t1.ref = t2.ref;
	t1.pos = t2.pos; t1.ps = t2.ps; t1.pe = t1.pe;
}
// Note:
// map_pep matches from SV_ID to peptide contents
// map_event matches from peptide SEQUENCE to its information
/**********************************************/
void getDTA_SVEvent_in_TSV( char *tsv_file, char *out_file, const map<int, pc_t> &map_pep, const map<string, vector<string> > &map_event, int abe_type, int mix_type )
{
	char *readline = (char*)malloc(MAX_LINE);
	char *match    = (char*)malloc(MAX_LINE);
	char *misc     = (char*)malloc(TOKEN_LENGTH);
	char *mix      = (char*)malloc(TOKEN_LENGTH);
	char *m_pep    = (char*)malloc(TOKEN_LENGTH);
	
	int count = 0,	good_tsv = 0,	offset =0;
	uint32_t n_svp = 0; // total number of sv peptides;

	int n_mod = 0;
	string pep_str;
	vector<int> vec_id;

	int file_type = -1; // 1 for 17 column, 2 otherwise
	int num_token;

	vector<string> event_vec;
	vector<string> seq_vec; // for printing protein sequence
	int seq_idx,
		n_add;	// number of elements added
	vector<int> prot_vec; // idx for merged event list;
	vector<int> flag_vec; // flag for each entry;
	int s_flag = 0, 	// summary for single entry
		final_flag 	= 0, // summary based on all entries in flag_vec
		othermix_flag 	= 0,
		samemix_flag	= 0;

	FILE *fp = fopen( tsv_file, "r");
	FILE *out_fp = fopen( out_file, "w");


	string log_file = add_extension( out_file, "alltsv");
	FILE *log_fp = fopen( log_file.c_str(), "w");

	map<int, pc_t>::const_iterator pit;// iterator for protein id
	string seq; // sequence for accessing map_event,or field 21 ProtSeq
	map<string, vector<string> >::const_iterator it; // iterator for peptide sequence
	int ret;
	int flag_psm = 0;

	mh_t cur_event,	// the one currently written to file
		tmp_event; // storing fusion header information
	int event_flag = 0;

	while( NULL != fgets(readline, MAX_LINE, fp) )
	{
		if ( 0 > file_type )// Assure that we will not have invalid access problems
		{
			num_token = get_num_word( readline);
			if ( 17 == num_token ){ file_type = 1;}
			//else if ( 17 < num_token ){ file_type = 2;}
			else
			{
				E("Abort: Format Inconsistency issue in MS-GF+ Search Results %s: %d columns detected\n", tsv_file, num_token);
				exit(1);
			}
		}
		
		readline[strcspn(readline, "\n")] = 0; // Get Rid of Terminating New Line

		if ( 0 == strncmp("#", readline, 1))
		{ 
			if ( 1 == file_type )
			{      
				fprintf(out_fp, "%s\tRawPep\tDTAInfo\tPepInfo\tProtSeq\tEventID\tMixInfo\t", readline);
				fprintf(out_fp, "PSMInfo\tPatient\tID\tOVERLAP\tLEFT\tRIGHT\tP_Start\tP_End\t" );
				fprintf(out_fp, "SV_Type\tRef\tPOS\tPOS_S\tPOS_E\tL_Before\tL_SV\tGENE\tISOFORM\t\n" );
				fprintf(log_fp, "%s\tRawPep\tDTAInfo\tPepInfo\tProtSeq\tEventID\tMixInfo\n", readline);
			} 
			else
			{       
				fprintf(out_fp, "%s\tProtSeq\tEventID\tMixInfo\t", readline);
				fprintf(out_fp, "PSMInfo\tPatient\tID\tOVERLAP\tLEFT\tRIGHT\tP_Start\tP_End\t" );
				fprintf(out_fp, "SV_Type\tRef\tPOS\tPOS_S\tPOS_E\tL_Before\tL_SV\tGENE\tISOFORM\t\n" );
				fprintf(log_fp, "%s\tProtSeq\tEventID\tMixInfo\n", readline);
			}
			continue; 
		}

		get_nth_word( mix, readline, 1 ); // For detecting same sample or not
		
		get_nth_word( match, readline, 11 );
		flag_psm = getNumProtein( match, vec_id, abe_type );
		n_svp = 0;

		event_vec.clear();	// for sv event
		seq_vec.clear();	seq_idx = -1;	// for amino acid sequence in a protein
		prot_vec.clear();		
		flag_vec.clear();	// if this event is in correct mix or not
		n_add = 0;

		// Ok. Two Questions need to be answered. 
		// First, can it bematched to at least one patients within this mixture? 
		// Second, how can we only output the corresponding patient?


		// For future analysis, we need to maintain two attributes whenever adding new elements
		// which protein is this
		// Is this one the correct mixture/patient
		if ( 4 < flag_psm ) // The PSM contains no decoy, and only the types we want
		{
			L("===NewDTA===\n");
			for( int i = 0; i < (int)vec_id.size(); i ++ )
			{ 
				pit = map_pep.find( vec_id[i] );	//(0 to AAWR and 1)
				if ( map_pep.end() != pit )
				{
					seq = pit->second.pep;
					seq_vec.push_back(seq);
					seq_idx++; // Event in this round connects to seq_vec[seq_idx];

					it = map_event.find( seq );
					if ( map_event.end() != it )
					{
						L( "===NewProt\t%d\t%s===\n", i, seq.c_str() );
						n_svp += (int)it->second.size();
						//n_add = augment_fusion_list( event_vec, it->second, flag_vec, mix );
						n_add = augment_sv_list( event_vec, it->second, flag_vec, mix );
						for (int k = 0; k < n_add; k++){ prot_vec.push_back(seq_idx);}
						//for( int j =0; j <(int)it->second.size(); j++)
						//{	fprintf(out_fp, "%s\t%s\t%s\n", readline, seq.c_str(), it->second[j].c_str() );		}
					}
					else{ E("Error: missing Peptide %s\n", seq.c_str()) ; }
				}
				else{ E("Error: missing sequence ID %d\n", vec_id[i]);}
			}
			L("=== SANITY %lu %lu %lu===\n", event_vec.size(), prot_vec.size(), flag_vec.size());
			L("=== ENDDTA %lu in %d ===\n", event_vec.size(), n_svp);	
			
			final_flag 	= 0;	othermix_flag 	= 0;	samemix_flag	= 0;
			for(int k = 0; k < (int)flag_vec.size(); k++)
			{
				if ( flag_vec[k])	{	samemix_flag = 1;	}
				else	{ othermix_flag = 1;}
			}

			// I need to clear cur_event
			clear_mht( cur_event);
			for(int k = 0; k < (int)flag_vec.size(); k++)
			{
				final_flag = 4*flag_vec[k] + 2*(1-othermix_flag ) + samemix_flag;
				if ( final_flag && (!mix_type || flag_vec[k]) ) // Output when mix_type == 0 or flag == 1
				{
					if ( 1 == file_type )
					{
						get_nth_word( match, readline, 10 );
						n_mod = get_msgf_peptide( match, pep_str);
						event_flag = parse_mistrvar_match( event_vec[k], seq_vec[ prot_vec[k] ], pep_str, tmp_event);
						// detect if we need to output the next tmp_event here
						if ( !IsDupSV( tmp_event, cur_event) )
						{ 
							fprintf(out_fp, "%s\t%s\t-1\t-1\t%s\t", readline, pep_str.c_str(), seq_vec[ prot_vec[k] ].c_str()  );
							fprintf(out_fp, "%s_%d_%d_%d_%c_%d_%d_%d\t%d\t", tmp_event.pat.c_str(), tmp_event.id, tmp_event.ori, tmp_event.rf, tmp_event.strand, tmp_event.ol, tmp_event.s, tmp_event.e, final_flag);
							//fprintf(out_fp, "%s\t%s\t-1\t-1\t%s\tNA\t%d\t", readline, pep_str.c_str(), seq_vec[ prot_vec[k] ].c_str(), final_flag);
							fprintf(out_fp, "%d\t%s\t%d\t%d\t%d\t%d\t%d\t%d\t", event_flag, tmp_event.pat.c_str(), tmp_event.id, tmp_event.ol, tmp_event.lp, tmp_event.rp, tmp_event.s, tmp_event.e);
							fprintf(out_fp, "%s\t%s\n", tmp_event.gid.c_str(), tmp_event.tid.c_str() );
							//fprintf(out_fp, "%s\t%s\t%u\t%u\t%u\t%d\t%d\t%s\t%s\n", tmp_event.sv_type.c_str(), tmp_event.ref.c_str(), tmp_event.pos, tmp_event.ps, tmp_event.pe, tmp_event.l_b4, tmp_event.l_sv, tmp_event.gid.c_str(), tmp_event.tid.c_str() );
							cur_event = tmp_event;
						}
					}
					else
					{
						get_nth_word( match, readline, 18 );
						copyToString( match, pep_str );
						event_flag = parse_mistrvar_match( event_vec[k], seq_vec[ prot_vec[k] ], pep_str, tmp_event);
						//fprintf(out_fp, "%s\t%s\t%s\t%d\t", readline, seq_vec[ prot_vec[k] ].c_str(), event_vec[k].c_str(), final_flag);	
						if ( !IsDupSV( tmp_event, cur_event ))
						{
							//fprintf(out_fp, "%s\t%s\t", readline, pep_str.c_str(), seq_vec[ prot_vec[k] ].c_str()  );
							fprintf(out_fp, "%s\t%s\t", readline, seq_vec[ prot_vec[k] ].c_str()  );
							fprintf(out_fp, "%s_%d_%d_%d_%c_%d_%d_%d\t%d\t", tmp_event.pat.c_str(), tmp_event.id, tmp_event.ori, tmp_event.rf, tmp_event.strand, tmp_event.ol, tmp_event.s, tmp_event.e, final_flag);
							//fprintf(out_fp, "%s\t%s\tNA\t%d\t", readline, seq_vec[ prot_vec[k] ].c_str(),final_flag);	
							fprintf(out_fp, "%d\t%s\t%d\t%d\t%d\t%d\t%d\t%d\t", event_flag, tmp_event.pat.c_str(), tmp_event.id, tmp_event.ol, tmp_event.lp, tmp_event.rp, tmp_event.s, tmp_event.e);
							fprintf(out_fp, "%s\t%s\n", tmp_event.gid.c_str(), tmp_event.tid.c_str() );
							//fprintf(out_fp, "%s\t%s\t%u\t%u\t%u\t%d\t%d\t%s\t%s\n", tmp_event.sv_type.c_str(), tmp_event.ref.c_str(), tmp_event.pos, tmp_event.ps, tmp_event.pe, tmp_event.l_b4, tmp_event.l_sv, tmp_event.gid.c_str(), tmp_event.tid.c_str() );
							cur_event = tmp_event;
						}
					}
				}
			}

			// Summary for matchinfo
			s_flag = 2*(1-othermix_flag ) + samemix_flag;
			if ( 1 == file_type )
			{
				get_nth_word( match, readline, 10 );
				n_mod = get_msgf_peptide( match, pep_str);
				fprintf(log_fp, "%s\t%s\t-1\t-1\tNA\tNA\t%d\n", readline, pep_str.c_str(), s_flag);
			}
			else
			{	fprintf(log_fp, "%s\tNA\tNA\t%d\n", readline,  s_flag);	}
			good_tsv++;			
		} 
		count++;
	}
	E("Total/Good TSVRecord\t%d\t%d\n", count, good_tsv);

	flag_vec.clear();
	prot_vec.clear();
	seq_vec.clear();
	event_vec.clear();

	fclose(fp);
	fclose(out_fp);
	fclose(log_fp);
	free(m_pep);
	free(misc);
	free(mix);
	free(readline);
}
/**********************************************/
int parse_defuse_match( const string &event_str, const string &prot, const string &pep, dh_t &token)
{
	int flag = 0;

	size_t found = prot.find(pep);
	if (string::npos == found)
	{
		E( "Error in %s and %s\n", pep.c_str(), prot.c_str());
		exit(1);
	}
	
	int l_prot = (int)prot.size();
	int l_pep = (int)pep.size();
	int left = (int)found; // number of aa on the left
	int right = l_prot - l_pep - left; // number of aa of the right
	
	if ( 0 > right ){E("Wrong Peptide Boundary in %s\n", event_str.c_str() );}

	L("HMM %s %s %d %d %d\n", pep.c_str(), prot.c_str(),(int)found,  l_pep, l_prot);
	
	char *pat=(char*)malloc(TOKEN_LENGTH);
	char *id_str=(char*)malloc(TOKEN_LENGTH);
	char *b_str=(char*)malloc(TOKEN_LENGTH);
	char *stop_str=(char*)malloc(TOKEN_LENGTH);
	char *s_str=(char*)malloc(TOKEN_LENGTH);
	char *e_str=(char*)malloc(TOKEN_LENGTH);

	char *misc=(char*)malloc(MAX_LINE);
	char c;
	char ori, rf;
	
	int id = -1, bp =0, stop = 0, 
		s_1 =0, e_1 = 0 ; // both inclusive
	int ol_length = 0;

	int ret=0;
	ret = sscanf( event_str.c_str(),"%[^_]_%[^_]_%c_%c_%c%[^-]-%[^_]_%c%[^_]_%c%[^-]-%s", pat, id_str, &ori, &rf, &c, b_str, misc, &c, stop_str, &c, s_str, e_str );
	sscanf( id_str, "%d", &id );
	sscanf( b_str, "%d", &bp );
	sscanf( stop_str, "%d", &stop );
	sscanf( s_str, "%d", &s_1 );
	sscanf( e_str, "%d", &e_1 );

	int pep_s = s_1 + left;
	int pep_e = e_1 - right;
	
	if ( pep_e < bp ) {flag = 1;}
	else if ( bp < pep_s) {flag = 2;}
	else{flag = 4;} // in-between


	if ( (stop < bp) && ( stop < pep_s)){ flag+=8;}
	else if ( (stop > bp) && ( stop > pep_s)){ flag+=16;}

	L("Event %s with %d => PAT %s %d %d %d %d %d %d %d\n", event_str.c_str(), flag, pat, id, bp, stop, s_1, e_1, pep_s, pep_e );
	token.pat =string(pat);
	token.id = id;
	token.bp = bp;
	token.stop = stop;
	token.s = pep_s;
	token.e = pep_e;


	free(pat);
	free(id_str);
	free(b_str);
	free(stop_str);
	free(s_str);
	free(e_str);
	free(misc);

	return flag;
	
}
/**********************************************/
// In event_str, 10th and 11th filed are position of original sequences in the sv proteins
int parse_mistrvar_match( const string &event_str, const string &prot, const string &pep, mh_t &token)
{
	int flag = 0;
	//L("parse mistrvar match\n");
	//L("para1:%s\n", event_str.c_str());
	//L("para2:%s\n", prot.c_str());
	//L("para3:%s\n", pep.c_str());
	size_t found = prot.find(pep);
	if (string::npos == found)
	{
		E( "Error in parse_mistrvar_match: %s is not in  %s\n", pep.c_str(), prot.c_str());
		exit(1);
	}
	
	int l_prot = (int)prot.size();
	int l_pep = (int)pep.size();
	int left = (int)found; // number of aa on the left
	int right = l_prot - l_pep - left; // number of aa of the right
	
	if ( 0 > right )
	{	E("Invalid Peptide Boundary %d for %s in %s\n", right, pep.c_str(), event_str.c_str() );}

	//L("ADJ: %s %s %d %d %d %d\n", pep.c_str(), prot.c_str(), left, right,  l_pep, l_prot);
	
	char *pat=(char*)malloc(TOKEN_LENGTH);
	//char *id_str=(char*)malloc(TOKEN_LENGTH);
	//char *b_str=(char*)malloc(TOKEN_LENGTH);
	//char *stop_str=(char*)malloc(TOKEN_LENGTH);
	//char *s_str=(char*)malloc(TOKEN_LENGTH);
	//char *e_str=(char*)malloc(TOKEN_LENGTH);
	
	char *sv_type=(char*)malloc(TOKEN_LENGTH);
	char *ref=(char*)malloc(TOKEN_LENGTH);
	char *gid=(char*)malloc(TOKEN_LENGTH);
	char *tid=(char*)malloc(TOKEN_LENGTH);

	char *misc=(char*)malloc(MAX_LINE);
	char c;
	//char ori, rf;
	
	int id = -1, 
		lp =0, rp = 0, 
		s_1 =0, e_1 = 0 ; // both inclusive
	int ori, rf;
	int ol_length = 0, final_ol = 0, total_l = 0;
	int l_iso = 0, pos1 =0, pos2 =0;
	int offset;

	uint32_t pos, pos_s, pos_e;

	int ret=0;
	//ret = sscanf( event_str.c_str(),"%s %s %s %d %d %d %c %d %d %d %d %d %d %d %s %s %u %u %u %s %s %d %d %d %n", 
	//	misc, misc,
	//	pat, &id, &ori, &rf, &c, &ol_length, &ol_length, &ol_length,
	//	&lp, &rp, &s_1, &e_1,
	//	sv_type, ref, 
	//	&pos, &pos_s, &pos_e, gid, tid, 
	//	&l_iso, &l_before, &l_sv, &offset);
	ret = sscanf( event_str.c_str(),"%s %s %d %s %s %d %d %c %d %d %d %d %d %d %d %d %d",
		pat, misc, &id, gid, tid,
		&ori, &rf, &c, &ol_length, &total_l,
		&lp, &rp, &s_1, &e_1,
		&pos1, &pos2, &l_iso);

	//sscanf( id_str, "%d", &id );
	//sscanf( b_str, "%d", &bp );
	//sscanf( stop_str, "%d", &stop );
	//sscanf( s_str, "%d", &s_1 );
	//sscanf( e_str, "%d", &e_1 );



	int pep_s = lp + left;
	int pep_e = rp - right;
	
	if ( 0 < ol_length ){ final_ol = overlap_int( pep_s, pep_e, s_1, e_1);}
	flag = 16; // Since all are before stop codon;
	if ( final_ol){flag +=1;} // overlap
	if ( final_ol == pep_e - pep_s +1) { flag += 2;}	// peptide is a substring of sv
	if ( final_ol == e_1 - s_1 + 1) {flag +=4;}	// peptide contains whole sv


	L(">>%d for %s\n", flag, event_str.c_str() );
	L("<< %s %d %d %d %d %d %d %d %d %d\n", pat, id, ol_length, final_ol, lp, rp, s_1, e_1, pep_s, pep_e );
	token.pat =string(pat);
	token.id = id;
	token.ol = final_ol;
	token.lp = pep_s;
	token.rp = pep_e;
	token.s = s_1;
	token.e = e_1;
	token.ori = ori;
	token.rf =rf;
	token.strand = c;
	//token.pos = pos;
	//token.ps = pos_s;
	//token.pe = pos_e;
	//token.l_b4 = l_before;
	//token.l_sv = l_sv;
	//copyToString( sv_type, token.sv_type);
	copyToString( gid, token.gid );
	copyToString( tid, token.tid );
	//copyToString( ref, token.ref );
	//token.sv_type =string(sv_type);
	//token.gid= string(gid);
	//token.tid = string(tid);


	free(pat);
	free(sv_type);
	free(ref);
	free(gid);
	free(tid);
	free(misc);


	return flag;
	
}
/**********************************************/
int IsDupSV( const mh_t &t1, const mh_t &t2 )
{
	int flag = 0; // different SV peptide records
	if ( (t1.pat == t2.pat)
	&& ( t1.id == t2.id)
	&& ( t1.ori == t2.ori)
	&& ( t1.rf == t2.rf)
	&& ( t1.strand == t2.strand)
	&& ( t1.ol == t2.ol)
	&& ( t1.lp - t1.s == t2.lp - t2.s)
	&& ( t1.rp - t1.e == t2.rp - t2.e)
	)
	{	flag = 1;	}
	return flag;
}

// This is NOT a good idea. Move it later
/**********************************************/
void get_defuse_table( char *tsv_file, map<string, vector<string> > &map_info )
{
	char *readline=(char*)malloc(MAX_LINE);
	char *match=(char*)malloc(MAX_LINE);
	
	int count = 0,
		good_tsv = 0;

	string raw_str ="", pat_str = "";

	map<string, vector<string> >::iterator it; // event iterator

	FILE *fp = fopen( tsv_file, "r");
	
	while( NULL != fgets(readline, MAX_LINE, fp) )
	{
		//readline[strcspn(readline, "\n")] = 0; // Get Rid of Terminating New Line
		if( 0 == strncmp("c", readline, 1 ) ) // defuse use cluster_id in header and digit for real records
		{	continue;	}
		else
		{
			get_nth_word( match, readline, 46 );
			copyToString( match, raw_str );
			pat_str = raw_str.substr(8,8); // DANGEROUS! HARDCODED!
			if ( map_info.end() == map_info.find( pat_str) )
			{
				vector<string> tmp_vec;
				map_info[ pat_str ] = tmp_vec;
			}
			map_info[pat_str].push_back( string(readline) );
		}

		count++;
		if (!(count%500000)) {E(".");}
	}

	//for( it = map_info.begin(); it != map_info.end(); it++){ L("%s\t%ld\n", it->first.c_str(), it->second.size());}
	E("\nPatient/Record\t%ld\t%d\n", map_info.size(), count);
	
	fclose(fp);

	free(match);
	free(readline);
}

/**********************************************/
int search_fusion_list( const int p_id, const vector<string> &pred_vec, string &fusion_str )
{
	char *match=(char*)malloc(MAX_LINE);

	int flag = 0;
	int limit = (int) pred_vec.size();

	int e_id, offset;
	fusion_str = "";
	for( int i =0; i< limit; i++ )
	{
		sscanf( pred_vec[i].c_str(), "%d %n", &e_id, &offset);
		if ( p_id == e_id)
		{
			get_nth_word( match, pred_vec[i].c_str(), 31);
			fusion_str += match;
			fusion_str += "\t";
			get_nth_word( match, pred_vec[i].c_str(), 32);
			fusion_str += match;
			fusion_str += "\t";
			get_nth_word( match, pred_vec[i].c_str(), 29);
			fusion_str += match;
			fusion_str += "\t";
			get_nth_word( match, pred_vec[i].c_str(), 30);
			fusion_str += match;
			fusion_str += "\t";
			get_nth_word( match, pred_vec[i].c_str(), 25);
			fusion_str += match;
			fusion_str += "\t";
			get_nth_word( match, pred_vec[i].c_str(), 26);
			fusion_str += match;
			fusion_str += "\t";
			get_nth_word( match, pred_vec[i].c_str(), 38);
			fusion_str += match;
			fusion_str += "\t";
			get_nth_word( match, pred_vec[i].c_str(), 39);
			fusion_str += match;
			fusion_str += "\t";
			get_nth_word( match, pred_vec[i].c_str(), 8);
			fusion_str += match;
			fusion_str += "\t";
			get_nth_word( match, pred_vec[i].c_str(), 18);
			fusion_str += match;
			fusion_str += "\t";
			get_nth_word( match, pred_vec[i].c_str(), 54);
			fusion_str += match;
			fusion_str += "\t";
			get_nth_word( match, pred_vec[i].c_str(), 53);
			fusion_str += match;
			fusion_str += "\t";
			get_nth_word( match, pred_vec[i].c_str(), 12);
			fusion_str += match;
			fusion_str += "\t";
			get_nth_word( match, pred_vec[i].c_str(), 13);
			fusion_str += match;
			fusion_str += "\t";
			get_nth_word( match, pred_vec[i].c_str(), 14);
			fusion_str += match;
			fusion_str += "\t";
			get_nth_word( match, pred_vec[i].c_str(), 65);
			fusion_str += match;
			flag = 1; 
			break;
		}
	}

	free(match);
}
/**********************************************/
void extract_defuse_entry( char *tsv_file, char *out_file, const map<string, vector<string> > &map_info )
{
	char *readline=(char*)malloc(MAX_LINE);
	char *match=(char*)malloc(MAX_LINE);
	
	int count = 0,
		good_tsv = 0;

	int num_token = 0;
	string pat_str = "";
	int p_id = -1;
	int flag = 0;
	string fusion_str="";

	map<string, vector<string> >::const_iterator it; // event iterator

	FILE *fp = fopen( tsv_file, "r");
	FILE *out_fp = fopen( out_file, "w");
	
	while( NULL != fgets(readline, MAX_LINE, fp) )
	{
		if ( 30 > get_num_word( readline) )
		{
			E("Abort: Format Inconsistency issue in annotated TSV Results %s: %d columns detected\n", tsv_file, num_token);
			exit(1);
		}

		readline[strcspn(readline, "\n")] = 0; // Get Rid of Terminating New Line
		if( 0 == strncmp("#", readline, 1 ) ) // defuse use cluster_id in header and digit for real records
		{
			fprintf( out_fp, "%s\tGENE1\tGENE2\tTYPE1\tTYPE2\tCHR1\tCHR2\tBP1\tBP2\tALT\tEXON\tReadThrough\tORF\tHOMOLOGY\tEST\tCDNA\tProb\n", readline);
			continue;	
		}
		else
		{
			get_nth_word( match, readline, 25 );
			copyToString( match, pat_str );
			get_nth_word( match, readline, 26 );
			sscanf( match, "%d", &p_id);

			it = map_info.find(pat_str);
			if ( map_info.end() == it )
			{	E("Missing Patient %s\n", pat_str.c_str());	}
			else
			{
				flag = search_fusion_list( p_id, it->second, fusion_str );
				if (!flag){E("Cannot find %s\n", readline);}
				fprintf( out_fp, "%s\t%s\n", readline, fusion_str.c_str() );
			}
		}

		count++;
		if (!(count%500000)) {E(".");}
	}

	//E("\nPatient/Record\t%ld\t%d\n", map_info.size(), count);
	
	fclose(fp);

	free(match);
	free(readline);
}
///**********************************************/
//void convert_ensembl_ref(char *chr_char, string &chr_str)
//{
//	chr_str = "";
//	chr_str = string(chr_char).substr(3);
//	if ("M" == chr_str ){chr_str += "T";} // from M to MT
//}
//
///**********************************************/
//void convert_repeat_info(char *fea, string &repeat_info)
//{
//	repeat_info = "REPEAT";
//
//	if ( !strcmp("DNA", fea))
//	{
//		repeat_info = "DNA";
//	}
//	else if ( !strcmp("RNA", fea))
//	{
//		repeat_info = "RNA";
//	}
//	else if ( !strcmp("LINE", fea))
//	{
//		repeat_info = "LINE";
//	}
//	else if ( !strcmp("SINE", fea))
//	{
//		repeat_info = "SINE";
//	}
//	else if ( !strcmp("LTR", fea))
//	{
//		repeat_info = "LTR";
//	}
//}

///**********************************************/
//bool sortRepeatByStart( const repeat_t &r1, const repeat_t &r2 )
//{
//	return r1.s < r2.s;
//}
///**********************************************/
//bool sortRepeatByEnd( const repeat_t &r1, const repeat_t &r2 )
//{
//	return r1.e < r2.e;
//}
/**********************************************/
int validate_matched(char *matched_pep)
{
	char *tag=(char*)malloc(TOKEN_LENGTH);
	char c;
	int offset;
	int flag_sv = 0, decoy_sv = 0;
	int count = 0, ret = 0;
	//L("%s\n", matched_pep);
	while( *matched_pep )
	{
		ret = sscanf( matched_pep, "%[^\;]%c%n", tag, &c, &offset);
		//L("%d\t%s\t%s\n", ret, matched_pep, tag);
		if ( 0 == strncmp("SV", tag, 2))
		{	flag_sv = 1;	}
		else if ( 0 == strncmp("XXX", tag, 3))
		{	decoy_sv = 1;	}	
		else
		{	
			flag_sv = 0;	
			break;
		}
		matched_pep += offset;
		if ( 2 != ret){break;} // for the last record without colon
	}
	// We keep PSM with only special prefix (CPTAC and XXX)
	if ( (decoy_sv) && (flag_sv)){flag_sv++;} // 2 indicates both CPTAC and decoy 
	return flag_sv;
	
}
/**********************************************/
void count_MSGF_TSV( char *tsv_file, char *out_file )
{
	string total_seq="", total_rc="";
	
	char *readline=(char*)malloc(MAX_LINE);
	char *misc=(char*)malloc(TOKEN_LENGTH);
	char *match=(char*)malloc(MAX_LINE);
	//char *fea=(char*)malloc(TOKEN_LENGTH);
	//char *sim=(char*)malloc(TOKEN_LENGTH);

	//char header;
	int count = 0,
		good_tsv = 0;
	int offset =0;
	uint32_t start = 0, end = 0,
			score =0, rid = 0;
	//float sim = 0.0; // similarity
	string new_ref;// formatted reference without chr prefix
	string repeat_info;// formatted reference without chr prefix
	FILE *fp = fopen( tsv_file, "r");
	FILE *out_fp = fopen( out_file, "w");
	//FILE *out_fp= fopen( out_file, "w");
	//int cur = 0, cur_gene = 0;
	int ret;
	int flag_psm = 0;
	while( NULL != fgets(readline, MAX_LINE, fp) )
	{
		if ( 0 == strncmp("#", readline, 1)){ continue; }
		ret = sscanf( readline, "%s %s %s %[^\t]\t%s %s %s %s %s %s %s %n\n", 
			misc, misc, misc, misc, misc, misc, misc, misc, misc, misc,
			match, &offset);
		flag_psm = validate_matched( match );
		//fprintf(out_fp, "%d\t%s", flag_psm, readline);
		if ( flag_psm )
		{	
			fprintf(out_fp, "%s", readline);	
			good_tsv++;
		} 
		//{
		//	continue;
		//} // Ignore header by skipping weird reference
		//L("%d\t%u\t%s\t%u\t%u\t%s\t%u\n", ret, score, ref, start, end, fea, rid);

		//if ( 7 >= ret ){ E("Undefined Format:%s", readline);}

		//convert_ensembl_ref( ref, new_ref);
		//convert_repeat_info( fea, repeat_info);

		//tmp_repeat.rid = rid;
		//tmp_repeat.score = score;
		//tmp_repeat.s = start;
		//tmp_repeat.e = end;
		//tmp_repeat.ref = new_ref;
		//tmp_repeat.r_info = repeat_info;
		//map_repeat[new_ref].push_back(tmp_repeat);
		count++;

	}


	E("Total/Good TSVRecord\t%d\t%d\n", count, good_tsv);
	//E("Total References with Predictions:\t%d\n", (int)map_repeat.size());
	fclose(fp);
	fclose(out_fp);
	//free(fea);	free(ref);
	free(misc);
	free(readline);
}
/**********************************************/
// Unfortunately we need to get original FASTA file for double checking.
// REASON: We ignore sequence contents for seq longer than 100aa
void parse_Seq_Freq( char *seq_file, char *freq_file, map< int, pc_t > &map_pepcount )
{
	
	char *readline=(char*)malloc(MAX_LINE);
	char *misc=(char*)malloc(TOKEN_LENGTH);
	char *match=(char*)malloc(MAX_LINE);
	//char *fea=(char*)malloc(TOKEN_LENGTH);
	//char *sim=(char*)malloc(TOKEN_LENGTH);

	//char header;
	int count = 0, id =0, fix = 0;
	int offset =0;
	uint32_t start = 0, end = 0,
			score =0, rid = 0;
	pc_t pc_item;
	string raw_str;
	//float sim = 0.0; // similarity
	FILE *fp = fopen( freq_file, "r");
	int ret;
	int flag_psm = 0;
	while( NULL != fgets(readline, MAX_LINE, fp) )
	{
		if ( 0 == strncmp("#", readline, 1)){ continue; }
		sscanf( readline, "%d %s %u", &id, match, &pc_item.count );
		//L("Echo\t%d\t%s\t%u\n", id, match, pc_item.count);
		copyToString( match, pc_item.pep );
		map_pepcount[id] = pc_item;
	}
	fclose(fp);
	E("Total Entries Get:\t%lu\n", map_pepcount.size());

	FILE *seq_fp = fopen(seq_file, "r");

	while( NULL != fgets(readline, MAX_LINE, seq_fp) )
	{
		if ( 0 == strncmp(">", readline, 1)){ continue; }
		copyToStringStrip( readline, raw_str );
		if ( map_pepcount.find(count) == map_pepcount.end() )
		{
			E("Error: Seq File Inconsistent\n");
		}
		else if ( map_pepcount[count].pep != raw_str )
		{
			//E("OK\t%s\t%s\n", map_pepcount[count].pep.c_str(), raw_str.c_str() );
			L("Fixing_Seq\t%d\n", count);
			map_pepcount[count].pep = raw_str;
			fix++;
		}
		count++;
	}
	E("Fix Entry:\t%d\n", fix );

	fclose(seq_fp);
	free(match);
	free(misc);
	free(readline);
}
/**********************************************/
// WARNING: HARD CODED FOR OUR CPTAC PURPOSE!!!
int get_pep_count(char *matched_pep, vector<int> &vec_id)
{
	char *tag=(char*)malloc(TOKEN_LENGTH);
	char c;
	int offset, of2;
	int pep_id = 0;
	int flag_sv = 0, decoy_sv = 0;
	int count = 0, ret = 0;
	vec_id.clear();
	//L("%s\n", matched_pep);
	while( *matched_pep )
	{
		ret = sscanf( matched_pep, "%[^\;]%c%n", tag, &c, &offset);
		//L("%d\t%s\t%s\n", ret, matched_pep, tag);
		if ( 0 == strncmp("F", tag, 1))
		{	
			sscanf( tag, "F_%d(%n", &pep_id, &of2);
			vec_id.push_back( pep_id);
			//L("%d\n", pep_id  );
			flag_sv = 1;	
		}
		else if ( 0 == strncmp("XXX", tag, 3))
		{	decoy_sv = 1;	}	
		else
		{	
			flag_sv = 0;	
			break;
		}
		matched_pep += offset;
		if ( 2 != ret){break;} // for the last record without colon
	}
	// We keep PSM with only special prefix (CPTAC and XXX)
	if ( (decoy_sv) && (flag_sv)){flag_sv++;} // 2 indicates both CPTAC and decoy 
	return flag_sv;
	
}

/**********************************************/
uint32_t get_num_svp( const vector<int> &vec_id, const map<int, pc_t> &map_pep, map<string, int> &map_svpep )
{
	map<int, pc_t>::const_iterator it;
	uint32_t n_svp = 0;
	int limit = (int)vec_id.size();
	for ( int i = 0; i < limit; i++ )
	{
		it = map_pep.find( vec_id[i] );
		if ( it!=map_pep.end() )
		{
			L("Check\t%d\t%s\t%u\n", vec_id[i], it->second.pep.c_str(), it->second.count);
			n_svp += it->second.count;
			// Below we collect all peptides which really matter from the selected TSVs
			if ( map_svpep.find(it->second.pep) == map_svpep.end())
			{
				map_svpep[ it->second.pep] = 1;
			}
			else{ map_svpep[it->second.pep]++; }
		}
		else{ E("Missing Merged Peptide ID %d\n", vec_id[i] );}
	}
	return n_svp;
}
/**********************************************/
void match_MSGF_TSV( char *tsv_file, char *out_file, const map<int, pc_t> &map_pep, map<string, int> &map_svpep )
{
	string total_seq="", total_rc="";

	char *readline=(char*)malloc(MAX_LINE);
	char *misc=(char*)malloc(TOKEN_LENGTH);
	char *match=(char*)malloc(MAX_LINE);
	char *m_pep=(char*)malloc(TOKEN_LENGTH);
	//char *sim=(char*)malloc(TOKEN_LENGTH);

	//char header;
	int count = 0,
		good_tsv = 0;
	int offset =0;
	uint32_t n_svp = 0; // total number of sv peptides;
	uint32_t start = 0, end = 0,
			score =0, rid = 0;

	int n_mod = 0;
	string new_psm, new_pep;
	vector<int> vec_id;
	string new_ref;// formatted reference without chr prefix
	string repeat_info;// formatted reference without chr prefix
	FILE *fp = fopen( tsv_file, "r");
	FILE *out_fp = fopen( out_file, "w");
	//FILE *out_fp= fopen( out_file, "w");
	//int cur = 0, cur_gene = 0;
	int ret;
	int flag_psm = 0;
	while( NULL != fgets(readline, MAX_LINE, fp) )
	{
		if ( 0 == strncmp("#", readline, 1)){ continue; }
		ret = sscanf( readline, "%s %s %s %[^\t]\t%s %s %s %s %s %s %s %n\n", 
			misc, misc, misc, misc, misc, misc, misc, misc, misc, 
			m_pep,	match, &offset);
		flag_psm = get_pep_count( match, vec_id );
		//fprintf(out_fp, "%d\t%s", flag_psm, readline);
		if ( flag_psm )
		{	
			copyToStringStrip( readline, new_psm);
			n_mod = get_msgf_peptide( m_pep, new_pep );
			// Calculate number of relevant Entries for the spectra
			n_svp = get_num_svp( vec_id, map_pep, map_svpep);
			fprintf(out_fp, "%s\t%s\t%lu\t%u\n", new_psm.c_str(), new_pep.c_str(), vec_id.size(), n_svp );	
			good_tsv++;
		} 
		count++;
	}
	E("Total/Good TSVRecord\t%d\t%d\n", count, good_tsv);
	fclose(fp);
	fclose(out_fp);
	free(m_pep);//	free(ref);
	free(misc);
	free(readline);
}
///**********************************************/
//int get_nth_word( char *target, const char *src, int n)
//{
//	int of = 0;
//	target[0]= '\0';
//	while( n && (sscanf(src, "%s%n", target, &of)) )
//	{
//		src+=of;
//		n--;
//	}
//	if ( n )
//	{
//		E("Too Many fields in %s\n", src);
//		return 0;
//	}
//	return 1;
//}
// Motivation: While we generate a huge table of SV Peptides, only small portions of them 
// are worth to be considered in the analysis. We can only extract these records, which might be 
// less than 10k entries before doing final comparison
/**********************************************/
void extract_PepTable( char *pep_file, char *out_file, const map<string, int> &map_svpep )
{
	map<string, int>::const_iterator it;

	char *readline=(char*)malloc(MAX_LINE);
	char *misc=(char*)malloc(TOKEN_LENGTH);
	char *match=(char*)malloc(MAX_LINE);
	char *m_pep=(char*)malloc(TOKEN_LENGTH);
	//char *sim=(char*)malloc(TOKEN_LENGTH);
	char *log_file=(char*)malloc(MAX_LINE);
	strcpy(log_file, out_file);
	strcat(log_file, ".pepinfo");

	//char header;
	int count = 0,
		good_tsv = 0;
	int offset =0;
	uint32_t n_svp = 0; // total number of sv peptides;
	uint32_t start = 0, end = 0,
			score =0, rid = 0;

	int n_mod = 0;
	string new_psm, new_pep;
	vector<int> vec_id;
	string new_ref;// formatted reference without chr prefix
	string repeat_info;// formatted reference without chr prefix
	FILE *fp = fopen( pep_file, "r");
	FILE *log_fp = fopen( log_file, "w");
	//FILE *out_fp= fopen( out_file, "w");
	//int cur = 0, cur_gene = 0;
	int ret;
	int flag_psm = 0;
	while( NULL != fgets(readline, MAX_LINE, fp) )
	{
		if ( 0 == strncmp("#", readline, 1)){ continue; }
		if ( get_nth_word( match, readline, 25) )
		{
			copyToString( match, new_pep);
			it = map_svpep.find( new_pep );
			if ( it != map_svpep.end() )
			{
				fprintf( log_fp, "%s", readline);
			}
		}
		count++;
		if ( 0 == count%100000){E(".");}
	}
	E("\n");
	//E("Total/Good TSVRecord\t%d\t%d\n", count, good_tsv);
	fclose(fp);
	fclose(log_fp);
	free(log_file);
	free(m_pep);//	free(ref);
	free(misc);
	free(readline);
}
/**********************************************/
void parse_TargetPeptides( char *pep_file, map<string, vector<string> > &map_target )
{
	map<string, vector<string> >::iterator it;

	char *readline=(char*)malloc(MAX_LINE);
	char *misc=(char*)malloc(TOKEN_LENGTH);
	char *match=(char*)malloc(MAX_LINE);
	char *m_pep=(char*)malloc(TOKEN_LENGTH);
	//char *sim=(char*)malloc(TOKEN_LENGTH);
	//char *log_file=(char*)malloc(MAX_LINE);
	//strcpy(log_file, out_file);
	//strcat(log_file, ".pepinfo");

	//char header;
	int count = 0,
		total = 0;
	int offset =0;
	uint32_t n_svp = 0; // total number of sv peptides;
	uint32_t start = 0, end = 0,
			score =0, rid = 0;

	string new_txt, new_pep;
	vector<string> vec_info;
	FILE *fp = fopen( pep_file, "r");
	//FILE *log_fp = fopen( log_file, "w");
	//FILE *out_fp= fopen( out_file, "w");
	//int cur = 0, cur_gene = 0;
	int ret;
	int flag_psm = 0;
	while( NULL != fgets(readline, MAX_LINE, fp) )
	{
		if ( 0 == strncmp("#", readline, 1)){ continue; }
		if ( get_nth_word( match, readline, 25) )
		{
			copyToString( match, new_pep);
			copyToString( readline, new_txt);
			it = map_target.find( new_pep );
			if ( it != map_target.end() )
			{
				it->second.push_back( new_txt);
			}
			else
			{
				vec_info.clear();
				vec_info.push_back(new_txt);
				map_target[new_pep] = vec_info;
			}
		}
		count++;
		if ( 0 == count%100000){E(".");}
	}
	E("\n");

	for( it = map_target.begin(); it != map_target.end(); it++)
	{
		total += (int)it->second.size();
	}
	E("Info\t%lu\t%d\n", map_target.size(), total);
	fclose(fp);
	//fclose(log_fp);
	//free(log_file);
	free(m_pep);//	free(ref);
	free(misc);
	free(readline);
}
/**********************************************/
int adj_psm_summary( map<string, int> &map_summary, const vector<string> &vec_info )
{
	map<string, int>::iterator it;
	int limit = (int)vec_info.size();
	char *match=(char*)malloc(MAX_LINE);
	string p_name;
	for( int i = 0; i < limit; i++ )
	{
		if ( get_nth_word( match, vec_info[i].c_str(), 3) )
		{
			copyToString(match, p_name);
			it = map_summary.find( p_name);
			if ( it == map_summary.end() )
			{
				map_summary[ p_name ] = 1;
			}
			else{ it->second++; }
		}
	}
	free(match);
}
/**********************************************/
int detect_mix( const string &p_str, const string &m_str)
{
	int flag = 0;
	if ( string::npos != m_str.find(p_str.substr(0,4)) )
	{	//L("\tFOUND\t%s\t%s\n", p_str.substr(0,4).c_str(), m_str.c_str() );
		flag = 1;	
	}//else{	L("\tDAMN\t%s\t%s\n", p_str.substr(0,4).c_str(), m_str.c_str() );	}
	return flag;
}
/**********************************************/
uint32_t expand_svp( const vector<int> &vec_id, const map<int, pc_t> &map_pep, const map<string, vector<string> > &map_target, map<string, int> &map_summary, const string &mix_str, int &final_mix )
{
	map<int, pc_t>::const_iterator it;
	map<string, vector<string> >::const_iterator t_it;
	map_summary.clear();

	uint32_t n_svp = 0, test = 0;
	int limit = (int)vec_id.size();
	int in_mix = 0, // if the patient belongs to the mixture
		not_mix = 0; // if the patient does not belong to the mixture
	final_mix = 0; // 0: all are not in the mix. 1: all are in the mix. 2: some are but some aren't

	for ( int i = 0; i < limit; i++ )
	{
		it = map_pep.find( vec_id[i] );
		if ( it!=map_pep.end() )
		{
			L("Check\t%d\t%s\t%u\n", vec_id[i], it->second.pep.c_str(), it->second.count);
			n_svp += it->second.count;
			t_it = map_target.find( it->second.pep );
			// Below we collect all peptides which really matter from the selected TSVs
			if ( t_it  == map_target.end())
			{
				E("Missing Peptide Entry %s\n", it->second.pep.c_str() );
			}
			else
			{ 
				test += (uint32_t)t_it->second.size(); 
				adj_psm_summary( map_summary, t_it->second );
			}
		}
		else{ E("Missing Merged Peptide ID %d\n", vec_id[i] );}
	}
	L("CMP\t%u\t%u\n", n_svp, test);
	
	map<string, int>::iterator s_it;
	for( s_it = map_summary.begin(); s_it != map_summary.end(); s_it++)
	{
		L("\t%s\t%d\n", s_it->first.c_str(), s_it->second);
		if ( detect_mix( s_it->first, mix_str) ){ in_mix = 1;}
		else{not_mix = 1;}
	}
	if ( in_mix && not_mix) {final_mix = 2;}
	else if (in_mix) {final_mix = 1;}
	//L("\tFINAL\t%d\n", final_mix);

	return n_svp;
}
/**********************************************/
void adj_out( const string &new_pep, const string &src, const string &mix_str, FILE *out)
{
	char *readline=(char*)malloc(MAX_LINE);
	char *misc=(char*)malloc(TOKEN_LENGTH);
	char *patient=(char*)malloc(TOKEN_LENGTH);
	char *sv=(char*)malloc(TOKEN_LENGTH);
	char *chr=(char*)malloc(TOKEN_LENGTH);
	char *gene=(char*)malloc(TOKEN_LENGTH);
	char *iso=(char*)malloc(TOKEN_LENGTH);
	char *match=(char*)malloc(MAX_LINE);
	char *raw=(char*)malloc(MAX_LINE);

	string ori_pep, patient_name;

	int id, ori, frame;
	char strand;
	int pro_l, pep_l, overlap_l, lb, rb, sv_s, sv_e,
		iso_l, before_l, sv_l;
	uint32_t pos, s, e;
	int offset;

	int ret = sscanf( src.c_str(), "%s %s %s %d %d %d %c %d %d %d %d %d %d %d %s %s %u %u %u %s %s %d %d %d %s %s %n", 
	misc, misc, patient,
	&id, &ori, &frame, &strand, 
	&pro_l, &pep_l, &overlap_l, &lb, &rb, &sv_s, &sv_e,
	sv, chr,
	&pos, &s, &e,
	gene, iso,
	&iso_l, &before_l, &sv_l,
	match, raw, &offset	);

	copyToString(match, ori_pep);
	copyToString(patient, patient_name);
	int shift = (int)ori_pep.find( new_pep);
	int new_lb = lb + shift;
	int new_rb = new_lb + (int)new_pep.size() - 1;
	int new_overlap = over_l( new_lb , new_rb, sv_s, sv_e);
	int mix_flag =  detect_mix( patient_name, mix_str) ;
	fprintf(out, "%s\t%d\t%d\t%d\t%c\t", patient, id, ori, frame, strand );
	fprintf(out, "%d\t%d\t%d\t%d\t%d\t", pro_l, pep_l, overlap_l, lb, rb );
	fprintf(out, "%d\t%d\t%s\t%s\t%u\t%u\t%u\t", sv_s, sv_e, sv, chr, pos, s, e );
	fprintf(out, "%d\t%d\t%d\t%d\t", mix_flag, new_overlap, new_lb, new_rb );
	fprintf(out, "%s\t%s\t%d\t%d\t%d\t", gene, iso, iso_l, before_l, sv_l);
	fprintf(out, "%s\t%s\n", match, raw);
}
/**********************************************/
int output_expansion( const vector<int> &vec_id, const map<int, pc_t> &map_pep, const map<string, vector<string> > &map_target, const map<string, int> &map_summary, const string &mix_str, int final_mix, const string &new_psm, const string &new_pep, int num_mod, int num_svp, FILE *out )
{
	map<int, pc_t>::const_iterator it;
	map<string, vector<string> >::const_iterator t_it;

	//uint32_t n_svp = 0, test = 0;
	int limit = (int)vec_id.size();
	//int in_mix = 0, // if the patient belongs to the mixture
	//	not_mix = 0; // if the patient does not belong to the mixture
	//final_mix = 0; // 0: all are not in the mix. 1: all are in the mix. 2: some are but some aren't
	int j = 0;
	for ( int i = 0; i < limit; i++ )
	{
		it = map_pep.find( vec_id[i] );
		if ( it!=map_pep.end() )
		{
			//fprintf( out, "%s\t%s\t%d\t%lu\t%d\t%lu\t%d\t",
			//	new_psm.c_str(), new_pep.c_str(), num_mod, vec_id.size(), num_svp, map_summary.size(), final_mix );
				
			t_it = map_target.find( it->second.pep );
			// Below we collect all peptides which really matter from the selected TSVs
			if ( t_it  == map_target.end())
			{
				E("Missing Peptide Entry %s\n", it->second.pep.c_str() );
			}
			else
			{

				for ( int j = 0; j < (int)t_it->second.size(); j++ )
				{
					//L("%s\t%s\n", it->second.pep.c_str(), t_it->second[j].c_str() );
					fprintf( out, "%s\t%s\t%d\t%lu\t%d\t%lu\t%d\t",
							new_psm.c_str(), new_pep.c_str(), num_mod, vec_id.size(), num_svp, map_summary.size(), final_mix );
					adj_out( new_pep, t_it->second[j], mix_str, out);					
					//L("%s\t%s\t%s\n", new_pep.c_str(), t_it->second[j].c_str(), mix_str.c_str() );
				}
			}
		}
		else{ E("Missing Merged Peptide ID %d\n", vec_id[i] );}
	}
	//L("CMP\t%u\t%u\n", n_svp, test);
	//
	//map<string, int>::iterator s_it;
	//for( s_it = map_summary.begin(); s_it != map_summary.end(); s_it++)
	//{
	//	L("\t%s\t%d\n", s_it->first.c_str(), s_it->second);
	//	if ( detect_mix( s_it->first, mix_str) ){ in_mix = 1;}
	//	else{not_mix = 1;}
	//}
	//if ( in_mix && not_mix) {final_mix = 2;}
	//else if (in_mix) {final_mix = 1;}
	//L("\tFINAL\t%d\n", final_mix);

	return 1;
}
/**********************************************/
void expand_MSGF_TSV( char *tsv_file, char *out_file, const map<int, pc_t> &map_pep, const map<string, vector<string> > &map_target )
{

	char *readline=(char*)malloc(MAX_LINE);
	char *misc=(char*)malloc(TOKEN_LENGTH);
	char *match=(char*)malloc(MAX_LINE);
	char *m_pep=(char*)malloc(TOKEN_LENGTH);
	char *mix_name=(char*)malloc(TOKEN_LENGTH);

	int count = 0,
		good_tsv = 0;
	int flag_mix = 0; // 0 if all sv from non-mix patient; 1 if all sv from in-mix patient; 2 if both
	int offset =0;
	uint32_t n_svp = 0; // total number of sv peptides;
	uint32_t start = 0, end = 0,
			score =0, rid = 0;

	int n_mod = 0;
	string new_psm, new_pep, new_mix;
	vector<int> vec_id;
	map<string, int> psm_summary; // count of patients source for a single spectra
	int num_exp = 0;

	FILE *fp = fopen( tsv_file, "r");
	FILE *out_fp = fopen( out_file, "w");
	//FILE *out_fp= fopen( out_file, "w");
	//int cur = 0, cur_gene = 0;
	int ret;
	int flag_psm = 0;
	while( NULL != fgets(readline, MAX_LINE, fp) )
	{
		if ( 0 == strncmp("#", readline, 1)){ continue; }
		ret = sscanf( readline, "%s %s %s %[^\t]\t%s %s %s %s %s %s %s %n\n", 
			mix_name, misc, misc, misc, misc, misc, misc, misc, misc, 
			m_pep,	match, &offset);
		flag_psm = get_pep_count( match, vec_id );
		//fprintf(out_fp, "%d\t%s", flag_psm, readline);
		if ( flag_psm )
		{	
			copyToStringStrip( readline, new_psm);
			copyToStringStrip( mix_name, new_mix);
			n_mod = get_msgf_peptide( m_pep, new_pep );
			// Calculate number of relevant Entries for the spectra
			n_svp = expand_svp( vec_id, map_pep, map_target, psm_summary, new_mix, flag_mix );
			L("\tFINAL\t%d\n", flag_mix);
			// Expand to ALL Possible SV Peptides
			output_expansion( vec_id, map_pep, map_target, psm_summary, new_mix, flag_mix, new_psm, new_pep, n_mod, n_svp, out_fp );
			fprintf(out_fp, "%s\t%s\t%lu\t%u\n", new_psm.c_str(), new_pep.c_str(), vec_id.size(), n_svp );	
			good_tsv++;
		} 
		count++;
	}
	E("Total/Good TSVRecord\t%d\t%d\n", count, good_tsv);
	fclose(fp);
	fclose(out_fp);
	free(m_pep);
	free(mix_name);
	free(misc);
	free(readline);
}
/**********************************************/
void OutputSVPEP( char *out_file, const map<string, int> &map_svpep )
{
	map<string, int>::const_iterator it;

	char *log_file=(char*)malloc(MAX_LINE);
	strcpy(log_file, out_file);
	strcat(log_file, ".dict");

	FILE *log_fp = fopen( log_file, "w");
	for ( it = map_svpep.begin(); it != map_svpep.end(); it++)
	{
		fprintf(log_fp, "%s\t%d\n", it->first.c_str(), it->second );
	}
	//E("Total/Good TSVRecord\t%d\t%d\n", count, good_tsv);
	fclose(log_fp);
	free(log_file);
}

// RNA-Seq Part
//
/**********************************************/
int get_sam_bound( const sam_t &tmp_sam, uint32_t &s1, uint32_t &e1, uint32_t &s2, uint32_t &e2 )
{
	int flag = 1; // 1 when the mappings is forward strand, 0 when reverse complement
	if ( 0x10 & tmp_sam.flag ) // reverse complement
	{
		s2 = tmp_sam.pos;
		e2 = tmp_sam.pos + tmp_sam.rl - 1;
		flag = 0;
	}
	else
	{
		s1 = tmp_sam.pos;
		e1 = tmp_sam.pos + tmp_sam.rl - 1;
	}
	return flag;
}
/**********************************************/
int get_sv_bound( const string &refid, uint32_t &sv_s, uint32_t &sv_e ) // Let's do the 
{
	char *misc=(char*)malloc(TOKEN_LENGTH);
	char c;
	uint32_t left = 0, sv_l = 0;
	//L( "Analyze %s\n", refid.c_str());
	sscanf( refid.c_str(), "%[^\_]%c%[^\_]%c%[^\_]%c%u%c%u", misc, &c, misc, &c,  misc, &c, &left, &c, &sv_l);
	//L( "Get %s %u %u\n", misc, left, sv_l);
	sv_s = left + 1;
	sv_e = left + sv_l;
	free(misc);
	return 1;
}

/**********************************************/
// Everything is 1-based, both inclusive
void analyze_sampair( const vector< sam_t > &vec_sam, FILE *con_fp, FILE *dis_fp, int &n_con, int &n_cross )
{	// Assume there are always only two elements in the vector
	//L( "Check %s\t%s\n", vec_sam[0].rid.c_str(), vec_sam[1].rid.c_str() );
	int orient = 0; // 0 when vec_sam[0] is on the left, 1 when it is on the right
	uint32_t s1 = 0 , e1 = 0 , s2 = 0 , e2 = 0;
	uint32_t sv_s = 0, sv_e = 0;
	int flag_all = 0, flag_l = 0, flag_r = 0;
	
	if ( vec_sam[0].rid != vec_sam[1].rid )
	{ L( "Error: Not a pair of reads %s\t%s\n", vec_sam[0].rid.c_str(), vec_sam[1].rid.c_str() );}
	else
	{
		// Only if they are concordant
		if (  ( 0x2 & vec_sam[0].flag ) && ( 0x2 & vec_sam[1].flag ))
		{
			//fprintf( con_fp, "%s%s", vec_sam[0].info.c_str(), vec_sam[1].info.c_str() );
			orient = get_sam_bound( vec_sam[0], s1, e1, s2, e2 );
			orient = get_sam_bound( vec_sam[1], s1, e1, s2, e2 );
			get_sv_bound( vec_sam[0].refid, sv_s, sv_e );
			//////fprintf( con_fp, "%u\t%u\n", sv_s, sv_e );
			flag_all = overlap_length( sv_s, sv_e, s1, e2 );
			flag_l = overlap_length( sv_s, sv_e, s1, e1 );
			flag_r = overlap_length( sv_s, sv_e, s2, e2 );
			//fprintf( con_fp, "%u\t%u\t%d\t%d\t%d\n", sv_s, sv_e, flag_all, flag_l, flag_r );
			if ( flag_all || flag_l || flag_r )
			{
				fprintf( con_fp, "%s\t%s\t%u\t%d\t%d\t%d\t", vec_sam[0].refid.c_str(), vec_sam[0].rid.c_str(), sv_e - sv_s + 1 , flag_all, flag_l, flag_r);
				fprintf( con_fp, "%u\t%u\t%u\t%u\t%u\t%u\t", sv_s, sv_e, s1, e1, s2, e2 );
				fprintf( con_fp, "%d\t%d\t%d\t", orient, vec_sam[0].hd, vec_sam[1].hd );
				fprintf( con_fp, "%s\t%s\t%u\t%u\t%u\t%u\t", vec_sam[0].md.c_str(), vec_sam[1].md.c_str(), vec_sam[0].flag, vec_sam[1].flag, vec_sam[0].pos, vec_sam[1].pos);
				fprintf( con_fp, "%s\t%s\t%s\t%s\n", vec_sam[0].seq.c_str(), vec_sam[1].seq.c_str(), vec_sam[0].qual.c_str() , vec_sam[1].qual.c_str() );
				//GREAT1\t%s\t%s\nGREAT2\t%s\t%s\n", vec_sam[0].md.c_str(), vec_sam[0].info.c_str(), vec_sam[1].md.c_str(), vec_sam[1].info.c_str() );
				fprintf( dis_fp, "%s\n%s\n", vec_sam[0].info.c_str(), vec_sam[1].info.c_str() );
				n_cross++;
			}

			n_con++;
		}
		//else
		//{
		//	fprintf( dis_fp, "%s%s", vec_sam[0].info.c_str(), vec_sam[1].info.c_str() );
		//}
	}
}


/**********************************************/
void get_OPTField( char *option_tag, sam_t &tmp_sam)//string &md_str)
{
	char *tag=(char*)malloc(MAX_LINE);
	char c;
	int value=-1;
	int offset;
	while( *option_tag )
	{
		sscanf( option_tag, "%s %n", tag, &offset);
		if ( 0 == strncmp("MD", tag, 2))
		{
			copyToString(tag + 5, tmp_sam.md );
			//value=md_length(tag+5);
		}
		else if ( 0 == strncmp("NM", tag, 2))
		{
			sscanf( tag + 5, "%d", &tmp_sam.hd);
		}
		option_tag+=offset;
	}
	free(tag);
}
/**********************************************/
void parse_RNASeq_SV( char *sam_file, char *outfile)
{

	char *readline=(char*)malloc(MAX_LINE);
	char *readid=(char*)malloc(MAX_LINE);
	char *prev_id=(char*)malloc(MAX_LINE);
	char *refid=(char*)malloc(MAX_LINE);
	char *cigar=(char*)malloc(MAX_LINE);
	char *rnext=(char*)malloc(MAX_LINE);
	char *seq=(char*)malloc(MAX_LINE);
	char *qual=(char*)malloc(MAX_LINE);
	uint32_t flag, pos, mapq, pnext, tlen;
	int offset;

	string mapping_str;
	vector< sam_t > tmp_vec;
	int pair_ct = 0; 


	int num_con = 0, num_cross = 0;

	char *out_name=(char*)malloc(MAX_LINE);
	char *log_name=(char*)malloc(MAX_LINE);
	strcpy(out_name, outfile);
	strcpy(log_name, outfile);
	strcat(out_name, ".report");
	strcat(log_name, ".log");
	FILE *out_fp = fopen( out_name, "w");
	FILE *log_fp = fopen( log_name, "w");
	//FILE *oea_fp = fopen( "oea.read", "w");
	FILE *fp;// = fopen( sam_file, "r");
	if ( '\0' == sam_file[0] ) 
	{
		fp = stdin;
	}
	else 
	{
		fp = fopen(sam_file, "r");
	}
	sam_t tmp_sam;

	int count = 0;
	while( NULL != fgets(readline, MAX_LINE, fp) )
	{
		if ( 0 == strncmp( readline, "@", 1) ) // sam header
		{	continue;	}

		//sscanf( readline, "%s %u %s %u %u %s %s %u %u %s %s %n", readid, &flag,
		//	refid, &pos, &mapq, cigar, rnext, &pnext, &tlen, seq, qual,
		//	&offset);
		sscanf( readline, "%s %u %s %u %u %s %s %u %d %s %s %n", readid, &tmp_sam.flag,
			refid, &tmp_sam.pos, &mapq, cigar, rnext, &tmp_sam.pnext, &tmp_sam.tlen, seq, qual,
			&offset);

		copyToString( readid, tmp_sam.rid );
		copyToString( refid, tmp_sam.refid );
		copyToString( cigar, tmp_sam.cigar );
		copyToString( rnext, tmp_sam.rnext );
		copyToString( seq, tmp_sam.seq );
		copyToString( qual, tmp_sam.qual );
		copyToStringStrip( readline, tmp_sam.info );
		tmp_sam.rl = calculate_ml( cigar);
		//get_MDField( readline + offset, tmp_sam.md);
		get_OPTField( readline + offset, tmp_sam );
		tmp_vec.push_back( tmp_sam );
		pair_ct++;

		if ( 2 == pair_ct )
		{
			analyze_sampair( tmp_vec, out_fp, log_fp, num_con, num_cross );
			tmp_vec.clear();
			pair_ct = 0;
			//L("Checking Read %d\n", count);
		}

		count++;
	}
	E("Number_ALL/Con/Cross:\t%d\t%d\t%d\n", count/2, num_con, num_cross);
	fclose(fp);
	fclose(out_fp);
	fclose(log_fp);
	free(out_name);
	free(log_name);
																																																	   //
																																																	   //
}
/**********************************************/
int count_hd( char *md )
{
	int bad_base = 0; // number of invalid bases in overlappings
	while( *md)
	{
		if ( !isdigit(*md) ){ bad_base++;}
		md++;
	}
	return bad_base; 
}

/**********************************************/
int check_mapping( char *md, int left, int right, int &l_bad, int &r_bad )
{
	int length = 0;
	int tmp = 0;
	int bad_base = 0; // number of invalid bases in overlappings
	while( *md)
	{
		if ( isdigit(*md) )
		 { tmp = 10 * tmp + (*md - '0');}
		 else
		 {
		 	length += tmp;
			tmp = 0;
			if ( (left <= length ) && ( length <= right )){ bad_base++;}
			
			if ( length < left){ l_bad++;}
			else if ( right < length){ r_bad++;}
		 }
		 md++;
	}
	if ( tmp ){ length += tmp;} // just in case all digits
	return bad_base; 
}
/**********************************************/
// We ASSUME THERE IS AT LEAST ONE BP OVERLAP
// If the mapping actually cross bp 
int check_cross( uint32_t sv_s, uint32_t sv_e, uint32_t s, uint32_t e, char *md, int &over_length, int &bad_l, int &l_effect, int &r_effect )
{
	int flag = 0; // 1 for actual crossing
	int sv_length = sv_e - sv_s + 1;
	int bad_base = 0; // bad base within overlapping part
	int l_bad = 0, r_bad = 0; // bad base on left/right side of bp
	int left = -1, right = -1; //overlapping region on read. 0-based both inclusvie
	//l_effect = 0; r_effect = 0;
	if ( ( sv_s <= s) && ( e <= sv_e) )	// definitely a hit
	{ 
		bad_base = count_hd(md);
		over_length = e - s + 1;
	} 
	else if ( overlap_length( sv_s, sv_e, s, e ) )
	{
		right = sv_e - s;
		if ( sv_s <= s)
		{	left = 0;	}
		else
		{
			left = sv_s - s ;
			if ( e < sv_e ){ right = e - s;}
		}

		//L("%u\t%u\t%u\t%u\t%u\t%u\n", sv_s, sv_e, s, e, left, right );		
		over_length = right - left + 1;
		bad_base = check_mapping( md, left, right, l_bad, r_bad );
	}

	bad_l = bad_base; 
	if ( bad_l < over_length ) 
	{
		flag = 1;	// there are mapped valid bases 
		l_effect = left - l_bad; // before bp1
		r_effect = e - s - right - r_bad; //after bp2
		//if ( l_effect || r_effect) { flag = 2;} // REAL CROSS
		// Get Strict Condition
		//if ( (0 == left) && (  r_effect ) && ( 1 < over_length -bad_l )) { flag = 2;} // both sides have mapped bases
		//else if ( ( right == e - s ) && ( l_effect ) && ( 1 < over_length - bad_l )) { flag = 2;}
		//else if ( ( 0 < left ) && ( right < e-s) && (1 < l_effect) || (1 < r_effect) ){ flag = 2;} // sv is a substring of read
		if ( ( 0 < left ) && ( right < e-s) && (1 < l_effect) && (1 < r_effect) ){ flag = 2;} // sv is a substring of read
	} 


	L("SUM\t%d\t%d\t%u\t%u\t%u\t%u\t%u\t%d\t%d\t%d\t%d\n\n", flag, bad_l, over_length, sv_s, sv_e, s, e, left, right, l_effect, r_effect );		
	return flag;
}
/**********************************************/
void extract_RNASeq_Read( char *sam_file, char *outfile)
{

	char *readline=(char*)malloc(MAX_LINE);
	char *readid=(char*)malloc(MAX_LINE);
	char *prev_id=(char*)malloc(MAX_LINE);
	char *refid=(char*)malloc(MAX_LINE);
	char *cigar=(char*)malloc(MAX_LINE);
	char *rnext=(char*)malloc(MAX_LINE);
	char *seq=(char*)malloc(MAX_LINE);
	char *qual=(char*)malloc(MAX_LINE);
	char *md1=(char*)malloc(MAX_LINE);
	char *md2=(char*)malloc(MAX_LINE);
	uint32_t flag, pos, mapq, pnext, tlen;
	uint32_t sv_s, sv_e, l_s, l_e, r_s, r_e;
	int sv_length, over_all, l_over, r_over,
		orient, hd1, hd2,
		offset;

	int l_valid, r_valid,
		valid = 0; // if the mapping actually cross the bp
	int l_length, r_length; // overlap length of left and right side
	int l_bad, r_bad;
	int l_le, l_re,
		r_le, r_re;

	string mapping_str;

	int n_pass = 0, n_ignore = 0;

	char *out_name=(char*)malloc(MAX_LINE);
	char *log_name=(char*)malloc(MAX_LINE);
	strcpy(out_name, outfile);
	strcpy(log_name, outfile);
	strcat(out_name, ".pass");
	strcat(log_name, ".ignore");
	FILE *out_fp = fopen( out_name, "w");
	FILE *log_fp = fopen( log_name, "w");
	//FILE *oea_fp = fopen( "oea.read", "w");
	FILE *fp;// = fopen( sam_file, "r");
	if ( '\0' == sam_file[0] ) 
	{
		fp = stdin;
	}
	else 
	{
		fp = fopen(sam_file, "r");
	}

	string all_record;

	int count = 0;
	while( NULL != fgets(readline, MAX_LINE, fp) )
	{
		if ( 0 == strncmp( readline, "@", 1) ) // sam header
		{	continue;	}

		sscanf( readline, "%s %s %d %d %d %d %u %u %u %u %u %u %d %d %d %s %s %n", 
			refid, readid, 
			&sv_length, &over_all, &l_over, &r_over, 
			&sv_s, &sv_e, &l_s, &l_e, &r_s, &r_e,
			&orient, &hd1, &hd2,
			md1, md2,
			&offset);

		l_valid = 0; r_valid = 0; valid = 0; // assume not valid
		l_length =0; r_length =0; l_bad = 0; r_bad = 0;
		l_le = 0; l_re = 0; r_le = 0; r_re= 0;
		if ( 0 == orient )
		{
			L("0\t%s", readline);
			if (l_over) {l_valid = check_cross( sv_s, sv_e, l_s, l_e, md1, l_length, l_bad, l_le, l_re );}
			if (r_over) {r_valid = check_cross( sv_s, sv_e, r_s, r_e, md2, r_length, r_bad, r_le, r_re );}
		}
		else
		{
			L("1\t%s", readline);
			if (r_over) {r_valid = check_cross( sv_s, sv_e, l_s, l_e, md1, r_length, r_bad, r_le, r_re );}
			if (l_over) {l_valid = check_cross( sv_s, sv_e, r_s, r_e, md2, l_length, l_bad, l_le, l_re );}
		}

		copyToStringStrip( readline, all_record );
		if ( l_valid + r_valid )
		{
			//fprintf( out_fp, "%s\t%d\t%d\t%d\t%d\t%d\t%d\n", all_record.c_str(), l_valid, r_valid, l_bad, r_bad, l_length, r_length); n_pass++;
			fprintf( out_fp, "%s\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\n", all_record.c_str(), l_valid, r_valid, l_le, l_length - l_bad, l_re, r_le, r_length - r_bad, r_re ); n_pass++;
		}
		else
		{
			//fprintf( log_fp, "%s\t%d\t%d\t%d\t%d\t%d\t%d\n", all_record.c_str(), l_valid, r_valid, l_bad, r_bad, l_length, r_length);n_ignore++;
			fprintf( log_fp, "%s\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\n", all_record.c_str(), l_valid, r_valid, l_le, l_length - l_bad , l_re, r_le, r_length - r_bad, r_re ); n_ignore++;
		}


		count++;
	}
	E("Number_ALL/Pass/Ignore:\t%d\t%d\t%d\n", count, n_pass, n_ignore);
	fclose(fp);
	fclose(out_fp);
	fclose(log_fp);
	free(out_name);
	free(log_name);
																																																	   //
																																																	   //
}
/**********************************************/
void summarize_RNASeq_Read( char *sam_file, char *outfile)
{

	char *readline=(char*)malloc(MAX_LINE);
	char *readid=(char*)malloc(MAX_LINE);
	char *prev_id=(char*)malloc(MAX_LINE);
	char *refid=(char*)malloc(MAX_LINE);
	char *cigar=(char*)malloc(MAX_LINE);
	char *rnext=(char*)malloc(MAX_LINE);
	char *seq=(char*)malloc(MAX_LINE);
	char *qual=(char*)malloc(MAX_LINE);
	char *misc0=(char*)malloc(MAX_LINE);
	char *misc1=(char*)malloc(MAX_LINE);
	char *misc2=(char*)malloc(MAX_LINE);
	char *md1=(char*)malloc(MAX_LINE);
	char *md2=(char*)malloc(MAX_LINE);
	uint32_t flag, pos, mapq, pnext, tlen;
	uint32_t sv_s, sv_e, l_s, l_e, r_s, r_e;
	int sv_length, over_all, l_over, r_over,
		orient, hd1, hd2,
		offset;
	int sv_id;

	int l_valid, r_valid,
		valid = 0; // if the mapping actually cross the bp
	int l_length, r_length; // overlap length of left and right side
	int l_le, l_re,
		r_le, r_re;
	int n_read = 0;
	string mapping_str;

	int n_pass = 0, n_ignore = 0;
	int n_total = 0, n_left = 0, n_right = 0, n_both = 0; // all supports, support left, support right, support both;
	int t_flag;
	int cur_length = 0;
	int max_l = 0, max_r = 0; // max overlap on both side

	char *out_name=(char*)malloc(MAX_LINE);
	char *log_name=(char*)malloc(MAX_LINE);
	strcpy(out_name, outfile);
	strcpy(log_name, outfile);
	strcat(out_name, ".summary");
	strcat(log_name, ".detail");
	FILE *out_fp = fopen( out_name, "w");
	FILE *log_fp = fopen( log_name, "w");
	//FILE *oea_fp = fopen( "oea.read", "w");
	FILE *fp;// = fopen( sam_file, "r");
	if ( '\0' == sam_file[0] ) 
	{
		fp = stdin;
	}
	else 
	{
		fp = fopen(sam_file, "r");
	}

	string all_record;

	map<string, string > map_read; // (read_id, content)
	map<string, string >::iterator it;
	int current_id = -1;// the sv_id we are examingin so far
	int count = 0;
	while( NULL != fgets(readline, MAX_LINE, fp) )
	{
		if ( 0 == strncmp( readline, "@", 1) ) // sam header
		{	continue;	}

		sscanf( readline, "%[^\_]_%d%s %s %d %d %d %d %u %u %u %u %u %u %d %d %d %s %s %d %d %d %d %s %s %s %s %d %d %d %d %d %d %d %d %n", 
			misc1, &sv_id, misc2, 
			readid, 
			&sv_length, &over_all, &l_over, &r_over, 
			&sv_s, &sv_e, &l_s, &l_e, &r_s, &r_e,
			&orient, &hd1, &hd2,
			md1, md2,
			&t_flag, &t_flag, &t_flag, &t_flag,
			seq	, qual, seq, qual,
			&l_valid, &r_valid, &l_le, &l_length,  &l_re, &r_le, &r_length, &r_re,
			&offset);

		//L( "%s %d %s\n", misc1, sv_id, readid);
		if ( sv_id != current_id ) // output the current event
		{
			if ( 0 < (int)map_read.size() )
			{
				// Output the Contents
				fprintf( out_fp, "%s\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\n", misc0, current_id, cur_length, n_total, n_left, n_right, n_both, max_l, max_r);
				n_read += n_total;
				for( it = map_read.begin(); it != map_read.end(); it ++)
				{
					fprintf( log_fp, "%s\n", it->second.c_str());
				}
				
			}
			current_id = sv_id;
			cur_length = sv_length;
			strncpy( misc0, misc1, MAX_LINE);
			map_read.clear();
			n_total = 0; n_left = 0; n_right = 0; n_both = 0; max_l = 0; max_r = 0;
		}

		// We should not consider reads fully contained in long SVs
		if ( ( l_length < l_e - l_s - 2  ) || ( r_length < r_e - r_s  - 2  ) )
		{
			it = map_read.find( readid );
			if ( map_read.end() == it )
			{

				copyToStringStrip( readline, all_record );
				map_read[ readid ] = all_record;
				n_total++;
				if ( (l_valid) && (r_valid)) { n_both++; }
				else if ( l_valid) { n_left++;}
				else if ( r_valid) { n_right++; }

				if ( max_l < l_length){ max_l = l_length;}
				if ( max_r < r_length){ max_r = r_length;}
			}
		}




		count++;
	}
	//Last Record
	if ( 0 < (int)map_read.size() )
	{
		// Output the Contents
		fprintf( out_fp, "%s\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\n", misc1, current_id, cur_length, n_total, n_left, n_right, n_both, max_l, max_r);
		n_read += n_total;
		for( it = map_read.begin(); it != map_read.end(); it ++)
		{
			fprintf( log_fp, "%s\n", it->second.c_str());
		}

	}
	E("Number_Event/Read:\t%d\t%d\n", count, n_read);
	fclose(fp);
	fclose(out_fp);
	fclose(log_fp);
	free(out_name);
	free(log_name);
																																																	   //
																																																	   //
}
/**********************************************/
void count_ProteinID_TSV( char *tsv_file, char *out_file, map<string, int> &map_svpep )
{
	string total_seq="", total_rc="";

	char *readline=(char*)malloc(MAX_LINE);
	char *misc=(char*)malloc(TOKEN_LENGTH);
	char *match=(char*)malloc(MAX_LINE);
	char *m_pep=(char*)malloc(TOKEN_LENGTH);
	//char *sim=(char*)malloc(TOKEN_LENGTH);

	//char header;
	int count = 0,
		good_tsv = 0;
	int offset =0;
	uint32_t n_svp = 0; // total number of sv peptides;
	uint32_t start = 0, end = 0,
			score =0, rid = 0;

	int n_mod = 0;
	string new_psm, new_pep;
	vector<int> vec_id;
	string new_ref;// formatted reference without chr prefix
	string repeat_info;// formatted reference without chr prefix
	FILE *fp = fopen( tsv_file, "r");
	FILE *out_fp = fopen( out_file, "w");
	//FILE *out_fp= fopen( out_file, "w");
	//int cur = 0, cur_gene = 0;
	int ret;
	int flag_psm = 0;
	while( NULL != fgets(readline, MAX_LINE, fp) )
	{
		if ( 0 == strncmp("#", readline, 1)){ continue; }
		ret = sscanf( readline, "%s %s %s %s %s %s %s %s %s %s %s %n\n", 
			misc, misc, misc, misc, misc, misc, misc, misc, misc, 
			m_pep,	match, &offset);
		flag_psm = get_pep_count( match, vec_id );
		//fprintf(out_fp, "%d\t%s", flag_psm, readline);
		//if ( flag_psm )
		//{	
		//	copyToStringStrip( readline, new_psm);
		//	n_mod = get_msgf_peptide( m_pep, new_pep );
		//	// Calculate number of relevant Entries for the spectra
		//	n_svp = get_num_svp( vec_id, map_pep, map_svpep);
		//	fprintf(out_fp, "%s\t%s\t%lu\t%u\n", new_psm.c_str(), new_pep.c_str(), vec_id.size(), n_svp );	
		//	good_tsv++;
		//} 
		count++;
	}
	E("Total/Good TSVRecord\t%d\t%d\n", count, good_tsv);
	fclose(fp);
	fclose(out_fp);
	free(m_pep);//	free(ref);
	free(misc);
	free(readline);
}

// Adjust MS-GF+ Confidence Q-value based on two-class classification //
/**********************************************/
// decoy_flag 	1: ignore anything matching to decoy. 
// 				0: include everthing
//void getTwoClassPvalue( char *tsv_file, char *out_file, const map<string, int> &k_map, const map<string, int> &n_map, const map<string, int> &p_map, float q_value, int decoy_flag )
void getTwoClassPvalue( char *tsv_file, char *out_file , const map<string, int> &k_map, const map<string, int> &n_map, const map<string, int> &p_map   )
{
	char *readline = (char*)malloc( MAX_LINE );
	char *match    = (char*)malloc( MAX_LINE );
	char *spec_id  = (char*)malloc( TOKEN_LENGTH );
	char *cur_id   = (char*)malloc( TOKEN_LENGTH );

	// Filename of seperated known tsv and novel tsv
	string known_tsv = add_extension( out_file, "known");
	string novel_tsv = add_extension( out_file, "novel");
	


	int count = 0;
	int offset =0;
	
	int pep_flag = 0;
	int n_mod = 0;
	string pep_str="", rev_pep="";

	int file_type = -1; // 1 for 17-column. Let's deal with Original MS-GF+ data here
	int num_token;
	float q_tsv = 0.0;

	const char *decoy_prefix = "XXX";
	const char *known_prefix = "ENSP";
	char *pch;
	int valid_flag = 0; // valid 1 as long as it's related to known or decoy_known. 0 otherwise

	map<string, int> cache_map; // A cache once we met some strings that we visit before
	map<string, int>::iterator it;
	int n_sig = 0, n_dup = 0;
	int i =0;
	string tmp_str;
	vector<string> vec_tsv; // tmp records for all lines associated to the same ID

	FILE *fp       = fopen( tsv_file, "r" );
	FILE *known_fp = fopen( known_tsv.c_str(), "w" );
	FILE *novel_fp = fopen( novel_tsv.c_str(), "w" );
	int  ret;
	
	while( NULL != fgets(readline, MAX_LINE, fp) )
	{
		if ( 0 > file_type)
		{
			num_token = get_num_word( readline );
			if ( 17 == num_token ){ file_type = 1;}
			else
			{
				E("Abort: Format Inconsistency issue in MS-GF+ Search Results %s: %d columns detected\n", tsv_file, num_token);
				exit(1);
			}
		}
		
		readline[strcspn(readline, "\n")] = 0; // Get Rid of Terminating New Line
		if ( 0 == strncmp("#", readline, 1) )
		{	// header 
			fprintf( known_fp, "%s\tPEPFLAG\n", readline);
			fprintf( novel_fp, "%s\n", readline);
			continue; 
		}

		get_nth_word( spec_id, readline, 2); // DTA ID
		
		// Starting a new spec ID
		if ( 0 != strncmp( cur_id, spec_id, TOKEN_LENGTH ) )
		{
			//L("START%s\t%d %d\n", spec_id, pep_flag, valid_flag);
			if ( 0 < vec_tsv.size() )
			{
				if ( 0 < valid_flag )
				{
					for( i = 0; i < (int)vec_tsv.size(); i++)
					{ fprintf( known_fp, "%s\t%d\n", vec_tsv[i].c_str(), valid_flag );}
					n_sig++;
				}
				else
				{
					for( i = 0; i < (int)vec_tsv.size(); i++)
					{ fprintf( novel_fp, "%s\n", vec_tsv[i].c_str());}
					n_dup++;
				}
				vec_tsv.clear();
			}
			// Starting New Records
			strncpy( cur_id, spec_id, TOKEN_LENGTH );
			valid_flag = 0;
		}
		
		// Assign the dta to known category as long as the protein is known
		get_nth_word( match, readline, 11); // protein sequence header
		pch = strstr( match, known_prefix);
		//L("b4:%d %d\n", pep_flag, valid_flag);
		if ( pch != NULL ) { valid_flag = 5; }
		//L("after:%d %d\n", pep_flag, valid_flag);
		
		get_nth_word( match, readline, 16);	// q_tsv
		sscanf(match, "%f", &q_tsv);

		
		// Even the peptide is only assigned to novel sequences we still search peptide database to do confirmation
		if ( 0 == valid_flag )
		{
			//L("PEP %d %d\n", pep_flag, valid_flag);
			get_nth_word( match, readline, 10); // peptide sequence
			n_mod = get_msgf_peptide( match, pep_str);
			// Check Sequence Contents			
			pep_flag = 0;
			it = cache_map.find( pep_str);
			if ( cache_map.end() != it )
			{       pep_flag = it->second; }
			else
			{
				//L("pep:%d;", pep_flag);
				if (    find_substring( pep_str, k_map ) ) {pep_flag = 4;}
				else if (       find_substring( pep_str, n_map ) ) {pep_flag = 2;}
				else if (       find_substring( pep_str, p_map ) ) {pep_flag = 1;}
				//L("%d\n", pep_flag);
				cache_map[pep_str] = pep_flag; 
				//L("FLAG\t%s\t%d\n", pep_str.c_str(), pep_flag);
			}

			// To make sure it's not a decoy from known sequences
			if ( 0 == pep_flag )
			{
				make_rev( pep_str, rev_pep);
				it = cache_map.find( rev_pep );
				if ( cache_map.end() != it )
				{       pep_flag = it->second; }
				else
				{
					//L("pep:%d;", pep_flag);
					if (    find_substring( rev_pep, k_map ) ) {pep_flag = 3;}
					else if (       find_substring( rev_pep, n_map ) ) {pep_flag = 3;}
					else if (       find_substring( rev_pep, p_map ) ) {pep_flag = 3;}
					//L("%d\n", pep_flag);
					cache_map[ rev_pep ] = pep_flag; 
					//L("FLAG\t%s\t%d\n", pep_str.c_str(), pep_flag);
				}
			}

			

			valid_flag = pep_flag;
		}

		copyToString( readline, tmp_str);
		vec_tsv.push_back( tmp_str ); 
		
		count++;
		if ( 0 == count%10000){E(".");}
	}

	if ( 0 < vec_tsv.size() )
	{
		if ( 0 < valid_flag)
		{
			for( i = 0; i < (int)vec_tsv.size(); i++)
			{ fprintf( known_fp, "%s\t%d\n", vec_tsv[i].c_str(), valid_flag );}
			n_sig++;
		}
		else
		{
			for( i = 0; i < (int)vec_tsv.size(); i++)
			{ fprintf( novel_fp, "%s\n", vec_tsv[i].c_str() );}
			n_dup++;
		}
		vec_tsv.clear();
	}
	E("\n");
	E("Known/Novel Record\t%d\t%d\n", n_sig, n_dup);
	fclose( fp );
	fclose( known_fp );
	fclose( novel_fp );
	free( readline );
 	free( match );
 	free( spec_id );
 	free( cur_id );
}
/**********************************************/
bool sortByScore( const rank_t &p1, const rank_t &p2 )
{
	return ( (p1.e < p2.e) || ( (p1.e == p2.e) && ( p1.decoy < p2.decoy )) );
}
/**********************************************/
// Adjust MS-GF+ P-value based on two-class classification /////
/**********************************************/
// Since Filtration Step Starts Here We allows flag to specify including decoy not not
// decoy_flag 	1: ignore anything matching to decoy. 
// 				0: include everthing
//void getTwoClassPvalue( char *tsv_file, char *out_file, const map<string, int> &k_map, const map<string, int> &n_map, const map<string, int> &p_map, float q_value, int decoy_flag )
void AdjPvalue( char *out_file )
{
	char *readline=(char*)malloc(MAX_LINE);
	char *match=(char*)malloc(MAX_LINE);
	char *spec_id=(char*)malloc(MAX_LINE);
	char *cur_id=(char*)malloc(MAX_LINE);
	// Filename of seperated known tsv and novel tsv
 	//char *raw_tsv=(char*)malloc(MAX_LINE);
	//char *adj_tsv=(char*)malloc(MAX_LINE);
	//strcpy( raw_tsv, out_file );
	//strcpy( adj_tsv, out_file );
	//strcat( raw_tsv, ".novel");
	//strcat( adj_tsv, ".adj");
	
	// Filename of seperated known tsv and novel tsv
	string raw_tsv = add_extension( out_file, "novel");
	string adj_tsv = add_extension( out_file, "adj");

	map< string, rank_t> map_rank;
	map< string, rank_t>::iterator r_it;

	vector<rank_t> vec_rank;
	rank_t rank_item;

	int count = 0;
	int offset =0;
	
	int pep_flag = 0;
	int n_mod = 0;
	string pep_str="";
	string score_str="";

	int file_type = -1; // 1 for 17-column. Let's deal with Original MS-GF+ data here
	int num_token;
	float q_tsv = 0.0;
	const char *decoy_prefix = "XXX";
	char *pch;
	int decoy_flag = 0; // 1 if it's a decoy sequences. 0 otherwise

	map<string, int> cache_map; // A cache once we met some strings that we visit before
	map<string, int>::iterator it;
	int n_sig = 0, n_dup = 0;
	int i =0;
	int t_decoy = 0, t_num = 0;
	string tmp_str;
	vector<string> vec_tsv; // tmp records for all lines associated to the same ID

	FILE *raw_fp = fopen( raw_tsv.c_str(), "r");
	int ret;

	while( NULL != fgets(readline, MAX_LINE, raw_fp) )
	{
		if ( 0 > file_type)
		{
			num_token = get_num_word( readline);
			if ( 17 == num_token ){ file_type = 1;}
			else
			{
				E("Abort: Format Inconsistency issue in MS-GF+ Search Results %s: %d columns detected\n", raw_tsv.c_str(), num_token);
				exit(1);
			}
		}

		readline[strcspn(readline, "\n")] = 0; // Get Rid of Terminating New Line

		
		if ( 0 == strncmp("#", readline, 1))
		{	continue; }

		get_nth_word( match, readline, 11); // protein sequence header
		pch = strstr( match, decoy_prefix);
		decoy_flag = 0;
		if ( pch != NULL ) { decoy_flag = 1; }
		get_nth_word( match, readline, 15); // sequence header
		sscanf( match, "%f", &q_tsv);

		copyToString( match, score_str );
		r_it = map_rank.find( score_str);
		if ( r_it != map_rank.end() )
		{
			r_it->second.num++;
			if ( decoy_flag) 
			{	r_it->second.n_decoy++;	}
		}
		else
		{
			rank_item.num=1;
			rank_item.n_decoy=0;
			if ( decoy_flag) 
			{	rank_item.n_decoy++;}
			rank_item.e=q_tsv;
			rank_item.score = score_str;
			map_rank[ score_str ] = rank_item;
			
		}
		
		count++;

		if ( 0 == count%10000){E(".");}
	}

	// Sorting the Vector
	for (r_it = map_rank.begin(); r_it != map_rank.end(); r_it ++ )
	{
		vec_rank.push_back( r_it->second);
	}
	sort( vec_rank.begin(), vec_rank.end(), sortByScore );
	map_rank.clear();

	for (i = 0; i< vec_rank.size(); i++)
	{
		vec_rank[i].n_decoy += t_decoy;
		vec_rank[i].num += t_num;
		L( "%d %s %f %d %d %f\n", i, vec_rank[i].score.c_str(), vec_rank[i].e, vec_rank[i].n_decoy, vec_rank[i].num, vec_rank[i].n_decoy*1.0/vec_rank[i].num);
		t_decoy = vec_rank[i].n_decoy;
		t_num = vec_rank[i].num;
		map_rank[ vec_rank[i].score ] = vec_rank[i];
	}
	E("%d Decoy in  %d  Records\n", t_decoy, t_num);
	fclose( raw_fp );
	// Outputing Results

	FILE *fp = fopen( raw_tsv.c_str(), "r");
	FILE *adj_fp = fopen( adj_tsv.c_str(), "w");
	vector<string> vec_str;
	while( NULL != fgets(readline, MAX_LINE, fp) )
	{
		vec_str.clear();
		vec_str = splitString( readline, '\t');
		num_token = (int)vec_str.size();
		if ( 0 == strncmp("#", readline, 1))
		{
			for( i=0; i< num_token-2; i++ )
			{	fprintf( adj_fp, "%s\t", vec_str[i].c_str() );	}
			fprintf( adj_fp, "AdjPepQValue\n");
			continue; 
		}
		
		r_it = map_rank.find( vec_str[14] );
		if ( r_it != map_rank.end() )
		{
			for(i=0; i< num_token-2; i++){ fprintf( adj_fp, "%s\t", vec_str[i].c_str() );}
			fprintf( adj_fp, "%f\n", r_it->second.n_decoy*1.0/r_it->second.num );
		}
		else{ E("Missing information for %s\n", vec_str[14].c_str());}

	}

	//E("Total References with Predictions:\t%d\n", (int)map_repeat.size());
	fclose( fp );
	fclose( adj_fp );
	free( readline );
 	free( match );
 	free( spec_id );
	//free( raw_tsv );
	//free( adj_tsv );
}
/**********************************************/
// Get Peptide-level q-value based on original MS-GF+ results containing only aberrant search results
//
// out_file.novel contains only spectra matching to aberrant peptides or their decoy 
// For each peptide, obtain its best E-value along with the sequence property (target or decoy)
// Sort all peptides by their E-value and get the new peptide-level q-value
//
/**********************************************/
void AdjPepQvalue( char *out_file )
{
	char *readline = (char*)malloc( MAX_LINE );
	char *match    = (char*)malloc( MAX_LINE );
	char *spec_id  = (char*)malloc( TOKEN_LENGTH );
	char *cur_id   = (char*)malloc( TOKEN_LENGTH );
	
	// Filename of seperated novel tsv and output adj file
	string raw_tsv = add_extension( out_file, "novel");
	string adj_tsv = add_extension( out_file, "adj");

	// peptide_str -> ranking
	map< string, rank_t> map_rank;
	map< string, rank_t>::iterator r_it;

	vector<rank_t> vec_rank;
	rank_t rank_item;

	int count = 0;
	int offset =0;
	
	int pep_flag = 0;
	int n_mod = 0;
	string pep_str="", score_str="";

	int file_type = -1; // 1 for 17-column. Stop if the file is not Original MS-GF+ data
	int num_token;
	float q_tsv = 0.0;
	const char *decoy_prefix = "XXX";
	char *pch;
	int decoy_flag = 0; // 1 if it's a decoy sequences. 0 otherwise

	map<string, int> cache_map; // A cache once we met some strings that we visit before
	map<string, int>::iterator it;
	int n_sig = 0, n_dup = 0;
	int i =0;
	int t_decoy = 0, t_num = 0;
	string tmp_str;
	vector<string> vec_tsv; // tmp records for all lines associated to the same ID

	FILE *raw_fp = fopen( raw_tsv.c_str(), "r");
	int ret;

	while( NULL != fgets( readline, MAX_LINE, raw_fp ) )
	{
		if ( 0 > file_type)
		{
			num_token = get_num_word( readline );
			if ( 17 == num_token ){ file_type = 1;}
			else
			{
				E("Abort: Format Inconsistency issue in MS-GF+ Search Results %s: %d columns detected\n", raw_tsv.c_str(), num_token);
				exit(1);
			}
		}

		readline[strcspn(readline, "\n")] = 0; // Get Rid of Terminating New Line

		
		if ( 0 == strncmp("#", readline, 1))
		{	continue; }

		// retrieve scores and property for a peptide sequence
		get_nth_word( match, readline, 10); // peptide sequence
		n_mod = get_msgf_peptide( match, pep_str );

		get_nth_word( match, readline, 11); // protein sequence header
		pch = strstr( match, decoy_prefix);
		decoy_flag = 0;
		if ( pch != NULL ) { decoy_flag = 1; }

		get_nth_word( match, readline, 15); // Original E-score
		sscanf( match, "%f", &q_tsv);
		copyToString( match, score_str );

		r_it = map_rank.find( pep_str );
		if ( r_it != map_rank.end() )
		{
			r_it->second.num++;

			if ( r_it->second.e > q_tsv )
			{
				//L("Update %s from %s to %s\n", pep_str.c_str(), r_it->second.score.c_str(), score_str.c_str() );
				r_it->second.e = q_tsv;
				r_it->second.score = score_str;
			}

			if ( decoy_flag ) 
			{	
				r_it->second.decoy = 1;	
				r_it->second.n_decoy++;
			}
		}
		else
		{
			rank_item.num = 1;
			rank_item.n_decoy = decoy_flag;
			rank_item.decoy = 0;
			if ( decoy_flag ) 
			{	rank_item.decoy = 1; }
			rank_item.e=q_tsv;
			rank_item.score = score_str;
			rank_item.pep = pep_str;
			map_rank[ pep_str ] = rank_item;
		}
		
		count++;

		if ( 0 == count%10000){E(".");}
	}

	//for( r_it = map_rank.begin(); r_it != map_rank.end(); r_it++)
	//{
	//	L("%s\t%d\t%f\n", r_it->first.c_str(), r_it->second.decoy, r_it->second.e);
	//}


	// Sorting the Peptide Vector Based on E-Value
	for (r_it = map_rank.begin(); r_it != map_rank.end(); r_it ++ )
	{
		vec_rank.push_back( r_it->second );
	}
	sort( vec_rank.begin(), vec_rank.end(), sortByScore );
	map_rank.clear();

	for (i = 0; i< vec_rank.size(); i++)
	{
		vec_rank[i].n_decoy = vec_rank[i].decoy + t_decoy;
		vec_rank[i].num = 1 + t_num;
		L("%d %s %s %f Type:%d ", i, vec_rank[i].pep.c_str(), vec_rank[i].score.c_str(), vec_rank[i].e, vec_rank[i].decoy );
		L("%d %d %f\n", vec_rank[i].n_decoy, vec_rank[i].num, vec_rank[i].n_decoy*1.0/vec_rank[i].num);
		t_decoy = vec_rank[i].n_decoy;
		t_num = vec_rank[i].num;
		map_rank[ vec_rank[i].pep ] = vec_rank[i];
	}
	E( "%d Decoy in %d Records\n", t_decoy, t_num);
	fclose( raw_fp );
	
	// Outputing Results

	FILE *fp = fopen( raw_tsv.c_str(), "r");
	FILE *adj_fp = fopen( adj_tsv.c_str(), "w");
	vector<string> vec_str;
	while( NULL != fgets(readline, MAX_LINE, fp) )
	{
		vec_str.clear();
		vec_str = splitString( readline, '\t');
		num_token = (int)vec_str.size();
		if ( 0 == strncmp("#", readline, 1))
		{
			for( i=0; i<= num_token-2; i++ )
			{ fprintf( adj_fp, "%s\t", vec_str[i].c_str() );}
			fprintf( adj_fp, "AdjPepQValue\n");
			continue; 
		}
		
		get_nth_word( match, readline, 10); // peptide sequence
		n_mod = get_msgf_peptide( match, pep_str );
		
		r_it = map_rank.find( pep_str );
		if ( r_it != map_rank.end() )
		{
			for( i=0; i<= num_token-2; i++ )
			{ fprintf( adj_fp, "%s\t", vec_str[i].c_str() );}
			fprintf( adj_fp, "%f\n", r_it->second.n_decoy*1.0/r_it->second.num );
		}
		else{ E("Missing Peptide %s\n", pep_str.c_str());}

	}

	//E("Total References with Predictions:\t%d\n", (int)map_repeat.size());
	fclose( fp );
	fclose( adj_fp );
	free( readline );
 	free( match );
 	free( spec_id );
}
///**********************************************/
//void copyToString( char *src, string &new_str)
//{
//	int limit=strlen(src);
//	new_str.clear();
//	new_str.reserve(limit);
//	for( int i = 0; i < limit; i ++)
//	{
//		new_str.push_back(src[i]);
//	}
//}

///**********************************************/
//bool sortByStart( const unit_pred &p1, const unit_pred &p2 )
//{
//	return p1.s < p2.s;
//}
///**********************************************/
///**********************************************/
//void SortPredictionOnly( map<string, vector<unit_pred> > &new_record, map<string, vector<unit_pred> > &map_record  )
//{
//	int limit=0;
//	map<string, vector<unit_pred> >::iterator it;
//	for( it = map_record.begin(); it != map_record.end(); it++)
//	{
//		// sorting by the starting coordinate
//		sort( it->second.begin(), it->second.end(), sortByStart);
//		limit=(int)it->second.size();
//		int diff = 0;
//
//		vector<unit_pred> tmp_pred;
//		tmp_pred.reserve( limit );
//		tmp_pred.push_back(it->second[0]);
//		L( "==%d\t%s\t%u\t%u\t%u\n", it->second[0].id, it->first.c_str(), it->second[0].s, it->second[0].e, it->second[0].s);
//		for( int i =1; i < limit; i ++)
//		{
//			tmp_pred.push_back( it->second[i] );
//		}
//
//		new_record[it->first] = tmp_pred;
//	}
//	
//
//}
///**********************************************/
//void analyze_record( unit_pred &pred)
//{
//	// determine the boundary of inversion calls
//	int a =0;
//}
///**********************************************/
//// To Compute the extended boundary of contigs on the reference
//void MergePrediction( map<string, vector<unit_pred> > &sorted_record,  char *outfile  )
//{
//	int limit=0;
//	map<string, vector<unit_pred> >::iterator it;
//	for( it = sorted_record.begin(); it != sorted_record.end(); it++)
//	{
//		// Reading sorted record for a chromosome
//		limit=(int)it->second.size();
//		int diff = 0;
//
//		//vector<unit_pred> tmp_pred;
//		//tmp_pred.reserve( limit );
//		//tmp_pred.push_back(it->second[0]);
//		L( "==%d\t%s\t%u\t%u\t%u\n", it->second[0].id, it->first.c_str(), it->second[0].s, it->second[0].e, it->second[0].s);
//		for( int i =1; i < limit; i ++)
//		{
//			// calculate the boundary of this record
//			 analyze_record( it->second[i]);
//			//if ( ( it->second[i].s != it->second[i-1].s) ||  
//			//	(it->second[i].e != it->second[i-1].e) 
//			//	)
//			//{
//			//	tmp_pred.push_back( it->second[i] );
//			//	diff = it->second[i].s - it->second[i-1].e;
//			//	if ( 0 >= diff)
//			//	{
//			//		E("Warning\tOverlap %s %d %u %u with %d\n", it->first.c_str(), it->second[i].id, it->second[i].s, it->second[i].e, diff) ;
//			//	}
//			//	L( "==%d\t%s\t%u\t%u\t%d\n", it->second[i].id, it->first.c_str(), it->second[i].s, it->second[i].e, diff);
//
//			//}
//			//else
//			//{
//			//	E("Warning\tduplicate %s %d %u %u\n", it->first.c_str(), it->second[i].id, it->second[i].s, it->second[i].e);
//			//}
//		}
//
//		//new_record[it->first] = tmp_pred;
//	}
//	
//
//}
///**********************************************/
//void SortPrediction( map<string, vector<unit_pred> > &new_record, map<string, vector<unit_pred> > &map_record  )
//{
//	int limit=0;
//	map<string, vector<unit_pred> >::iterator it;
//	for( it = map_record.begin(); it != map_record.end(); it++)
//	{
//		// sorting by the starting coordinate
//		sort( it->second.begin(), it->second.end(), sortByStart);
//		limit=(int)it->second.size();
//		int diff = 0;
//
//		vector<unit_pred> tmp_pred;
//		tmp_pred.reserve( limit );
//		tmp_pred.push_back(it->second[0]);
//		L( "==%d\t%s\t%u\t%u\t%u\n", it->second[0].id, it->first.c_str(), it->second[0].s, it->second[0].e, it->second[0].s);
//		for( int i =1; i < limit; i ++)
//		{
//			if ( ( it->second[i].s != it->second[i-1].s) ||  
//				(it->second[i].e != it->second[i-1].e) 
//				)
//			{
//				tmp_pred.push_back( it->second[i] );
//				diff = it->second[i].s - it->second[i-1].e;
//				if ( 0 >= diff)
//				{
//					E("Warning\tOverlap %s %d %u %u with %d\n", it->first.c_str(), it->second[i].id, it->second[i].s, it->second[i].e, diff) ;
//				}
//				L( "==%d\t%s\t%u\t%u\t%d\n", it->second[i].id, it->first.c_str(), it->second[i].s, it->second[i].e, diff);
//
//			}
//			else
//			{
//				E("Warning\tduplicate %s %d %u %u\n", it->first.c_str(), it->second[i].id, it->second[i].s, it->second[i].e);
//			}
//		}
//
//		new_record[it->first] = tmp_pred;
//	}
//	
//
//}
///**********************************************/
//void OutputPrediction( map<string, vector<unit_pred> > &new_record, char *output  )
//{
//	char *out_name=(char*)malloc(MAX_LINE);
//	strcpy(out_name, output);
//	strcat(out_name, ".predict");
//	FILE *out_fp=fopen( out_name, "w");
//	int count = 0;
//
//	map<string, vector<unit_pred> >::iterator it;
//	for( it = new_record.begin(); it != new_record.end(); it++)
//	{
//		int limit = it->second.size();
//		for ( int i=0; i < limit; i++)
//		{
//			fprintf( out_fp, "%d\t%d\t%s\t%u\t%u\n", count, it->second[i].id, it->first.c_str(), it->second[i].s, it->second[i].e);
//			fprintf( out_fp, "%s%s%s", it->second[i].ref.c_str(), it->second[i].align.c_str(), it->second[i].contig.c_str());
//			count++;
//		}
//	}
//	fclose(out_fp);
//	
//}
