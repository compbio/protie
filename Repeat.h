#ifndef __REPEAT__
#define __REPEAT__
#include <map>
#include <vector>
#include <string>
#include "Common.h"
typedef struct
{
	uint32_t rid;
	uint32_t score;
	uint32_t s;
	uint32_t e;
	std::string ref;
	std::string r_info;
}repeat_t;


void convert_ensembl_ref( char *chr_char, std::string &chr_str );//, std::map< std::string, std::vector< unit_pred> > &map_record );
void convert_repeat_ref( char *fea, std::string &repeat_info );//, std::map< std::string, std::vector< unit_pred> > &map_record );
bool sortRepeatByStart(const repeat_t &p1, const repeat_t &p2);
bool sortRepeatByEnd(const repeat_t &p1, const repeat_t &p2);
void parse_Repeat( char *repeat_file, std::map<std::string, std::vector<repeat_t> > &repeat_map );//, std::map< std::string, std::vector< unit_pred> > &map_record );
//void parse_Repeat( char *predict_file, std::map< std::string, std::vector< unit_pred> > &map_record );


//void copyToString( char *src, std::string &new_str);
//void SortPredictionOnly( std::map< std::string, std::vector< unit_pred > > &new_record, std::map< std::string, std::vector< unit_pred> > &map_record );
//void OutputPrediction( std::map< std::string, std::vector< unit_pred > > &new_record, char *output);

//void analyze_record( unit_pred &pred );
//void MergePrediction( std::map< std::string, std::vector< unit_pred > > &sorted_record, char *output);

#endif
