#ifndef __PEPTIDE__
#define __PEPTIDE__
//#include <vector>
#include <map>
#include <string>

#include "Common.h"
typedef struct
{
	int id;
	int pepid;
	int status; // 0 for modified; 1 for as predicted
	int num_gene;
	int cds;
	char strand;
	std::string chr;
	uint32_t s; // 0-based inclusive
	uint32_t e; // 0-based EXCLUSIVE
	uint32_t pos; // 0-based inclusive
	int bp_s;
	int bp_e;
	int len;
	std::string sv; // type of sv
	std::string patient;
	std::string gid;
	std::string tid;
	std::string seq;
}sv_trans_t;


//// Protein Level
//int ol_length( const int s1, const int e1, const int s2, const int e2);
//void IsoformToProtein( char *pred_file, char *out_file );
//// Peptide Level with ALL possible peptides before meeting stop codon
//int get_mis_str( const string &pep_str, string &mis_str, vector<int> kr_vec, int idx, int &left, int &right);
//int get_kr_pos( const string &pep_str, vector<int> &kr_vec);
//void IsoformToPeptide( char *pred_file, char *out_file );
//
//void ConvertToProteins( char *pred_file, char *out_file, char *lib_name, int pep_length, int mode);
void DNATranslate( const std::string &g_seq, std::vector<std::string> &trasn_vec, int flag );
//int bp_dist(int left, int right, int bp);
char ToProtein( const std::string &trigram);
//void SummarizeFasta( char *pred_file, char *out_file );
//void ExtractPeptideFromFasta( char *pred_file, char *out_file );
//void ExtractPeptideFromFasta_Mis( char *pred_file, char *out_file );
//int generate_peptide(string &final_str, int left, int right, char *raw_str);

// for deFuse Prediction 
void ParseDefuse( char *predict_file, char *out_file);
int scan_bp(const std::string &raw_str, std::string &new_str);
void translate_fusion( char *id, const std::string &raw_str, int strand, int bp, char *out_file);

// for MiStrVar SV Transcript Fasta File
void clear_trans( sv_trans_t &t);
int check_protein( const std::string &prot, int aa_start, FILE *log );
void get_protein( sv_trans_t &t, FILE *prot_fa, FILE *pep_fa, FILE *pep_info );
char to_aa( const std::string &str);
void ParseSVTranscript( char *predict_file, char *out_file);
#endif
