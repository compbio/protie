#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>
#include <fstream>
#include <string>
#include <map>

#include "Flag.h"
#include "Common.h"
//#include "GeneAnnotation.h"

#define FASTA_LINE 50

using namespace std;

// Return Yes if this is not a known protein type
/**********************************************/
int IsUnknownProtein( int prot_t )
{
	int flag = 0;
	if ( 4 > prot_t){flag = 1;}
	return flag;
}
/**********************************************/
int IsKnownProtein( int prot_t )
{
	int flag = 0;
	if ( 4 == prot_t){flag = 1;}
	return flag;
}
/**********************************************/
int IsNovelProtein( int prot_t )
{
	int flag = 0;
	if ( 2 == prot_t){flag = 1;}
	return flag;
}
/**********************************************/
int IsPutativeProtein( int prot_t )
{
	int flag = 0;
	if ( 1 == prot_t){flag = 1;}
	return flag;
}

// Classify Protein Property: 1 for putative, 2 for novel, 4 for known proteins
/**********************************************/
int getProteinType( char *prot_type )
{
	int prot_t = 1;
	if ( 0 == strncmp("k", prot_type, 1 ) ) // known
	{ prot_t = 4; }
	else if ( 0 == strncmp("n", prot_type, 1 ) ) // novel
	{ prot_t = 2; }
	else if ( 0 == strncmp("p", prot_type, 1 ) ) // putative
	{ prot_t = 1; }
	else
	{
		E( "Cannot Recognize Protein Type %s\n", prot_type );
		prot_t = 0;
	}
	return prot_t;
}
