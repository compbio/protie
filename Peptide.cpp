#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <iostream>
#include <fstream>
#include <string>
#include <map>

//#include "Fusion.h"
#include "Peptide.h"
#include "Common.h"
//#include "GeneAnnotation.h"

#define FASTA_LINE 50

int peptide_id = 0;
int my_weights[] = {0,5,1,5,5,5,2,5,5,5,5,5,5,5,5,5,5,5,5,3,5,5,5,5,5,5};
char my_aa[]=
{
	'K',//AA
	'N',
	'K',
	'N',
	'T',//AC
	'T',
	'T',
	'T',
	'R', // AG
	'S',
	'R',
	'S',
	'I', // AT
	'I',
	'M',
	'I',
	'Q', // CA
	'H',
	'Q',
	'H',
	'P', // CC
	'P',
	'P',
	'P',
	'R', //CG
	'R',
	'R',
	'R',
	'L', //CT
	'L',
	'L',
	'L',
	'E', //GA
	'D',
	'E',
	'D',
	'A', //GC
	'A',
	'A',
	'A',
	'G', //GG
	'G',
	'G',
	'G',
	'V', //GT
	'V',
	'V',
	'V',
	'-', //TA
	'Y',
	'-',
	'Y',
	'S', //TC
	'S',
	'S',
	'S',
	'-', //TG
	'C',
	'W',
	'C',
	'L', //TT,
	'F',
	'L',
	'F'
};
using namespace std;
///**********************************************/
//int ol_length( const int s1, const int e1, const int s2, const int e2)
//{
//	int length = 0;
//	if ( (s2 <= s1) && ( e1 <= e2) )
//	{
//		length = e1 - s1 + 1;
//	}
//	else if ( (s1 <= s2) && ( e2 <= e1) )
//	{
//		length = e2 - s2 + 1;
//	}
//	else if ( (s2 <= s1) && ( s1 <= e2))
//	{
//		length = e2 - s1 + 1;
//	}
//	else if ( (s2 <= e1) && ( e1 <= e2))
//	{
//		length = e1 - s2 + 1;
//	}
//	return length;
//
//}
//
///**********************************************/
//void IsoformToProtein( char *predict_file , char *out_file )
//{
//	string total_seq="", total_rc="";
//
//	char *readline=(char*)malloc(MAX_LINE);
//	char *patient=(char*)malloc(MAX_LINE);
//	char *chr=(char*)malloc(TOKEN_LENGTH);
//	char *g_id=(char*)malloc(TOKEN_LENGTH);
//	char *t_id=(char*)malloc(TOKEN_LENGTH);
//	char *fea=(char*)malloc(TOKEN_LENGTH);
//	char *iso_seq = (char*)malloc(MAX_LINE);
//
//	char *pep_out=(char*)malloc(MAX_LINE);
//	char *info_out=(char*)malloc(MAX_LINE);
//	strcpy(pep_out, out_file);
//	strcat(pep_out, ".prot");
//	strcpy(info_out, out_file);
//	strcat(info_out, ".protable");
//
//	FILE *out_fp = fopen(pep_out, "w");
//	FILE *info_fp = fopen(info_out, "w");
//
//
//	char strand;
//	int iso_length =0, before_sv = 0, total_sv = 0,
//		bp_1= 0, bp_2 =0; // 0-based bp, both inclusive
//	uint32_t pos =0, start = 0, end = 0;
//
//	int count = 0;
//	int id = 0, support = 0, offset =0;
//	//unit_vcf tmp_vcf;
//	FILE *fp = fopen( predict_file, "r");
//	int ret;
//	int i = 0, orient = 0;
//	int bp_idx = 0, bp_res = 0,valid_length =0;
//	size_t found;
//	vector<string> protein_array;
//	int tmp=0, t_flag =0, t_iso=0;
//	string pep_str="";
//	while( NULL != fgets(readline, MAX_LINE, fp) )
//	{
//		if ( 0 == strncmp("#", readline, 1) )
//		{
//			continue;
//		}
//		t_flag=0;
//		ret = sscanf( readline, "%s %d %s %s %u %u %u %s %s %c %d %d %d %s\n",
//		//ret = sscanf( readline, "%s %u %c %s %s %f %s %s\n",
//			patient, &id, fea, chr, &pos, &start, &end, g_id, t_id, &strand,
//			&iso_length, &before_sv, &total_sv,
//			iso_seq);
//
//		bp_1 = before_sv;
//		bp_2 = before_sv + total_sv - 1; // SV is from bp_1 to bp_2. 0-based, both inclusive
//		//E("OK %s\n", misc);
//		copyToString( iso_seq, total_seq);
//		// Orient 1: 5->3
//		orient = 1;
//		DNATranslate(total_seq, protein_array,  orient );
//		for(i =0; i < (int)protein_array.size(); i++)
//		{
//			bp_idx = (bp_1-i)/3;
//			bp_res = (bp_1-i)%3;
//			found = protein_array[i].find('-');
//			valid_length = (int)protein_array[i].size();
//			if ( found!=string::npos)
//			{
//				valid_length=(int)found;
//			}
//
//			if ( (bp_idx <= valid_length) && ( 5 <= valid_length))
//			{
//				pep_str = protein_array[i].substr(0, valid_length).c_str() ;
//				// Output the fastq to peptide, and log to profile
//				fprintf(out_fp, ">%d_%s\n", tmp, patient);
//				fprintf(out_fp, "%s\n", pep_str.c_str());
//				fprintf(info_fp, "%d\t0\t%s\t%d\t%d\t%d\t%c\t",tmp, patient, id, orient, i, strand);
//				fprintf(info_fp, "%lu\t%d\t%d\t%s\t%s\t", protein_array[i].size(), bp_idx, valid_length, fea, chr);
//				fprintf(info_fp, "%u\t%u\t%u\t%s\t%s\t%d\t%d\t%d\t%s\n", pos, start, end, g_id, t_id, iso_length,
//					before_sv, total_sv, pep_str.c_str() );
//
//
//				tmp++;
//				t_flag=1;
//			}
//			L("1 %d %lu %lu %d %d %s \n", i, protein_array.size(), protein_array[i].size(), bp_idx, valid_length,
//			protein_array[i].substr(0, valid_length).c_str() );
//		}
//		// Orient 0: 3->5
//		make_rc( total_seq, total_rc);
//		orient = 0;
//		DNATranslate(total_rc, protein_array,  orient);
//		for(i =0; i < (int)protein_array.size(); i++)
//		{
//			bp_idx = (bp_1-i)/3;
//			bp_res = (bp_1-i)%3;
//			found = protein_array[i].find('-');
//			valid_length = (int)protein_array[i].size();
//			if ( found!=string::npos)
//			{
//				valid_length=(int)found;
//			}
//			if ( (bp_idx <= valid_length) && ( 5 <= valid_length))
//			{
//				pep_str = protein_array[i].substr(0, valid_length).c_str() ;
//				// Output the fastq to peptide, and log to profile
//				fprintf(out_fp, ">%d_%s\n", tmp, patient);
//				fprintf(out_fp, "%s\n", pep_str.c_str());
//				fprintf(info_fp, "%d\t0\t%s\t%d\t%d\t%d\t%c\t",tmp, patient, id, orient, i, strand);
//				fprintf(info_fp, "%lu\t%d\t%d\t%s\t%s\t", protein_array[i].size(), bp_idx, valid_length, fea, chr);
//				fprintf(info_fp, "%u\t%u\t%u\t%s\t%s\t%d\t%d\t%d\t%s\n", pos, start, end, g_id, t_id, iso_length,
//					before_sv, total_sv, pep_str.c_str() );
//				tmp++;
//				t_flag =1;
//			}
//			L("0 %d %lu %lu %d %d %s \n", i, protein_array.size(), protein_array[i].size(), bp_idx, valid_length, protein_array[i].substr(0, valid_length).c_str() );
//			//L("0 %d %lu %lu\n", i, protein_array.size(),  protein_array[i].size() );
//		}
//
//		t_iso+= t_flag;
//		count++;
//		if ( 0 == count%1000){E(".");}
//	}
//	E("\nTotal Isoform\t%d\t%d\t%d\n", count, t_iso, tmp);
//	//E("Total References with Predictions:\t%d\n", (int)map_record.size());
//	fclose(fp);
//
//	free(iso_seq);
//	free(fea);
//	free(t_id);
//	free(g_id);
//	free(patient);
//	free(chr);
//	free(readline);
//
//}
/**********************************************/
// min: 1 element
int get_kr_pos( const string &pep_str, vector<int> &kr_vec)
{
	kr_vec.clear();
	int limit=(int)pep_str.size();
	int i = 0, last = 0;
	for( i = 0; i < limit; i++ )
	{
		if ( ('K'==pep_str[i] ) || ('R'==pep_str[i]))
		{ kr_vec.push_back(i); last = i;}
	}
	if ( i != limit -1){ kr_vec.push_back(limit - 1);} // always include to the end of a peptide
	return (int)kr_vec.size();
}
/**********************************************/
// mis-cleavage string ends in KR in kr_vec[idx]. Both inclusive
int get_mis_str( const string &pep_str, string &mis_str, vector<int> &kr_vec, int idx, int &left, int &right )
{
	// From idx to
	int l_bd = 0, r_bd = 0, p_l = 0 ;

	if ( 1 == idx ) // inclusive
	{
		l_bd = 0;
		r_bd = kr_vec[1]; // from 0 to r_bf
	}
	else
	{
		l_bd = kr_vec[idx-2]+1;
		r_bd = kr_vec[idx];
	}
	p_l = r_bd - l_bd + 1;;
	mis_str = pep_str.substr( l_bd, p_l);
	left = l_bd;
	right = r_bd;
	return p_l;
}
///**********************************************/
//void IsoformToPeptide( char *predict_file , char *out_file )
//{
//	string total_seq="", total_rc="";
//
//	char *readline=(char*)malloc(MAX_LINE);
//	char *patient=(char*)malloc(MAX_LINE);
//	char *chr=(char*)malloc(TOKEN_LENGTH);
//	char *g_id=(char*)malloc(TOKEN_LENGTH);
//	char *t_id=(char*)malloc(TOKEN_LENGTH);
//	char *fea=(char*)malloc(TOKEN_LENGTH);
//	char *iso_seq = (char*)malloc(MAX_LINE);
//
//	char *pep_out=(char*)malloc(MAX_LINE);
//	char *info_out=(char*)malloc(MAX_LINE);
//	strcpy(pep_out, out_file);
//	strcat(pep_out, ".pep");
//	strcpy(info_out, out_file);
//	strcat(info_out, ".peptable");
//
//	FILE *out_fp = fopen(pep_out, "w");
//	FILE *info_fp = fopen(info_out, "w");
//
//
//	char strand;
//	int iso_length =0, before_sv = 0, total_sv = 0,
//		bp_1= 0, bp_2 =0, // 0-based bp, both inclusive
//		r_bp1 = 0, r_bp2 = 0; // for reversed strand.
//	uint32_t pos =0, start = 0, end = 0;
//
//	int count = 0;
//	int id = 0, support = 0, offset =0;
//	//unit_vcf tmp_vcf;
//	FILE *fp = fopen( predict_file, "r");
//	int ret;
//	int i = 0, orient = 0;
//	int bp_idx = 0, bp2_idx = 0, bp_res = 0,valid_length =0;
//	int r2_idx = 0;// for 3 -> 5 strand
//	size_t found;
//	vector<string> protein_array;
//	int tmp=0, t_flag =0, t_iso=0, t_mis = 0;
//	int t_cross = 0; // the actual number of peptide crossing SVs
//	string pep_str="", mis_str="";
//	int left = 0, right = 0, mis_l = 0;//for miscleavage
//	vector<int> kr_vec;
//	int mis_size = 0;
//	int overlap_l = 0;
//	while( NULL != fgets(readline, MAX_LINE, fp) )
//	{
//		if ( 0 == strncmp("#", readline, 1) )
//		{
//			continue;
//		}
//		t_flag=0;
//		ret = sscanf( readline, "%s %d %s %s %u %u %u %s %s %c %d %d %d %s\n",
//		//ret = sscanf( readline, "%s %u %c %s %s %f %s %s\n",
//			patient, &id, fea, chr, &pos, &start, &end, g_id, t_id, &strand,
//			&iso_length, &before_sv, &total_sv,
//			iso_seq);
//
//		bp_1 = before_sv;
//		bp_2 = before_sv + total_sv - 1; // SV is from bp_1 to bp_2. 0-based, both inclusive
//
//		//E("OK %s\n", misc);
//		copyToString( iso_seq, total_seq);
//		r_bp1 = (int)total_seq.size() - before_sv - total_sv;
//		r_bp2 = r_bp1 + total_sv - 1;
//		L("==%s\t%d\t%d\t%c\t%lu\t%d\t%d\t%d\t%d\t%d\t%d\n", patient, id, orient, strand, total_seq.size(), before_sv, total_sv, bp_1, bp_2, r_bp1, r_bp2);
//		// Orient 1: 5->3
//		orient = 1;
//		DNATranslate(total_seq, protein_array,  orient );
//		for(i =0; i < (int)protein_array.size(); i++)
//		{
//			bp_idx = (bp_1-i)/3;
//			bp_res = (bp_1-i)%3;
//			bp2_idx = (bp_2-i)/3;
//			found = protein_array[i].find('-');
//			valid_length = (int)protein_array[i].size();
//			if ( found!=string::npos)
//			{
//				valid_length=(int)found;
//			}
//
//			L("<<%s\t%d\t%d\t%d\t%c\t%d\t%d\t%d\t%d\t%d\n", patient, id, orient, i, strand, valid_length, bp_idx, bp2_idx, bp_1, bp_2);
//			if ( (bp_idx <= valid_length) && ( 5 <= valid_length))
//			{
//				pep_str = protein_array[i].substr(0, valid_length).c_str() ;
//				// get substring with at most 2 KR
//				mis_size = get_kr_pos( pep_str, kr_vec);
//				L(">>%s\t%d\t%d\t%d\t%c\t%lu\t%lu\t%d\t%d", patient, id, orient, i, strand, pep_str.size(), kr_vec.size(), bp_idx, bp2_idx);
//				for( int w = 0; w < (int)kr_vec.size();w++)
//				{	L("\t%d", kr_vec[w]);}
//				L("\n");
//				if ( 2 >= mis_size)
//				{
//					overlap_l = ol_length( bp_idx, bp2_idx, 0, valid_length - 1);
//					// Output the fastq to peptide, and log to profile
//					fprintf(out_fp, ">%d_%s_%d\n", tmp, patient, t_mis);
//					fprintf(out_fp, "%s\n", pep_str.c_str());
//					fprintf(info_fp, "%d\t%d\t%s\t%d\t%d\t%d\t%c\t",tmp, t_mis, patient, id, orient, i, strand);
//					//fprintf(info_fp, "%lu\t%d\t%d\t%d\t0\t%d%s\t%s\t", protein_array[i].size(), bp_idx, bp2_idx, valid_length, valid_length -1, fea, chr);
//					fprintf(info_fp, "%lu\t%d\t%d\t0\t%d\t%d\t%d\t%s\t%s\t", protein_array[i].size(), valid_length, overlap_l, valid_length -1, bp_idx, bp2_idx, fea, chr);
//					fprintf(info_fp, "%u\t%u\t%u\t%s\t%s\t%d\t%d\t%d\t%s\t%s\n", pos, start, end, g_id, t_id, iso_length,
//							before_sv, total_sv, pep_str.c_str(), pep_str.c_str() );
//					t_mis++;
//					if ( overlap_l ){ t_cross++;}
//				}
//				else
//				{
//					for( int t = 1; t < mis_size ; t++ )
//					{
//						mis_l = get_mis_str( pep_str, mis_str, kr_vec, t , left, right);
//						overlap_l = ol_length( bp_idx, bp2_idx, left, right );
//						if (  5 <= mis_l ) // && ( ol_length( bp_idx, bp2_idx, left, right )))
//						{
//							fprintf(out_fp, ">%d_%s_%d\n", tmp, patient, t_mis );
//							fprintf(out_fp, "%s\n", mis_str.c_str());
//							fprintf(info_fp, "%d\t%d\t%s\t%d\t%d\t%d\t%c\t",tmp, t_mis, patient, id, orient, i, strand);
//							//fprintf(info_fp, "%lu\t%d\t%d\t%d\t%d\t%d\t%s\t%s\t", protein_array[i].size(), bp_idx, bp2_idx, valid_length, left, right, fea, chr);
//							fprintf(info_fp, "%lu\t%d\t%d\t%d\t%d\t%d\t%d\t%s\t%s\t", protein_array[i].size(), valid_length, overlap_l, left, right, bp_idx, bp2_idx, fea, chr);
//							fprintf(info_fp, "%u\t%u\t%u\t%s\t%s\t%d\t%d\t%d\t%s\t%s\n", pos, start, end, g_id, t_id, iso_length,
//									before_sv, total_sv, mis_str.c_str(), pep_str.c_str() );
//							t_mis++;
//						}
//						if ( overlap_l ){ t_cross++;}
//					}
//				}
//
//
//				tmp++;
//				t_flag=1;
//			}
//			L("1 %d %lu %lu %d %d %s \n", i, protein_array.size(), protein_array[i].size(), bp_idx, valid_length,
//			protein_array[i].substr(0, valid_length).c_str() );
//		}
//		// Orient 0: 3->5
//		make_rc( total_seq, total_rc);
//		orient = 0;
//		DNATranslate(total_rc, protein_array,  orient);
//		for(i =0; i < (int)protein_array.size(); i++)
//		{
//			bp_idx = (r_bp1 - i )/3;
//			bp_res = (r_bp1 - i )%3;
//			bp2_idx = (r_bp2-i)/3;
//			//bp_idx = (bp_1-i)/3;
//			//bp_res = (bp_1-i)%3;
//			found = protein_array[i].find('-');
//			valid_length = (int)protein_array[i].size();
//			if ( found!=string::npos)
//			{
//				valid_length=(int)found;
//			}
//			L("<<%s\t%d\t%d\t%d\t%c\t%d\t%d\t%d\t%d\t%d\n", patient, id, orient, i, strand, valid_length, bp_idx, bp2_idx, r_bp1, r_bp2);
//			if ( (bp_idx <= valid_length) && ( 5 <= valid_length))
//			{
//				pep_str = protein_array[i].substr(0, valid_length).c_str() ;
//				// get substring with at most 2 KR
//				mis_size = get_kr_pos( pep_str, kr_vec);
//				L(">>%s\t%d\t%d\t%d\t%c\t%lu\t%lu\t%d\t%d", patient, id, orient, i, strand, pep_str.size(), kr_vec.size(), bp_idx, bp2_idx);
//				for( int w = 0; w < (int)kr_vec.size();w++)
//				{	L("\t%d", kr_vec[w]);}
//				L("\n");
//				if ( 2 >= mis_size)
//				{
//					overlap_l = ol_length( bp_idx, bp2_idx, 0, valid_length - 1);
//					// Output the fastq to peptide, and log to profile
//					fprintf(out_fp, ">%d_%s_%d\n", tmp, patient, t_mis);
//					fprintf(out_fp, "%s\n", pep_str.c_str());
//					fprintf(info_fp, "%d\t%d\t%s\t%d\t%d\t%d\t%c\t",tmp, t_mis, patient, id, orient, i, strand);
//					//fprintf(info_fp, "%lu\t%d\t%d\t%d\t0\t%d%s\t%s\t", protein_array[i].size(), bp_idx, bp2_idx, valid_length, valid_length -1, fea, chr);
//					fprintf(info_fp, "%lu\t%d\t%d\t0\t%d\t%d\t%d\t%s\t%s\t", protein_array[i].size(), valid_length, overlap_l, valid_length -1, bp_idx, bp2_idx, fea, chr);
//					fprintf(info_fp, "%u\t%u\t%u\t%s\t%s\t%d\t%d\t%d\t%s\t%s\n", pos, start, end, g_id, t_id, iso_length,
//							before_sv, total_sv, pep_str.c_str(), pep_str.c_str() );
//					t_mis++;
//					if ( overlap_l ){ t_cross++;}
//				}
//				else
//				{
//					for( int t = 1; t < mis_size ; t++ )
//					{
//						mis_l = get_mis_str( pep_str, mis_str, kr_vec, t , left, right);
//						overlap_l = ol_length( bp_idx, bp2_idx, left, right );
//						if (  5 <= mis_l ) // && ( ol_length( bp_idx, bp2_idx, left, right )))
//						{
//							fprintf(out_fp, ">%d_%s_%d\n", tmp, patient, t_mis );
//							fprintf(out_fp, "%s\n", mis_str.c_str());
//							fprintf(info_fp, "%d\t%d\t%s\t%d\t%d\t%d\t%c\t",tmp, t_mis, patient, id, orient, i, strand);
//							//fprintf(info_fp, "%lu\t%d\t%d\t%d\t%d\t%d\t%s\t%s\t", protein_array[i].size(), bp_idx, bp2_idx, valid_length, left, right, fea, chr);
//							fprintf(info_fp, "%lu\t%d\t%d\t%d\t%d\t%d\t%d\t%s\t%s\t", protein_array[i].size(), valid_length, overlap_l, left, right, bp_idx, bp2_idx, fea, chr);
//							fprintf(info_fp, "%u\t%u\t%u\t%s\t%s\t%d\t%d\t%d\t%s\t%s\n", pos, start, end, g_id, t_id, iso_length,
//									before_sv, total_sv, mis_str.c_str(), pep_str.c_str() );
//							t_mis++;
//						}
//						if ( overlap_l ){ t_cross++;}
//					}
//				}
//
//
//				tmp++;
//				t_flag =1;
//			}
//			L("0 %d %lu %lu %d %d %s \n", i, protein_array.size(), protein_array[i].size(), bp_idx, valid_length, protein_array[i].substr(0, valid_length).c_str() );
//			//L("0 %d %lu %lu\n", i, protein_array.size(),  protein_array[i].size() );
//		}
//
//		t_iso+= t_flag;
//		count++;
//		if ( 0 == count%1000){E(".");}
//	}
//	E("\nTotal Isoform\t%d\t%d\t%d\t%d\t%d\n", count, t_iso, tmp, t_mis, t_cross); // number_isoform, number_isoform_wth_valid_protein, number_protein, number_peptide, number_of_crossing_peptide
//	//E("Total References with Predictions:\t%d\n", (int)map_record.size());
//	fclose(fp);
//
//	free(iso_seq);
//	free(fea);
//	free(t_id);
//	free(g_id);
//	free(patient);
//	free(chr);
//	free(readline);
//
//}
//
///**********************************************/
///**********************************************/
//void ConvertToProteins( char *pred_file, char *out_file, char *lib_name, int pep_length, int mode )
//{
//	vector<string> token_array,
//					fusion_array,
//					protein_array,
//					fragment_array;
//
//	//vector<int> pos_vec;
//	string total_seq="", total_rc="";
//	int left_i = 0, right_i = 0, split_i = 0; // 1-based system
//	int frame_num = 0, pep_num = 0;
//	int orient = 0;
//	int line_count = 0;
//	int dist = 0; // dist to breakpoint
//	int cond =0; // condition flag
//	FILE *fp = fopen( pred_file, "r");
//	FILE *out_fp= fopen( out_file, "w");
//	char *readline=(char*)malloc(MAX_LINE);
//	while( NULL != fgets(readline, MAX_LINE, fp) )
//	{
//
//		if( 0 == strncmp("cluster", readline, 7 ) ) // header line in tsv file
//		{
//			continue;
//		}
//
//		token_array = splitString( readline, '\t' );
//		fusion_array = splitStringOld( token_array[1], '|');
//		left_i = (int)fusion_array[0].size();
//		right_i = (int)fusion_array[1].size();
//		if ( 0 < right_i )
//		{
//			total_seq = fusion_array[0] + fusion_array[1];
//		}
//
//		//L("Number of Token\t%d\n", (int)fusion_array.size());
//		L("Fusion %s\n", total_seq.c_str());
//		//DNATranslate(fusion_array[1]);
//		// Orient 1: 5->3
//		orient = 1;
//		DNATranslate(total_seq, protein_array,  orient );
//		split_i = left_i;
//		frame_num = (int)protein_array.size();
//		for( int i =0; i < frame_num; i ++ )
//		{
//			fragment_array = splitStringOld( protein_array[i], '-');
//			pep_num = (int) fragment_array.size();
//			L(">%d-%d\n%s\n",orient,  i, protein_array[i].c_str());
//			int acc_l = i+1, acc_r = 0, tmp_len=0; // 1-based accumulated length
//			cond = 0; // reset for this frame
//			for( int j = 0; j < pep_num; j++)
//			{
//				tmp_len=(int)fragment_array[j].size();
//				acc_r = acc_l + 3*tmp_len -1;
//				if ( pep_length < tmp_len ) // actual peptide
//				{
//					dist = bp_dist(acc_l, acc_r, split_i);
//					if ( mode <= 1)
//					{
//							fprintf( out_fp, ">%s_%s_%d_%d_%d\n", lib_name, token_array[0].c_str(), orient, i+1, dist );
//							fprintf( out_fp, "%s\n", fragment_array[j].c_str());
//					}
//					else
//					{
//						size_t found = fragment_array[j].find('M');
//						if (found != std::string::npos)
//						{
//							fprintf( out_fp, ">%s_%s_%d_%d_%d\n", lib_name, token_array[0].c_str(), orient, i+1, dist );
//							fprintf( out_fp, "%s\n", fragment_array[j].substr(found).c_str());
//							cond = 1;
//						}
//					}
//
//					L("=%d %d %d %d %d %s\n", j, acc_l, acc_r, split_i, dist, fragment_array[j].c_str());
//					if ( 1 == mode)
//						{ break; } // only need the first peptide
//					if ( (3 == mode) && (1 ==cond))
//						{ break;}
//				}
//				else
//				{
//					L("=%d %d %d %d %s\n", j, acc_l, acc_r, split_i,  fragment_array[j].c_str());
//				}
//				acc_l = acc_r + 4;
//			}
//		}
//		//DNATranslate(fusion_array[0], 1);
//		// Orient 0: 3->5
//		make_rc( total_seq, total_rc);
//		orient = 0;
//		DNATranslate(total_rc, protein_array,  orient);
//		split_i = right_i;
//		frame_num = (int)protein_array.size();
//		for( int i =0; i < frame_num; i ++ )
//		{
//			fragment_array = splitStringOld( protein_array[i], '-');
//			pep_num = (int) fragment_array.size();
//			L(">%d-%d\n%s\n",orient,  i, protein_array[i].c_str());
//			int acc_l = i+1, acc_r = 0, tmp_len=0; // 1-based accumulated length
//			cond = 0; // reset for this frame
//			for( int j = 0; j < pep_num; j++)
//			{
//				tmp_len=(int)fragment_array[j].size();
//				acc_r = acc_l + 3*tmp_len -1;
//				if ( pep_length < tmp_len ) // actual peptide
//				{
//					dist = bp_dist(acc_l, acc_r, split_i);
//					if ( mode <= 1)
//					{
//							fprintf( out_fp, ">%s_%s_%d_%d_%d\n", lib_name, token_array[0].c_str(), orient, i+1, dist );
//							fprintf( out_fp, "%s\n", fragment_array[j].c_str());
//					}
//					else
//					{
//						size_t found = fragment_array[j].find('M');
//						if (found != std::string::npos)
//						{
//							fprintf( out_fp, ">%s_%s_%d_%d_%d\n", lib_name, token_array[0].c_str() , orient, i+1, dist );
//							fprintf( out_fp, "%s\n", fragment_array[j].substr(found).c_str());
//							cond = 1;
//						}
//					}
//
//					L("=%d %d %d %d %d %s\n", j, acc_l, acc_r, split_i, dist, fragment_array[j].c_str());
//					if ( 1 == mode)
//						{ break; } // only need the first peptide
//					if ( (3 == mode) && (1 ==cond))
//						{ break;}
//				}
//				else
//				{
//					L("=%d %d %d %d %s\n", j, acc_l, acc_r, split_i,  fragment_array[j].c_str());
//				}
//				acc_l = acc_r + 4;
//			}
//			//fragment_array = splitStringOld( protein_array[i], '-');
//			//pep_num = (int) fragment_array.size();
//			//L(">%d-%d\n%s\n",orient,  i, protein_array[i].c_str());
//			//int acc_l = i+1, acc_r = 0, tmp_len=0; // 1-based accumulated length
//			//for( int j = 0; j < pep_num; j++)
//			//{
//			//	tmp_len=(int)fragment_array[j].size();
//			//	acc_r = acc_l + 3*tmp_len -1;
//			//	if ( pep_length < tmp_len )
//			//	{
//			//		dist = bp_dist(acc_l, acc_r, split_i);
//			//		fprintf( out_fp, ">%s_%d_%d_%d_%d\n", lib_name, line_count, orient, i+1, dist );
//			//		fprintf( out_fp, "%s\n", fragment_array[j].c_str());
//			//		L("=%d %d %d %d %d %s\n", j, acc_l, acc_r, split_i, dist, fragment_array[j].c_str());
//			//	}
//			//	L("=%d %d %d %d %s\n", j, acc_l, acc_r, split_i,  fragment_array[j].c_str());
//			//	acc_l = acc_r + 4;
//			//}
//		}
//
//		line_count++;
//	}
//	fclose(fp);
//	fclose(out_fp);
//}
//
//
/**********************************************/
void DNATranslate0( const string &g_seq, vector<string> &trans_vec,  int flag)//, int left_i, int right_i )
{
	trans_vec.clear();
	trans_vec.reserve(3);
	int limit= (int)g_seq.size();
	int last = limit -2; // last starting position in 1-based system



	//string trigram;
	string tmp_str;
	for( int start =0; start < 3; start++) // three reading frames
	{
		tmp_str.clear();
		tmp_str.reserve( ( (int)g_seq.size())/3 + 1 );
		for( int i=start; i < last ; i+=3 )
		{
			tmp_str.push_back( ToProtein( g_seq.substr(i,3) ) );
			//L("%d: %s %c F%d %d %d\n", i, trigram.c_str(), aa, start, last, limit);
		}
		//L("%d-%d:%s\n", flag, start, tmp_str.c_str() );
		trans_vec.push_back(tmp_str);
	}
}

/**********************************************/
void DNATranslate( const string &g_seq, vector<string> &trans_vec,  int flag)//, int left_i, int right_i )
{
	trans_vec.clear();
	trans_vec.reserve(3);
	trans_vec.push_back("");
	trans_vec.push_back("");
	trans_vec.push_back("");
	trans_vec[0].reserve(10000);
	trans_vec[1].reserve(10000);
	trans_vec[2].reserve(10000);
	int limit= (int)g_seq.size();
	int last = limit -2; // last starting position in 1-based system



	char tmp_char;
	for( int i =0; i < last; i++) // three reading frames
	{
		tmp_char = to_aa( g_seq.substr(i,3) );
		trans_vec[ i%3 ].push_back(tmp_char);
	}
}
/////**********************************************/
////void make_rc( const string &raw_str, string &new_str)
////{
////	new_str = "";
////	int limit = (int)raw_str.size();
////	for( int i =0; i < limit; i++ )
////	{
////		switch( raw_str[limit-1-i])
////		{
////			case 'A':
////				new_str.push_back('T');
////				break;
////			case 'C':
////				new_str.push_back('G');
////				break;
////			case 'G':
////				new_str.push_back('C');
////				break;
////			case 'T':
////				new_str.push_back('A');
////				break;
////			default:
////				new_str.push_back('N');
////		}
////	}
////}
//
//
//
///**********************************************/
//// assume left <= right
//// change the the indexof aa
//int bp_dist( int left, int right, int bp)
//{
//	int dist = 0;
//	int tmp_bp = 0;
//	if ( left <= bp)
//	{
//		if (  right < bp)
//		{
//			dist = right - bp;
//		}
//		else
//		{
//			tmp_bp = bp+1;
//			dist =1 + (tmp_bp -left)/3;
//			//if ( 2 == (tmp_bp-left)%3 )
//			//{
//			//	dist =(tmp_bp -left)/3;
//			//}
//			//else
//			//{
//			//	dist =1 + (tmp_bp -left)/3;
//			//}
//		}
//	}
//	else
//	{
//		dist = left -bp;
//	}
//	return dist;
//}
//
/**********************************************/
char ToProtein ( const string &trigram)
{
	map<string, char> codon;
	codon["TTT"]='F';
	codon["TTC"]='F';
	codon["TTA"]='L';
	codon["TTG"]='L';
	codon["CTT"]='L';
	codon["CTC"]='L';
	codon["CTA"]='L';
	codon["CTG"]='L';
	codon["ATT"]='I';
	codon["ATC"]='I';
	codon["ATA"]='I';
	codon["ATG"]='M';
	codon["GTT"]='V';
	codon["GTC"]='V';
	codon["GTA"]='V';
	codon["GTG"]='V';
	codon["TCT"]='S';
	codon["TCC"]='S';
	codon["TCA"]='S';
	codon["TCG"]='S';
	codon["CCT"]='P';
	codon["CCC"]='P';
	codon["CCA"]='P';
	codon["CCG"]='P';
	codon["ACT"]='T';
	codon["ACC"]='T';
	codon["ACA"]='T';
	codon["ACG"]='T';
	codon["GCT"]='A';
	codon["GCC"]='A';
	codon["GCA"]='A';
	codon["GCG"]='A';
	codon["TAT"]='Y';
	codon["TAC"]='Y';
	codon["TAA"]='-';
	codon["TAG"]='-';
	codon["CAT"]='H';
	codon["CAC"]='H';
	codon["CAA"]='Q';
	codon["CAG"]='Q';
	codon["AAT"]='N';
	codon["AAC"]='N';
	codon["AAA"]='K';
	codon["AAG"]='K';
	codon["GAT"]='D';
	codon["GAC"]='D';
	codon["GAA"]='E';
	codon["GAG"]='E';
	codon["TGT"]='C';
	codon["TGC"]='C';
	codon["TGA"]='-';
	codon["TGG"]='W';
	codon["CGT"]='R';
	codon["CGC"]='R';
	codon["CGA"]='R';
	codon["CGG"]='R';
	codon["AGT"]='S';
	codon["AGC"]='S';
	codon["AGA"]='R';
	codon["AGG"]='R';
	codon["GGT"]='G';
	codon["GGC"]='G';
	codon["GGA"]='G';
	codon["GGG"]='G';
	map< string, char >::iterator it;
	it = codon.find(trigram);
	char aa;
	if ( it != codon.end() )
	{
		aa = (*it).second;
	}
	else
	{
		aa = '*';
		E("Warning in %s\n", trigram.c_str());
	}
	return aa;
}

///*********************************************/
//void SummarizeFasta( char *pred_file, char *out_file )
//{
//	vector<string> token_array;
//	map<string, vector<tr_length> >::iterator it;
//	string t_id;
//	//int reg_num = 0, // the number of gene regions for a single id
//	//	f_id, // the index for multi-region transcript
//	//	f_length; // length of the transcript
//	FILE *fp = fopen(pred_file, "r");
//	FILE *out_fp = fopen(out_file, "w");
//	int count=0;
//	int seq_length = 0, total_length = 0 ;
//
//
//	char *readline  = (char*)malloc(MAX_LINE);
//	char *seqname = (char*)malloc(MAX_LINE);
//
//	while( NULL != fgets(readline, MAX_LINE, fp) )
//	{
//		if( 0 == strncmp(">", readline, 1 ) )
//		{
//			strcpy( seqname, readline);
//			//continue;
//		}
//		else
//		{
//			seq_length = (int)strlen(readline);
//			count++;
//			fprintf( out_fp, "%d\t%d\n", count, seq_length);
//			//count++;
//			total_length += seq_length;
//		}
//
//	}
//	E("Summary: %d seq with %f avg aa length.\n", count, total_length*1.0/count);
//	fclose( out_fp);
//	fclose( fp );
//
//}
//
///*********************************************/
//void ExtractPeptideFromFasta( char *pred_file, char *out_file )
//{
//	vector<string> header_array;
//	vector<string> token_array;
//	map<string, vector<tr_length> >::iterator it;
//	string t_id;
//	//int reg_num = 0, // the number of gene regions for a single id
//	//	f_id, // the index for multi-region transcript
//	//	f_length; // length of the transcript
//	FILE *fp = fopen(pred_file, "r");
//	FILE *out_fp = fopen(out_file, "w");
//	int count=0;
//	int pos = 0;
//	int left = 0, right = 0;
//	int seq_length = 0, total_length = 0 ;
//	int bp_pos =0;
//	int i =0;
//	int new_bp = 0;
//
//	char *readline  = (char*)malloc(MAX_LINE);
//	char *seqname = (char*)malloc(MAX_LINE);
//	string final_str;
//	while( NULL != fgets(readline, MAX_LINE, fp) )
//	{
//		if( 0 == strncmp(">", readline, 1 ) )
//		{
//			header_array = splitString(readline, '_');
//			bp_pos = atoi( header_array[4].c_str() );
//			E("%d %s_%s_%s_%s_%s\n", bp_pos, header_array[0].c_str(), header_array[1].c_str(), header_array[2].c_str(), header_array[3].c_str(), header_array[4].c_str() );
//			//continue;
//		}
//		else
//		{
//			if ( 0 > bp_pos )
//			{
//			E("NO_BP\t%s_%s_%s_%s_%s\n", header_array[0].c_str(), header_array[1].c_str(), header_array[2].c_str(), header_array[3].c_str(), header_array[4].c_str() );
//			}
//			else
//			{
//				seq_length = (int)strlen(readline);
//				left = -1;
//				right = -1;
//				E("BP\t%d\t%s_%s_%s_%s_%s\n", seq_length,  header_array[0].c_str(), header_array[1].c_str(), header_array[2].c_str(), header_array[3].c_str(), header_array[4].c_str() );
//				E("%s", readline);
//				for( i = 0; i < seq_length; i ++)
//				{
//					if ('K' == readline[i] || 'R' == readline[i] )
//					{
//						if ( i <  bp_pos -1 ) // 0-based vs 1-based
//						{
//							left = i+1;
//							E("%d\n", left);
//						}
//						else if ( bp_pos -1  <= i) // 0-based vs 1-based
//						{
//							right = i+1;
//							E("%d\n", right);
//							E("%d\t%d\n", left, right);
//							break;
//						}
//					}
//				}
//				if ( -1 == right )
//				{
//					right = seq_length-1;
//					E("CHR to %d\n", right);
//				}
//				if ( -1 == left)
//				{
//					left = 1;// why not ZERO?
//					E("CHL to %d\n", left);
//				}
//				//left + 1 to right -1
//				if ( left < right )
//				{
//					final_str ="";
//					for( i = left; i <= right-1; i++ )
//					{
//						final_str += readline[i];
//					}
//					new_bp = bp_pos - left; // 1-based bp in the new sequence
//					E("SEQ\t%d\t%s\n",new_bp, final_str.c_str());
//					fprintf( out_fp, "%s_%s_%s_%s_%s_%d\n", header_array[0].c_str(), header_array[1].c_str(), header_array[2].c_str(), header_array[3].c_str(), header_array[4].c_str(), new_bp );
//					fprintf(out_fp, "%s\n", final_str.c_str());
//				}
//				else
//				{
//					E("BAD_BP\t%s_%s_%s_%s_%s\t%d\t%d\n", header_array[0].c_str(), header_array[1].c_str(), header_array[2].c_str(), header_array[3].c_str(), header_array[4].c_str(), left, right );
//				}
//			}
//
//		}
//
//	}
//	//E("Summary: %d seq with %f avg aa length.\n", count, total_length*1.0/count);
//	fclose( out_fp);
//	fclose( fp );
//
//}
//
///*********************************************/
//// Let's allow for 1 mis-cleavage
//void ExtractPeptideFromFasta_Mis( char *pred_file, char *out_file )
//{
//	vector<string> header_array;
//	vector<string> token_array;
//	map<string, vector<tr_length> >::iterator it;
//	string t_id;
//	//int reg_num = 0, // the number of gene regions for a single id
//	//	f_id, // the index for multi-region transcript
//	//	f_length; // length of the transcript
//	FILE *fp = fopen(pred_file, "r");
//	FILE *out_fp = fopen(out_file, "w");
//	int count=0;
//	int pos = 0;
//	int l1=0, l2 =0;
//	int r1 = 0, r2 = 0;
//	//int left = 0, right = 0;
//	int seq_length = 0, total_length = 0 ;
//	int bp_pos =0;
//	int i =0;
//	int new_bp = 0;
//	int status = 0;
//	vector<int> l_vec, r_vec;
//
//	char *readline  = (char*)malloc(MAX_LINE);
//	char *seqname = (char*)malloc(MAX_LINE);
//	string final_str;
//	while( NULL != fgets(readline, MAX_LINE, fp) )
//	{
//		if( 0 == strncmp(">", readline, 1 ) )
//		{
//			header_array = splitString(readline, '_');
//			bp_pos = atoi( header_array[4].c_str() );
//			E("%d %s_%s_%s_%s_%s\n", bp_pos, header_array[0].c_str(), header_array[1].c_str(), header_array[2].c_str(), header_array[3].c_str(), header_array[4].c_str() );
//			//continue;
//		}
//		else// sequence part
//		{
//			if ( 0 > bp_pos )
//			{
//			E("NO_BP\t%s_%s_%s_%s_%s\n", header_array[0].c_str(), header_array[1].c_str(), header_array[2].c_str(), header_array[3].c_str(), header_array[4].c_str() );
//			}
//			else
//			{
//				seq_length = (int)strlen(readline);
//				l1 = -1, l2 = -1;//left = -1;
//				r1 = -1, r2 = -1;//right = -1;
//				E("BP\t%d\t%s_%s_%s_%s_%s\n", seq_length,  header_array[0].c_str(), header_array[1].c_str(), header_array[2].c_str(), header_array[3].c_str(), header_array[4].c_str() );
//				E("%s", readline);
//				for( i = 0; i < seq_length; i ++)
//				{
//					if ('K' == readline[i] || 'R' == readline[i] )
//					{
//						if ( i <  bp_pos -1 ) // 0-based vs 1-based
//						{
//							l1 = l2;
//							l2 = i+1;	//left = i+1;
//							E("L\t%d\t%d\n", l1, l2);
//						}
//						else if ( bp_pos -1  <= i) // 0-based vs 1-based
//						{
//							if ( -1 == r1 )
//							{
//								r1 = i+1;
//								E("R1\t%d\n", r1);
//							}
//							else
//							{
//								r2 = i+1;
//								E("R2\t%d\t%d\n", r1, r2);
//								break;
//							}
//						}
//					}
//
//				}
//				E("ALL\t%d\t%d\t%d\t%d\t%d\n", l1, l2, bp_pos, r1, r2);
//				l_vec.clear();
//				r_vec.clear();
//
//				if ( -1 == l1)
//				{
//					l1 = 0;
//					E("CHL to %d\n", l1);
//					// we don't care if l2 is -1
//				}
//				if ( -1 == r2 )
//				{
//					r2 = seq_length-1;
//					E("CHR to %d\n", r2);
//					//don't care if r1 is -1
//				}
//				final_str="";
//				status = generate_peptide(final_str, l1, r1, readline);
//				if ( 1 == status )
//				{
//					new_bp = bp_pos - l1; // 1-based bp in the new sequence
//					E("SEQ-11\t%d\t%s\n",new_bp, final_str.c_str());
//					fprintf( out_fp, "%s_%s_%s_%s_%s_0_%d\n", header_array[0].c_str(), header_array[1].c_str(), header_array[2].c_str(), header_array[3].c_str(), header_array[4].c_str(), new_bp );
//					fprintf(out_fp, "%s\n", final_str.c_str());
//				}
//
//				if ( -1 == l2 )
//				{
//					final_str="";
//					status = generate_peptide(final_str, l1, r2, readline);
//					if ( 1 == status )
//					{
//						new_bp = bp_pos - l1; // 1-based bp in the new sequence
//						E("SEQ-12\t%d\t%s\n",new_bp, final_str.c_str());
//						fprintf( out_fp, "%s_%s_%s_%s_%s_1_%d\n", header_array[0].c_str(), header_array[1].c_str(), header_array[2].c_str(), header_array[3].c_str(), header_array[4].c_str(), new_bp );
//						fprintf(out_fp, "%s\n", final_str.c_str());
//					}
//				}
//
//				final_str="";
//				status = generate_peptide(final_str, l2, r1, readline);
//				if ( 1 == status )
//				{
//					new_bp = bp_pos - l2; // 1-based bp in the new sequence
//					E("SEQ-21\t%d\t%s\n",new_bp, final_str.c_str());
//					fprintf( out_fp, "%s_%s_%s_%s_%s_2_%d\n", header_array[0].c_str(), header_array[1].c_str(), header_array[2].c_str(), header_array[3].c_str(), header_array[4].c_str(), new_bp );
//					fprintf(out_fp, "%s\n", final_str.c_str());
//				}
//
//				final_str="";
//				status = generate_peptide(final_str, l2, r2, readline);
//				if ( 1 == status )
//				{
//					new_bp = bp_pos - l2; // 1-based bp in the new sequence
//					E("SEQ-22\t%d\t%s\n",new_bp, final_str.c_str());
//					fprintf( out_fp, "%s_%s_%s_%s_%s_3_%d\n", header_array[0].c_str(), header_array[1].c_str(), header_array[2].c_str(), header_array[3].c_str(), header_array[4].c_str(), new_bp );
//					fprintf(out_fp, "%s\n", final_str.c_str());
//				}
//				//if ( left < right )
//				//{
//				//	final_str ="";
//				//	for( i = left; i <= right-1; i++ )
//				//	{
//				//		final_str += readline[i];
//				//	}
//				//	new_bp = bp_pos - left; // 1-based bp in the new sequence
//				//	E("SEQ\t%d\t%s\n",new_bp, final_str.c_str());
//				//	fprintf( out_fp, "%s_%s_%s_%s_%s_%d\n", header_array[0].c_str(), header_array[1].c_str(), header_array[2].c_str(), header_array[3].c_str(), header_array[4].c_str(), new_bp );
//				//	fprintf(out_fp, "%s\n", final_str.c_str());
//				//}
//				//else
//				//{
//				//	E("BAD_BP\t%s_%s_%s_%s_%s\t%d\t%d\n", header_array[0].c_str(), header_array[1].c_str(), header_array[2].c_str(), header_array[3].c_str(), header_array[4].c_str(), left, right );
//				//}
//			}
//
//		}
//
//	}
//	//E("Summary: %d seq with %f avg aa length.\n", count, total_length*1.0/count);
//	fclose( out_fp);
//	fclose( fp );
//
//}
//int generate_peptide( string &final_str, int left, int right, char *raw_str)
//{
//	int status = -1;
//	int length = 0;
//	if ( (-1 < left) && (-1 < right ) )
//	{
//		length = right - left;
//		if ( 5 <= length )
//		{
//			status = 1; // we need to generate string
//			for( int i =left; i <= right -1; i++)
//			{
//				final_str += raw_str[i];
//			}
//		}
//		else{E("TOOSHORT\t%d\t%d\n", left, right);}
//
//	}
//	else{E("BAD_BP\t%d\t%d\n", left, right);}
//	return status;
//}


// For deFuse Prediction Results
/*********************************************/
void ParseDefuse( char *predict_file, char *out_file )
{
	//vector<string> token_array;
	//map<string, vector<tr_length> >::iterator it;
	string t_id;
	//int reg_num = 0, // the number of gene regions for a single id
	//	f_id, // the index for multi-region transcript
	//	f_length; // length of the transcript
	FILE *fp = fopen(predict_file, "r");
	//FILE *out_fp = fopen(out_file, "w");
	int count=0;
	int seq_length = 0, total_length = 0 ;


	char *readline  = (char*)malloc(MAX_LINE);
	char *id = (char*)malloc(MAX_LINE);
	char *raw_seq = (char*)malloc(MAX_LINE);
	string r_seq,
			seq,
			seq_rc;
	int bp = 0, r_bp = 0;
	//char *seq = (char*)malloc(MAX_LINE);
	//char *chr = (char*)malloc(MAX_LINE);
	//char *misc = (char*)malloc(MAX_LINE);
	//char *gene_name = (char*)malloc(MAX_LINE);
	//uint32_t s1 = 0, e1 = 0,
	//	s2 = 0, e2 = 0,
	//	s3 = 0, e3 = 0;
	//char strand;

	//tri_exon tri_token;
	int offset = 0;

	while( NULL != fgets(readline, MAX_LINE, fp) )
	{
		if ( 0 == strncmp("cluster", readline, 7) )
		{
			continue;
		}

		sscanf( readline, "%s %s %n",
				id, raw_seq, &offset );

		L("=%s", readline);
		//L(">\t%s\n>\t%s\n", id, raw_seq );
		r_seq=string(raw_seq);
		bp = scan_bp( r_seq, seq ); // 0-based position of the bp
		translate_fusion( id, seq, 1, bp, out_file);

		r_bp = (int)seq.size() - bp; // bp + r_bp == string_length.
		make_rc(seq, seq_rc);
		translate_fusion( id, seq_rc, 0, r_bp, out_file);

		count++;
		if ( 0 == count/100) { E(".");}


		////L("%s>%s\n>%s", readline, e1, e2);
	}
	E("\n");
	//E("Summary: %d predictions parsed.\n", (int)vec_predict.size() );

	//free(gene_name);
	//free(misc);
	//free(chr);
	free(readline);
	//fclose( out_fp);
	fclose( fp );

}
/**********************************************/
void translate_fusion( char *pred_id, const string &seq, int strand, int bp, char *out_file)
{	// strand 1 for following the fusion. 0 otherwise
	int limit = (int)seq.size();
	vector<int> kr_vec;
	int stop_pos = -1;
	int pos = 0; // temporal pos. 0-based
	char aa;
	string peptide;
	string f_peptide;

	char *output=(char*)malloc(MAX_LINE);
	char *log=(char*)malloc(MAX_LINE);
	strcpy(output, out_file);
    strcpy(log, out_file);
	strcat(output, ".fa");
	strcat(log, ".log");
	FILE *out_fp = fopen( output, "a");
	FILE *log_fp = fopen( log, "a");



	// 5' strand
	string final_segment = seq;
	vector< pair<int, int> > vec_range;

	fprintf(log_fp, "=%s\t%s\n", pred_id, final_segment.c_str());
	// Q1: where is the bp? BP is between bp-1 and bp position, 0-based.
	// Q2: where is the first stop codon? 0-based again.
	int bp_idx = 0, bp_res = 0;
	for ( int frame = 0; frame < 3; frame++ )
	{
		peptide.clear();
		kr_vec.clear();
		kr_vec.push_back(0);
		stop_pos = 10000;
		pos = 0;
		bp_idx = (bp-frame)/3; // we use the location AFTER bp to represent the bp
		bp_res = (bp-frame)%3;

		for( int i = frame; i < limit - 2 ; i=i+3) // assure there are 3 nn
		{
			aa = ToProtein(final_segment.substr(i,3));
			if ('-' == aa)
			{
				if ( pos < stop_pos){	stop_pos = pos; }
				if ( 0 < pos ) // since we always start from 0
				{
					kr_vec.push_back(pos);
				}
			}
			else if ( ('K' == aa) || ( 'R' == aa) )
			//else if ( ('K' == aa) || ( 'R' == aa) || ('*' == aa) )
			{
				if ( 0 < pos ) // since we always start from 0
				{
					kr_vec.push_back(pos);
				}
			}
			pos++;
			peptide.push_back(aa);
		}
		fprintf(log_fp, "<%d\t%d\t%d\t%d\t%s\n", strand, frame, bp_idx, bp_res, peptide.c_str());
		// from 0 to length of peptide
		for(int k=0; k < (int)kr_vec.size(); k++){ fprintf(log_fp, "%d\t", kr_vec[k]);}
		fprintf(log_fp, "\n");

		vec_range.clear();
		int pep_len = peptide.size();

		for(int k=0; k < (int)kr_vec.size(); k++)
		{
			int start = kr_vec[k]; //we start from start
			if ( (peptide[start] =='K') || (peptide[start]=='R') || (peptide[start]=='-')){start++;} // start after K or R
			int end = start;
			int current = start;
			int violate=0;

			//if ( start <= stop_pos)
			//{
				while( current < pep_len )//(violate < 2 )
				{
					//if ( '-' == peptide[current])
					if ( '-' == peptide[current])
					{
						end = current -1;
						vec_range.push_back(make_pair(start, end));
						//if ( 5 <= end - start  + 1 )
						//fprintf(log_fp, ">%d\t%d\t%s\n", start, end, peptide.substr(start, end-start+1).c_str() );
						violate = 3;
						break;
					}
					else if ( ('K' == peptide[current] ) || ('R' == peptide[current]))
					{
						violate +=1;
						vec_range.push_back(make_pair(start, end));
						//fprintf(log_fp, ">%d\t%d\t%s\n", start, end, peptide.substr(start, end-start+1).c_str() );
						if ( 2 == violate)
						{
							break; // otherwise we can one extra step after the second K/R
						}
					}
					current +=1;
					end = current;
				}
				if ( violate < 2)
				{
					vec_range.push_back(make_pair(start, pep_len-1));
				}
			//}
		}
		//E("WHY");
		for( int tmp_i=0; tmp_i < (int)vec_range.size(); tmp_i++)
		{
			int t_s= vec_range[tmp_i].first;
			int t_e= vec_range[tmp_i].second;
			fprintf(log_fp, ">%d\t%d\t%s\n", t_s, t_e, peptide.substr(t_s, t_e - t_s+1).c_str() );

			if ( 5 <= t_e - t_s + 1)
			{
				fprintf( out_fp, ">%s_%s_%d_%d_b%d-%d_s%d_p%d-%d\n", out_file, pred_id, strand, frame, bp_idx, bp_res, stop_pos, t_s, t_e);
				//fprintf( out_fp, ">%s_%d_%d_b%d-%d_s%d_p%d-%d\n", out_file, strand, frame, bp_idx, bp_res, stop_pos, t_s, t_e);
				fprintf( out_fp, "%s\n", peptide.substr(t_s, t_e - t_s+1).c_str() );
			}
		}




	}

	fclose(out_fp);
	fclose(log_fp);
}
/**********************************************/
// Get the breakpoint of original string
int scan_bp( const string &raw_str, string &new_str)
{
	new_str.clear();
	int limit = (int)raw_str.size();
	int j = -1; // 0-based position
	int bp = 0;
	for( int i =0; i < limit; i++ )
	{
		if ('|' != raw_str[i])
		{
			new_str.push_back(raw_str[i]);
			j++;
			//L("%d %c %c\n", j, raw_str[i], new_str[j]);
		}
		else
		{
			//L("!!%d\t\t%c\n", i, new_str[j]);
			bp = i;
		}
	}
	//new_str.push_back('\0');
	return bp;
}





// Convert Peptide Information From Annotated Transcript Fasta
/**********************************************/
void clear_trans( sv_trans_t &t)
{
	t.id = 0;
	t.pepid = 0;
	t.status = 1;
	t.num_gene = 1;
	t.cds = 1;
	t.strand = '\0';
	t.chr.clear();
	t.s=0; t.e = 0; t.pos = 0;
	t.bp_s = 0; t.bp_e = 0; t.len = 0;
	t.sv.clear();
	t.patient.clear();
	t.gid.clear();
	t.tid.clear();
	t.seq.clear();
}


/**********************************************/
// 0 if stop condon happens before meeting bp
// otherwise report the length of effective peptide
// aa_start is 0-based!
int check_protein( const string &prot, int aa_start, FILE *log )
{
	int flag = -1;
	for( int i = 0; i < (int)prot.size() ; i++)
	{ 
		if ('-' == prot[i])
		{
			if ( ( i < 5 ) || (i <= aa_start) )
			{
				flag = 0;
				//fprintf( log, "FAIL: %d before %d\n", i, aa_start);
			}
			else
			{ flag = i; } // so it can be considered as effective length
			break;
		}
	}
	if ( -1 == flag){ flag = (int)prot.size();}
	return flag;
}


/**********************************************/
void get_protein( sv_trans_t &t, FILE *prot_log, FILE *pep_fa, FILE *pep_log )
{
	// Sanity Check 
	//fprintf( prot_log, ">%s %d %s %s %u %u %u %c %s %s %d %d %d %d %d %d\n",
	//		t.patient.c_str(), t.id, t.sv.c_str(), t.chr.c_str(),
	//		t.pos, t.s, t.e, t.strand, 
	//		t.gid.c_str(), t.tid.c_str(),
	//		t.bp_s+1, t.bp_e+1, t.len,
	//		t.status, t.num_gene, t.cds);
	//fold_output( prot_log, t.seq );


	int orient; // 1 
	int i, j;
	int valid_length= 0; // 1 if the protein contains SV before meeting the first stop codon. 0 otherwise.
	int overlap_l  = 0;
	string pep_str="", mis_str="", rc_str = "";
	vector<int> kr_vec;
	int mis_size;
	int mis_l;
	int left, right;


	int bp_s = 0, bp_e = 0;
	int s_idx = 0, s_res = 0, e_idx = 0, e_res = 0;
	int adj_idx;
	vector<string> protein_array;
	// orient 1: 5 -> 3 of t.seq
	orient = 1;
	bp_s = t.bp_s ; // Already Convert To 0-based inclusive
	bp_e = t.bp_e ; // Already Convert To 0-based inclusive
	DNATranslate( t.seq, protein_array,  orient );
	for( i = 0; i < (int)protein_array.size(); i++ )
	{
		// set up breakpoints on peptides
		adj_idx = ( bp_s >= i) ? bp_s - i: 0;
		s_idx = adj_idx/3;
		s_res = adj_idx%3;
		e_idx = ( bp_e -i)/3;
		e_res = ( bp_e -i)%3;
		
		valid_length = check_protein( protein_array[i], s_idx, prot_log );
		if ( valid_length )
		{
			// Generate Peptides
			fprintf( prot_log, ">%s %d S:5 RF:%d %s %s %u %u %u %c %s %s %d %d %d %d %d %d\n",
					t.patient.c_str(), t.id, i, t.sv.c_str(), t.chr.c_str(),
					t.pos, t.s, t.e, t.strand, 
					t.gid.c_str(), t.tid.c_str(),
					t.bp_s+1, t.bp_e+1, t.len,
					t.status, t.num_gene, t.cds);
			fprintf( prot_log, "%s\n", protein_array[i].c_str() );
			pep_str = protein_array[i].substr(0, valid_length ).c_str() ;
			mis_size = get_kr_pos( pep_str, kr_vec);
			if ( 2 >= mis_size)
			{
				left = 0; right = valid_length - 1;

				overlap_l = over_l( s_idx, e_idx, left, right );
				//Output the fastq to peptide, and log to profile
				fprintf( pep_fa, ">%s_%d\n", t.patient.c_str(), peptide_id );
				fprintf( pep_fa, "%s\n", pep_str.c_str());
				
				fprintf( pep_log, "%s\t%d\t%d\t", t.patient.c_str(), peptide_id, t.id );
				fprintf( pep_log, "%s\t%s\t", t.gid.c_str(), t.tid.c_str());
				fprintf( pep_log, "%d\t%d\t%c\t", orient, i, t.strand);
				fprintf( pep_log, "%d\t%d\t", overlap_l, valid_length);
				fprintf( pep_log, "%d\t%d\t", left, right );
				fprintf( pep_log, "%d\t%d\t", s_idx, e_idx);
				fprintf( pep_log, "%d\t%d\t%d\n", t.bp_s, t.bp_e, t.len);

				peptide_id++;
			}
			else
			{
				for( int j = 1; j < mis_size ; j++ )
				{
					mis_l = get_mis_str( pep_str, mis_str, kr_vec, j , left, right);
					overlap_l = over_l( s_idx, e_idx, left, right );
					if (  5 > mis_l )
					{ continue; }

					fprintf( pep_fa, ">%s_%d\n", t.patient.c_str(), peptide_id );
					fprintf( pep_fa, "%s\n", mis_str.c_str());

					fprintf( pep_log, "%s\t%d\t%d\t", t.patient.c_str(), peptide_id, t.id );
					fprintf( pep_log, "%s\t%s\t", t.gid.c_str(), t.tid.c_str());
					fprintf( pep_log, "%d\t%d\t%c\t", orient, i, t.strand);
					fprintf( pep_log, "%d\t%d\t", overlap_l, valid_length);
					fprintf( pep_log, "%d\t%d\t", left, right );
					fprintf( pep_log, "%d\t%d\t", s_idx, e_idx);
					fprintf( pep_log, "%d\t%d\t%d\n", t.bp_s, t.bp_e, t.len);

					peptide_id++;
				}
			}
		}
	}
	// orient 0: 3 -> 5 of t.seq
	orient = 0;
	//bp_s = (int)t.seq.size() - t.bp_s;
	//bp_e = (int)t.seq.size() - t.bp_e;
	bp_s = (int)t.seq.size() -1 - t.bp_e; // also 0-based
	bp_e = (int)t.seq.size() -1 - t.bp_s; // also 0-based
	make_rc( t.seq, rc_str );
	DNATranslate( rc_str, protein_array,  orient );
	for( i = 0; i < (int)protein_array.size(); i++ )
	{
		//L("RC %d %d %lu %d %d\n", t.bp_s, t.bp_e, t.seq.size(), bp_s, bp_e );
		//L("RC %d %d %d %d\n", s_idx, s_res, e_idx, e_res);
		// set up breakpoints on peptides
		adj_idx = ( bp_s >= i) ? bp_s - i: 0;
		s_idx = adj_idx/3;
		s_res = adj_idx%3;
		e_idx = ( bp_e -i)/3;
		e_res = ( bp_e -i)%3;
		
		valid_length = check_protein( protein_array[i], s_idx, prot_log );
		
		if ( valid_length )
		{
			// Generate Peptides
			fprintf( prot_log, ">%s %d S:3 RF:%d %s %s %u %u %u %c %s %s %d %d %d %d %d %d\n",
					t.patient.c_str(), t.id, i, t.sv.c_str(), t.chr.c_str(),
					t.pos, t.s, t.e, t.strand, 
					t.gid.c_str(), t.tid.c_str(),
					t.bp_s+1, t.bp_e+1, t.len,
					t.status, t.num_gene, t.cds);
			fprintf( prot_log, "%s\n", protein_array[i].c_str() );
			pep_str = protein_array[i].substr(0, valid_length ).c_str() ;
			mis_size = get_kr_pos( pep_str, kr_vec);
			if ( 2 >= mis_size)
			{
				left = 0; right = valid_length - 1;

				overlap_l = over_l( s_idx, e_idx, left, right );
				//Output the fastq to peptide, and log to profile
				fprintf( pep_fa, ">%s_%d\n", t.patient.c_str(), peptide_id );
				fprintf( pep_fa, "%s\n", pep_str.c_str());
				
				fprintf( pep_log, "%s\t%d\t%d\t", t.patient.c_str(), peptide_id, t.id );
				fprintf( pep_log, "%s\t%s\t", t.gid.c_str(), t.tid.c_str());
				fprintf( pep_log, "%d\t%d\t%c\t", orient, i, t.strand);
				fprintf( pep_log, "%d\t%d\t", overlap_l, valid_length);
				fprintf( pep_log, "%d\t%d\t", left, right );
				fprintf( pep_log, "%d\t%d\t", s_idx, e_idx);
				fprintf( pep_log, "%d\t%d\t%d\n", bp_s, bp_e, t.len);

				peptide_id++;
			}
			else
			{
				for( int j = 1; j < mis_size ; j++ )
				{
					mis_l = get_mis_str( pep_str, mis_str, kr_vec, j , left, right);
					overlap_l = over_l( s_idx, e_idx, left, right );
					if (  5 > mis_l )
					{ continue; }

					fprintf( pep_fa, ">%s_%d\n", t.patient.c_str(), peptide_id );
					fprintf( pep_fa, "%s\n", mis_str.c_str());

					fprintf( pep_log, "%s\t%d\t%d\t", t.patient.c_str(), peptide_id, t.id );
					fprintf( pep_log, "%s\t%s\t", t.gid.c_str(), t.tid.c_str());
					fprintf( pep_log, "%d\t%d\t%c\t", orient, i, t.strand);
					fprintf( pep_log, "%d\t%d\t", overlap_l, valid_length);
					fprintf( pep_log, "%d\t%d\t", left, right );
					fprintf( pep_log, "%d\t%d\t", s_idx, e_idx);
					fprintf( pep_log, "%d\t%d\t%d\n", bp_s, bp_e, t.len);

					peptide_id++;
				}
			}
		}
	}

}
/**********************************************/
char to_aa( const string &str)
{
	char x = '*';
	int weight = 0;
	int index = 0;
	for (int i = 0; i <3 ; i++)
	{
		weight = my_weights[str[i] - 'A'];
		if ( 3 < weight )
		{
			break;
		}
		index = 4*index + my_weights[str[i] - 'A'];

	}
	x = my_aa[index];
	//L("Trigram %s:%d %c\n", str.c_str(), index, x);
	return x;
}
/**********************************************/
void get_protein1( sv_trans_t &t, FILE *prot_log, FILE *pep_fa, FILE *pep_log )
{
	// Sanity Check 
	//fprintf( prot_log, ">%s %d %s %s %u %u %u %c %s %s %d %d %d %d %d %d\n",
	//		t.patient.c_str(), t.id, t.sv.c_str(), t.chr.c_str(),
	//		t.pos, t.s, t.e, t.strand, 
	//		t.gid.c_str(), t.tid.c_str(),
	//		t.bp_s+1, t.bp_e+1, t.len,
	//		t.status, t.num_gene, t.cds);
	//fold_output( prot_log, t.seq );


	int orient; // 1 
	int i, j;
	int valid_length= 0; // 1 if the protein contains SV before meeting the first stop codon. 0 otherwise.
	int overlap_l  = 0;
	string pep_str="", mis_str="", rc_str = "";
	vector<int> kr_vec;
	int mis_size;
	int mis_l;
	int left, right;


	int bp_s = 0, bp_e = 0;
	int s_idx = 0, s_res = 0, e_idx = 0, e_res = 0;
	int adj_idx;
	vector<string> protein_array;
	// orient 1: 5 -> 3 of t.seq
	orient = 1;
	bp_s = t.bp_s;
	bp_e = t.bp_e;


	vector<string> vec_str;
	vec_str.push_back("");
	vec_str.push_back("");
	vec_str.push_back("");
	vec_str[0].reserve(10000);
	vec_str[1].reserve(10000);
	vec_str[2].reserve(10000);
	char tmp_char;
	int frame_idx = 0;
	for( i = 0; i < (int)t.seq.size() - 2 ; i++ )
	{
		//to_aa( t.seq.substr( i, 3) );
		//tmp_char = ToProtein( t.seq.substr(i,3));
		tmp_char = to_aa( t.seq.substr(i,3));
		vec_str[ i%3 ].push_back(tmp_char);
	}

	for(i = 0 ; i < 3; i++)
	{
		fprintf( prot_log, ">%s %d S:5 RF:%d %s %s %u %u %u %c %s %s %d %d %d %d %d %d\n",
				t.patient.c_str(), t.id, i, t.sv.c_str(), t.chr.c_str(),
				t.pos, t.s, t.e, t.strand, 
				t.gid.c_str(), t.tid.c_str(),
				t.bp_s+1, t.bp_e+1, t.len,
				t.status, t.num_gene, t.cds);
		fprintf( prot_log, "%s\n", vec_str[i].c_str() );
	}

	vec_str[0].clear();
	vec_str[1].clear();
	vec_str[2].clear();
	vec_str[0].reserve(10000);
	vec_str[1].reserve(10000);
	vec_str[2].reserve(10000);
	make_rc( t.seq, rc_str );
	frame_idx = 0;
	for( i = 0; i < (int)rc_str.size() - 2 ; i++ )
	{
		//tmp_char = ToProtein( rc_str.substr(i,3));
		tmp_char = to_aa( rc_str.substr(i,3));
		vec_str[ i%3 ].push_back(tmp_char);
	}

	for(i = 0 ; i < 3; i++)
	{
		fprintf( prot_log, ">%s %d S:3 RF:%d %s %s %u %u %u %c %s %s %d %d %d %d %d %d\n",
				t.patient.c_str(), t.id, i, t.sv.c_str(), t.chr.c_str(),
				t.pos, t.s, t.e, t.strand, 
				t.gid.c_str(), t.tid.c_str(),
				t.bp_s+1, t.bp_e+1, t.len,
				t.status, t.num_gene, t.cds);
		fprintf( prot_log, "%s\n", vec_str[i].c_str() );
	}


}
// t.bp_s and t_.bp_e is shown as 1-based in trasncript fata header.
// we adjust them to 0-based, both inclusive,  in computation
/**********************************************/
void ParseSVTranscript ( char *predict_file , char *out_file )
{
	
	char *readline = (char*)malloc( MAX_LINE );
	char *pat      = (char*)malloc( TOKEN_LENGTH );
	char *svtype   = (char*)malloc( TOKEN_LENGTH );
	char *svchr    = (char*)malloc( TOKEN_LENGTH );
	char *gid      = (char*)malloc( TOKEN_LENGTH );
	char *tid      = (char*)malloc( TOKEN_LENGTH );
	
	string prot_fa     = add_extension( out_file, "prot");
 	FILE *prot_fp      = fopen( prot_fa.c_str(), "w");	
	string pep_fa      = add_extension( out_file, "pep");
 	FILE *pep_fp       = fopen( pep_fa.c_str(), "w");	
	string pep_info    = add_extension( out_file, "peptable");
 	FILE *info_fp      = fopen( pep_info.c_str(), "w");	

	string trans_seq = "";
	
	sv_trans_t t;
	int limit;
	
	FILE *fp = fopen( predict_file, "r");
	while( NULL != fgets(readline, MAX_LINE, fp) )
	{
		if ( 0 == strncmp(">", readline, 1) )
		{
			if ( 0 < t.seq.size() )
			{
				get_protein( t, prot_fp, pep_fp, info_fp );	
				//get_protein1( t, prot_fp, pep_fp, info_fp );	
			}

			// start a new record 
			sscanf( readline, ">%s %d %s %s %u %u %u %c %s %s %d %d %d %d %d %d\n",
					pat, &t.id, svtype, svchr,
					&t.pos, &t.s, &t.e, &t.strand, 
					gid, tid,
					&t.bp_s, &t.bp_e, &t.len,
					&t.status, &t.num_gene, &t.cds);

			copyToString( pat, t.patient );
			copyToString( svtype, t.sv );
			copyToString( svchr, t.chr );
			copyToString( gid, t.gid );
			copyToString( tid, t.tid );
			//t.bp_s --;
			//t.bp_e --;


			t.pepid = 0;
			t.seq.clear();
			t.seq.reserve( t.len );
			continue;
		}

		limit=(int)strlen(readline);
		for( int i = 0; i < limit; i ++)
		{
			if ( '\n' != readline[i] )
			{ t.seq.push_back( readline[i]);	}
		}
	}
	// Last Record
	if ( 0 < t.seq.size() )
	{
		get_protein( t, prot_fp, pep_fp, info_fp );	
		//get_protein1( t, prot_fp, pep_fp, info_fp );	
	}
	fclose( fp );
	fclose( prot_fp );
	fclose( pep_fp );
	fclose( info_fp );

	free( readline );
	free( pat );
	free( svtype );
	free( svchr );
	free( gid );
	free( tid );
}


///**********************************************/
//void hello_sv( char *predict_file , char *out_file )
//{
//	string total_seq="", total_rc="";
//
//	char *readline=(char*)malloc(MAX_LINE);
//	char *patient=(char*)malloc(MAX_LINE);
//	char *chr=(char*)malloc(TOKEN_LENGTH);
//	char *g_id=(char*)malloc(TOKEN_LENGTH);
//	char *t_id=(char*)malloc(TOKEN_LENGTH);
//	char *fea=(char*)malloc(TOKEN_LENGTH);
//	char *iso_seq = (char*)malloc(MAX_LINE);
//
//	char *pep_out=(char*)malloc(MAX_LINE);
//	char *info_out=(char*)malloc(MAX_LINE);
//	strcpy(pep_out, out_file);
//	strcat(pep_out, ".pep");
//	strcpy(info_out, out_file);
//	strcat(info_out, ".peptable");
//
//	FILE *out_fp = fopen(pep_out, "w");
//	FILE *info_fp = fopen(info_out, "w");
//
//
//	char strand;
//	int iso_length =0, before_sv = 0, total_sv = 0,
//		bp_1= 0, bp_2 =0, // 0-based bp, both inclusive
//		r_bp1 = 0, r_bp2 = 0; // for reversed strand.
//	uint32_t pos =0, start = 0, end = 0;
//
//	int count = 0;
//	int id = 0, support = 0, offset =0;
//	//unit_vcf tmp_vcf;
//	FILE *fp = fopen( predict_file, "r");
//	int ret;
//	int i = 0, orient = 0;
//	int bp_idx = 0, bp2_idx = 0, bp_res = 0,valid_length =0;
//	int r2_idx = 0;// for 3 -> 5 strand
//	size_t found;
//	vector<string> protein_array;
//	int tmp=0, t_flag =0, t_iso=0, t_mis = 0;
//	int t_cross = 0; // the actual number of peptide crossing SVs
//	string pep_str="", mis_str="";
//	int left = 0, right = 0, mis_l = 0;//for miscleavage
//	vector<int> kr_vec;
//	int mis_size = 0;
//	int overlap_l = 0;
//	while( NULL != fgets(readline, MAX_LINE, fp) )
//	{
//		if ( 0 == strncmp("#", readline, 1) )
//		{
//			continue;
//		}
//		t_flag=0;
//		ret = sscanf( readline, "%s %d %s %s %u %u %u %s %s %c %d %d %d %s\n",
//		//ret = sscanf( readline, "%s %u %c %s %s %f %s %s\n",
//			patient, &id, fea, chr, &pos, &start, &end, g_id, t_id, &strand,
//			&iso_length, &before_sv, &total_sv,
//			iso_seq);
//
//		bp_1 = before_sv;
//		bp_2 = before_sv + total_sv - 1; // SV is from bp_1 to bp_2. 0-based, both inclusive
//
//		//E("OK %s\n", misc);
//		copyToString( iso_seq, total_seq);
//		r_bp1 = (int)total_seq.size() - before_sv - total_sv;
//		r_bp2 = r_bp1 + total_sv - 1;
//		L("==%s\t%d\t%d\t%c\t%lu\t%d\t%d\t%d\t%d\t%d\t%d\n", patient, id, orient, strand, total_seq.size(), before_sv, total_sv, bp_1, bp_2, r_bp1, r_bp2);
//		// Orient 1: 5->3
//		orient = 1;
//		DNATranslate(total_seq, protein_array,  orient );
//		for(i =0; i < (int)protein_array.size(); i++)
//		{
//			bp_idx = (bp_1-i)/3;
//			bp_res = (bp_1-i)%3;
//			bp2_idx = (bp_2-i)/3;
//			found = protein_array[i].find('-');
//			valid_length = (int)protein_array[i].size();
//			if ( found!=string::npos)
//			{
//				valid_length=(int)found;
//			}
//
//			L("<<%s\t%d\t%d\t%d\t%c\t%d\t%d\t%d\t%d\t%d\n", patient, id, orient, i, strand, valid_length, bp_idx, bp2_idx, bp_1, bp_2);
//			if ( (bp_idx <= valid_length) && ( 5 <= valid_length))
//			{
//				pep_str = protein_array[i].substr(0, valid_length).c_str() ;
//				// get substring with at most 2 KR
//				mis_size = get_kr_pos( pep_str, kr_vec);
//				L(">>%s\t%d\t%d\t%d\t%c\t%lu\t%lu\t%d\t%d", patient, id, orient, i, strand, pep_str.size(), kr_vec.size(), bp_idx, bp2_idx);
//				for( int w = 0; w < (int)kr_vec.size();w++)
//				{	L("\t%d", kr_vec[w]);}
//				L("\n");
//				if ( 2 >= mis_size)
//				{
//					overlap_l = ol_length( bp_idx, bp2_idx, 0, valid_length - 1);
//					// Output the fastq to peptide, and log to profile
//					fprintf(out_fp, ">%d_%s_%d\n", tmp, patient, t_mis);
//					fprintf(out_fp, "%s\n", pep_str.c_str());
//					fprintf(info_fp, "%d\t%d\t%s\t%d\t%d\t%d\t%c\t",tmp, t_mis, patient, id, orient, i, strand);
//					//fprintf(info_fp, "%lu\t%d\t%d\t%d\t0\t%d%s\t%s\t", protein_array[i].size(), bp_idx, bp2_idx, valid_length, valid_length -1, fea, chr);
//					fprintf(info_fp, "%lu\t%d\t%d\t0\t%d\t%d\t%d\t%s\t%s\t", protein_array[i].size(), valid_length, overlap_l, valid_length -1, bp_idx, bp2_idx, fea, chr);
//					fprintf(info_fp, "%u\t%u\t%u\t%s\t%s\t%d\t%d\t%d\t%s\t%s\n", pos, start, end, g_id, t_id, iso_length,
//							before_sv, total_sv, pep_str.c_str(), pep_str.c_str() );
//					t_mis++;
//					if ( overlap_l ){ t_cross++;}
//				}
//				else
//				{
//					for( int t = 1; t < mis_size ; t++ )
//					{
//						mis_l = get_mis_str( pep_str, mis_str, kr_vec, t , left, right);
//						overlap_l = ol_length( bp_idx, bp2_idx, left, right );
//						if (  5 <= mis_l ) // && ( ol_length( bp_idx, bp2_idx, left, right )))
//						{
//							fprintf(out_fp, ">%d_%s_%d\n", tmp, patient, t_mis );
//							fprintf(out_fp, "%s\n", mis_str.c_str());
//							fprintf(info_fp, "%d\t%d\t%s\t%d\t%d\t%d\t%c\t",tmp, t_mis, patient, id, orient, i, strand);
//							//fprintf(info_fp, "%lu\t%d\t%d\t%d\t%d\t%d\t%s\t%s\t", protein_array[i].size(), bp_idx, bp2_idx, valid_length, left, right, fea, chr);
//							fprintf(info_fp, "%lu\t%d\t%d\t%d\t%d\t%d\t%d\t%s\t%s\t", protein_array[i].size(), valid_length, overlap_l, left, right, bp_idx, bp2_idx, fea, chr);
//							fprintf(info_fp, "%u\t%u\t%u\t%s\t%s\t%d\t%d\t%d\t%s\t%s\n", pos, start, end, g_id, t_id, iso_length,
//									before_sv, total_sv, mis_str.c_str(), pep_str.c_str() );
//							t_mis++;
//						}
//						if ( overlap_l ){ t_cross++;}
//					}
//				}
//
//
//				tmp++;
//				t_flag=1;
//			}
//			L("1 %d %lu %lu %d %d %s \n", i, protein_array.size(), protein_array[i].size(), bp_idx, valid_length,
//			protein_array[i].substr(0, valid_length).c_str() );
//		}
//		// Orient 0: 3->5
//		make_rc( total_seq, total_rc);
//		orient = 0;
//		DNATranslate(total_rc, protein_array,  orient);
//		for(i =0; i < (int)protein_array.size(); i++)
//		{
//			bp_idx = (r_bp1 - i )/3;
//			bp_res = (r_bp1 - i )%3;
//			bp2_idx = (r_bp2-i)/3;
//			//bp_idx = (bp_1-i)/3;
//			//bp_res = (bp_1-i)%3;
//			found = protein_array[i].find('-');
//			valid_length = (int)protein_array[i].size();
//			if ( found!=string::npos)
//			{
//				valid_length=(int)found;
//			}
//			L("<<%s\t%d\t%d\t%d\t%c\t%d\t%d\t%d\t%d\t%d\n", patient, id, orient, i, strand, valid_length, bp_idx, bp2_idx, r_bp1, r_bp2);
//			if ( (bp_idx <= valid_length) && ( 5 <= valid_length))
//			{
//				pep_str = protein_array[i].substr(0, valid_length).c_str() ;
//				// get substring with at most 2 KR
//				mis_size = get_kr_pos( pep_str, kr_vec);
//				L(">>%s\t%d\t%d\t%d\t%c\t%lu\t%lu\t%d\t%d", patient, id, orient, i, strand, pep_str.size(), kr_vec.size(), bp_idx, bp2_idx);
//				for( int w = 0; w < (int)kr_vec.size();w++)
//				{	L("\t%d", kr_vec[w]);}
//				L("\n");
//				if ( 2 >= mis_size)
//				{
//					overlap_l = ol_length( bp_idx, bp2_idx, 0, valid_length - 1);
//					// Output the fastq to peptide, and log to profile
//					fprintf(out_fp, ">%d_%s_%d\n", tmp, patient, t_mis);
//					fprintf(out_fp, "%s\n", pep_str.c_str());
//					fprintf(info_fp, "%d\t%d\t%s\t%d\t%d\t%d\t%c\t",tmp, t_mis, patient, id, orient, i, strand);
//					//fprintf(info_fp, "%lu\t%d\t%d\t%d\t0\t%d%s\t%s\t", protein_array[i].size(), bp_idx, bp2_idx, valid_length, valid_length -1, fea, chr);
//					fprintf(info_fp, "%lu\t%d\t%d\t0\t%d\t%d\t%d\t%s\t%s\t", protein_array[i].size(), valid_length, overlap_l, valid_length -1, bp_idx, bp2_idx, fea, chr);
//					fprintf(info_fp, "%u\t%u\t%u\t%s\t%s\t%d\t%d\t%d\t%s\t%s\n", pos, start, end, g_id, t_id, iso_length,
//							before_sv, total_sv, pep_str.c_str(), pep_str.c_str() );
//					t_mis++;
//					if ( overlap_l ){ t_cross++;}
//				}
//				else
//				{
//					for( int t = 1; t < mis_size ; t++ )
//					{
//						mis_l = get_mis_str( pep_str, mis_str, kr_vec, t , left, right);
//						overlap_l = ol_length( bp_idx, bp2_idx, left, right );
//						if (  5 <= mis_l ) // && ( ol_length( bp_idx, bp2_idx, left, right )))
//						{
//							fprintf(out_fp, ">%d_%s_%d\n", tmp, patient, t_mis );
//							fprintf(out_fp, "%s\n", mis_str.c_str());
//							fprintf(info_fp, "%d\t%d\t%s\t%d\t%d\t%d\t%c\t",tmp, t_mis, patient, id, orient, i, strand);
//							//fprintf(info_fp, "%lu\t%d\t%d\t%d\t%d\t%d\t%s\t%s\t", protein_array[i].size(), bp_idx, bp2_idx, valid_length, left, right, fea, chr);
//							fprintf(info_fp, "%lu\t%d\t%d\t%d\t%d\t%d\t%d\t%s\t%s\t", protein_array[i].size(), valid_length, overlap_l, left, right, bp_idx, bp2_idx, fea, chr);
//							fprintf(info_fp, "%u\t%u\t%u\t%s\t%s\t%d\t%d\t%d\t%s\t%s\n", pos, start, end, g_id, t_id, iso_length,
//									before_sv, total_sv, mis_str.c_str(), pep_str.c_str() );
//							t_mis++;
//						}
//						if ( overlap_l ){ t_cross++;}
//					}
//				}
//
//
//				tmp++;
//				t_flag =1;
//			}
//			L("0 %d %lu %lu %d %d %s \n", i, protein_array.size(), protein_array[i].size(), bp_idx, valid_length, protein_array[i].substr(0, valid_length).c_str() );
//			//L("0 %d %lu %lu\n", i, protein_array.size(),  protein_array[i].size() );
//		}
//
//		t_iso+= t_flag;
//		count++;
//		if ( 0 == count%1000){E(".");}
//	}
//	E("\nTotal Isoform\t%d\t%d\t%d\t%d\t%d\n", count, t_iso, tmp, t_mis, t_cross); // number_isoform, number_isoform_wth_valid_protein, number_protein, number_peptide, number_of_crossing_peptide
//	//E("Total References with Predictions:\t%d\n", (int)map_record.size());
//	fclose(fp);
//
//	free(iso_seq);
//	free(fea);
//	free(t_id);
//	free(g_id);
//	free(patient);
//	free(chr);
//	free(readline);
//
//}
